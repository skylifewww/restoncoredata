//
//  CDRestaurant+CoreDataProperties.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 25.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//
//

#import "CDRestaurant+CoreDataProperties.h"

@implementation CDRestaurant (CoreDataProperties)

+ (NSFetchRequest<CDRestaurant *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDRestaurant"];
}

@dynamic addition_date;
@dynamic address;
@dynamic avgBill;
@dynamic catalogPriority;
@dynamic city;
@dynamic descriptionRestaurant;
@dynamic discountData;
@dynamic discounts;
@dynamic discountsType;
@dynamic distanceRest;
@dynamic distric;
@dynamic images;
@dynamic intCatalogPriority;
@dynamic intPosition;
@dynamic isFavorited;
@dynamic isLiked;
@dynamic kitchen;
@dynamic lat;
@dynamic likeCount;
@dynamic logo;
@dynamic lon;
@dynamic menuFile;
@dynamic menuFileType;
@dynamic menuFileUrl;
@dynamic options;
@dynamic personsLimits;
@dynamic position;
@dynamic rating;
@dynamic ratingCount;
@dynamic restaurantId;
@dynamic reviewCount;
@dynamic shortDescript;
@dynamic subtitle;
@dynamic subway;
@dynamic subwayName;
@dynamic telephone;
@dynamic title;
@dynamic type;
@dynamic views;
@dynamic workTimes;

- (void)initWithDictionary:(NSDictionary *)obj{
    self.restaurantId = obj[@"ID"];
    self.logo = obj[@"LOGO"];
    self.city = obj[@"CITY"];
    self.kitchen = obj[@"KITCHEN"];
    self.descriptionRestaurant = obj[@"DESCRIPTION"];
    self.descriptionRestaurant = [NSString stringWithFormat:@"   %@",self.descriptionRestaurant];
    self.descriptionRestaurant = [self.descriptionRestaurant stringByReplacingOccurrencesOfString:@"\n" withString:@"\n   "];
    self.discountData = obj[@"DISCOUNTS_DATA"];
    self.discountsType = obj[@"DISCOUNTS_TYPE"];
    self.discounts = (int16_t)[obj[@"DISCOUNTS"] integerValue];
    //    NSLog(@"DISCOUNTS: %@", obj[@"DISCOUNTS"]);
    //    NSLog(@"self.discounts: %hd", self.discounts);
    self.title = obj[@"NAME"];
    self.personsLimits = obj[@"MAX_PEOPLE_COUNT"];
    self.shortDescript = obj[@"SHORT_DESCRIPTION"];
    self.subway = obj[@"SUBWAY"];

    if (![obj[@"SUBWAYNAME"] isEqual:[NSNull null]]){
        self.subwayName = obj[@"SUBWAYNAME"];
    }
    self.telephone = obj[@"TELEPHONE"];
    self.views = obj[@"VIEWS"];
    self.address = obj[@"ADDRESS"];
    self.avgBill = (int32_t)[obj[@"AVG_BILL"] integerValue];
    self.options = obj[@"OPTIONS"];
    self.isLiked = (int16_t)obj[@"ISLIKED"];
    self.likeCount = (int64_t)obj[@"LIKECOUNT"];
    self.isFavorited = (boolean_t)obj[@"MYFAVORITE"];
    self.reviewCount = (int64_t)[obj[@"REVIEWCOUNT"] integerValue];
    self.type = obj[@"TYPE"];
    self.rating = [obj[@"RATING"] doubleValue];
    self.distric = obj[@"RAION"];
    self.addition_date = obj[@"ADDITION_DATE"];

    if ([obj[@"POSITION"] isEqual:[NSNull null]]){
        self.position = @"10000";
    } else {
        self.position = obj[@"POSITION"];
    }

    if ([obj[@"CATALOG_PRIORITY"] isEqual:[NSNull null]]){
        self.catalogPriority = @"10000";
        self.intCatalogPriority = (int32_t)[self.catalogPriority integerValue];
    } else {
        self.catalogPriority = obj[@"CATALOG_PRIORITY"];
        self.intCatalogPriority = (int32_t)[self.catalogPriority integerValue];
    }

    self.lat = [obj[@"LAT"] doubleValue];
    self.lon = [obj[@"LNG"] doubleValue];

    //    self.coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
    self.subtitle = [self.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                            withString:@""];;
    NSArray *imgDics = obj[@"IMAGES"];
    NSMutableArray *mutableImages = [[NSMutableArray alloc] initWithCapacity:imgDics.count];
    for (NSDictionary *dic in imgDics) {
        [mutableImages addObject:dic[@"IMAGE"]];
    }
    self.images = mutableImages;

    self.workTimes = obj[@"WORKING_TIME"];
    self.menuFile = obj[@"MENU_FILE"];
    self.menuFileType = obj[@"MENU_FILE_TYPE"];
    self.menuFileUrl = obj[@"MENU_FILE_URL"];
}

@end
