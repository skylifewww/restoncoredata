//
//  OrderCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/11/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

@interface OrderCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *restonName;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberPersons;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *bookBottom;
@property (weak, nonatomic) IBOutlet UIImageView *orderIcon;
@property (weak, nonatomic) IBOutlet UIImageView *orderPersons;
@property (weak, nonatomic) IBOutlet UIImageView *orderClock;

@property (nonatomic, strong) RNOrderInfo *reserv;


-(void)updateCell;

@end
