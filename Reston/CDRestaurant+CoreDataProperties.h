//
//  CDRestaurant+CoreDataProperties.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 25.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//
//

#import "CDRestaurant+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDRestaurant (CoreDataProperties)

+ (NSFetchRequest<CDRestaurant *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *addition_date;
@property (nullable, nonatomic, copy) NSString *address;
@property (nonatomic) int32_t avgBill;
@property (nullable, nonatomic, copy) NSString *catalogPriority;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *descriptionRestaurant;
@property (nullable, nonatomic, copy) NSString *discountData;
@property (nonatomic) int16_t discounts;
@property (nullable, nonatomic, copy) NSString *discountsType;
@property (nullable, nonatomic, copy) NSString *distanceRest;
@property (nullable, nonatomic, copy) NSString *distric;
@property (nullable, nonatomic, retain) NSObject *images;
@property (nonatomic) int32_t intCatalogPriority;
@property (nonatomic) int32_t intPosition;
@property (nonatomic) BOOL isFavorited;
@property (nonatomic) int16_t isLiked;
@property (nullable, nonatomic, retain) NSObject *kitchen;
@property (nonatomic) double lat;
@property (nonatomic) int64_t likeCount;
@property (nullable, nonatomic, copy) NSString *logo;
@property (nonatomic) double lon;
@property (nullable, nonatomic, copy) NSString *menuFile;
@property (nullable, nonatomic, copy) NSString *menuFileType;
@property (nullable, nonatomic, copy) NSString *menuFileUrl;
@property (nullable, nonatomic, retain) NSObject *options;
@property (nullable, nonatomic, copy) NSString *personsLimits;
@property (nullable, nonatomic, copy) NSString *position;
@property (nonatomic) double rating;
@property (nonatomic) int64_t ratingCount;
@property (nullable, nonatomic, copy) NSString *restaurantId;
@property (nonatomic) int64_t reviewCount;
@property (nullable, nonatomic, copy) NSString *shortDescript;
@property (nullable, nonatomic, copy) NSString *subtitle;
@property (nullable, nonatomic, copy) NSString *subway;
@property (nullable, nonatomic, copy) NSString *subwayName;
@property (nullable, nonatomic, copy) NSString *telephone;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *views;
@property (nullable, nonatomic, retain) NSObject *workTimes;

- (void)initWithDictionary:(NSDictionary *)obj;

@end

NS_ASSUME_NONNULL_END
