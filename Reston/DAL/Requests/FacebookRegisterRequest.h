//
//  FacebookRegisterRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface FacebookRegisterRequest : RNRequest

@property (nonatomic, copy) NSString *nameFB;
@property (nonatomic, copy) NSString *emailFB;
@property (copy, nonatomic) NSString *clientId;
@property (copy, nonatomic) NSString *photoFB;
@end
