//
//  UploadIAvatarRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 07.11.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "UploadIAvatarRequest.h"
#import "UploadIAvatarResponse.h"
#import "RNUser.h"

@implementation UploadIAvatarRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    UploadIAvatarResponse *responseWithSuccess = [[UploadIAvatarResponse alloc]init];
    
    if (jsonDic[@"token"]){
    responseWithSuccess.token = jsonDic[@"token"];
        [RNUser sharedInstance].token = responseWithSuccess.token;
    responseWithSuccess.pathAvatar = jsonDic[@"file_path"];
    } else if (jsonDic[@"error"]){
        responseWithSuccess.errorMessage = jsonDic[@"error"];
    }
    
    return responseWithSuccess;
}



- (NSString *)methodSignature {
    NSString *text = [NSString stringWithFormat:@"?action=uploadUserPhoto&token=%@&avatar=data:image/png;base64,%@", [RNUser sharedInstance].token, self.photoAvatar];
    
    NSLog(@"action=uploadUserPhoto %@",text);
    
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

//-(NSString *)methodType{
//
//    return @"POST";
//}
//- (NSData *)serialize {
//    return [NSData data];
//}

@end
