//
//  RNGetRestaurantRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetRestaurantRequest.h"
#import "RNGetRestaurantResponse.h"
#import "RNUser.h"

@implementation RNGetRestaurantRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSDictionary *obj =jsonDic[@"restaurant"];
    
    RNRestaurant *restaurant=[[RNRestaurant alloc]initWithDictionary:obj];
    RNGetRestaurantResponse *responseRestaurant = [[RNGetRestaurantResponse alloc]init];
    responseRestaurant.restaurant = restaurant;
    return responseRestaurant;
}

- (NSString *)methodSignature {
    
    NSLog(@"token %@",[RNUser sharedInstance].token);
     NSLog(@"restaurantId %ld",(long)_restaurantId.integerValue);
    
    return   [NSString stringWithFormat:@"?action=getrestaurant&restaurantId=%ldf&token=%@&lang_code=%@",(long)_restaurantId.integerValue,[RNUser sharedInstance].token, _langCode];
}

-(NSString *)methodType{
    
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end
