//
//  RNEditProfileRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 21.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRegisterRequest.h"

@interface RNEditProfileRequest : RNRegisterRequest
@property (nonatomic, copy) NSString *birthDate;
@property (nonatomic, copy) NSString *photo;


@end
