//
//  GetOffersRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "GetOffersRequest.h"
#import "GetOffersResponse.h"
#import "Offer.h"

@implementation GetOffersRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    GetOffersResponse *response = [[GetOffersResponse alloc]init];
    if (![jsonDic[@"offers"] isKindOfClass:[NSNull class]] && jsonDic[@"offers"] != NULL) {
        
        NSArray *array = jsonDic[@"offers"];
        NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
        for (NSDictionary *dic in array) {
            [result addObject:[[Offer alloc] initWithDictionary:dic]];
        }
        
        response.offers = result;
        
    } else{
        NSLog(@"NO data offers");
        return nil;
    }
    
    return response;
    
}

- (NSString *)methodSignature {
    return [NSString stringWithFormat:@"?action=getoffers&restaurantId=%@",_restaurantId];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end

