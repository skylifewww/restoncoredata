//
//  GetRestTypeRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "GetRestTypeRequest.h"
#import "GetRestTypeResponse.h"
#import "RestType.h"
#import "RNUser.h"

@implementation GetRestTypeRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *typesArray = jsonDic[@"types"];
    [typesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RestType *restType = [[RestType alloc]init];
        restType.restTypeId = obj[@"ID"];
        restType.restTypeName = obj[@"NAME"];
        [tmpArray addObject:restType];
    }];
    GetRestTypeResponse *response = [[GetRestTypeResponse alloc]init];
    response.restTypes = tmpArray;
    
    return response;
    
}

- (NSString *)methodSignature {
    
    NSString* getRestTypes = [NSString stringWithFormat:@"?action=getresttypes&token=%@&lang_code=%@",[RNUser sharedInstance].token, _langCode];
    NSLog(@"NSString* getRestTypes %@",getRestTypes);
    
    return [getRestTypes stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end

