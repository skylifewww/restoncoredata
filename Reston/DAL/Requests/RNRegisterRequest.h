//
//  RNRegisterRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface RNRegisterRequest : RNRequest

@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;


@end
