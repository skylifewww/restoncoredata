//
//  RNSearchByWordRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/23/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNSearchByWordRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "RNRestaurant.h"
#import "RNUser.h"

@implementation RNSearchByWordRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    NSArray *restrauntsArray = jsonDic[@"restaurants"];
    __block NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    [restrauntsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RNRestaurant *restaurant=[[RNRestaurant alloc]initWithDictionary:obj];
        [tmpArray addObject:restaurant];
    }];
    RNGetRestaurantsResponse *responseRestaurants = [[RNGetRestaurantsResponse alloc]init];
    responseRestaurants.restrauntsArray = tmpArray;
    
    return responseRestaurants;
}

- (NSString *)methodSignature {
    NSString *token = [RNUser sharedInstance].token;
    NSString *result = [NSString stringWithFormat:@"?action=searchrestaurants&searchword=%@&token=%@",_searchWord,token];

    return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end

