//
//  RNGetRestaurantRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"
@interface RNGetRestaurantRequest : RNRequest

@property (nonatomic, copy) NSNumber *restaurantId;
@property (nonatomic, copy) NSString *langCode;
@end
