//
//  GetOffersRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface GetOffersRequest : RNRequest

@property (nonatomic, copy) NSNumber *restaurantId;

@end
