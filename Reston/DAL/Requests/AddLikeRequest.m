//
//  AddLikeRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 24.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "AddLikeRequest.h"
#import "AddLikeResponse.h"
#import "RNUser.h"

@implementation AddLikeRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    AddLikeResponse *addLikeResponse = [[AddLikeResponse alloc]init];
    addLikeResponse.success = jsonDic[@"addlike"];
    return addLikeResponse;
}

- (NSString *)methodSignature {
    NSString *token = [RNUser sharedInstance].token;
    NSString *text = [NSString stringWithFormat:@"?action=addlike&restaurantId=%@&token=%@",_restaurantId,token];
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}




@end
