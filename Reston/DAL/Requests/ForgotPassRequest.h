//
//  ForgotPassRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 12.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface ForgotPassRequest : RNRequest

@property (nonatomic, copy) NSString *email;
@end
