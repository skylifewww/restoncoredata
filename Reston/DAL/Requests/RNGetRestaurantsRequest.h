//
//  RNGetRestaurantsRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface RNGetRestaurantsRequest : RNRequest

@property (nonatomic, assign) Boolean isAuth;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *langCode;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSNumber *subway;
@property (nonatomic, copy) NSNumber *numberOfPeople;

@end
