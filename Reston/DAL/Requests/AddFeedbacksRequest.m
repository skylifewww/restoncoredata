//
//  AddFeedbacksRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/4/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "AddFeedbacksRequest.h"
#import "AddFeedbacksResponse.h"
#import "RNUser.h"
#import "RNFeedback.h"

@implementation AddFeedbacksRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];

    if (jsonDic[@"error"]) {
        return nil;
    }
    AddFeedbacksResponse *response = [[AddFeedbacksResponse alloc]init];
    response.responseString = jsonDic[@"addfeedback"];
    return response;
}

- (NSString *)methodSignature {
    
    NSLog(@"_restaurantId %@ _like %@ _text %@ token %@",_restaurantId, _like, _text,[RNUser sharedInstance].token);
     NSString *text1 = [NSString stringWithFormat:@"?action=addfeedback&restaurantId=%@&like=%@&text=%@&token=%@",_restaurantId, _like, _text,[RNUser sharedInstance].token];
    
    NSLog(@"%@", text1);
    
     return [text1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"POST";
}
- (NSData *)serialize {
    return [NSData data];
}

@end
