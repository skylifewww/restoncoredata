//
//  RNAuthRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNAuthRequest.h"
#import "RNUser.h"
#import "RNAuthorizationResponse.h"

@implementation RNAuthRequest
- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    RNAuthorizationResponse *responseWithToken = [[RNAuthorizationResponse alloc]init];
    responseWithToken.token = jsonDic[@"token"];
    [RNUser sharedInstance].token = responseWithToken.token;
    /*
     FNAME
     FPHONE
     LNAME
     EMAIL
     CITY
     */
    NSDictionary *extendDic = jsonDic[@"user_data"];
    if ([extendDic[@"FNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = extendDic[@"FNAME"];
        NSLog(@"[RNUser sharedInstance].name %@", [RNUser sharedInstance].name);
    }
    if ([extendDic[@"LNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = [NSString stringWithFormat:@"%@ %@",[RNUser sharedInstance].name, extendDic[@"LNAME"]];
        
    }
    
    if ([extendDic[@"FPHONE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].phone = extendDic[@"FPHONE"];
    }
    if ([extendDic[@"EMAIL"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].email = extendDic[@"EMAIL"];
        NSLog(@"[RNUser sharedInstance].email %@", [RNUser sharedInstance].email);
    }
//    if ([extendDic[@"ADDRESS"] isKindOfClass:[NSString class]]) {
//        [RNUser sharedInstance].photo = extendDic[@"ADDRESS"];
//        NSLog(@"[RNUser sharedInstance].photo %@", [RNUser sharedInstance].photo);
//    }
    if ([extendDic[@"PHOTO"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].photo = extendDic[@"PHOTO"];
    }
    if ([extendDic[@"BIRTH_DATE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].birthDate = extendDic[@"BIRTH_DATE"];
    }
//    if ([extendDic[@"ID"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].idUser = extendDic[@"ID"];
//    }
    
    return responseWithToken;
}

- (NSString *)methodSignature {
    NSString *text = [NSString stringWithFormat:@"?action=auth&username=%@&password=%@",_username,_password];
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}

@end
