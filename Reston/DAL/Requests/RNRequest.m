//
//  RNRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

NSString *const kServerURL = @"http://api.reston.com.ua/api.php";

@interface RNRequest ()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation RNRequest

- (void)sendWithcompletion:(void (^)(RNResponse *response, NSError *error))completion{
    if (_session == nil) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:configuration
                                                     delegate:nil
                                                delegateQueue:nil];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kServerURL,[self methodSignature]];
    NSURL* url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    //[request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
//    NSData *requestData = [self serialize];
//    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)requestData.length] forHTTPHeaderField:@"Content-Length"];
    
   // request.HTTPBody = requestData;
    request.HTTPMethod = [self methodType];
    
    NSURLSessionDataTask *postDataTask = [_session dataTaskWithRequest:request
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             
                                                             NSJSONReadingOptions options =  NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves |NSJSONReadingAllowFragments;
                                                             
                                                             if (data == nil || [data isKindOfClass:[NSNull class]]) {
                                                                 
                                                             [[[UIAlertView alloc] initWithTitle:DPLocalizedString(@"error_title", nil)
                                                                                         message:DPLocalizedString(@"error_text", nil)
                                                                                        delegate:nil
                                                                               cancelButtonTitle:@"Ok"
                                                                               otherButtonTitles:nil] show];
                                                                 
                                                                 
                                                             }
                                                             
                                                             
#ifdef DEBUG
                                                             //NSLog(@"%@",jsonArray);
#endif
                                                             if (error != nil || data == nil || [data isKindOfClass:[NSNull class]]) {
                                                                 NSLog(@"Error %@", [error localizedDescription]);
                                                                 completion(nil, error);
                                                                 return;
                                                             }
                                                             
                                                             id jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                                                            options:options
                                                                                                              error:nil];
                                                             
                                                             NSInteger status = ((NSHTTPURLResponse *)response).statusCode;
                                                             if (status != 200) {
                                                                 NSString *str = [NSString stringWithFormat:@"Status %ti",status];
                                                                 if (jsonArray != nil && ![jsonArray isKindOfClass:[NSNull class]]) {
                                                                     str = [NSString stringWithFormat:@"Status %ti",status];
                                                                 }
                                                                 completion (nil, [NSError errorWithDomain:@"Response Domain"
                                                                                                      code:status userInfo:@{NSLocalizedDescriptionKey : str}]);
                                                                 return;
                                                             }
                                                             if (error != nil ) {
                                                                 NSLog(@"Error %@", [error localizedDescription]);
                                                                 completion(nil, error);
                                                                 return;
                                                             }
                                                             completion([self bindResponseData:data],nil);
                                                         });
                                                     }];
    
    [postDataTask resume];
    
}

- (NSString *)methodSignature {
    NSAssert(NO, @"Should be overriden %@",NSStringFromClass([self class]));
    return @"";
}
- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSAssert(NO, @"Must be overriden");
    return nil;
}

- (NSData *)serialize {
    NSAssert(NO, @"Must be overriden");
    return nil;
}
- (NSString *) methodType{
    return @"GET";
}

@end
