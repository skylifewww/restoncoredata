//
//  RNAddToFavRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface RNAddToFavRequest : RNRequest

@property (nonatomic, copy) NSNumber *restaurantId;

@end
