//
//  RNGetSubwaysRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 10.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetSubwaysRequest.h"
#import "RNGetSubwaysResponse.h"
#import "RNSubway.h"
#import "RNUser.h"

@implementation RNGetSubwaysRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *subwayArray = jsonDic[@"subways"];
    [subwayArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RNSubway *subway = [[RNSubway alloc]init];
        subway.subwayId = obj[@"ID"];
        subway.subwayName = obj[@"NAME"];
        subway.cityId = obj[@"CITY"];
        [tmpArray addObject:subway];
    }];
    RNGetSubwaysResponse *respnse = [[RNGetSubwaysResponse alloc]init];
    respnse.subwayArray = tmpArray;
    [RNUser sharedInstance].subways = tmpArray;
    return respnse;
    
}

- (NSString *)methodSignature {
    
    if ([[_city uppercaseString] isEqualToString:@"КИЕВ"]) {
        self.city = @"1";
    } else if ([[_city uppercaseString] isEqualToString:@"ХАРЬКОВ"]) {
            self.city = @"9";
    } else {
        self.city = @"4";
    }
    
    NSString* getsubways = [NSString stringWithFormat:@"?action=getsubways&city=%@&token=%@&lang_code=%@", self.city, [RNUser sharedInstance].token, _langCode];
    NSLog(@"NSString* getsubways %@",getsubways);
    return [getsubways stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end
