//
//  RNSearchByWordRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 6/23/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface RNSearchByWordRequest : RNRequest

@property (nonatomic, copy) NSString *searchWord;

@end
