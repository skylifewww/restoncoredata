//
//  UploadIAvatarRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 07.11.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "RNRequest.h"

@interface UploadIAvatarRequest : RNRequest

@property (strong, nonatomic) NSString* photoAvatar;

@end
