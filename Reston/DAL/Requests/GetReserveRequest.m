//
//  GetReserveRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/11/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "GetReserveRequest.h"
#import "GetReserveResponse.h"
#import "RNUser.h"
#import "RNOrderInfo.h"

@implementation GetReserveRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *reservsArray = jsonDic[@"myreservs"];
    [reservsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RNOrderInfo *reserv = [[RNOrderInfo alloc]init];
        reserv.reservID = obj[@"ID"];
        reserv.dateStr = obj[@"ORDER_DATE"];
        reserv.date = obj[@"ORDER_DATE"];
        reserv.timeStr = obj[@"ORDER_TIME"];
        reserv.peopleCount = obj[@"ORDER_PERSONS"];
        reserv.status = obj[@"STATUS"];
        NSDictionary* rest = obj[@"RESTAURANT_DATA"];
        reserv.restName = rest[@"NAME"];
        reserv.restAddress = rest[@"ADDRESS"];
        [tmpArray addObject:reserv];
    }];
    GetReserveResponse *responseReserves = [[GetReserveResponse alloc]init];
    responseReserves.reserves = tmpArray;
    return responseReserves;
}

- (NSString *)methodSignature {
    NSString *result = [NSString stringWithFormat:@"?action=myreservs&token=%@&lang_code=%@",[RNUser sharedInstance].token, _langCode];
    
    return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

-(NSString *)methodType{
    
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end

