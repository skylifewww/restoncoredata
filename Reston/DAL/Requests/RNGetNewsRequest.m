//
//  RNGetNewsRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/17/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetNewsRequest.h"
#import "RNGetNewsResponse.h"
#import "RNUser.h"

@implementation RNGetNewsRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSArray *array = jsonDic[@"news"];
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
    for (NSDictionary *dic in array) {
        [result addObject:[[RNNEw alloc] initWithDictionary:dic]];
    }
    RNGetNewsResponse *response = [[RNGetNewsResponse alloc]init];
    response.news = result;
    return response;
    
}

- (NSString *)methodSignature {
    return [NSString stringWithFormat:@"?action=getnews&restaurantId=%@&token=%@",_restaurantId,[RNUser sharedInstance].token];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}




@end
