//
//  FacebookRegisterRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "FacebookRegisterRequest.h"
#import "FacebookRegisterResponse.h"
#import "RNUser.h"

@implementation FacebookRegisterRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    FacebookRegisterResponse *responseWithToken = [[FacebookRegisterResponse alloc]init];
    responseWithToken.token = jsonDic[@"token"];
    [RNUser sharedInstance].token = responseWithToken.token;
    /*
     FNAME
     FPHONE
     LNAME
     EMAIL
     CITY
     */
    responseWithToken.autorisation = jsonDic[@"autorisation"];
    NSDictionary *extendDic = jsonDic[@"user_data"];
    if ([extendDic[@"FNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = extendDic[@"FNAME"];
    }
    if ([extendDic[@"LNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = [NSString stringWithFormat:@"%@ %@",[RNUser sharedInstance].name, extendDic[@"LNAME"]];
    }
    
    if ([extendDic[@"FPHONE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].phone = extendDic[@"FPHONE"];
    }
    if ([extendDic[@"EMAIL"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].email = extendDic[@"EMAIL"];
    }
//    if ([extendDic[@"ADDRESS"] isKindOfClass:[NSString class]]) {
//        [RNUser sharedInstance].photo = extendDic[@"ADDRESS"];
//    }
    if ([extendDic[@"PHOTO"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].photo = extendDic[@"PHOTO"];
    }
    if ([extendDic[@"BIRTH_DATE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].birthDate = extendDic[@"BIRTH_DATE"];
    }
    [RNUser sharedInstance].idUser = extendDic[@"ID"];
    
    return responseWithToken;
}

- (NSString *)methodSignature {
    
    //    NSString *result = [NSString stringWithFormat:@"?action=registerFb&fb_client_id=367004023702530"];
    //    NSString *result = [NSString stringWithFormat:@"?action=authFB&client_id=367004023702530"];
    
    NSString *result = [NSString stringWithFormat:@"?action=registerFb&fb_client_id=%@&email=%@",_clientId, _emailFB];
    
    //    NSInteger id = [_clientId integerValue];
    
    //     NSString *result = [NSString stringWithFormat:@"?action=registerFb&fb_client_id=%ld&email=%@",(long)id, _emailFB];
    
    if (_nameFB.length > 0) {
        NSMutableArray * names = [[_nameFB componentsSeparatedByString:@" "] mutableCopy];
        result = [NSString stringWithFormat:@"%@&fname=%@",result,[names firstObject]];
        
        if (names.count > 1) {
            [names removeObjectAtIndex:0];
            result = [NSString stringWithFormat:@"%@&lname=%@",result,[names componentsJoinedByString:@" "]];
        }
        
    }
        if (_photoFB) {
            result = [NSString stringWithFormat:@"%@&photo=%@",result,_photoFB];
        }
    
//     NSLog(@"registerFb %@", result);
    
    return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}

@end


