//
//  AddFeedbacksRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/4/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface AddFeedbacksRequest : RNRequest
@property (nonatomic, ) NSNumber *like;
@property (nonatomic, copy) NSNumber *restaurantId;
@property (nonatomic, copy) NSString *text;
@end
