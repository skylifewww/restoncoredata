//
//  RNRegisterRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRegisterRequest.h"
#import "RNResponse.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"

@implementation RNRegisterRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    RNAuthorizationResponse *responseWithToken = [[RNAuthorizationResponse alloc]init];
    responseWithToken.errorMessage = jsonDic[@"error"];
    responseWithToken.token = jsonDic[@"token"];
    NSDictionary *extendDic = jsonDic[@"user_data"];
    if ([extendDic[@"FNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = extendDic[@"FNAME"];
    }
    if ([extendDic[@"LNAME"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].name = [NSString stringWithFormat:@"%@ %@",[RNUser sharedInstance].name, extendDic[@"LNAME"]];
    }
    
    if ([extendDic[@"FPHONE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].phone = extendDic[@"FPHONE"];
    }
    if ([extendDic[@"EMAIL"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].email = extendDic[@"EMAIL"];
    }
//    if ([extendDic[@"ADDRESS"] isKindOfClass:[NSString class]]) {
//        [RNUser sharedInstance].photo = extendDic[@"ADDRESS"];
//    }
    if ([extendDic[@"PHOTO"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].photo = extendDic[@"PHOTO"];
    }
    if ([extendDic[@"BIRTH_DATE"] isKindOfClass:[NSString class]]) {
        [RNUser sharedInstance].birthDate = extendDic[@"BIRTH_DATE"];
    }
    [RNUser sharedInstance].idUser = extendDic[@"ID"];
    return responseWithToken;
}

- (NSString *)methodSignature {
    NSString *text = [NSString stringWithFormat:@"?action=register&email=%@&password=%@&password1=%@",_email,_password,_password];
    
    if (_userName.length > 0) {
        NSMutableArray * names = [[_userName componentsSeparatedByString:@" "] mutableCopy];
        text = [NSString stringWithFormat:@"%@&fname=%@",text,[names firstObject]];
        
        if (names.count > 1) {
            [names removeObjectAtIndex:0];
            text = [NSString stringWithFormat:@"%@&lname=%@",text,[names componentsJoinedByString:@" "]];
        }
        
    }
    if (_phone.length > 0) {
        text = [NSString stringWithFormat:@"%@&fphone=%@",text,_phone];
    }
 return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
//    return [text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(NSString *)methodType{
    
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end
