//
//  RNCancelRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 6/17/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface RNCancelRequest : RNRequest

@property (nonatomic, copy) NSNumber *reserveId;

@end
