//
//  FacebookAuthRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/26/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface FacebookAuthRequest : RNRequest

@property (nonatomic, copy) NSString *nameFB;
@property (nonatomic, copy) NSString *emailFB;
@property (copy, nonatomic) NSString *clientId;
@end
