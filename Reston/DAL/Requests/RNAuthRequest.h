//
//  RNAuthRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface RNAuthRequest : RNRequest

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

@end
