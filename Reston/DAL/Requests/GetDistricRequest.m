//
//  GetDistricRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "GetDistricRequest.h"
#import "GetDistricResponse.h"
#import "Distric.h"
#import "RNUser.h"

@implementation GetDistricRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *districsArray = jsonDic[@"raions"];
    [districsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Distric *distric = [[Distric alloc]init];
        distric.districId = obj[@"ID"];
        distric.districName = obj[@"NAME"];
        distric.cityId = obj[@"CITY"];
        [tmpArray addObject:distric];
    }];
    GetDistricResponse *response = [[GetDistricResponse alloc]init];
    response.districs = tmpArray;
    
    return response;
    
}

- (NSString *)methodSignature {
    
    if ([[_city uppercaseString] isEqualToString:@"КИЕВ"]) {
        self.city = @"1";
    } else {
        self.city = @"4";
    }
    
    NSString* getDistrics = [NSString stringWithFormat:@"?action=getraion&city=%@&token=%@",@"1",[RNUser sharedInstance].token];
    NSLog(@"NSString* getDistrics %@",getDistrics);
    
    return [getDistrics stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end


