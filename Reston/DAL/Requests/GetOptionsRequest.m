//
//  GetOptionsRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "GetOptionsRequest.h"
#import "GetOptionsResponse.h"
#import "Option.h"
#import "RNUser.h"

@implementation GetOptionsRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *optionsArray = jsonDic[@"options"];
    [optionsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Option *option = [[Option alloc]init];
        option.optionId = obj[@"ID"];
        option.optionName = obj[@"NAME"];
        [tmpArray addObject:option];
    }];
    GetOptionsResponse *response = [[GetOptionsResponse alloc]init];
    response.options = tmpArray;

    return response;
    
}

- (NSString *)methodSignature {

    NSString* getOptions = [NSString stringWithFormat:@"?action=getoptions&token=%@&lang_code=%@",[RNUser sharedInstance].token, _langCode];
    NSLog(@"NSString* getsubways %@",getOptions);
    
     return [getOptions stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end

