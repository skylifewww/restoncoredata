//
//  RNGetFeedbacksRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetFeedbacksRequest.h"
#import "RNGetFeedbacksResponse.h"
#import "RNUser.h"
#import "RNFeedback.h"

@implementation RNGetFeedbacksRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    RNGetFeedbacksResponse *response = [[RNGetFeedbacksResponse alloc]init];
    if (jsonDic[@"feedbacks"] != NULL) {
    
           NSArray *array = jsonDic[@"feedbacks"];
        NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
        for (NSDictionary *dic in array) {
            [result addObject:[[RNFeedback alloc] initWithDictionary:dic]];
        }
        
        response.feedbacks = result;
    
    } else{
        NSLog(@"NO data feedbacks");
        return nil;
    }
    
    return response;
    
}

- (NSString *)methodSignature {
    return [NSString stringWithFormat:@"?action=getfeedbacks&restaurantId=%@&token=%@",_restaurantId,[RNUser sharedInstance].token];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end
