//
//  WriteUSRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 16.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface WriteUSRequest : RNRequest

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *themeMessage;
@property (nonatomic, copy) NSString *userName;

@end
