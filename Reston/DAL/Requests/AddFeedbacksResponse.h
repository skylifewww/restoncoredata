//
//  AddFeedbacksResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/4/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RNResponse.h"

@interface AddFeedbacksResponse : RNResponse

@property (nonatomic, copy) NSString *responseString;

@end
