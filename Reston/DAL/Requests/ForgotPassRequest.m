//
//  ForgotPassRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 12.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "ForgotPassRequest.h"
#import "ForgotPassResponse.h"

@implementation ForgotPassRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    ForgotPassResponse *responsePass = [[ForgotPassResponse alloc]init];
    if (jsonDic[@"forgotpass"]){
        
        responsePass.success = jsonDic[@"forgotpass"];
        
    } else if(jsonDic[@"error"]){
        
        responsePass.error = jsonDic[@"error"];
    }
    
    return responsePass;
}

- (NSString *)methodSignature {
    NSString *text = [NSString stringWithFormat:@"?action=forgotpass&email=%@",_email];
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}

@end
