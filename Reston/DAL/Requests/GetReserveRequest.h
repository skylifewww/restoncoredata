//
//  GetReserveRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/11/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface GetReserveRequest : RNRequest
@property (nonatomic, copy) NSString *langCode;

@end
