//
//  RNMakeReserveRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNMakeReserveRequest.h"
#import "RNMakeReserveResponse.h"
#import "RNUser.h"

@implementation RNMakeReserveRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    RNMakeReserveResponse *responseReserve = [[RNMakeReserveResponse alloc]init];
    responseReserve.success = jsonDic[@"reserv"];
    responseReserve.error = jsonDic[@"error"];
    return responseReserve;
}

- (NSString *)methodSignature {
    NSString *result = [NSString stringWithFormat:@"?action=reserv&restaurantId=%@&number_of_people=%@&date=%@&time=%@&ref_name=ios&phone=%@",_restaurantId,_numberOfPeople,_date,_time, _phone];
    
    if (_userName.length> 0) {
        result = [NSString stringWithFormat:@"%@&userName=%@",result,_userName];
    }
    if (_email.length> 0) {
        result = [NSString stringWithFormat:@"%@&email=%@",result,_email];
    }
//    if (_phone.length> 0) {
//        result = [NSString stringWithFormat:@"%@&phone=%@",result,_phone];
//    }
    if (_comments.length> 0) {
        result = [NSString stringWithFormat:@"%@&text=%@",result,_comments];
    }
    if ([RNUser sharedInstance].token.length> 0) {
        result = [NSString stringWithFormat:@"%@&token=%@",result,[RNUser sharedInstance].token];
    }
    if (_userName.length > 0) {
        NSMutableArray * names = [[_userName componentsSeparatedByString:@" "] mutableCopy];
        result = [NSString stringWithFormat:@"%@&fname=%@",result,[names firstObject]];
        
        if (names.count > 1) {
            [names removeObjectAtIndex:0];
            result = [NSString stringWithFormat:@"%@&lname=%@",result,[names componentsJoinedByString:@" "]];
        }
    }
    NSLog(@"result %@", result);
    
return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
//    return @"result";

}




-(NSString *)methodType{
    
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end
