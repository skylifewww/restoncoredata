//
//  WriteUSRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 16.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "WriteUSRequest.h"
#import "WriteUSResponse.h"

@implementation WriteUSRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    WriteUSResponse *responseWriteUS = [[WriteUSResponse alloc]init];
    responseWriteUS.contactus = jsonDic[@"contactus"];
    return responseWriteUS;
}

- (NSString *)methodSignature {
    NSString *result = [NSString stringWithFormat:@"?action=contactus&email=%@&name=%@&message=%@&subject=%@",_email,_userName,_message,_themeMessage];

    NSLog(@"result %@", result);
    return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

-(NSString *)methodType{
    
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end

