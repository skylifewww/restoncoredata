//
//  RNRemoveFromFavRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRemoveFromFavRequest.h"
#import "RNAddToFavResponse.h"
#import "RNUser.h"

@implementation RNRemoveFromFavRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    RNAddToFavResponse *responseFavorites = [[RNAddToFavResponse alloc]init];
    responseFavorites.responseString = jsonDic[@"removefromfavorite"];
    return responseFavorites;
}

- (NSString *)methodSignature {
    NSString *token = [RNUser sharedInstance].token;//addtofavorite
    NSString *text = [NSString stringWithFormat:@"?action=removefromfavorite&restaurantId=%@&token=%@",_restaurantId,token];
  return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}


@end
