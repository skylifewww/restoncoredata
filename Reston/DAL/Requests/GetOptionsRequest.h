//
//  GetOptionsRequest.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface GetOptionsRequest : RNRequest
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *langCode;
@end
