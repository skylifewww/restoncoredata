//
//  RNGetRestaurantsRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "RNRestaurant.h"
#import "RNUser.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "CDRestaurant+CoreDataProperties.h"
#import "CoreDataManager.h"

@implementation RNGetRestaurantsRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    
    
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    __block NSError *error;
    NSArray *restrauntsArray = jsonDic[@"restaurants"];
    __block NSMutableArray *tmpCDArray = [[NSMutableArray alloc]init];
    [[CoreDataManager sharedManager] deleteAllObjectsWithEntityName:@"CDRestaurant"];
    [restrauntsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CDRestaurant *restaurantCD = [NSEntityDescription insertNewObjectForEntityForName:@"CDRestaurant" inManagedObjectContext:[[CoreDataManager sharedManager] managedObjectContext]];
        [restaurantCD initWithDictionary:obj];
        if (![[[CoreDataManager sharedManager] managedObjectContext] save:&error]) {
            NSLog(@"%@", [error localizedDescription]);
        }
        [tmpCDArray addObject:restaurantCD];
    }];
//    [[CoreDataManager sharedManager] saveContext];
    RNGetRestaurantsResponse *responseRestaurants = [[RNGetRestaurantsResponse alloc]init];
    if (tmpCDArray.count > 0){
        responseRestaurants.success = @"YES";
    } else {
        responseRestaurants.success = @"NO";
    }
    return responseRestaurants;
}

- (NSString *)methodSignature {
    
    _isAuth = YES;
    
    NSString* lang = [[NSString alloc] init];
    NSString* langCode = [[NSString alloc] init];
    if (_cityName != nil){
        
        NSLog(@"cityName != nil");
        if ([_cityName isEqualToString:@"1"]){
            langCode = @"ru";
            lang = @"ru";
            _city = [_cityName uppercaseString];
        } else if ([_cityName isEqualToString:@"9"]){
            langCode = @"ru";
            lang = @"ru";
            _city = [_cityName uppercaseString];
        } else if ([_cityName isEqualToString:@"6"]){
            langCode = @"ua";
            lang = @"uk";
            _city = [_cityName uppercaseString];
        } else if ([_cityName isEqualToString:@"8"]){
            langCode = @"ua";
            lang = @"uk";
            _city = [_cityName uppercaseString];
        } else {
            langCode = @"ru";
            lang = @"ru";
            _cityName = @"1";
            _city = [_cityName uppercaseString];
        }
    } else {
        langCode = @"ru";
        lang = @"ru";
        _cityName = @"1";
    }
    
    
    NSString *language = [[NSUserDefaults standardUserDefaults] stringForKey:languageOnApp];
    if(!language)
    {
        NSString *languageDivice = [NSLocale preferredLanguages][0] ;
        languageDivice = [languageDivice componentsSeparatedByString:@"-"][0];
        if( [langArray containsObject:languageDivice])
            [[NSUserDefaults standardUserDefaults] setObject:languageDivice forKey:languageOnApp];
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"ru" forKey:languageOnApp];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        if ([language isEqualToString:@"ru"]){
            langCode = @"ru";
        } else if ([language isEqualToString:@"uk"]){
            langCode = @"ua";
        }
        lang = language;
    }
    
    dp_set_current_language(lang);
    
    _langCode = langCode;
    _city = _cityName;
    [RNUser sharedInstance].currCity = _cityName;
    
    NSString *result = [[NSString alloc] init];
    
//    if (_isAuth == YES){
//        result = [NSString stringWithFormat:@"?action=getrestaurants&city=%@&token=%@&lang_code=%@",_city,_token,_langCode];
//        if (_name.length > 0) {
//            result = [NSString stringWithFormat:@"%@&name=%@",result,_name];
//        }
//    } else {
//        result = [NSString stringWithFormat:@"?action=getrestaurants&city=%@&lang_code=%@",_city,_langCode];
//        if (_name.length > 0) {
//            result = [NSString stringWithFormat:@"%@&name=%@",result,_name];
//        }
//    }
    
    if (_isAuth == YES){
        result = [NSString stringWithFormat:@"?action=getrestaurants&&token=%@",_token];
        if (_name.length > 0) {
            result = [NSString stringWithFormat:@"%@&name=%@",result,_name];
        }
    } else {
        result = [NSString stringWithFormat:@"?action=getrestaurants&city=%@",_city];
        if (_name.length > 0) {
            result = [NSString stringWithFormat:@"%@&name=%@",result,_name];
        }
    }
    NSLog(@"url: %@",result);
    return [result stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}

@end
