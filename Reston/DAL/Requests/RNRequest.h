//
//  RNRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "RNResponse.h"

extern NSString *const kServerURL;

@interface RNRequest : NSObject

- (void)sendWithcompletion:(void (^)(RNResponse *response, NSError *error))completion;
- (RNResponse *)bindResponseData:(NSData *)responseData;
- (NSString *) methodType;
- (NSString *) methodSignature;
- (NSData *) serialize;

@end
