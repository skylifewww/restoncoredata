//
//  RNGetFavorites.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNGetFavoritesRequest.h"
#import "RNGetFavoritesResponse.h"
#import "RNUser.h"
#import "RNRestaurant.h"

@implementation RNGetFavoritesRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    RNGetFavoritesResponse *responseFavorites = [[RNGetFavoritesResponse alloc]init];
    NSArray *restrauntsArray = jsonDic[@"restaurants"];
    __block NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    [restrauntsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        RNRestaurant *restaurant=[[RNRestaurant alloc]initWithDictionary:obj];
        [tmpArray addObject:restaurant];
    }];
    responseFavorites.favModels = tmpArray;

    return responseFavorites;
}

- (NSString *)methodSignature {
    NSString *token = [RNUser sharedInstance].token;
    NSLog(@"NSString *token Favorites %@", token);
//    if (_like == nil && _restaurantId == nil) {
        //getrestaurants&city=
        NSString *text1 = [NSString stringWithFormat:@"?action=getfavorite&token=%@&lang_code=%@",token, _langCode];
//     NSString *text1 = [NSString stringWithFormat:@"?action=getfavorites&myfavorite=1&token=%@",token];
//        NSString *text1 = [NSString stringWithFormat:@"?action=getfavorites&myfavorite=1&token=%@",token];
        return [text1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];;
//    }
    
//    NSString *text = [NSString stringWithFormat:@"?action=getfavorites&like=%@&token=%@",_like,token];
//    NSString *text = [NSString stringWithFormat:@"?action=favorite&like=%@&token=%@",_like,token];
//    if (_restaurantId != nil) {
//        text = [NSString stringWithFormat:@"%@&restaurantId=%@",text,_restaurantId];
//    }
//    if (_text != nil) {
//        text = [NSString stringWithFormat:@"%@&text=%@",text,_text];
//    }
    
    return [text1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}


@end
