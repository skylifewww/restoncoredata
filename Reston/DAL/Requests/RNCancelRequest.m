//
//  RNCancelRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/17/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNCancelRequest.h"
#import "RNUser.h"
#import "RNCancelResponse.h"
@import UIKit;
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"


@implementation RNCancelRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    id result = jsonDic[@"success"];
    if ([result isKindOfClass:[NSString class]] && [result isEqualToString:@"reserv deleted"]) {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:DPLocalizedString(@"dell_order", nil)
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
    }
    RNCancelResponse *response = [[RNCancelResponse alloc]init];
    return response;
    
}


- (NSString *)methodSignature {
    return [NSString stringWithFormat:@"?action=reservdelete&reserv=%@&token=%@",_reserveId,[RNUser sharedInstance].token];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}


@end
