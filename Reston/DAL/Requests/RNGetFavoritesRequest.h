//
//  RNGetFavorites.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRequest.h"

@interface RNGetFavoritesRequest : RNRequest

@property (nonatomic, copy) NSNumber *like;

// favor restaurant
@property (nonatomic, copy) NSNumber *restaurantId;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *langCode;

@end
