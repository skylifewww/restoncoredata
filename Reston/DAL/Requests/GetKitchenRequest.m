//
//  GetKitchenRequest.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "GetKitchenRequest.h"
#import "GetKitchenResponse.h"
#import "Kitchen.h"
#import "RNUser.h"

@implementation GetKitchenRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    NSArray *kitchenArray = jsonDic[@"kitchen"];
    [kitchenArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Kitchen *kitchen = [[Kitchen alloc]init];
        kitchen.kitchenId = obj[@"ID"];
        kitchen.kitchenName = obj[@"NAME"];
        [tmpArray addObject:kitchen];
    }];
    GetKitchenResponse *response = [[GetKitchenResponse alloc]init];
    response.kitchens = tmpArray;
    
    return response;
    
}

- (NSString *)methodSignature {
    
    NSString* getKitchens = [NSString stringWithFormat:@"?action=getkitchen&token=%@&lang_code=%@&city=%@",[RNUser sharedInstance].token, _langCode, _city];
    NSLog(@"NSString* getKitchens %@",getKitchens);
    
    return [getKitchens stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}
- (NSData *)serialize {
    return [NSData data];
}
@end


