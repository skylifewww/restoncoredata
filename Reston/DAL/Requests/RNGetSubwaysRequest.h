//
//  RNGetSubwaysRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 10.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRequest.h"

@interface RNGetSubwaysRequest : RNRequest
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *langCode;
@end
