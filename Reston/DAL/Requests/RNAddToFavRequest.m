//
//  RNAddToFavRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNAddToFavRequest.h"
#import "RNUser.h"
#import "RNAddToFavResponse.h"

@implementation RNAddToFavRequest
- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    if (jsonDic[@"error"]) {
        return nil;
    }
    RNAddToFavResponse *responseFavorites = [[RNAddToFavResponse alloc]init];
    responseFavorites.responseString = jsonDic[@"addtofavorite"];
    return responseFavorites;
}

- (NSString *)methodSignature {
    NSString *token = [RNUser sharedInstance].token;
    NSString *text = [NSString stringWithFormat:@"?action=addtofavorite&restaurantId=%@&token=%@",_restaurantId,token];
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

- (NSString *)methodType{
    return @"GET";
}

- (NSData *)serialize {
    return [NSData data];
}

@end
