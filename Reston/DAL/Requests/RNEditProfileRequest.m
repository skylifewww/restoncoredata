//
//  RNEditProfileRequest.m
//  Reston
//
//  Created by Yurii Oliiar on 21.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNEditProfileRequest.h"
#import "RNUser.h"
#import "RNAuthorizationResponse.h"

@implementation RNEditProfileRequest

- (RNResponse *)bindResponseData:(NSData *)responseData {
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:kNilOptions error:nil];
    RNAuthorizationResponse *responseWithSuccess = [[RNAuthorizationResponse alloc]init];
    responseWithSuccess.editProfile = jsonDic[@"editprofile"];
    
    return responseWithSuccess;
}



- (NSString *)methodSignature {
    NSString *text = [NSString stringWithFormat:@"?action=editprofile&token=%@&email=%@", [RNUser sharedInstance].token, self.email];
    
    if (self.password.length > 0) {
        text = [NSString stringWithFormat:@"%@&password=%@&password1=%@",text,self.password,self.password];
    }
    
    if (self.userName.length > 0) {
        NSMutableArray * names = [[self.userName componentsSeparatedByString:@" "] mutableCopy];
        text = [NSString stringWithFormat:@"%@&fname=%@",text,[names firstObject]];
        
        if (names.count > 1) {
            [names removeObjectAtIndex:0];
            text = [NSString stringWithFormat:@"%@&lname=%@",text,[names componentsJoinedByString:@" "]];
        }
        
    }
    if (self.city.length > 0) {
        text = [NSString stringWithFormat:@"%@&city=%@",text,self.city];
    }
    if (self.phone.length > 0) {    
        text = [NSString stringWithFormat:@"%@&fphone=%@",text,self.phone];
    }
    if (self.birthDate.length > 0) {
        text = [NSString stringWithFormat:@"%@&birth_date=%@",text,self.birthDate];
    }
    if (self.photo.length > 0) {
        text = [NSString stringWithFormat:@"%@&photo=%@",text,self.photo];
    }
//    if (self.photo.length > 0) {
//        text = [NSString stringWithFormat:@"%@&address=%@",text,self.photo];
//    }
    
    return [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
}

@end
