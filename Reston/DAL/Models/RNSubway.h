//
//  RNSubway.h
//  Reston
//
//  Created by Yurii Oliiar on 11.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RNSubway : NSObject

@property (nonatomic, copy) NSNumber *subwayId;
@property (nonatomic, copy) NSString *subwayName;
@property (nonatomic, copy) NSNumber *cityId;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
