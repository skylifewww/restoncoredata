//
//  RNSubway.m
//  Reston
//
//  Created by Yurii Oliiar on 11.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNSubway.h"

@implementation RNSubway
- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.subwayName = dic[@"NAME"];
        self.subwayId = dic[@"ID"];
        self.cityId = dic[@"CITY"];
    }
    return self;
}

@end
