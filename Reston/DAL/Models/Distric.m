//
//  Distric.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "Distric.h"

@implementation Distric
- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.districName = dic[@"NAME"];
        self.districId = dic[@"ID"];
        self.cityId = dic[@"CITY"];
    }
    return self;
}

@end
