//
//  FilterInfo.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/25/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNSubway.h"
#import "RNRestaurant.h"

@interface FilterInfo : NSObject

@property (nonatomic, strong) RNRestaurant *restaurant;
@property (nonatomic, strong) NSArray *subway;
@property (nonatomic, strong) NSArray *typeRest;
@property (nonatomic, strong) NSArray *distric;
@property (nonatomic, strong) NSArray *options;
@property (nonatomic, strong) NSString *avgBill;
@property (nonatomic, strong) NSArray *kitchen;
@property (nonatomic, strong) NSArray *workRestons;
@end
