//
//  Option.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Option : NSObject

@property (nonatomic, copy) NSNumber *optionId;
@property (nonatomic, copy) NSString *optionName;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
