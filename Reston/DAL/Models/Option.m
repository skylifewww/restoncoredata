//
//  Option.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "Option.h"

@implementation Option
- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.optionName = dic[@"NAME"];
        self.optionId = dic[@"ID"];
    }
    return self;
}

@end
