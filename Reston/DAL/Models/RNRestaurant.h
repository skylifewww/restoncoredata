//
//  RNRestaurant.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface RNRestaurant : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSNumber *restaurantId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSArray *kitchen;
@property (nonatomic, copy) NSString *distric;
@property (nonatomic, copy) NSNumber *subway;
@property (nonatomic, copy) NSString *subwayName;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSNumber *views;
@property (nonatomic, copy) NSNumber *discountData;
@property (nonatomic, copy) NSNumber *discounts;
@property (nonatomic, copy) NSString *discountsType;
@property (nonatomic, copy) NSString *descriptionRestaurant;
@property (nonatomic, copy) NSString *shortDescription;
@property (nonatomic, copy) NSString *personsLimits;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *avgBill;
@property (nonatomic, copy) NSArray *options;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic, copy) NSNumber *isLiked;
@property (nonatomic, copy) NSNumber *likeCount;
@property (nonatomic, copy) NSNumber *isFavorited;
@property (nonatomic, copy) NSNumber *reviewCount;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString* distanceRest;
@property (nonatomic, copy) NSString* rating;
@property (nonatomic, copy) NSDate* addition_date;
@property (nonatomic, copy) NSNumber *dayOfWeek;
@property (nonatomic, copy) NSDate* timeFrom;
@property (nonatomic, copy) NSDate* timeTo;
@property (nonatomic, copy) NSArray* workTimes;
@property (nonatomic, copy) NSString* position;
@property (nonatomic, copy) NSString* catalogPriority;
@property (nonatomic, assign) NSInteger intCatalogPriority;
@property (nonatomic, assign) NSInteger intPosition;
@property (nonatomic, copy) NSString *menuFile;
@property (nonatomic, copy) NSString *menuFileType;
@property (nonatomic, copy) NSString *menuFileUrl;


/* unused
 
 add DISCOUNTS
@property (nonatomic, copy) NSString *addition_date;//ADDITION_DATE

@property (nonatomic, copy) NSNumber *isAvailable; //AVAILABLE - BOOL
@property (nonatomic, copy) NSNumber *isBanquetOn;//BOOL
@property (nonatomic, copy) NSNumber *dutyEnd;
@property (nonatomic, copy) NSNumber *dutyStart;
@property (nonatomic, copy) NSNumber *isEcsmpsignOn; // BOOL
@property (nonatomic, copy) NSNumber *fixedMain;
@property (nonatomic, copy)  NSNumber *fixedRand;
@property (nonatomic, copy) NSNumber *foursquareId;
@property (nonatomic, copy) NSNumber *mainImageId;
@property (nonatomic, copy) NSNumber *markerId;
@property (nonatomic, copy) NSString *menuFile;
@property (nonatomic, copy) NSString *menuFileType;
@property (nonatomic, copy) NSURL *menuFileUrl;
@property (nonatomic, copy) NSString *site;
@property (nonatomic, copy) NSString *slug;
@property (nonatomic, copy) NSNumber  *isSmsApproveOn; //BOOL
@property (nonatomic, copy) NSNumber *isStatisticOn; //BOOL

@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, copy) NSNumber *vid;
@property (nonatomic, copy) NSData *videoData;

@property (nonatomic, copy) NSNumber *isVisible;//BOOL
*/

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end



