//
//  TimeModel.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.12.2017.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeModel : NSObject

@property (strong, nonatomic) NSString* hours;
@property (strong, nonatomic) NSString* minutes;
@property (assign, nonatomic) Boolean isNowMorning;
@property (assign, nonatomic) Boolean isNextDay;
@end
