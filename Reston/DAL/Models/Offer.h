//
//  Offer.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRestaurant.h"

@interface Offer : NSObject

@property (nonatomic, copy) NSString *dateStart;
@property (nonatomic, copy) NSString *dateEnd;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *offerID;
//@property (nonatomic, strong) NSNumber *restID;
@property (nonatomic, strong) RNRestaurant* restaurant;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
