//
//  RNNEw.m
//  Reston
//
//  Created by Yurii Oliiar on 6/17/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNNEw.h"

@implementation RNNEw

- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.date = dic[@"DATE"];
        self.image = dic[@"IMAGE"];
        self.shortText = dic[@"SHORT_TEXT"];
        self.text = dic[@"TEXT"];
        self.title = dic[@"TITLE"];
        self.imageFullString = @"http://reston.com.ua/uploads/img/news/original/";
    }
    return self;
}

@end
