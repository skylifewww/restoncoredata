//
//  PestType.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RestType.h"

@implementation RestType
- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.restTypeName = dic[@"NAME"];
        self.restTypeId = dic[@"ID"];
    }
    return self;
}

@end
