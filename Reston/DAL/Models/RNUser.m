//
//  RNUser.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNUser.h"

@implementation RNUser

+ (RNUser *)sharedInstance{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RNUser alloc] init];
    });
    return sharedInstance;
}

- (NSString *)subwayNameFord:(NSNumber *)subwaId {
    for (RNSubway *subway in _subways) {
        if (subway.subwayId.integerValue == subwaId.integerValue) {
            return subway.subwayName;
        }
    }
    return @"";
}

@end
