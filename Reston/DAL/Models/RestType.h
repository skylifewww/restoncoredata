//
//  PestType.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestType : NSObject
@property (nonatomic, copy) NSNumber *restTypeId;
@property (nonatomic, copy) NSString *restTypeName;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
