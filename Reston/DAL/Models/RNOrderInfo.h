//
//  RNOrderInfo.h
//  Reston
//
//  Created by Yurii Oliiar on 6/13/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNSubway.h"
#import "RNRestaurant.h"

@interface RNOrderInfo : NSObject

@property (nonatomic, strong) RNRestaurant *restaurant;
@property (nonatomic, strong) RNSubway *subway;
@property (nonatomic, copy) NSString *restAddress;
@property (nonatomic, copy) NSString *restName;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSNumber *peopleCount;
@property (nonatomic, copy) NSNumber *reservID;
@property (nonatomic, copy) NSString *dateStr;
@property (nonatomic, copy) NSString *timeStr;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSDate *time;
@property (nonatomic, copy) NSString *timeString;
@property (nonatomic, copy) NSString *notes;
@property (nonatomic, copy) NSString *currentDay;

@end
