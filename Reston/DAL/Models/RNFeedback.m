//
//  RNFeedback.m
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNFeedback.h"

@implementation RNFeedback

- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.username = dic[@"USER_FNAME"];
        self.author = dic[@"AUTHOR"];
        self.text = dic[@"TEXT"];
        self.date = dic[@"DATE"];
        self.state = dic[@"STATE"];
        
        if (![dic[@"SOURCE_USER"] isKindOfClass:[NSNull class]]){
        
        NSDictionary *sourceUser = dic[@"SOURCE_USER"];
        if (sourceUser[@"NAME"] ){
            self.name = sourceUser[@"NAME"] ;
        }
        
        if (sourceUser[@"PHOTO"]){
            self.photo = sourceUser[@"PHOTO"];
        }
      }
        
    }
    return self;
}

@end
