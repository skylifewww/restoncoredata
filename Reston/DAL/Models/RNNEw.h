//
//  RNNEw.h
//  Reston
//
//  Created by Yurii Oliiar on 6/17/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNRestaurant.h"

@interface RNNEw : NSObject

@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *shortText;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSString *imageString;
@property (nonatomic, strong) NSString *imageFullString;
@property (nonatomic, strong) RNRestaurant* restaurant;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
