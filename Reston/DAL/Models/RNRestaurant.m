//
//  RNRestaurant.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNRestaurant.h"

typedef NS_ENUM(NSInteger, RestonType)
{
    Restourant = 1,
    Caffe,
    Bar,
    WeekdayWednesday,
    WeekdayThursday,
    WeekdayFriday,
    WeekdaySaturday
};

@implementation RNRestaurant

- (instancetype)initWithDictionary:(NSDictionary *)obj{
    self.restaurantId = obj[@"ID"];
    self.kitchen = obj[@"KITCHEN"];
    self.logo = obj[@"LOGO"];
    self.city = obj[@"CITY"];
    self.descriptionRestaurant = obj[@"DESCRIPTION"];
    self.descriptionRestaurant = [NSString stringWithFormat:@"   %@",_descriptionRestaurant];
    self.descriptionRestaurant = [_descriptionRestaurant stringByReplacingOccurrencesOfString:@"\n" withString:@"\n   "];
    self.discountData = obj[@"DISCOUNTS_DATA"];
    self.discountsType = obj[@"DISCOUNTS_TYPE"];
    self.discounts = obj[@"DISCOUNTS"];
    self.title = obj[@"NAME"];
    self.personsLimits = obj[@"MAX_PEOPLE_COUNT"];
    self.shortDescription = obj[@"SHORT_DESCRIPTION"];
//    NSLog(@"self.personsLimits %@", self.personsLimits);
//    NSLog(@"self.discounts %@", self.discounts);


    self.subway = obj[@"SUBWAY"];

    if (![obj[@"SUBWAYNAME"] isEqual:[NSNull null]]){
        self.subwayName = obj[@"SUBWAYNAME"];
    }


    self.telephone = obj[@"TELEPHONE"];
    self.views = obj[@"VIEWS"];
    self.address = obj[@"ADDRESS"];
    self.avgBill = obj[@"AVG_BILL"];
    self.options = obj[@"OPTIONS"];
    self.isLiked = obj[@"ISLIKED"];
    self.likeCount = obj[@"LIKECOUNT"];
    self.isFavorited = obj[@"MYFAVORITE"];
    self.reviewCount = obj[@"REVIEWCOUNT"];
    self.type = obj[@"TYPE"];
    self.rating = obj[@"RATING"];
    self.distric = obj[@"RAION"];
    self.addition_date = obj[@"ADDITION_DATE"];
    self.position = obj[@"POSITION"];
    self.catalogPriority = obj[@"CATALOG_PRIORITY"];
    if ([self.catalogPriority isEqual:[NSNull null]]){
        self.intCatalogPriority = 1000;

    } else {
       self.intCatalogPriority = self.catalogPriority.integerValue;
    }

       NSNumber *lat = obj[@"LAT"];
    NSNumber *lng = obj[@"LNG"];
    self.coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
    self.subtitle = [_address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                             withString:@""];;
    NSArray *imgDics = obj[@"IMAGES"];
    NSMutableArray *mutableImages = [[NSMutableArray alloc] initWithCapacity:imgDics.count];
    for (NSDictionary *dic in imgDics) {
        [mutableImages addObject:dic[@"IMAGE"]];
    }
    self.images = mutableImages;

    self.workTimes = obj[@"WORKING_TIME"];
    self.menuFile = obj[@"MENU_FILE"];
    self.menuFileType = obj[@"MENU_FILE_TYPE"];
    self.menuFileUrl = obj[@"MENU_FILE_URL"];

    return self;
}

//- (void)initWithDictionary:(NSDictionary *)obj{
//    self.restaurantId = obj[@"ID"];
//    self.logo = obj[@"LOGO"];
//    self.city = obj[@"CITY"];
//    self.kitchen = obj[@"KITCHEN"];
//    self.descriptionRestaurant = obj[@"DESCRIPTION"];
//    self.descriptionRestaurant = [NSString stringWithFormat:@"   %@",self.descriptionRestaurant];
//    self.descriptionRestaurant = [self.descriptionRestaurant stringByReplacingOccurrencesOfString:@"\n" withString:@"\n   "];
//    self.discountData = obj[@"DISCOUNTS_DATA"];
//    self.discountsType = obj[@"DISCOUNTS_TYPE"];
//    self.discounts = (int16_t)obj[@"DISCOUNTS"];
//    self.title = obj[@"NAME"];
//    self.personsLimits = obj[@"MAX_PEOPLE_COUNT"];
//    self.shortDescript = obj[@"SHORT_DESCRIPTION"];
//    self.subway = obj[@"SUBWAY"];
//    
//    if (![obj[@"SUBWAYNAME"] isEqual:[NSNull null]]){
//        self.subwayName = obj[@"SUBWAYNAME"];
//    }
//    self.telephone = obj[@"TELEPHONE"];
//    self.views = obj[@"VIEWS"];
//    self.address = obj[@"ADDRESS"];
//    self.avgBill = obj[@"AVG_BILL"];
//    self.options = obj[@"OPTIONS"];
//    self.isLiked = (int16_t)obj[@"ISLIKED"];
//    self.likeCount = (int64_t)obj[@"LIKECOUNT"];
//    self.isFavorited = (boolean_t)obj[@"MYFAVORITE"];
//    self.reviewCount = (int64_t)obj[@"REVIEWCOUNT"];
//    self.type = obj[@"TYPE"];
//    self.rating = [obj[@"RATING"] doubleValue];
//    self.distric = obj[@"RAION"];
//    self.addition_date = obj[@"ADDITION_DATE"];
//    //        self.position = obj[@"POSITION"];
//    
//    
//    if ([obj[@"CATALOG_PRIORITY"] isEqual:[NSNull null]]){
//        
//        self.catalogPriority = @"10000";
//        self.intCatalogPriority = (int32_t)[self.catalogPriority integerValue];
//        
//    } else {
//        self.catalogPriority = obj[@"CATALOG_PRIORITY"];
//        self.intCatalogPriority = (int32_t)[self.catalogPriority integerValue];
//    }
//    
//    self.lat = [obj[@"LAT"] doubleValue];
//    self.lon = [obj[@"LNG"] doubleValue];
//    
//    //    self.coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
//    self.subtitle = [self.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
//                                                            withString:@""];;
//    NSArray *imgDics = obj[@"IMAGES"];
//    NSMutableArray *mutableImages = [[NSMutableArray alloc] initWithCapacity:imgDics.count];
//    for (NSDictionary *dic in imgDics) {
//        [mutableImages addObject:dic[@"IMAGE"]];
//    }
//    self.images = mutableImages;
//    
//    self.workTimes = obj[@"WORKING_TIME"];
//    self.menuFile = obj[@"MENU_FILE"];
//    self.menuFileType = obj[@"MENU_FILE_TYPE"];
//    self.menuFileUrl = obj[@"MENU_FILE_URL"];
//}
@end
