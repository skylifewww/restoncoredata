    //
//  PriceSlider.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "PriceSlider.h"

@implementation PriceSlider

- (id) initWithFrame: (CGRect)rect{
    if ((self=[super initWithFrame:CGRectMake(rect.origin.x,rect.origin.y,90,27)])){
        [self awakeFromNib];
    }
    return self;
}

- (CGRect)trackRectForBounds:(CGRect)bounds {
    CGRect rect = CGRectMake(0, 0, 100, 30);//change it to any size you want
    return rect;
}

@end
