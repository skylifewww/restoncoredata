//
//  RNOrderInfo.m
//  Reston
//
//  Created by Yurii Oliiar on 6/13/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNOrderInfo.h"

@implementation RNOrderInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        self.peopleCount = @4;
        self.city = @"Киев";
        self.date = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components: NSCalendarUnitHour| NSCalendarUnitMinute
                                                   fromDate:[NSDate dateWithTimeIntervalSinceNow:900]];
        if (components.minute > 45) {
            components.minute = 0;
            components.hour = components.hour == 23 ? 0 : components.hour + 1;
        } else if (components.minute > 30) {
            components.minute = 45;
        } else if (components.minute > 15) {
            components.minute = 30;
        } else {
            components.minute = 15;
        }
        
        self.time = [calendar dateFromComponents:components];
    }
    return self;
}

@end
