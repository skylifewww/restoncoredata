//
//  Distric.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Distric : NSObject
@property (nonatomic, copy) NSNumber *districId;
@property (nonatomic, copy) NSString *districName;
@property (nonatomic, copy) NSNumber *cityId;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
