//
//  RNUser.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNSubway.h"

@interface RNUser : NSObject

@property (nonatomic, copy) NSString *idUser;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *photoFB;
@property (nonatomic, copy) NSString *realCity;
@property (nonatomic, copy) NSString *currCity;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *tokenPushNotif;
@property (nonatomic, copy) NSString *birthDate;
@property (nonatomic, copy) NSString *addresPhoto;
@property (assign, nonatomic) Boolean isAuth;
@property (assign, nonatomic) Boolean isGeoEnabled;
@property (assign, nonatomic) Boolean isThisCity;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;



@property (nonatomic, copy)NSArray *subways;
- (NSString *)subwayNameFord:(NSNumber *)subwaId;

+ (RNUser *)sharedInstance;

/*
 FNAME = "<null>";
 FPHONE = "<null>";
 LNAME = "<null>"
 EMAIL = "test@gmail.com";
 CITY = "<null>";
 
 ADDRESS = "<null>";
 "BIRTH_DATE" = "<null>";
 CART = "<null>";
 GENDER = "<null>";
 ICQ = "<null>";
 ID = 3836;
 "LAST_VISIT" = "2015-06-14 22:29:12";
 ;
 "PHONE_COMFIRMED" = "<null>";
 PHOTO = "";
 SKYPE = "<null>";
 SNAME = "<null>";

 */

@end
