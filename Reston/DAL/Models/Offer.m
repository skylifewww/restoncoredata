//
//  Offer.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "Offer.h"

@implementation Offer

- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.dateStart = dic[@"DATE_START"];
        self.dateEnd = dic[@"DATE_END"];
        self.image = dic[@"IMAGE"];
        self.offerID = dic[@"ID"];
        self.text = dic[@"TEXT"];
        self.title = dic[@"NAME"];
    }
    return self;
}

@end
