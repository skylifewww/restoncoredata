//
//  RNFeedback.h
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RNFeedback : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSNumber *state;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *photo;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
