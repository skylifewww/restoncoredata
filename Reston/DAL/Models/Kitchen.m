//
//  Kitchen.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "Kitchen.h"

@implementation Kitchen
- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self != nil) {
        self.kitchenName = dic[@"NAME"];
        self.kitchenId = dic[@"ID"];
    }
    return self;
}

@end
