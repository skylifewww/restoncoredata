//
//  Kitchen.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Kitchen : NSObject

@property (nonatomic, copy) NSNumber *kitchenId;
@property (nonatomic, copy) NSString *kitchenName;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
