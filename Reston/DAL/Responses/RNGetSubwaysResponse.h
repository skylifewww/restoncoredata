//
//  RNGetSubwaysResponse.h
//  Reston
//
//  Created by Yurii Oliiar on 11.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNResponse.h"

@interface RNGetSubwaysResponse : RNResponse

@property (nonatomic, copy) NSArray *subwayArray;

@end
