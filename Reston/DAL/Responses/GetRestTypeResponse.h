//
//  GetRestTypeResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/3/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "RNResponse.h"

@interface GetRestTypeResponse : RNResponse
@property (nonatomic, copy) NSArray *restTypes;
@end
