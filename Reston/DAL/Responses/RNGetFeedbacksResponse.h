//
//  RNGetFeedbacksResponse.h
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNResponse.h"
#import "RNFeedback.h"

@interface RNGetFeedbacksResponse : RNResponse

@property (nonatomic, copy) NSArray *feedbacks;

@end
