//
//  ForgotPassResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 12.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"

@interface ForgotPassResponse : RNResponse
@property (nonatomic, copy) NSString *success;
@property (nonatomic, copy) NSString *error;
@end
