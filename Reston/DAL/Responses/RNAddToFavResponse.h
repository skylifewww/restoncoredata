//
//  RNAddToFavResponse.h
//  Reston
//
//  Created by Yurii Oliiar on 6/25/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNResponse.h"

@interface RNAddToFavResponse : RNResponse

@property (nonatomic, copy) NSString *responseString;
@end
