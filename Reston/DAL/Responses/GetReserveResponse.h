//
//  GetReserveResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/11/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"

@interface GetReserveResponse : RNResponse
@property (nonatomic, copy) NSArray *reserves;
@end
