//
//  RNGetRestaurantRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"
#import "RNRestaurant.h"

@interface RNGetRestaurantResponse : RNResponse

@property (nonatomic, strong) RNRestaurant *restaurant;

@end
