//
//  AddLikeResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 24.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"

@interface AddLikeResponse : RNResponse
@property (nonatomic, copy) NSString *success;

@end
