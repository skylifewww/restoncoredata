//
//  UploadIAvatarResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 07.11.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "RNResponse.h"

@interface UploadIAvatarResponse : RNResponse

@property (nonatomic, copy) NSString *success;
@property (nonatomic, copy) NSString *pathAvatar;
@property (nonatomic, copy) NSString *token;

@end
