//
//  RNMakeReserveRequest.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"
@interface RNMakeReserveResponse : RNResponse

@property (nonatomic, copy) NSString *success;
@property (nonatomic, copy) NSString *error;
@end
