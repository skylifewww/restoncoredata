//
//  FacebookRegisterResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"

@interface FacebookRegisterResponse : RNResponse

@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *autorisation;

@end
