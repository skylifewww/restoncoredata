//
//  WriteUSResponse.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 16.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RNResponse.h"

@interface WriteUSResponse : RNResponse
@property (nonatomic, copy) NSString *contactus;

@end
