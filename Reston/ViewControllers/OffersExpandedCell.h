//
//  OffersExpandedCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersExpandedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromToLabel;
@property (weak, nonatomic) IBOutlet UIImageView *offersImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
