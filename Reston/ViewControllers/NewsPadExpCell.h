//
//  NewsPadExpCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsPadExpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *datelabel;
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descripLabel;

@end
