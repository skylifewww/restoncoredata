//
//  RestontableCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 20.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import "RestontableCell.h"
@import CoreLocation;
#import "CDRestaurant+CoreDataProperties.h"
#import "UIImage+Cache.h"
#import "RNUser.h"
#import "RNCountPeopleHelper.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

//NSString* const SubmitButtonNotification = @"SubmitButtonNotification";

@interface RestontableCell()<CLLocationManagerDelegate>{
    
}
@end

@implementation RestontableCell

@synthesize submitButton = _submitButton;
@synthesize restNamelable = _restNamelable;
@synthesize restType = _restType;
@synthesize restAddress = _restAddress;
@synthesize ratingImage = _ratingImage;
@synthesize commentsCount = _commentsCount;
@synthesize averageLabel = _averageLabel;
@synthesize distanceRest = _distanceRest;
@synthesize restImage = _restImage;
@synthesize discountBack = _discountBack;
@synthesize restDiscount = _restDiscount;
@synthesize callButton = _callButton;


- (void)applyRestaurantData:(CDRestaurant *)rest {
    NSString* type = [NSString stringWithFormat:@"%@    ",rest.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",rest.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:12];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:10];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0]}];
    
    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:10];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];
    
    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [self.restType setAttributedText:attributedString1];
    
    NSUInteger rating = (int)(rest.rating*100);
    self.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    
    
    if ([RNUser sharedInstance].isGeoEnabled && [RNUser sharedInstance].latitude != 0 && [RNUser sharedInstance].longitude != 0){
        self.distansIcon.hidden = NO;
        
        CLLocationCoordinate2D coord= CLLocationCoordinate2DMake(rest.lat, rest.lon);
        CGFloat distancRest = [self getDistanceToRest:coord];
        
        NSInteger numKM = (NSInteger)distancRest / 1000;
        
        NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
        
        NSString* strKM = @"";
        if (numKM != 0){
            strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
        }
        NSString* strMM = @"";
        if (numMM != 0){
            strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
        }
        NSString* distanceRest = @"";
        
        if (numKM < 100){
            distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
            
        } else {
            distanceRest = [NSString stringWithFormat:@"%@", strKM];
        }
        
        self.distanceRest.text = distanceRest;
        
    } else {
        self.distansIcon.hidden = YES;
        self.distanceRest.text = @"";
    }
    
    NSString* restName = [rest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    self.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
    
    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    self.commentsCount.text = [NSString stringWithFormat:@"%lld %@",rest.reviewCount, commStr];
    
    NSArray *images = (NSArray*)rest.images;
    
    if (images.count > 0){
        [UIImage cachedImage:images[0]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    self.restImage.image = image;
                }];
        
    } else {
        self.restImage.image = [UIImage imageNamed:@"restLogo"];
    }
    //
    //    [self.submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.callButton addTarget:self action:@selector(callButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (rest.avgBill == 0) {
        self.averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        self.averageLabel.text = [NSString stringWithFormat:@"%d %@",rest.avgBill, avgStr];
    }
    
    if (rest.discounts == 0) {
        self.discountBack.image = nil;
        self.restDiscount.hidden = YES;
    } else {
        self.restDiscount.hidden = NO;
        self.restDiscount.text = [NSString stringWithFormat:@"-%hd%%",rest.discounts];
        self.discountBack.image = [UIImage imageNamed:@"discont"];
    }
    
    //    if(currIndex == indexPath.row){
    //        self.callButton.hidden = NO;
    //    }
}

-(double) getDistanceToRest:(CLLocationCoordinate2D)restCoord{
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:[RNUser sharedInstance].latitude longitude:[RNUser sharedInstance].longitude];
    
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:restCoord.latitude longitude:restCoord.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
    
    return distance;
}

-(NSString*) getRating:(NSUInteger)rating{
    
    NSString* nameRating;
    
    if (rating == 0){
        nameRating = @"rating5_0";
        
    } else if (rating < 19){
        
        nameRating = @"rating5_1";
        
    } else if (rating >= 20 && rating < 40){
        
        nameRating = @"rating5_2";
        
    } else if (rating >= 40 && rating < 60){
        
        nameRating = @"rating5_3";
        
    } else if (rating >= 60 && rating < 80){
        
        nameRating = @"rating5_4";
        
    } else if (rating > 80){
        
        nameRating = @"rating5_5";
        
    } else {
        
        nameRating = @"rating5_0";
        
    }
    return nameRating;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    self.submitButton.layer.cornerRadius = 3;
    self.submitButton.layer.masksToBounds = true;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    NSString* titleButton = DPLocalizedString(@"reserv_action", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
//    self.containerView.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.descripView.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.addressView.translatesAutoresizingMaskIntoConstraints = false;
    //    self.mapIconImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.ratingView.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentView.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentsIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.avgIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.distansIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
//    self.restNamelable.translatesAutoresizingMaskIntoConstraints = false;
//    self.restType.translatesAutoresizingMaskIntoConstraints = false;
//    self.restAddress.translatesAutoresizingMaskIntoConstraints = false;
//    self.ratingImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentsCount.translatesAutoresizingMaskIntoConstraints = false;
//    self.averageLabel.translatesAutoresizingMaskIntoConstraints = false;
//    self.distanceRest.translatesAutoresizingMaskIntoConstraints = false;
//
//    self.restImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.restImage.contentMode = UIViewContentModeScaleAspectFill;
//
//    self.discountBack.translatesAutoresizingMaskIntoConstraints = false;
//    self.restDiscount.translatesAutoresizingMaskIntoConstraints = false;
//    self.callButton.translatesAutoresizingMaskIntoConstraints = false;
    
//    [self setupView];
}

-(void) setupView {
    
    CGFloat screenWidth =  [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    
    [self.containerView.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor].active = YES;
    [self.containerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
    [self.containerView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
    
    if (screenWidth < 322) {
        [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.964].active = YES;
    } else {
        
        [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    }
    
    if (screenWidth < 322) {
        [self.containerView.heightAnchor constraintEqualToConstant:289].active = YES;
    } else {
        
        [self.containerView.heightAnchor constraintEqualToConstant:299].active = YES;
    }
    
    if (screenWidth < 322) {
        [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.05].active = YES;
        
        [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.05].active = YES;
    } else {
        
        [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.005].active = YES;
        
        [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.005].active = YES;
    }
    
    [self.restImage.topAnchor constraintEqualToAnchor:self.containerView.topAnchor].active = YES;
    
    
    if (screenWidth < 322) {
        [self.restImage.heightAnchor  constraintEqualToConstant:156].active = YES;
    } else {
        
        [self.restImage.heightAnchor  constraintEqualToConstant:160].active = YES;
    }
    
    
    if (screenWidth < 322) {
        [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:12].active = YES;
    } else {
        
        [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:18].active = YES;
    }

    [self.callButton.centerYAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:116].active = YES;
    [self.callButton.widthAnchor  constraintEqualToConstant:56].active = YES;
    [self.callButton.heightAnchor  constraintEqualToConstant:56].active = YES;
    
    
    if (screenWidth < 322) {
        [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-2].active = YES;
    } else {
        
        [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-8].active = YES;
    }
    
    [self.discountBack.topAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:11].active = YES;
    [self.discountBack.widthAnchor  constraintEqualToConstant:60].active = YES;
    [self.discountBack.heightAnchor  constraintEqualToConstant:34].active = YES;
    
    [self.restDiscount.rightAnchor constraintEqualToAnchor:self.discountBack.rightAnchor constant:-4].active = YES;
    [self.restDiscount.centerYAnchor constraintEqualToAnchor:self.discountBack.centerYAnchor].active = YES;
    
    
    [self.descripView.centerXAnchor constraintEqualToAnchor:self.containerView.centerXAnchor].active = YES;
    [self.descripView.topAnchor constraintEqualToAnchor:self.restImage.bottomAnchor].active = YES;
    [self.descripView.bottomAnchor constraintEqualToAnchor:self.containerView.bottomAnchor].active = YES;
    [self.descripView.widthAnchor constraintEqualToAnchor:self.containerView.widthAnchor].active = YES;
    
    
    [self.restNamelable.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:9].active = YES;
    [self.restNamelable.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:5].active = YES;
    [self.restNamelable.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-10].active = YES;
    
    
    [self.addressView.centerXAnchor constraintEqualToAnchor:self.descripView.centerXAnchor].active = YES;
    [self.addressView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:38].active = YES;
    [self.addressView.widthAnchor constraintEqualToAnchor:self.descripView.widthAnchor].active = YES;
    
    
    [self.restType.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: 10].active = YES;
    [self.restType.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
    [self.restType.widthAnchor constraintEqualToAnchor:self.addressView.widthAnchor].active = YES;
    
    
    [self.ratingView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:10].active = YES;
    [self.ratingView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:64].active = YES;
    [self.ratingView.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingView.heightAnchor constraintEqualToConstant:24].active = YES;
    
    
    [self.ratingImage.centerXAnchor constraintEqualToAnchor:self.ratingView.centerXAnchor].active = YES;
    [self.ratingImage.centerYAnchor constraintEqualToAnchor:self.ratingView.centerYAnchor].active = YES;
    [self.ratingImage.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingImage.heightAnchor constraintEqualToConstant:19].active = YES;
    
    [self.commentView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.commentView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:91].active = YES;
    [self.commentView.widthAnchor constraintEqualToConstant: screenWidth * 0.5].active = YES;
    [self.commentView.heightAnchor constraintEqualToConstant:28].active = YES;
    
    [self.commentsIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 12].active = YES;
    [self.commentsIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.commentsIcon.widthAnchor constraintEqualToConstant:17].active = YES;
    [self.commentsIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    [self.commentsCount.leftAnchor constraintEqualToAnchor:self.commentsIcon.rightAnchor constant: 5].active = YES;
    [self.commentsCount.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.avgIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: screenWidth * 0.21].active = YES;
    [self.avgIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.avgIcon.widthAnchor constraintEqualToConstant:17].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    [self.averageLabel.leftAnchor constraintEqualToAnchor:self.avgIcon.rightAnchor constant: 5].active = YES;
    [self.averageLabel.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: screenWidth * 0.38].active = YES;
    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.distansIcon.widthAnchor constraintEqualToConstant:18].active = YES;
    [self.distansIcon.heightAnchor constraintEqualToConstant:16].active = YES;
    
    [self.distanceRest.leftAnchor constraintEqualToAnchor:self.distansIcon.rightAnchor].active = YES;
    [self.distanceRest.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];

    } else {

    }
    
    [self.submitButton.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor].active = YES;
    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:80].active = YES;
    [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.37].active = YES;
    [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
}


- (IBAction)submitButton:(id)sender {
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:SubmitButtonNotification object:nil];
    
}

//- (IBAction)callActionButton:(id)sender {
//}
-(void) setHighlighted:(BOOL)highlighted{
    
    //        self.callButton.hidden = NO;
    //        self.callButton.userInteractionEnabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    //    self.callButton.hidden = NO;
    //    self.callButton.userInteractionEnabled = YES;
}

@end

