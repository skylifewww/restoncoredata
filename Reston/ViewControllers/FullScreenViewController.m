//
//  FullScreenViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/18/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//
#import "define.h"
#import "FullScreenViewController.h"
#import "UIImage+Cache.h"
#import "UIViewController+Orientation.h"
#import "X4ImageViewer.h"

#import "Reachability.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"


//@interface UINavigationController (SupportOrientation)
//
//@end
//
//@implementation UINavigationController (SupportOrientation)
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
//        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
//    }
//    return UIInterfaceOrientationMaskAll;
//}
//
//- (BOOL)shouldAutorotate
//{
//    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
//    {
//        BOOL autoRotate = (BOOL)[self.topViewController
//                                 performSelector:@selector(shouldAutorotate)
//                                 withObject:nil];
//        return autoRotate;
//        
//    }
//    return NO;
//}
//
//@end


@interface FullScreenViewController ()<X4ImageViewerDelegate>{

     CGFloat screenWidth;
    Reachability *_reachability;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageGradient;
@property (nonatomic, assign) UIInterfaceOrientation interfaceOrientation;
@property (weak, nonatomic) IBOutlet X4ImageViewer *imageViewer;

@end

@implementation FullScreenViewController

- (void)dealloc
{
    _imageViewer.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];

    screenWidth = self.view.bounds.size.width;
    self.view.backgroundColor = [UIColor blackColor];
    
    _imageGradient.image = [self drawGradientWithFrame:CGRectMake(0, 0, screenWidth, 65)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _imageViewer.currentPageIndex = _selectIndex;
    _imageViewer.carouselType = CarouselTypePageNone;
    _imageViewer.contentMode = ContentModeAspectFit;
    _imageViewer.bZoomEnable = YES;
    _imageViewer.bZoomRestoreAfterDimissed = NO;
    _imageViewer.delegate = self;
    
    NSMutableArray *arrayData = NSMutableArray.new;

    for (int index = 0; index < _images.count; index++) {
      
        NSString*strURL = [NSString stringWithFormat:@"%@%@", serverImageLink, _images[index]];
         [arrayData addObject: [NSURL URLWithString:strURL]];
    }
    
    [_imageViewer setImages:arrayData withPlaceholder:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChangedFullScreen:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)orientationChangedFullScreen:(NSNotification *)notification
{
    
//        if (UIInterfaceOrientationIsLandscape(INTERFACE_ORIENTATION()))
//        {
//    
//            _imageViewer.contentMode = ContentModeAspectFill;
//        }
//        else
//        {
//    
//           _imageViewer.contentMode = ContentModeAspectFit;
//        }
    
}
                                         
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.interfaceOrientation = UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) prefersStatusBarHidden{
    return YES;
}

- (IBAction)backButtonTapped:(id)sender {
    
    _images = nil;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage*)drawGradientWithFrame: (CGRect)frame;
{
    UIImage*image;
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, [UIScreen mainScreen].scale);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Gradient Declarations
    CGFloat gradientLocations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace,(__bridge CFArrayRef)@[(id)[UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.85].CGColor, (id)UIColor.clearColor.CGColor],gradientLocations);
    //// Rectangle Drawing
    CGRect rectangleRect = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.00000 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.00000 + 0.5), floor(CGRectGetWidth(frame) * 1.00000 + 0.5) - floor(CGRectGetWidth(frame) * 0.00000 + 0.5), floor(CGRectGetHeight(frame) * 1.00000 + 0.5) - floor(CGRectGetHeight(frame) * 0.00000 + 0.5));
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: rectangleRect];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, gradient,
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMinY(rectangleRect)),
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMaxY(rectangleRect)),
                                0);
    CGContextRestoreGState(context);
    
    
    //// Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
