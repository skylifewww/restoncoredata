//
//  SlideEnterViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 4/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "SlideEnterViewController.h"
#import "RNLoginViewController.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNRestaurant.h"
#import "CatalogRestViewController.h"
#import "NewMenuViewController.h"
#import "SlideMenuController.h"
#import "RNUser.h"
#import "Reachability.h"
#import "AppDelegate.h"

#import "LocationManager.h"
#import <INTULocationManager/INTULocationManager.h>

static NSString *const kFirstLaunchKey = @"FirstLaunchKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
//        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
//    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
//    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
//    {
//        BOOL autoRotate = (BOOL)[self.topViewController
//                                 performSelector:@selector(shouldAutorotate)
//                                 withObject:nil];
//        return autoRotate;
//        
//    }
    return NO;
}
@end



@interface SlideEnterViewController(){
    UIColor* mainColor;
    BOOL loading;
    NSArray* restons;
    Boolean isAuth;
    Reachability *_reachability;
    UIStoryboard* storyboard;
    Boolean isPhone;
    NSString* cityName;
    CGFloat sizeFont;

}

@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;
@property (assign, nonatomic) NSTimeInterval timeout;
@property (assign, nonatomic) INTULocationRequestID locationRequestID;

@end

@implementation SlideEnterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.desiredAccuracy = INTULocationAccuracyCity;
    self.timeout = 10.0;
    self.locationRequestID = NSNotFound;
    
    CGFloat screenWidth = self.view.bounds.size.width;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        if (screenWidth < 322) {
            sizeFont = 14.0f;
        } else {
            sizeFont = 17.0f;
        }
        
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
        sizeFont = 26.0f;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    loading = NO;
    mainColor = [UIColor colorWithRed:0.26 green:0.5 blue:0.0 alpha:1.0];
    isAuth = NO;
    
    if (![[[NSUserDefaults standardUserDefaults] stringForKey:kFirstLaunchKey] isEqual: @"1"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:kFirstLaunchKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    [self setupView];
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait |
    UIInterfaceOrientationPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
     self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = NO; //bool created in first step
    NSNumber *value1 = [NSNumber numberWithInt:UIInterfaceOrientationMaskPortrait]; //change the orientation as per your requirement
    [[UIDevice currentDevice] setValue:value1 forKey:@"orientation"];
}

-(void)onBackButtonTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)startSingleLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:self.desiredAccuracy
                                                                timeout:self.timeout
                                                   delayUntilAuthorized:YES
                                                                  block:
                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                  __typeof(weakSelf) strongSelf = weakSelf;
                                  
                                  if (status == INTULocationStatusSuccess) {
                                      NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request successful!. Current address:\n%@", address);
                                          cityName = address;
                                      }];
                                  }
                                  else if (status == INTULocationStatusTimedOut) {
                                      
                                      NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request timed out. Current address:\n%@", address);
                                          cityName = address;
                                      }];
                                  }
                                  else {
                                      
                                      [RNUser sharedInstance].isGeoEnabled = NO;
                                      cityName = @"1";
                                  }
                                  
                                  strongSelf.locationRequestID = NSNotFound;
                              }];
}

- (void)sendSearchRequestWithCity:(NSString*)cityName {
    if (loading) {
        return;
    }
    loading = YES;
    
    if ([self isInternetConnect]){
        
        [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
        
        RNGetRestaurantsRequest *request = [[RNGetRestaurantsRequest alloc] init];

        request.cityName = cityName;
        request.name = @"";
        request.isAuth = NO;
        request.token = @"";
        
        isAuth = NO;
        
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
            
            loading = NO;

            if (error != nil || [response isKindOfClass:[NSNull class]] || response == nil) {
                //                NSLog(@"error !!!!!!!!!!!!!!!!%@",error);
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            }
            RNGetRestaurantsResponse *r = (RNGetRestaurantsResponse *)response;
            [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
            
            if ([r.success isEqualToString:@"YES"]){
                NSLog(@"r.success %@", r.success);
                [self startIPhone];
            } else {
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            }
        }];
    }
}

- (void)showAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Start

- (void)startIPhone {
    NSLog(@"startIPhone");
    
    CatalogRestViewController* enterVC = [storyboard instantiateViewControllerWithIdentifier:@"CatalogRestViewController"];
    enterVC.isAuth = NO;

    UINavigationController *navVC = [[storyboard instantiateViewControllerWithIdentifier:@"CatalogNavRestViewController"] initWithRootViewController:enterVC];
    
    NewMenuViewController * menuVC = [storyboard instantiateViewControllerWithIdentifier:@"NewMenuViewController"];

    menuVC.isAuth = NO;

    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:navVC leftMenuViewController:menuVC];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:slideMenuController];
}

- (IBAction)choiseButton:(id)sender {

    [self sendSearchRequestWithCity:[RNUser sharedInstance].currCity];

}

- (IBAction)enterButton:(id)sender {

    RNLoginViewController * loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"RNLoginViewController"];
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self presentViewController:navVC animated:YES completion:nil];
 
}
- (IBAction)registerButtonAction:(id)sender {
    
    UIViewController * registerViewController = [storyboard instantiateViewControllerWithIdentifier:@"RNRegisterViewController"];
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:registerViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (NSAttributedString*)setAtrString:(NSString*)string withColor:(UIColor*) color{
    
    
    
    UIFont *font = [UIFont fontWithName:@"Thonburi" size:sizeFont];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    NSDictionary *attributes = @{NSFontAttributeName:font,
                                 NSForegroundColorAttributeName:color,
                                 NSBackgroundColorAttributeName:[UIColor clearColor],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 };
    
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraphStyle setLineSpacing:8];
    [paragraphStyle setHeadIndent:1];
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:string  attributes:attributes]];
    
    return attString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    self.slideImage.image = [UIImage imageNamed:_imageName];
  
    
    self.slideImage.contentMode = UIViewContentModeScaleAspectFill;
    
    
    
    self.slideIcon.image = [UIImage imageNamed:_iconName];
    
    self.slideIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    self.slideLabel.text = _text;

    NSAttributedString* buttonText = [self setAtrString:DPLocalizedString(@"go_to_restaraunts", nil) withColor:[UIColor whiteColor]];

    [self.choiseTextButton setAttributedTitle:buttonText forState:UIControlStateNormal];
    
    self.choiseTextButton.layer.masksToBounds = true;

    [self.enterButton setTintColor:mainColor];
    [self.enterButton.titleLabel setTextColor:mainColor];
    [self.registerButton.titleLabel setTextColor:mainColor];
    [self.registerButton setTintColor:mainColor];
    
    self.pageControl.currentPage = _pageIndex;
   
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        
        self.slideImage.translatesAutoresizingMaskIntoConstraints = false;
        self.slideIcon.translatesAutoresizingMaskIntoConstraints = false;
        self.slideLabel.translatesAutoresizingMaskIntoConstraints = false;
        self.choiseTextButton.translatesAutoresizingMaskIntoConstraints = false;
        self.pageControl.translatesAutoresizingMaskIntoConstraints = false;
        self.enterButton.translatesAutoresizingMaskIntoConstraints = false;
        self.registerButton.translatesAutoresizingMaskIntoConstraints = false;
        self.divideView.translatesAutoresizingMaskIntoConstraints = false;
        
        [self.slideLabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
      
        [self.slideImage.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:-3].active = YES;
        [self.slideImage.topAnchor constraintEqualToAnchor:self.view.topAnchor
                                                  constant:screenHeight * 0.1].active = YES;
        [self.slideImage.widthAnchor constraintEqualToConstant:screenWidth * 0.57].active = YES;
        [self.slideImage.heightAnchor constraintEqualToConstant:screenHeight * 0.56].active = YES;
        
        [self.slideIcon.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-(screenWidth * 0.12)].active = YES;
        [self.slideIcon.centerYAnchor constraintEqualToAnchor:self.slideImage.centerYAnchor].active = YES;
        [self.slideIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.18].active = YES;
        [self.slideIcon.heightAnchor constraintEqualToConstant:screenHeight * 0.21].active = YES;
        
        
        if (screenWidth < 322) {
            [self.slideLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        }
        [self.slideLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.slideLabel.centerYAnchor constraintEqualToAnchor:self.slideImage.bottomAnchor constant:screenHeight * 0.05].active = YES;
        [self.slideLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
        [self.slideLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
        
        [self.pageControl.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.pageControl.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(screenHeight * 0.036)].active = YES;

        if (screenWidth < 322) {
            [self.enterButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        }
        [self.enterButton.rightAnchor constraintEqualToAnchor:self.pageControl.leftAnchor].active = YES;
        [self.enterButton.bottomAnchor constraintEqualToAnchor:self.pageControl.topAnchor constant:-(screenHeight * 0.01)].active = YES;
        [self.enterButton.widthAnchor constraintEqualToConstant:64].active = YES;
        [self.enterButton.heightAnchor constraintEqualToConstant:42].active = YES;
        
        [self.divideView.leftAnchor constraintEqualToAnchor:self.enterButton.rightAnchor constant:8].active = YES;
        [self.divideView.centerYAnchor constraintEqualToAnchor:self.enterButton.centerYAnchor].active = YES;
        [self.divideView.widthAnchor constraintEqualToConstant:2].active = YES;
        [self.divideView.heightAnchor constraintEqualToConstant:18].active = YES;
        

        if (screenWidth < 322) {
            [self.registerButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        }
        [self.registerButton.leftAnchor constraintEqualToAnchor:self.divideView.rightAnchor constant:8].active = YES;
        [self.registerButton.centerYAnchor constraintEqualToAnchor:self.enterButton.centerYAnchor].active = YES;
        [self.registerButton.widthAnchor constraintEqualToConstant:100].active = YES;
        [self.registerButton.heightAnchor constraintEqualToConstant:42].active = YES;
        
        
        self.choiseTextButton.layer.cornerRadius = 3;
        
        if (screenWidth < 322) {

        }
        [self.choiseTextButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.choiseTextButton.bottomAnchor constraintEqualToAnchor:self.enterButton.topAnchor constant:-(screenHeight * 0.01)].active = YES;
        [self.choiseTextButton.widthAnchor constraintEqualToConstant:275].active = YES;
        [self.choiseTextButton.heightAnchor constraintEqualToConstant:42].active = YES;
    
        
///////////////////////////////////////////////////////////////////////////////////////////
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){

     self.choiseTextButton.layer.cornerRadius = 5;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleDefault;
}


@end
