//
//  ReservTableViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "ReservTableViewController.h"
#import "RNCalendarViewController.h"
#import "RNDatePickerView.h"
#import "RNEntityPickerView.h"
#import "ModalPopViewController.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"
#import "GetReserveRequest.h"
#import "GetReserveResponse.h"
#import "OrdersViewController.h"
#import "TimeModel.h"
#import "TimePickerView.h"
#import "RNGetRestaurantRequest.h"
#import "RNGetRestaurantResponse.h"


static NSString *const kReservCountKey = @"ReservCountKey";


@interface NSDate (Compare)

-(BOOL) isLaterThanOrEqualTo:(NSDate*)date;
-(BOOL) isEarlierThanOrEqualTo:(NSDate*)date;
-(BOOL) isLaterThan:(NSDate*)date;
-(BOOL) isEarlierThan:(NSDate*)date;

@end

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface ReservTableViewController ()<RNCalendarViewControllerDelegate, TimePickerViewDelegate>{

    NSUInteger countPeople;
    UIColor* mainColor;
    NSInteger currDayNumber;
    double addressFontSize;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    Boolean isPhone;
    NSInteger reservsCount;
    NSMutableArray<TimeModel*>* arrayTimeModel;
    Boolean isDateDidChange;
    NSString* preFrom;
    NSString* preTo;
    NSString* currFrom;
    NSString* currTo;
    Boolean isAllTime;
    TimePickerView* timePicker;
    Boolean isReservedDay;
    NSMutableArray* arrNoReservedDays;
//    UILabel* noReservedLabel;
//    UIView* noReservedView;
    CGFloat screenWidth;
    CGFloat screenHeight;
}

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation ReservTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth =  [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        addressFontSize = 10;
        restNameFontSize = 14.0;
        textFontSize = 12.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
        isPhone = YES;
    
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        addressFontSize = 14;
        restNameFontSize = 20.0;
        textFontSize = 16.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
       
    }
    
    countPeople = 4;
    isAllTime = NO;
    isReservedDay = YES;
    
    [self configureFormatters];
    
    self.orderInfo = [[RNOrderInfo alloc] init];
    if (_restourant != nil) {
        _orderInfo.restaurant = self.restourant;
        _orderInfo.peopleCount = @4;
    }
    NSLog(@"self.restourant %@", self.restourant);
    
    arrNoReservedDays = [NSMutableArray array];
    
    for (NSDictionary *dicDays in _restourant.workTimes){
        if ([dicDays[@"ON"] isEqualToString:@"0"]){
            NSString* strDate = dicDays[@"DATE"];
            if (![strDate isEqualToString:@"0000-00-00"]){
                [arrNoReservedDays addObject:strDate];
            }
        }
    }
    
    [self checkReservedForData:[NSDate date]];

    self.title = DPLocalizedString(@"reserve_table", nil);
    
    isDateDidChange = NO;
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];

    [self setupView];

    [self loadReservs];
}

- (void) changeReservesCountDelegate {
    if ([[self delegate] respondsToSelector:@selector(changeReservesCountDelegate:)]) {
        [self.delegate changeReservesCountDelegate:self];
    }
}

-(void) setupOpenReservsButton:(NSInteger)reservsCount{
    
    if (reservsCount == 0){
        if (self.navigationItem.rightBarButtonItem != nil){
            self.navigationItem.rightBarButtonItem = nil;
        }

    } else if (reservsCount >= 1){
        if (self.navigationItem.rightBarButtonItem != nil){
            self.navigationItem.rightBarButtonItem = nil;
        }
        _openReservBarButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(openReservs)];
        UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:15];
        
        UIImage* i = [UIImage imageNamed:@"bookMark"];

        [_openReservBarButton setBackgroundImage:i forState:UIControlStateNormal  barMetrics:UIBarMetricsDefault];
        
        
        [_openReservBarButton setTitleTextAttributes:@{NSFontAttributeName : typeFont, NSForegroundColorAttributeName : mainColor} forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = _openReservBarButton;
        [_openReservBarButton setEnabled:YES];
        [_openReservBarButton setTitle:[NSString stringWithFormat:@"%ld",(long)reservsCount]];
    }
}

- (void)openReservs{
    
    UIStoryboard* storyboard = [[UIStoryboard alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
    }
    
    OrdersViewController * ordersViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrdersViewController"];
    ordersViewController.isMenu = NO;
    
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:ordersViewController];
    
    [self.navigationController presentViewController:navVC animated:NO completion:nil];

}

- (void)loadReservs{
    GetReserveRequest *request = [[GetReserveRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        }
        GetReserveResponse *r = (GetReserveResponse *)response;
        
        reservsCount = r.reserves.count;
        
        [[NSUserDefaults standardUserDefaults] setInteger:reservsCount forKey:kReservCountKey];

        [[NSUserDefaults standardUserDefaults] synchronize];

        [self setupOpenReservsButton:reservsCount];

    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reservCountChangeNotification:)
                                                 name:ReservCountChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reservCountChangeNotification:)
                                                 name:OrdersCountChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dateChangeNotification:)
                                                 name:DateChangeNotification
                                               object:nil];
}

-(void) dateChangeNotification:(NSNotification*) notif{

    NSNumber* isNextNumber = [notif.userInfo objectForKey:@"dataChange"];
    Boolean isNext = isNextNumber.boolValue;
    
    
    if (isNext == YES) {
        
        if (isDateDidChange == NO){
        _orderInfo.date = [_orderInfo.date dateByAddingTimeInterval:60*60*24*1];

        isDateDidChange = YES;
            
        }
        
        NSString* strDay = [self getCurrentDay:_orderInfo.date];
        
        self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:_orderInfo.date]];
        
        
    } else {
        
        if (isDateDidChange == YES){
             _orderInfo.date = [_orderInfo.date dateByAddingTimeInterval:60*60*24*-1];

            isDateDidChange = NO;
        }
        
        NSString* strDay = [self getCurrentDay:_orderInfo.date];
        
        self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:_orderInfo.date]];
    }

}

-(void) reservCountChangeNotification:(NSNotification*) notif{

    NSNumber* reservCountNumber = [notif.userInfo objectForKey:@"reservCount"];
    NSInteger reservsCount = reservCountNumber.integerValue;

    [self setupOpenReservsButton:reservsCount];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)configureFormatters {
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"dd MMMM yyyy"];
        _dateFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
        _orderInfo.date = [NSDate date];
        
        self.timeFormatter = [[NSDateFormatter alloc] init];
        [_timeFormatter setDateFormat:@"HH:mm"];
        _timeFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
        _orderInfo.time = [self roundDateTo5Minutes:[[NSDate date] dateByAddingTimeInterval:1*60*60]];
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"dd MMMM yyyy"];
        _dateFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
        _orderInfo.date = [NSDate date];
        
        self.timeFormatter = [[NSDateFormatter alloc] init];
        [_timeFormatter setDateFormat:@"HH:mm"];
        _timeFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
        _orderInfo.time = [self roundDateTo5Minutes:[[NSDate date] dateByAddingTimeInterval:1*60*60]];
    }
}

- (void)showDatePicker {
    if ([self.view viewWithTag:0xDEADBEEF]) {
        return;
    }
    RNCalendarViewController *vc = [[RNCalendarViewController alloc] init];
    vc.selectedDate = _orderInfo.date;
    vc.delegate = self;
    vc.width = self.view.frame.size.width - 40;
    vc.center = self.view.center;
    [self presentViewController:vc
                       animated:YES
                     completion:NULL];
}

-(NSString*) helperTimeTo:(NSString*) strTimeTo{
    
    strTimeTo = [strTimeTo stringByReplacingOccurrencesOfString:@":00:00" withString:@":00"];

    if ([strTimeTo isEqualToString:@"24"]){
        strTimeTo = @"00";
        
    }
    if ([strTimeTo isEqualToString:@"25"]){
        strTimeTo = @"01";
        
    }
    if ([strTimeTo isEqualToString:@"26"]){
        strTimeTo = @"02";
    }
    if ([strTimeTo isEqualToString:@"27"]){
        strTimeTo = @"03";
    }
    if ([strTimeTo isEqualToString:@"28"]){
        strTimeTo = @"04";
    }
    if ([strTimeTo isEqualToString:@"29"]){
        strTimeTo = @"05";
    }
    if ([strTimeTo isEqualToString:@"30"]){
        strTimeTo = @"06";
    }
    
    return strTimeTo;
}

-(NSMutableArray<TimeModel*>*) getArrayTimeModelsForDateFrom:(NSString*)dateFrom andDateTo:(NSString*)dateTo isNow:(Boolean)isNow{

    NSDictionary* currDict = self.restourant.workTimes[currDayNumber];

    NSMutableArray* arrWorkTimes = [NSMutableArray new];
    if (isNow == YES){

        NSInteger currDayNumberUpdate = currDayNumber;
        if (currDayNumberUpdate == 0){
            
            currDayNumberUpdate = 7;
            
        }
    NSDictionary* preCurrDict = self.restourant.workTimes[currDayNumberUpdate - 1];
 
    NSString* preTimeTo = preCurrDict[@"TIME_TO"];
        
    preTimeTo = [preTimeTo stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
    NSString* preTimeToInt = [preTimeTo stringByReplacingOccurrencesOfString:@":" withString:@""];
    
         if (preTimeToInt.integerValue > 24){
        
        NSInteger preCount = preTimeToInt.integerValue - 25;

        for (int i = 0; i < preCount; i++) {
            TimeModel* timeModel = [TimeModel new];
            if (i == preCount - 1){

                timeModel.isNowMorning = YES;
            } else {
                timeModel.isNowMorning = NO;
            }
            timeModel.hours = [NSString stringWithFormat:@"%d", 24 + i];
            timeModel.hours = [self helperTimeTo:timeModel.hours];
//            NSLog(@"timeModel.hours %@", timeModel.hours);
//            NSLog(@"timeModel.isNowMorning %hhu", timeModel.isNowMorning);
            [arrWorkTimes addObject:timeModel];
        }
      }
        
    }
    
    preFrom = @"";
    preTo = @"";
    if (arrWorkTimes.count > 0){
        TimeModel* timeModelFirst = arrWorkTimes.firstObject;
        preFrom = timeModelFirst.hours;
        TimeModel* timeModelLast = arrWorkTimes.lastObject;
        preTo = timeModelLast.hours;
    }
    
    NSString* currWorkTimeFrom = currDict[@"TIME_FROM"];
//    NSLog(@"currWorkTimeFrom1 = %@", currWorkTimeFrom);
//    currWorkTimeFrom = [currWorkTimeFrom stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
    currWorkTimeFrom = [currWorkTimeFrom substringToIndex:NSMaxRange([currWorkTimeFrom rangeOfComposedCharacterSequenceAtIndex:1])];
//    NSLog(@"currWorkTimeFrom2 = %@", currWorkTimeFrom);
    NSString* currWorkTimeFromInt = [currWorkTimeFrom stringByReplacingOccurrencesOfString:@":" withString:@""];
//    NSLog(@"currWorkTimeFromInt = %@", currWorkTimeFromInt);
    
    NSString* currWorkTimeTo = currDict[@"TIME_TO"];
    currWorkTimeTo = [currWorkTimeTo substringToIndex:NSMaxRange([currWorkTimeFrom rangeOfComposedCharacterSequenceAtIndex:1])];
//    NSLog(@"currWorkTimeTo = %@", currWorkTimeTo);
//    currWorkTimeTo = [currWorkTimeTo stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
    NSString* currWorkTimeToInt = [currWorkTimeTo stringByReplacingOccurrencesOfString:@":" withString:@""];

    NSInteger currCount = currWorkTimeToInt.integerValue - currWorkTimeFromInt.integerValue - 1;
    
//    NSLog(@"arrWorkTimes = %@", arrWorkTimes);
    
    currFrom = @"";
    currTo = @"";
    
    
    
    if (currWorkTimeToInt.integerValue == 24 && currWorkTimeFromInt.integerValue == 0){
//        NSLog(@"currWorkTimeToInt.integerValue == %ld && currWorkTimeFromInt.integerValue == %ld", (long)currWorkTimeToInt.integerValue, (long)currWorkTimeFromInt.integerValue);
        currCount = 24;
        isAllTime = YES;
        
    }
    
    for (int i = 0; i < currCount; i++) {
        TimeModel* timeModel = [TimeModel new];
        timeModel.hours = [NSString stringWithFormat:@"%ld", (long)(currWorkTimeFromInt.integerValue + i)];
        timeModel.isNowMorning = NO;
        
        if (i == 0){
            currFrom = [self helperTimeTo:timeModel.hours];
        }
        
        if (timeModel.hours.integerValue >= 24){
            timeModel.isNextDay = YES;
        } else {
            timeModel.isNextDay = NO;
            if (i == currCount - 1){
                currTo = [self helperTimeTo:timeModel.hours];
            }
        }
        if (!isNow){
            
            timeModel.hours = [self helperTimeTo:timeModel.hours];
            
        }
        [arrWorkTimes addObject:timeModel];
    }
   
    arrayTimeModel = [NSMutableArray new];
    
    
    if (isNow){

        NSString* strTimeFrom = dateFrom;
//        NSLog(@"strTimeFrom = %@", strTimeFrom);
//        strTimeFrom = [strTimeFrom stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
        strTimeFrom = [strTimeFrom substringToIndex:NSMaxRange([strTimeFrom rangeOfComposedCharacterSequenceAtIndex:1])];
//        NSLog(@"strTimeFrom1 = %@", strTimeFrom);
        NSString* strTimeFromInt = [strTimeFrom stringByReplacingOccurrencesOfString:@":" withString:@""];
//        NSLog(@"strTimeFromInt = %@", strTimeFromInt);
        for (TimeModel* tm in arrWorkTimes) {
            TimeModel* timeModel = (TimeModel*)tm;
//             NSLog(@"timeModel.hours.integerValue %ld strTimeFromInt.integerValue %ld", timeModel.hours.integerValue, strTimeFromInt.integerValue);
            if (timeModel.hours.integerValue >= strTimeFromInt.integerValue){
                timeModel.hours = [self helperTimeTo:timeModel.hours];
//                 NSLog(@"timeModel.hours currentDict %@", timeModel.hours);
                
                [arrayTimeModel addObject:timeModel];
            }
        }
    } else {
        arrayTimeModel = [NSMutableArray arrayWithArray:arrWorkTimes];
    }

    return arrayTimeModel;
    
}

-(NSString*) setupFirsCurrentTimeForNow{

    NSDictionary* currDict = _restourant.workTimes[currDayNumber];
    
    NSDate* minTime;
    
    NSString* strDateNow = [_timeFormatter stringFromDate:[NSDate date]];
  
    minTime = [self roundDateTo5Minutes:[[_timeFormatter dateFromString:strDateNow] dateByAddingTimeInterval:0.5*60*60]];
    strDateNow = [_timeFormatter stringFromDate:minTime];
//    NSLog(@"strDateNow111 %@", strDateNow);
    NSString* strDateNowHours = [strDateNow substringToIndex:2];
//    strDateNowHours = @"23";
    
    NSString* currentTime;
    NSString* currentMinutes;

    NSMutableArray<TimeModel*>* arrayTimes = [self getArrayTimeModelsForDateFrom:strDateNowHours andDateTo:currDict[@"TIME_TO"] isNow:YES];
    if (arrayTimes.count == 0){
        _orderInfo.date = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
        NSString* strDay = [self getCurrentDay:_orderInfo.date];
        self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:_orderInfo.date]];
        currDayNumber = currDayNumber + 1;
        if (currDayNumber == 6){
            currDayNumber = 0;
        }
        currDict = _restourant.workTimes[currDayNumber];
        
        arrayTimes = [self getArrayTimeModelsForDateFrom:currDict[@"TIME_FROM"] andDateTo:currDict[@"TIME_TO"] isNow:NO];
        
        currentTime = arrayTimes.firstObject.hours;
        NSArray* arrCurrentMinutes = [self setupArrMimutesFirst:[currentTime substringFromIndex:[currentTime length] - 2]];
        currentMinutes = arrCurrentMinutes.firstObject;
        
    } else {
        
        currentTime = arrayTimes.firstObject.hours;
        NSArray* arrCurrentMinutes = [self setupArrMimutesFirst:[strDateNow substringFromIndex:[strDateNow length] - 2]];
        currentMinutes = arrCurrentMinutes.firstObject;
    }

    NSString* firstCurrentTime = [NSString stringWithFormat:@"%@:%@", currentTime, currentMinutes];
    
    return firstCurrentTime;
}


- (void)showTimePicker {
    if ([self.view viewWithTag:0xDEADBEEF]) {
        return;
    }
    timePicker = [TimePickerView viewWithDelegate:self];
    
    NSDictionary* currDict = _restourant.workTimes[currDayNumber];

    NSDate* minTime;
    
    if ([[_dateFormatter stringFromDate:_orderInfo.date] isEqualToString:[_dateFormatter stringFromDate:[NSDate date]]]){
        
//        NSLog(@"_orderInfo.date %@", [_dateFormatter stringFromDate:_orderInfo.date]);
        
        NSString* strDateNow = [_timeFormatter stringFromDate:[NSDate date]];
//        NSLog(@"strDateNow %@", strDateNow);
        minTime = [self roundDateTo5Minutes:[[_timeFormatter dateFromString:strDateNow] dateByAddingTimeInterval:0.5*60*60]];
        strDateNow = [_timeFormatter stringFromDate:minTime];
        
        NSString* strDateNowHours = [strDateNow substringToIndex:2];
        NSString* strDateNowMinutes = [strDateNow substringFromIndex:[strDateNow length] - 2];
        
//        NSLog(@"strDateNowHours %@", strDateNowHours);
//        strDateNowHours = @"23";
//        NSLog(@"strDateNowHours.integerValue %ld", (long)strDateNowHours.integerValue);
//        NSDate* roundDate = [self roundDateTo5Minutes:[NSDate date]];
        
//        NSLog(@"currDict[@TIME_TO] %@", currDict[@"TIME_TO"]);
        
        arrayTimeModel = [self getArrayTimeModelsForDateFrom:strDateNowHours andDateTo:currDict[@"TIME_TO"] isNow:YES];
//        self.timeLabel.text = [_timeFormatter stringFromDate:[roundDate dateByAddingTimeInterval:0.5*60*60]];
//        NSLog(@"arrayTimeModel.firstObject.hours %@", arrayTimeModel.firstObject.hours);
        timePicker.selectedHour = arrayTimeModel.firstObject.hours;
//        picker.minMinutes = [strDateNow substringFromIndex:[strDateNow length] - 2];
//        NSLog(@"selectedMimutes %@", [strDateNow substringFromIndex:[strDateNow length] - 2]);
        
        
        if (preTo.length != 0 && preFrom.length != 0 && strDateNowHours.integerValue >= preFrom.integerValue && strDateNowHours.integerValue < preTo.integerValue){
            
//            NSLog(@"strDateNowHours.integerValue >= preFrom.integerValue");
            timePicker.arrFirstMinutes = [self setupArrMimutesFirst:strDateNowMinutes];
//            NSLog(@"picker.arrFirstMinutes %@", picker.arrFirstMinutes);
            
        } else if (preTo.length != 0 && preFrom.length != 0 && strDateNowHours.integerValue == preTo.integerValue){
            
//            NSLog(@"strDateNowHours.integerValue == preTo.integerValue");
            timePicker.arrFirstMinutes = [self setupArrMimutesLast:strDateNowMinutes];
//            NSLog(@"setupArrMimutesLast %@", picker.arrFirstMinutes);
            
        } else if (currFrom.length != 0 && strDateNowHours.integerValue >= currFrom.integerValue){
            
//            NSLog(@"strDateNowHours.integerValue >= currFrom.integerValue");
            timePicker.arrFirstMinutes = [self setupArrMimutesFirst:strDateNowMinutes];
//            NSLog(@"picker.arrFirstMinutes %@", picker.arrFirstMinutes);
            
        } else if (currFrom.length != 0 && currTo.length != 0 && strDateNowHours.integerValue == currTo.integerValue){
            
//            NSLog(@"strDateNowHours.integerValue == currTo.integerValue");
            timePicker.arrFirstMinutes = [self setupArrMimutesLast:strDateNowMinutes];
//            NSLog(@"picker.arrFirstMinutes %@", picker.arrFirstMinutes);
            
        } else {
            
            timePicker.arrFirstMinutes = @[@"00", @"15", @"30", @"45"];
//             NSLog(@"picker.arrFirstMinutes Full");
            
        }
   
//        NSLog(@"picker.arrFirstMinutes %@", picker.arrFirstMinutes);
        timePicker.isThisDay = YES;
        
    } else {
        
//        NSDictionary* currDict = _restourant.workTimes[currDayNumber];
        arrayTimeModel = [self getArrayTimeModelsForDateFrom:currDict[@"TIME_FROM"] andDateTo:currDict[@"TIME_TO"] isNow:NO];
        
//        NSLog(@"isEqualToDate == NO");
//        minTime = [_timeFormatter dateFromString:strTimeFrom];
//        NSLog(@"arrayTimeModel.firstObject.hours %@", arrayTimeModel.firstObject.hours);
        timePicker.selectedHour = arrayTimeModel.firstObject.hours;
        NSString* firstTime = [currDict[@"TIME_FROM"] substringToIndex:NSMaxRange([currDict[@"TIME_FROM"] rangeOfComposedCharacterSequenceAtIndex:4])];
        timePicker.arrFirstMinutes = [self setupArrMimutesFirst:[firstTime substringFromIndex:[firstTime length] - 2]];
//        NSLog(@"minutes %@", [firstTime substringFromIndex:[firstTime length] - 2]);
//        NSLog(@"picker.arrFirstMinutes %@", picker.arrFirstMinutes);
//        picker.arrFirstMinutes = [self setupArrMimutesLast:strDateNowMinutes];
//        picker.minMinutes = @"00";
        timePicker.isThisDay = NO;
        
    }
    
//    NSDate *nextDate = [[NSDate date] dateByAddingTimeInterval:60*60*24*1];
    timePicker.isAllTimes = isAllTime;
    timePicker.timeModel = arrayTimeModel;
    
    timePicker.tag = 0xDEADBEEF;
    timePicker.identifier = 2;
    if (arrayTimeModel.count > 0){
        [timePicker showInView:self.view];
    } else {
        [self showAlertWithTitle:DPLocalizedString(@"rest_don't_work", nil) andMessage:DPLocalizedString(@"unable_reserved", nil)];
    }
}

-(NSArray*) setupArrMimutesLast:(NSString*) minutes{
    
    //    NSLog(@"minutes %@", minutes);
    NSArray* minutesArr = [NSArray new];
   
    
    if (isAllTime == YES){
        minutesArr = @[@"00", @"15", @"30", @"45"];
    } else {
        if ([minutes isEqualToString:@"00"]){
            minutesArr = @[@"00", @"15", @"30"];
        } else if ([minutes isEqualToString:@"15"]){
            minutesArr = @[@"15", @"30"];
        } else if ([minutes isEqualToString:@"30"]){
            minutesArr = @[@"30"];
            
        }
    }
    //    NSLog(@"minutesArr %@", minutesArr);
    return minutesArr;
}

-(NSArray*) setupArrMimutesFirst:(NSString*) minutes{
    
//    NSLog(@"minutes %@", minutes);
    NSArray* minutesArr = [NSArray new];
    if ([minutes isEqualToString:@"00"]){
        minutesArr = @[@"00", @"15", @"30", @"45"];
    } else if ([minutes isEqualToString:@"15"]){
        minutesArr = @[@"15", @"30", @"45"];
    } else if ([minutes isEqualToString:@"30"]){
        minutesArr = @[@"30", @"45"];
    } else if ([minutes isEqualToString:@"45"]){
        minutesArr = @[@"45"];
    }
//    NSLog(@"minutesArr %@", minutesArr);
    return minutesArr;
}


-(NSString*) getCurrentDay:(NSDate*)date{
    NSTimeInterval utcoffset = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSTimeInterval interval = ([date timeIntervalSinceReferenceDate]+utcoffset)/(60.0*60.0*24.0);
    //mod 7 the number of days to identify day index
    long dayix=((long)interval+8) % 7;
    
    if (dayix == 0){
        
        currDayNumber = 6;
        
    } else {
        currDayNumber = dayix - 1;
    }
    NSDictionary *dic = _restourant.workTimes[currDayNumber];
    
    NSString* strDay = [self getDay:[dic[@"DAY_OF_WEEK"] integerValue]];
    return strDay;
}

-(void) checkReservedForData:(NSDate*)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    
    isReservedDay = YES;
    if ([arrNoReservedDays containsObject:dateStr]){
        isReservedDay = NO;
    }

    [self setupNoReservedView];
}

#pragma mark - RNCalendarViewControllerDelegate

- (void)calendarPicker:(RNCalendarViewController *)picker didChooseDate:(NSDate *)date {
    _orderInfo.date = date;
    
    if ([date isEqualToDate:[NSDate date]]){
        
//        NSLog(@"isNow = YES");
        
    } else {
//        NSLog(@"isNow = NO");
    }

    NSString* strDay = [self getCurrentDay:_orderInfo.date];

    self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:_orderInfo.date]];
    
    NSLog(@"self.dateLabel.text %@", self.dateLabel.text);
    
    NSLog(@"_dateFormatter stringFromDate:_orderInfo.date %@", [_dateFormatter stringFromDate:date]);
    
    [self checkReservedForData:_orderInfo.date];

    [picker dismissViewControllerAnimated:YES
                               completion:NULL];
}
- (void)calendarPicker:(RNCalendarViewController *)picker didCancelWithDate:(NSDate *)date {
    [picker dismissViewControllerAnimated:YES
                               completion:NULL];
}

#pragma mark - RNDatePickerViewDelegate

- (void)datePicker:(TimePickerView *)picker didCancelWithDate:(NSString *)date {
    
//    NSLog(@"(NSString *)date %@", date);
    [picker dismissFromView:self.view
                 completion:NULL];
}

- (void)datePicker:(TimePickerView *)picker didChooseDate:(NSString *)date {
//    NSLog(@"(NSString *)date %@", date);
    [picker dismissFromView:self.view
                 completion:NULL];
    _orderInfo.timeString = date;
    self.timeLabel.text = date;
    
   
}

- (BOOL)isInThePast:(NSDate *)time {
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:900];
    NSString *destDate = [_dateFormatter stringFromDate:_orderInfo.date];
    NSString *destTime = [_timeFormatter stringFromDate:time];
    NSString *dateTime = [NSString stringWithFormat:@"%@ %@",destDate,destTime];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    formatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
    NSDate *compareTime = [formatter dateFromString:dateTime];
    
    return [[now laterDate:compareTime] isEqualToDate:now];
}

- (IBAction)backButton:(id)sender {
    if (timePicker){
        [timePicker removeFromSuperview];
//        [timePicker dismissFromView:self.view
//                         completion:NULL];
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)dateChoiseButton:(id)sender {
    [self showDatePicker];
}
- (IBAction)timeChoiseButton:(id)sender {
    [self showTimePicker];
}
- (IBAction)minusButton:(id)sender {
    
    NSInteger maxPeople = _restourant.personsLimits.integerValue;
    if (maxPeople == 0){
        maxPeople = 16;
    }
    
    if (countPeople <= 1) {

    } else if (countPeople > 1){
        
        countPeople = countPeople - 1;
        _orderInfo.peopleCount = @(countPeople);
        self.personsCountLabel.text = [self helperPersonsWithCount:countPeople];
        
        if (countPeople < maxPeople) {
            [_plusButton setTintColor:mainColor];
        }
        if (countPeople <= 1) {
            [_minusButton setTintColor:[UIColor lightGrayColor]];
        } else {
            [_minusButton setTintColor:mainColor];
        }
    }
   
}
- (IBAction)plusButton:(id)sender {
    
    NSLog(@"_restourant.personsLimits %ld", (long)_restourant.personsLimits.integerValue);
    
    NSInteger maxPeople = _restourant.personsLimits.integerValue;
    if (maxPeople == 0){
        maxPeople = 16;
    }
    
    if (countPeople < maxPeople){
        countPeople = countPeople + 1;
        _orderInfo.peopleCount = @(countPeople);
        self.personsCountLabel.text = [self helperPersonsWithCount:countPeople];
        
        if (countPeople < maxPeople) {
            [_plusButton setTintColor:mainColor];
        } else if (countPeople == maxPeople) {
            [_plusButton setTintColor:[UIColor lightGrayColor]];
        }
        
        if (countPeople > 1) {
            [_minusButton setTintColor:mainColor];
        }
    }
}


-(NSString*) helperPersonsWithCount:(NSUInteger)cP{
    
    switch (cP) {
        case 1:
            return [NSString stringWithFormat:@"Для 1 %@", DPLocalizedString(@"person", nil)];
  
        case 21:
            return [NSString stringWithFormat:@"Для 21 %@", DPLocalizedString(@"person", nil)];
            
        case 31:
            return [NSString stringWithFormat:@"Для 31 %@", DPLocalizedString(@"person", nil)];
            
        default:
            return [NSString stringWithFormat:@"Для %lu %@",(unsigned long)cP, DPLocalizedString(@"persons", nil)];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"modalReserv"]) {

        ModalPopViewController *destViewController = segue.destinationViewController;
        destViewController.orderInfo = _orderInfo;
    }
}

-(void) setupNoReservedView{
    
    if (isReservedDay == YES){

        NSString* strDay = [self getCurrentDay:_orderInfo.date];
        self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:_orderInfo.date]];
        
        self.timeChoiseButton.hidden = NO;
        self.timeChoiseButton.enabled = YES;

        self.timeAction.enabled = YES;
        self.timeLabel.hidden = NO;
        self.timeArrow.hidden = NO;
        self.timeLabel.text = _orderInfo.timeString;
        self.continueButton.enabled = YES;

        [_noReservedLabel setText:@""];

    } else if (isReservedDay == NO){
        
        NSLog(@"isReservedDay %hhu", isReservedDay);
        
        self.timeChoiseButton.hidden = YES;
        self.timeChoiseButton.enabled = NO;

        self.timeAction.enabled = NO;
        self.timeLabel.hidden = YES;
        self.timeArrow.hidden = YES;

        [_noReservedLabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];

        _noReservedLabel.text = @"Заведение на этот день уже\n не принимает резервов,\n выберите другую дату!";

        self.continueButton.enabled = NO;
    }
}

-(void) setupView{
    
    self.continueButton.layer.cornerRadius = cornerRadius;
    self.continueButton.layer.masksToBounds = true;
    
    
    self.bookMark.translatesAutoresizingMaskIntoConstraints = false;
    self.backView.translatesAutoresizingMaskIntoConstraints = false;
    self.tailInage.translatesAutoresizingMaskIntoConstraints = false;
    self.restNameLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.restAddressLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.dateChoisLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.dateChoiseButton.translatesAutoresizingMaskIntoConstraints = false;
    self.dateView.translatesAutoresizingMaskIntoConstraints = false;
    self.dateLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.dateArrow.translatesAutoresizingMaskIntoConstraints = false;
    self.timeChoiselabel.translatesAutoresizingMaskIntoConstraints = false;
    self.timeChoiseButton.translatesAutoresizingMaskIntoConstraints = false;
    self.timeView.translatesAutoresizingMaskIntoConstraints = false;
    self.personsChioseLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.personsIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.personsView.translatesAutoresizingMaskIntoConstraints = false;
    self.continueButton.translatesAutoresizingMaskIntoConstraints = false;
    self.timeArrow.translatesAutoresizingMaskIntoConstraints = false;
    self.timeLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.personsCountLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.minusButton.translatesAutoresizingMaskIntoConstraints = false;
    self.plusButton.translatesAutoresizingMaskIntoConstraints = false;
    self.dataAction.translatesAutoresizingMaskIntoConstraints = false;
    self.timeAction.translatesAutoresizingMaskIntoConstraints = false;
    self.noReservedLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    
    [self.backView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.backView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.04 + 65].active = YES;
    [self.backView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-screenHeight * 0.06].active = YES;
    [self.backView.widthAnchor constraintEqualToConstant:screenWidth * 0.66].active = YES;
    
    [self.tailInage.leftAnchor constraintEqualToAnchor:self.backView.leftAnchor].active = YES;
    [self.tailInage.rightAnchor constraintEqualToAnchor:self.backView.rightAnchor].active = YES;
    [self.tailInage.bottomAnchor constraintEqualToAnchor:self.backView.bottomAnchor constant:25].active = YES;
    
    
    
    NSString* restName = [self.restourant.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
    }
    [self.restNameLabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    self.restNameLabel.text = [NSString stringWithFormat:@"\"%@\"",restName];
    if (screenWidth < 322) {
        [self.restNameLabel setFont:[UIFont fontWithName:@"Thonburi" size:12.0]];
    }
    
    [self.restNameLabel.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    
    if (screenWidth < 322) {
        [self.restNameLabel.topAnchor constraintEqualToAnchor:self.backView.topAnchor constant:screenHeight * 0.006].active = YES;
    } else {
        
        [self.restNameLabel.topAnchor constraintEqualToAnchor:self.backView.topAnchor constant:screenHeight * 0.01].active = YES;
    }
    
    [self.restNameLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.49].active = YES;
    
    [self.restAddressLabel setFont:[UIFont fontWithName:@"Thonburi" size:addressFontSize]];
    NSString *addresssStr = [self.restourant.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                                               withString:@""];
    self.restAddressLabel.text = addresssStr;
    [self.restAddressLabel.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.restAddressLabel.topAnchor constraintEqualToAnchor:self.restNameLabel.bottomAnchor].active = YES;
    [self.restAddressLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.6].active = YES;
    
    [self.dateChoisLabel setFont:[UIFont fontWithName:@"Thonburi" size:textFontSize]];
    [self.dateChoisLabel.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.dateChoisLabel.topAnchor constraintEqualToAnchor:self.restAddressLabel.bottomAnchor constant:screenHeight * 0.03].active = YES;
    
    [self.dateChoiseButton.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.dateChoiseButton.topAnchor constraintEqualToAnchor:self.dateChoisLabel.bottomAnchor constant:screenHeight * 0.03].active = YES;
    [self.dateChoiseButton.widthAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    [self.dateChoiseButton.heightAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    
    [self.dateView.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.dateView.topAnchor constraintEqualToAnchor:self.dateChoiseButton.bottomAnchor].active = YES;
    [self.dateView.widthAnchor constraintEqualToAnchor:self.backView.widthAnchor].active = YES;
    [self.dateView.heightAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    
    [self.dateLabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    
    NSString* strDay = [self getCurrentDay:[NSDate date]];
    self.dateLabel.text = [NSString stringWithFormat:@"%@, %@", strDay, [_dateFormatter stringFromDate:[NSDate date]]];
    [self.dateLabel.centerXAnchor constraintEqualToAnchor:self.dateView.centerXAnchor].active = YES;
    [self.dateLabel.centerYAnchor constraintEqualToAnchor:self.dateView.centerYAnchor].active = YES;
    
    [self.dateArrow.leftAnchor constraintEqualToAnchor:self.dateLabel.rightAnchor constant:4].active = YES;
    [self.dateArrow.centerYAnchor constraintEqualToAnchor:self.dateLabel.centerYAnchor].active = YES;
    [self.dateArrow.widthAnchor constraintEqualToConstant:9].active = YES;
    [self.dateArrow.heightAnchor constraintEqualToConstant:9].active = YES;
    
    [self.dataAction.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.dataAction.topAnchor constraintEqualToAnchor:self.dateChoisLabel.bottomAnchor].active = YES;
    [self.dataAction.widthAnchor constraintEqualToConstant:screenWidth * 0.6].active = YES;
    [self.dataAction.bottomAnchor constraintEqualToAnchor:self.dateLabel.bottomAnchor].active = YES;
    
    
    
    
    [self.timeChoiselabel setFont:[UIFont fontWithName:@"Thonburi" size:textFontSize]];
    [self.timeChoiselabel.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.timeChoiselabel.topAnchor constraintEqualToAnchor:self.dateView.bottomAnchor constant:screenHeight * 0.03].active = YES;
    
    [self.noReservedLabel.topAnchor constraintEqualToAnchor:self.timeChoiselabel.bottomAnchor constant:screenHeight * 0.02].active = YES;
    [self.noReservedLabel.leadingAnchor constraintEqualToAnchor:self.backView.leadingAnchor].active = YES;
    [self.noReservedLabel.trailingAnchor constraintEqualToAnchor:self.backView.trailingAnchor].active = YES;
    [self.noReservedLabel.heightAnchor constraintEqualToConstant:screenWidth * 0.16].active = YES;
    
    [self.timeChoiseButton.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.timeChoiseButton.topAnchor constraintEqualToAnchor:self.timeChoiselabel.bottomAnchor constant:screenHeight * 0.03].active = YES;
    [self.timeChoiseButton.widthAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    [self.timeChoiseButton.heightAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    
    [self.timeView.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.timeView.topAnchor constraintEqualToAnchor:self.timeChoiseButton.bottomAnchor].active = YES;
    [self.timeView.widthAnchor constraintEqualToAnchor:self.backView.widthAnchor].active = YES;
    [self.timeView.heightAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;

    self.timeLabel.text = [self setupFirsCurrentTimeForNow];
    _orderInfo.timeString = self.timeLabel.text;
    
    [self.timeLabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    
    [self.timeLabel.centerXAnchor constraintEqualToAnchor:self.timeView.centerXAnchor].active = YES;
    [self.timeLabel.centerYAnchor constraintEqualToAnchor:self.timeView.centerYAnchor].active = YES;
    
    [self.timeArrow.leftAnchor constraintEqualToAnchor:self.timeLabel.rightAnchor constant:4].active = YES;
    [self.timeArrow.centerYAnchor constraintEqualToAnchor:self.timeLabel.centerYAnchor].active = YES;
    [self.timeArrow.widthAnchor constraintEqualToConstant:9].active = YES;
    [self.timeArrow.heightAnchor constraintEqualToConstant:9].active = YES;
    [self.timeAction.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.timeAction.topAnchor constraintEqualToAnchor:self.timeChoiselabel.bottomAnchor].active = YES;
    [self.timeAction.widthAnchor constraintEqualToConstant:screenWidth * 0.6].active = YES;
    [self.timeAction.bottomAnchor constraintEqualToAnchor:self.timeLabel.bottomAnchor].active = YES;
    
    [self setupNoReservedView];

    [self.personsChioseLabel setFont:[UIFont fontWithName:@"Thonburi" size:textFontSize]];
    [self.personsChioseLabel.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.personsChioseLabel.topAnchor constraintEqualToAnchor:self.timeView.bottomAnchor constant:screenHeight * 0.03].active = YES;
    
    [self.personsIcon.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.personsIcon.topAnchor constraintEqualToAnchor:self.personsChioseLabel.bottomAnchor constant:screenHeight * 0.03].active = YES;
    [self.personsIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    [self.personsIcon.heightAnchor constraintEqualToConstant:screenWidth * 0.08].active = YES;
    
    [self.personsView.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.personsView.topAnchor constraintEqualToAnchor:self.personsIcon.bottomAnchor].active = YES;
    [self.personsView.widthAnchor constraintEqualToAnchor:self.backView.widthAnchor].active = YES;
    [self.personsView.heightAnchor constraintEqualToConstant:screenWidth * 0.1].active = YES;
    [self.personsCountLabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    self.personsCountLabel.text = [self helperPersonsWithCount:countPeople];
    [self.personsCountLabel.centerXAnchor constraintEqualToAnchor:self.personsView.centerXAnchor].active = YES;
    [self.personsCountLabel.centerYAnchor constraintEqualToAnchor:self.personsView.centerYAnchor].active = YES;

    [self.minusButton.rightAnchor constraintEqualToAnchor:self.personsCountLabel.leftAnchor constant:-10].active = YES;
    [self.minusButton.centerYAnchor constraintEqualToAnchor:self.personsCountLabel.centerYAnchor].active = YES;
    [self.minusButton.widthAnchor constraintEqualToConstant:22].active = YES;
    [self.minusButton.heightAnchor constraintEqualToConstant:22].active = YES;
    
    [self.plusButton.leftAnchor constraintEqualToAnchor:self.personsCountLabel.rightAnchor constant:10].active = YES;
    [self.plusButton.centerYAnchor constraintEqualToAnchor:self.personsCountLabel.centerYAnchor].active = YES;
    [self.plusButton.widthAnchor constraintEqualToConstant:22].active = YES;
    [self.plusButton.heightAnchor constraintEqualToConstant:22].active = YES;
    
    [self.continueButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    if (screenWidth < 322) {
        [self.continueButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    
    NSString* titleSendButton = DPLocalizedString(@"continue", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleSendButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.continueButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    [self.continueButton.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.continueButton.bottomAnchor constraintEqualToAnchor:self.backView.bottomAnchor constant:-screenHeight * 0.01].active = YES;
    
    if (isPhone){
        [self.continueButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.continueButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    } else {
        [self.continueButton.widthAnchor constraintEqualToConstant:240].active = YES;
        [self.continueButton.heightAnchor constraintEqualToConstant:71].active = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSDate *)roundDateTo5Minutes:(NSDate *)mydate{
    // Get the nearest 5 minute block
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSCalendarUnitHour | NSCalendarUnitMinute
                              fromDate:mydate];
    NSInteger minutes = [time minute];
    int remain = minutes % 15;

    mydate = [mydate dateByAddingTimeInterval:60*(15-remain)];

    
    
    
    return mydate;
}

-(NSString*) getDay:(NSInteger)type{
    
    NSString* dayOfWeek;
    
    switch (type) {
        case 1:
            dayOfWeek = DPLocalizedString(@"monday", nil);
            break;
        case 2:
            dayOfWeek = DPLocalizedString(@"tuesday", nil);
            break;
        case 3:
            dayOfWeek = DPLocalizedString(@"wednesday", nil);
            break;
        case 4:
            dayOfWeek = DPLocalizedString(@"thursday", nil);
            break;
        case 5:
            dayOfWeek = DPLocalizedString(@"friday", nil);
            break;
        case 6:
            dayOfWeek = DPLocalizedString(@"saturday", nil);
            break;
        case 7:
            dayOfWeek = DPLocalizedString(@"sunday", nil);
            break;
            
        default:
            break;
    }
    NSLog(@"dayOfWeek %@", dayOfWeek);
    return dayOfWeek;
}

@end
