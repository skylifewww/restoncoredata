//
//  RestonItemViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"
#import "RNRestaurant.h"
#import "CDRestaurant+CoreDataProperties.h"

@interface RestonItemViewController : UIViewController

//@property(strong, nonatomic) NSArray<RNRestaurant*>* restons;

//@property (nonatomic, copy) NSNumber *restaurantId;
//@property (nonatomic, strong) RNOrderInfo *orderInfo;
//@property (nonatomic, strong) RNRestaurant *convertRestaurant;
@property (nonatomic, strong) CDRestaurant *restCD;
@property (nonatomic, assign) Boolean fromMap;
//@property (nonatomic, strong) UIImage* backImage;
@property (nonatomic, strong) NSString *cityName;

@end
