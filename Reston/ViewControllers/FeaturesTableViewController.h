//
//  FeaturesTableViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterInfo.h"

extern NSString *const OptionsChangeNotification;

@interface FeaturesTableViewController : UITableViewController

@property (strong, nonatomic) FilterInfo* filterInfo;
@property (strong, nonatomic) NSString* currCity;
@property (strong, nonatomic) NSArray* filteredOptions;

@end
