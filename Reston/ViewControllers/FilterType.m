//
//  FilterType.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 03.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "FilterType.h"

@implementation FilterType

- (id)initWithTitle:(NSString*)title andSubtitle:(NSString *)subtitle{
    self = [super init];
    if (self != nil) {
      
        _title = title;
        _subtitle = subtitle;
       
    }
    return self;
}

@end
