//
//  NewsExpandViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNRestaurant.h"

@interface NewsExpandViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) NSNumber* restaurantID;
@property (nonatomic, strong) RNRestaurant *restaurant;
@property (strong, nonatomic) NSString* titleNav;
@property (strong, nonatomic) NSArray* newsAll;
@property (assign, nonatomic) Boolean isAllRests;

@end
