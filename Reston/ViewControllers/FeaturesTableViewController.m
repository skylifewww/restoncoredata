//
//  FeaturesTableViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "FeaturesTableViewController.h"
#import "FeaturesCell.h"
#import "GetOptionsRequest.h"
#import "GetOptionsResponse.h"
#import "Option.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


static NSString *const kOptionsKey = @"OptionsKey";
NSString *const OptionsChangeNotification = @"OptionsChangeNotification";

@interface FeaturesTableViewController (){
    
    NSMutableArray* selectArr;
    NSMutableArray* sendArr;
    NSArray<Option*>* features;
    NSArray<NSString*>* strOptions;
    NSString* currentFeatureName;
    NSIndexPath * checkedIndexPath;
    Option* option;
    UIColor* mainColor;
    
    Reachability *_reachability;
}

@end

@implementation FeaturesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"features_title", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    selectArr = [NSMutableArray array];
    sendArr = [NSMutableArray array];
    
    if (_filteredOptions.count > 0){
        
        strOptions = [_filteredOptions sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kOptionsKey];
        if (setSelect.count > selectArr.count && (![setSelect.firstObject  isEqualToString: @""])){
            selectArr = [NSMutableArray arrayWithArray:setSelect];
        }
        
        [self.tableView reloadData];
    } else {
        [self loadOptions];
    }
    
    self.tableView.allowsMultipleSelection = true;
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)loadOptions{
    GetOptionsRequest *request = [[GetOptionsRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    request.city = _currCity;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetOptionsResponse *r = (GetOptionsResponse *)response;
        features = r.options;
        
        features = [r.options sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            Option *r1 = obj1;
            Option *r2 = obj2;
            
            return [r1.optionName compare:r2.optionName];
        }];
        
        if (features.count == 0){
            
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];

        } else {
            
            NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kOptionsKey];
            if (setSelect.count > selectArr.count){
                for (Option* d in features){
                    for(NSString* str in setSelect){
                        if ([d.optionName isEqualToString:str]){
                            
                            [selectArr addObject:d];
                        }
                    }
                }
            }
            [self.tableView reloadData];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submitButton:(id)sender {

//        for (Option* rt in selectArr){
//            [sendArr addObject:rt.optionName];
            for (NSString* opt in selectArr){
                [sendArr addObject:opt];
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:OptionsChangeNotification object:nil userInfo:@{@"options" : sendArr}];
        
        [[NSUserDefaults standardUserDefaults] setObject:sendArr
                                                  forKey:kOptionsKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];

}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (features.count > 0){
//        return features.count;
//    }
    if (strOptions.count > 0){
        return strOptions.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FeaturesCell* cell = (FeaturesCell *)[tableView dequeueReusableCellWithIdentifier:@"FeaturesCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FeaturesCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [cell setBackgroundView:selectedBackgroundView];
    
//    if ([selectArr containsObject:features[indexPath.row]]) {
        if ([selectArr containsObject:strOptions[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    
//    Option* opt = features[indexPath.row];
//    
//    NSString* optStr = [opt.optionName stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
//    
//    cell.textLabel.text = optStr;

    
    NSString* optStr = [strOptions[indexPath.row] stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    
    cell.textLabel.text = optStr;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    option = features[indexPath.row];
//    currentFeatureName = option.optionName;
    currentFeatureName = strOptions[indexPath.row];
    NSLog(@"%@", currentFeatureName);
    
//    if ([selectArr containsObject:features[indexPath.row]] == NO) {
//        [selectArr addObject:features[indexPath.row]];
        if ([selectArr containsObject:strOptions[indexPath.row]] == NO) {
            [selectArr addObject:strOptions[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    } else {
//        [selectArr removeObject:features[indexPath.row]];
        [selectArr removeObject:strOptions[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    }

}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if ([selectArr containsObject:features[indexPath.row]]) {
//        [selectArr removeObject:features[indexPath.row]];
        if ([selectArr containsObject:strOptions[indexPath.row]]) {
            [selectArr removeObject:strOptions[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    } else {
//        [selectArr addObject:features[indexPath.row]];
        [selectArr addObject:strOptions[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end
