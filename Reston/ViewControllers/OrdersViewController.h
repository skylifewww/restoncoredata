//
//  OrdersViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/2/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMenuViewController.h"

extern NSString *const OrdersCountChangeNotification;


@interface OrdersViewController : UIViewController

@property (weak, nonatomic) id<LeftMenuProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (strong, nonatomic) UIButton *submitButton;
@property (strong, nonatomic) UIImageView *backOrders;
@property (weak, nonatomic) IBOutlet UILabel *myOrdersLabel;
@property (strong, nonatomic) UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (assign, nonatomic) Boolean isMenu;

@end
