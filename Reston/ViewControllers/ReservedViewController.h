//
//  ReservedViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"



@interface ReservedViewController : UIViewController


@property (nonatomic, strong) RNOrderInfo *orderInfo;

@property (weak, nonatomic) RNRestaurant *restourant;

@property (strong, nonatomic) NSString *userName;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *dearLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservedLabel1;
@property (weak, nonatomic) IBOutlet UILabel *reservedLabel2;
@property (weak, nonatomic) IBOutlet UIImageView *reservedImage;

@end
