//
//  NewsHeaderCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "NewsHeaderCell.h"

//
//NSString *const kNewsHeaderCellId = @"NewsHeaderCell";
const CGFloat kNewsHeaderCellHeight = 126;

@interface NewsHeaderCell ()


@end

@implementation NewsHeaderCell

@synthesize headerDateLabel = _headerDateLabel;
@synthesize headerTitle = _headerTitle;
@synthesize headerShortText = _headerShortText;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


    - (void)applyNew:(RNNEw *)model {
        _headerDateLabel.text = model.date;
        _headerTitle.text = model.title;
        _headerShortText.text = model.shortText;
 
    }
    
    + (CGFloat)heightForText:(NSString *)text{
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, 85);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = text;
    myTextView.font = [UIFont systemFontOfSize:11];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > 85 ? sz.height : 85;
    
    return result + 55;
}
    
    @end
