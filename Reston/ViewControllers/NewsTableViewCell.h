//
//  NewsTableViewCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kNewsExpandedCellId;
extern const CGFloat kNewsExpandedCellHeight;

#import "RNNEw.h"

@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *expandText;
@property (weak, nonatomic) IBOutlet UIImageView *expandImage;

- (void)applyNew:(RNNEw *)model;
//+ (CGFloat)heightForText:(NSString *)text
//            imagePresent:(BOOL)present;

@end
