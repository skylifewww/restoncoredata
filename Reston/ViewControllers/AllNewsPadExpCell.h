//
//  AllNewsPadExpCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 14.02.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllNewsPadExpCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *datelabel;
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descripLabel;
@property (weak, nonatomic) IBOutlet UIButton *reservButton;

@end
