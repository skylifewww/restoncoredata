//
//  CommentsCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/30/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "CommentsCell.h"

@implementation CommentsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
