//
//  UIViewController+SlideMenuControllerOC.m
//  ammonitum
//
//  Created by Vladimir Nybozhinsky on 13.05.17.
//  Copyright © 2017 Dima. All rights reserved.
//


#import "UIViewController+SlideMenuControllerOC.h"
#import "SlideMenuController.h"

@implementation UIViewController (SlideMenuControllerOC)

-(void)setNavigationBarItem {
    
    [self addLeftBarButtonWithTitle:@"\uf0c9"];
    
    [self.slideMenuController removeLeftGestures];
    [self.slideMenuController removeRightGestures];
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController addRightGestures];
}

-(void)setNavigationBackBarItem {
    
    [self addBackButtonWithTitle:@"\uf0c9" ];
    
    [self.slideMenuController removeLeftGestures];
    [self.slideMenuController removeRightGestures];
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController addRightGestures];
}



-(void)removeNavigationBarItem {
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    [self.slideMenuController removeLeftGestures];
    [self.slideMenuController removeRightGestures];
}

-(void)setNavigationBarArrowLeft {
    [self addLeftBarButtonWithImage:[UIImage imageNamed:@"arrowLeft.png"]];
    
    [self.slideMenuController removeLeftGestures];
    [self.slideMenuController removeRightGestures];
    [self.slideMenuController addLeftGestures];
    [self.slideMenuController addRightGestures];
}

@end

