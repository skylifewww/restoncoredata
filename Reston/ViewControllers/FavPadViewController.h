//
//  FavPadViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 10.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNRestaurant.h"
#import "RNOrderInfo.h"
#import "NewMenuViewController.h"


@interface FavPadViewController : UIViewController

@property (weak, nonatomic) id<LeftMenuProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UILabel *myFavLabel;
@property(strong, nonatomic) NSArray* restons;
@property (nonatomic, strong) RNRestaurant *restaurant;
@property (nonatomic, strong) RNOrderInfo *orderInfo;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (strong, nonatomic)  UIButton *submitButton;
@property (strong, nonatomic)  UILabel *textFavlabel;
@property (strong, nonatomic)  UIImageView *backFavIcon;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *topView;
//@property (nonatomic, assign) Boolean isThisCity;

@end
