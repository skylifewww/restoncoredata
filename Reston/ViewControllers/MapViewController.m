//
//  MapViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "MapViewController.h"
#import "ReservTableViewController.h"
#import "RNUser.h"
@import MapKit;
@import CoreLocation;
#import "Annotation.h"
#import "RestonItemViewController.h"
#import "TSClusterMapView.h"
#import "ClusteredAnnotationView.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "GetReserveRequest.h"
#import "GetReserveResponse.h"
#import "OrdersViewController.h"
#import "UIWindow+LoadingIndicator.h"
#import "UIImage+Cache.h"
#import "Reachability.h"

static NSString *const kReservCountKey = @"ReservCountKey";



@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end

@interface MapViewController ()<MKMapViewDelegate, CLLocationManagerDelegate>{
    UIColor* mainColor;
    double latitude;
    double longitude;
    CLLocationCoordinate2D restCoord;
    MKPointAnnotation* userAnnotation;
    Annotation* restaurantAnnotation;
    NSArray* annotations;
    NSMutableArray* annRestons;
    NSDictionary* dictAnnRestons;
    Boolean movedUser;
    NSNumber* currRestID;
    RNRestaurant* currRestaurant;
    Boolean isSelected;
    NSInteger currAnnIndex;
    MKAnnotationView * currPinView;
    Boolean didFinishLoadingMap;
    Reachability *_reachability;
    UIStoryboard* storyboard;
    Boolean isPhone;
    double addressFontSize;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    double typeFontSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    double descriptViewHeight;
//    UILabel* rightLabel;
//    UIImageView* rightImageView;
//    UIBarButtonItem *rightBarButton;
    NSInteger reservsCount;
    UIView* backGeoView;
//    NSString* realCity;
}

@property (nonatomic, strong) CLLocationManager *locationManager;

@end


@implementation MapViewController
@synthesize mapView = _mapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    screenWidth =  [UIScreen mainScreen].bounds.size.width;
    screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        typeFontSize = 12.0;
        addressFontSize = 10.0;
        restNameFontSize = 14.0;
        textFontSize = 12.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
        isPhone = YES;
        descriptViewHeight = 114;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        typeFontSize = 16.0;
        addressFontSize = 16.0;
        restNameFontSize = 22.0;
        textFontSize = 16.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
        descriptViewHeight = 162;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"rests_on_map", nil);
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    _openReservBarButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(openReservs)];
    
    [self loadReservs];
    
    annRestons = [NSMutableArray array];
    isSelected = NO;
    [_mapView setDelegate:self];
    
    _mapView.showsScale = true;
    _mapView.showsPointsOfInterest = true;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    movedUser = NO;
    didFinishLoadingMap = NO;
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
 
    if ([RNUser sharedInstance].isGeoEnabled){
        
        [self loadUserLocation];
    
        
    } else {
        
        [self checkLocationServicesAndStartUpdates];
    }
    
    [self setupView];
}

-(void) setupOpenReservsButton:(NSInteger)reservsCount{
    
    if (reservsCount < 1){
        [_openReservBarButton setEnabled:NO];
        
        UIImage* clearImage = [[UIImage alloc] init];
        [_openReservBarButton setBackgroundImage:clearImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [_openReservBarButton setTitle:@""];
        
    } else if (reservsCount >= 1){
        UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:15];
        
        UIImage* i = [UIImage imageNamed:@"bookMark"];
        //    i = [i imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        [_openReservBarButton setBackgroundImage:i forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        
        [_openReservBarButton setTitleTextAttributes:@{NSFontAttributeName : typeFont, NSForegroundColorAttributeName : mainColor} forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = _openReservBarButton;
        [_openReservBarButton setEnabled:YES];
        [_openReservBarButton setTitle:[NSString stringWithFormat:@"%ld",reservsCount]];
    }
}

- (void)openReservs{
    
    UIStoryboard* storyboard = [[UIStoryboard alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
    }
    
    OrdersViewController * ordersViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrdersViewController"];
    ordersViewController.isMenu = NO;
    
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:ordersViewController];
    
    [self.navigationController presentViewController:navVC animated:NO completion:nil];
    
}


-(void) checkLocationServicesAndStartUpdates
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    //Checking authorization status
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        if (![CLLocationManager locationServicesEnabled])
        {
            
        }
        
        else
        {
            
        }
        
        return;
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [_locationManager startUpdatingLocation];
    }
}


- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
            
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User denied location access request!!");
            [self setupBackGeoView];
            [self showGeoAlertWithTitle:DPLocalizedString(@"for_geo", nil) andMessage:DPLocalizedString(@"forward_to_settings_for_geo", nil)];
            
            [_locationManager stopUpdatingLocation];
          
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            
            [RNUser sharedInstance].isGeoEnabled = YES;
//            [self loadUserLocation];
            
            [self loadUserLocation];
            
//            [self.showRouteButton setTintColor:mainColor];
//            [self.showRouteButton setUserInteractionEnabled:YES];
            
            //        if ([self isInternetConnect]){
            if (_isAllRestons == YES &&  didFinishLoadingMap == YES){
                
                [self setupRegion:_userLocation];
                didFinishLoadingMap = YES;
            }
          
            [backGeoView removeFromSuperview];
            
            NSLog(@"Userlocation!!");
            
        } break;
        case kCLAuthorizationStatusNotDetermined:{
            
            [RNUser sharedInstance].isGeoEnabled = YES;
            [self loadUserLocation];
            
            //        if ([self isInternetConnect]){
            if (_isAllRestons == YES &&  didFinishLoadingMap == YES){
                
                [self setupRegion:_userLocation];
                didFinishLoadingMap = YES;
            }
         
            [backGeoView removeFromSuperview];
            NSLog(@"Userlocation!!");
            
        } break;
        case kCLAuthorizationStatusAuthorizedAlways: {
            [RNUser sharedInstance].isGeoEnabled = YES;
            [self loadUserLocation];
            
            //        if ([self isInternetConnect]){
            if (_isAllRestons == YES &&  didFinishLoadingMap == YES){
                
                [self setupRegion:_userLocation];
                didFinishLoadingMap = YES;
            }
          
            [backGeoView removeFromSuperview];
            NSLog(@"Userlocation!!");
            
        } break;
        default:
            break;
    }
}

- (void)showGeoAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSString* url = SYSTEM_VERSION_LESS_THAN(@"10.0") ? @"prefs:root=LOCATION_SERVICES" : @"App-Prefs:root=Privacy&path=LOCATION";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
        
    }];
    
    UIAlertAction* continueWithoutLocation = [UIAlertAction actionWithTitle:DPLocalizedString(@"pass_action", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [RNUser sharedInstance].isGeoEnabled = NO;
        [self loadUserLocation];

//        if ([self isInternetConnect]){
            if (_isAllRestons == YES &&  didFinishLoadingMap == YES){

                [self setupRegion:_userLocation];
                didFinishLoadingMap = YES;
            }
        [backGeoView removeFromSuperview];
//        }
    }];
    
    [alert addAction:actionCancel]; // 4
    [alert addAction:continueWithoutLocation]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setupBackGeoView {
    
    backGeoView = [[UIView alloc]initWithFrame:self.view.frame];
    backGeoView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backGeoView];
}


- (void)loadReservs{
    GetReserveRequest *request = [[GetReserveRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetReserveResponse *r = (GetReserveResponse *)response;
        
        reservsCount = r.reserves.count;
        
        [[NSUserDefaults standardUserDefaults] setInteger:reservsCount forKey:kReservCountKey];
        NSLog(@"[[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]  %ld", [[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self setupOpenReservsButton:reservsCount];
    }];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reservCountChangeNotification:)
                                                 name:OrdersCountChangeNotification
                                               object:nil];
    
}

-(void) reservCountChangeNotification:(NSNotification*) notif{
    NSLog(@"reservCountChangeNotification!!!!++++++");
    NSNumber* reservCountNumber = [notif.userInfo objectForKey:@"reservCount"];
    NSInteger reservsCount = reservCountNumber.integerValue;
    
    NSLog(@"reservCount============= %ld", reservsCount);
    
    [self setupOpenReservsButton:reservsCount];
    
    NSLog(@"reservCount reservCountChangeNotification %@",[NSString stringWithFormat:@"%ld",reservsCount]);
    
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void) clearMap{
    
    [_mapView removeAnnotations:annotations];
    [_mapView removeOverlays:_mapView.overlays];
}

- (IBAction)showRouteToRest:(id)sender {
    [self routeFromUser:_userLocation toRestaurant:_restaurant.coordinate];
//    [self routeFromUser:_restaurant.coordinate toRestaurant:_userLocation];
}

-(NSString *)getAddressFromLocation:(CLLocation *)location {
    
    __block NSString *city;
    __block NSString *administrativeArea;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             city = [NSString stringWithFormat:@"%@",[placemark locality]];
             NSLog(@"city %@",city);
             administrativeArea = [NSString stringWithFormat:@"%@",[placemark administrativeArea]];
             NSLog(@"administrativeArea %@",administrativeArea);
             
             
             if (![city isKindOfClass:[NSNull class]] || ![administrativeArea isKindOfClass:[NSNull class]]){
                 
                 NSLog(@"cityName != nil");
                 if ([city isEqualToString:@"Kiev"] || [administrativeArea isEqualToString:@"Kiev Oblast"] || [city isEqualToString:@"Киев"] || [city isEqualToString:@"Київ"]){
                     
                     [RNUser sharedInstance].realCity = @"1";
                     
                 } else if ([city isEqualToString:@"Lviv"] || [administrativeArea isEqualToString:@"Lviv Oblast"] || [city isEqualToString:@"Львів"] || [city isEqualToString:@"Львов"]){
                     
                     [RNUser sharedInstance].realCity = @"6";
                     
                 } else if ([city isEqualToString:@"Ivano-Frankivs'k"] || [administrativeArea isEqualToString:@"Ivano-Frankivsk Oblast"] || [city isEqualToString:@"Івано-Франківськ"] || [city isEqualToString:@"Ивано-Франковск"]){
                     [RNUser sharedInstance].realCity = @"8";
                     
                 } else if ([city isEqualToString:@"Kharkiv"] || [administrativeArea isEqualToString:@"Kharkiv Oblast"] || [city isEqualToString:@"Харків"] || [city isEqualToString:@"Харьков"]){
                     [RNUser sharedInstance].realCity = @"9";
                     
                 } else {
                     [RNUser sharedInstance].realCity = @"1234567";
                 }
             } else {
                 
                 [RNUser sharedInstance].realCity = @"1234567";
             }
             
             NSLog(@"realCity: %@  currCity: %@", [RNUser sharedInstance].realCity, [RNUser sharedInstance].currCity);
             
             if ([[RNUser sharedInstance].realCity isEqualToString:[RNUser sharedInstance].currCity]){
                 
                 [RNUser sharedInstance].isThisCity = YES;
                 
             } else {
                 
                 [RNUser sharedInstance].isThisCity = NO;
             }
         }
     }];
    return [RNUser sharedInstance].currCity;
}

- (void) loadUserLocation
{
    
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    latitude = _locationManager.location.coordinate.latitude;
    longitude = _locationManager.location.coordinate.longitude;
    NSLog(@"CatalogRestViewController latitude %f longitude %f",latitude, longitude);
    
    
    
    _userLocation = CLLocationCoordinate2DMake(latitude, longitude);
    
    if (_isAllRestons == NO){
        
        [self showUserAndRestOnMap];
    }
    
    
}

-(void) showUserAndRestOnMap{
    
    userAnnotation = [[MKPointAnnotation alloc] init];
    userAnnotation.title = DPLocalizedString(@"my_location", nil);
//    userAnnotation.subtitle = DPLocalizedString(@"my_location", nil);
    userAnnotation.coordinate = _userLocation;
    
    restaurantAnnotation = [[Annotation alloc] init];
    NSString* restName = [_restaurant.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    restaurantAnnotation.title = restName;
    restaurantAnnotation.subtitle = [NSString stringWithFormat:@"%@ %@",DPLocalizedString(@"location", nil), restName];
    restaurantAnnotation.coordinate = _restaurant.coordinate;
    
    if ([RNUser sharedInstance].isThisCity == YES && [RNUser sharedInstance].isGeoEnabled == YES) {
        NSLog(@"_isThisCity == YES %d", [RNUser sharedInstance].isThisCity);
        
       annotations = @[userAnnotation, restaurantAnnotation];
        _mapView.showsUserLocation = true;
    } else {
        NSLog(@"_isThisCity == NO");
        _mapView.showsUserLocation = NO;
        annotations = @[restaurantAnnotation];
    }
    
    
//    [self setupRegion:_userLocation];
    [self.mapView showAnnotations:annotations animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
}

//- (void)locationManager:(CLLocationManager *)manager
//didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
//
//    switch (status) {
//        case kCLAuthorizationStatusDenied:
//            NSLog(@"AuthorizationStatusDenied");
//            break;
//        case kCLAuthorizationStatusRestricted:
//            NSLog(@"AuthorizationStatusRestricted");
//            break;
//        case kCLAuthorizationStatusAuthorizedAlways:
//            NSLog(@"AuthorizationStatusAuthorizedAlways");
//            [_locationManager requestAlwaysAuthorization];
//            break;
//        case kCLAuthorizationStatusAuthorizedWhenInUse:
//            NSLog(@"AuthorizationStatusAuthorizedWhenInUse");
//            [_locationManager requestWhenInUseAuthorization];
//            break;
//        case kCLAuthorizationStatusNotDetermined:
//            NSLog(@"AuthorizationStatusNotDetermined");
//            [_locationManager requestWhenInUseAuthorization];
//            
//            break;
//            
//        default:
//            [_locationManager startUpdatingLocation];
//            break;
//    }
//}


- (void)setupRegion:(CLLocationCoordinate2D)userLocation {
    
    MKMapRect zoomRect = MKMapRectNull;

    if (_isAllRestons == YES){
        
        double offSetValue = _isFiltered ? 0.3 : -3;
        NSLog(@"_isThisCity == %d [RNUser sharedInstance].isGeoEnabled == %d", [RNUser sharedInstance].isThisCity, [RNUser sharedInstance].isGeoEnabled);
        
        if ([RNUser sharedInstance].isThisCity == YES && [RNUser sharedInstance].isGeoEnabled == YES) {
            NSLog(@"_isThisCity == YES");
            
            MKMapPoint annotationPoint = MKMapPointForCoordinate(userLocation);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 1, 1);
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
            _mapView.showsUserLocation = true;
        } else {
            _mapView.showsUserLocation = NO;
        }
        
        if (_restonsItems.count > 1){
            
            for(int i = 0; i < _restonsItems.count; i++) {
                [self addPinWithTitle:_restonsItems[i].title AndCoordinate:_restonsItems[i].coordinate andRestID:_restonsItems[i].restaurantId andIndex:i];
                MKMapPoint annotationPoint = MKMapPointForCoordinate(_restonsItems[i].coordinate);
                MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, offSetValue, offSetValue);
                zoomRect = MKMapRectUnion(zoomRect, pointRect);
                
                [self.mapView setVisibleMapRect:zoomRect animated:YES];
            }
        } else if (_restonsItems.count == 1 && [RNUser sharedInstance].isThisCity == NO){
            
            [self addPinWithTitle:_restonsItems[0].title AndCoordinate:_restonsItems[0].coordinate andRestID:_restonsItems[0].restaurantId andIndex:0];
            
            MKMapPoint annotationPoint = MKMapPointForCoordinate(_restonsItems[0].coordinate);
            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
            
            [self.mapView setVisibleMapRect:zoomRect animated:YES];
            
            [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(_restonsItems[0].coordinate, 1500, 3000) animated:YES];
            
        }
    } else {
        
        if ([RNUser sharedInstance].isThisCity == YES) {
            
            MKMapPoint userPoint = MKMapPointForCoordinate(_userLocation);
            MKMapRect pointUser = MKMapRectMake(userPoint.x, userPoint.y, 1, 1);
            zoomRect = MKMapRectUnion(zoomRect, pointUser);
            MKMapPoint restPoint = MKMapPointForCoordinate(_restaurant.coordinate);
            MKMapRect pointRest = MKMapRectMake(restPoint.x, restPoint.y, 1, 1);
            zoomRect = MKMapRectUnion(zoomRect, pointRest);
            _mapView.showsUserLocation = true;
        } else {
            _mapView.showsUserLocation = NO;
            MKMapPoint restPoint = MKMapPointForCoordinate(_restaurant.coordinate);
            MKMapRect pointRest = MKMapRectMake(restPoint.x, restPoint.y, 1, 1);
            zoomRect = MKMapRectUnion(zoomRect, pointRest);
        }
        [self.mapView setVisibleMapRect:zoomRect animated:YES];
    }
}

-(void)addPinWithTitle:(NSString *)title AndCoordinate:(CLLocationCoordinate2D)coordinate andRestID:(NSNumber*)restID andIndex:(NSUInteger)index
{
    Annotation *mapPin = [[Annotation alloc] init];
    
    NSString* restName = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    mapPin.title = restName;
    
    mapPin.coordinate = coordinate;
    mapPin.restID = restID;
    mapPin.isSelected = NO;
    mapPin.index = index;
    
    [annRestons addObject:mapPin];

    [self.mapView addClusteredAnnotations:annRestons];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    
    if (_isAllRestons == YES && [RNUser sharedInstance].isGeoEnabled == YES && didFinishLoadingMap != YES){
      
        [self setupRegion:_userLocation];
        didFinishLoadingMap = YES;
        
    }
    didFinishLoadingMap = YES;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    //    didFinishAddingAnnotationViews = YES;
    //    [self createImage];
}

- (void)mapView:(MKMapView *)mapView didAddOverlayViews:(NSArray *)overlayViews

{
    //    didFinishAddingOverlayViews = YES;
    //    [self createImage];
}


-(void) routeFromUser:(CLLocationCoordinate2D)userCoordinate toRestaurant:(CLLocationCoordinate2D)restaurantCoordinate{
    
    [_mapView setDelegate:self];
    
    MKPlacemark* restaurantPlace = [[MKPlacemark alloc] initWithCoordinate:restaurantCoordinate
                                                         addressDictionary: nil];
    MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark: restaurantPlace];
    
    MKPlacemark* userItem = [[MKPlacemark alloc] initWithCoordinate:userCoordinate
                                                  addressDictionary: nil];
    MKMapItem* start = [[MKMapItem alloc] initWithPlacemark: userItem];
    
    start.name = DPLocalizedString(@"my_location", nil);
    destination.name = [_restaurant.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    
    NSArray* items = [[NSArray alloc] initWithObjects: start, destination, nil];
    
    MKDirectionsRequest* request = [[MKDirectionsRequest alloc] init];
    request.source = start;
    request.destination = destination;
    request.transportType = MKDirectionsTransportTypeAny;
    
    //    MKDirections* directions = [[MKDirections alloc] initWithRequest:request];
    //    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
    //        if (error){
    //            NSLog(@"%@", error);
    //            return;
    //        }
    //
    //        MKRoute* route = response.routes.firstObject;
    //
    //        [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
    //        MKMapRect rect = (response.routes.firstObject).polyline.boundingMapRect;
    //
    ////         [self.mapView setVisibleMapRect:rect edgePadding:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0) animated:NO];
    //    }];
    
    NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
                             MKLaunchOptionsDirectionsModeDriving,
                             MKLaunchOptionsDirectionsModeKey, nil];
    [MKMapItem openMapsWithItems: items launchOptions: options];
    
}

#pragma mark - ADClusterMapView Delegate

- (void)removeAllRest{
    [self.mapView removeAnnotations:annRestons];
    [annRestons removeAllObjects];
}

- (MKAnnotationView *)mapView:(TSClusterMapView *)mapView viewForClusterAnnotation:(id<MKAnnotation>)annotation {
    
    ClusteredAnnotationView * view = (ClusteredAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([ClusteredAnnotationView class])];
    if (!view) {
        view = [[ClusteredAnnotationView alloc] initWithAnnotation:annotation
                                                   reuseIdentifier:NSStringFromClass([ClusteredAnnotationView class])];
    }
    
    return view;
}

- (void)mapView:(TSClusterMapView *)mapView willBeginBuildingClusterTreeForMapPoints:(NSSet<ADMapPointAnnotation *> *)annotations {
    //    NSLog(@"Kd-tree will begin mapping item count %lu", (unsigned long)annotations.count);
    
    //    _startTime = [NSDate date];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (annRestons.count > 100) {
            [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
        }
        else {
            [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
        }
    }];
}

- (void)mapView:(TSClusterMapView *)mapView didFinishBuildingClusterTreeForMapPoints:(NSSet<ADMapPointAnnotation *> *)annotations {
    //    NSLog(@"Kd-tree finished mapping item count %lu", (unsigned long)annotations.count);
    //    NSLog(@"Took %f seconds", -[_startTime timeIntervalSinceNow]);
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
    }];
}

- (void)mapViewWillBeginClusteringAnimation:(TSClusterMapView *)mapView{
    
    NSLog(@"Animation operation will begin");
}

- (void)mapViewDidCancelClusteringAnimation:(TSClusterMapView *)mapView {
    
    NSLog(@"Animation operation cancelled");
}

- (void)mapViewDidFinishClusteringAnimation:(TSClusterMapView *)mapView{
    
    NSLog(@"Animation operation finished");
}

- (void)userWillPanMapView:(TSClusterMapView *)mapView {
    
    NSLog(@"Map will pan from user interaction");
}

- (void)userDidPanMapView:(TSClusterMapView *)mapView {
    
    NSLog(@"Map did pan from user interaction");
}

- (BOOL)mapView:(TSClusterMapView *)mapView shouldForceSplitClusterAnnotation:(ADClusterAnnotation *)clusterAnnotation {
    
    return YES;
}

- (BOOL)mapView:(TSClusterMapView *)mapView shouldRepositionAnnotations:(NSArray<ADClusterAnnotation *> *)annotations toAvoidClashAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    return YES;
}




-(void) drawRoute:(id)sender{
    [self routeFromUser:_userLocation toRestaurant:currRestaurant.coordinate];
}

//- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
//    MKPolyline *polyline = (MKPolyline *)overlay;
//
//    MKPolylineRenderer* rend = [[MKPolylineRenderer alloc] initWithOverlay:polyline];
//
//    [rend setLineWidth:3.5];
//    [rend setStrokeColor:mainColor];
//    [rend setAlpha:1];
//    return rend;
//}



//- (UIView *)detailViewForAnnotation:(Annotation *)annotation {
//    UIView *view = [[UIView alloc] init];
//    view.translatesAutoresizingMaskIntoConstraints = false;
//
//    UIButton *restButton = [self restButtonForRestID:annotation.restID];
//    [view addSubview:restButton];
//
//    UIButton *routeButton = [self routeButtonForRestID:annotation];
//    [view addSubview:routeButton];
//
//    NSDictionary *views = NSDictionaryOfVariableBindings(restButton, routeButton);
//
//    [view addConstraint:[NSLayoutConstraint constraintWithItem:restButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
//     [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[restButton]-[routeButton]|" options:0 metrics:nil views:views]];
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[restButton]|" options:0 metrics:nil views:views]];
//
//    [view addConstraint:[NSLayoutConstraint constraintWithItem:routeButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[restButton]-[routeButton]|" options:0 metrics:nil views:views]];
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[routeButton]|" options:0 metrics:nil views:views]];
//
//    [restButton.widthAnchor constraintEqualToConstant:30].active = YES;
//    [restButton.heightAnchor  constraintEqualToConstant:30].active = YES;
//
//    [routeButton.leftAnchor constraintEqualToAnchor:restButton.rightAnchor constant:14].active = YES;
//    [routeButton.widthAnchor constraintEqualToConstant:30].active = YES;
//    [routeButton.heightAnchor  constraintEqualToConstant:30].active = YES;
//
//    return view;
//}

//- (UIButton *)restButtonForRestID:(NSNumber*)restID {
//    UIImage *image = [UIImage imageNamed:@"restoIconMenu"];
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button.imageView setTintColor:mainColor];
//    button.translatesAutoresizingMaskIntoConstraints = false; // use auto layout in this case
//    [button setImage:image forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(openRest:) forControlEvents:UIControlEventTouchUpInside];
//
//    return button;
//}



//- (UIButton *)routeButtonForRestID:(Annotation*)ann {
//    UIImage *image = [UIImage imageNamed:@"Route"];
//
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button.imageView setTintColor:mainColor];
//    button.translatesAutoresizingMaskIntoConstraints = false; // use auto layout in this case
//    [button setImage:image forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(drawRoute:) forControlEvents:UIControlEventTouchUpInside];
//
//    return button;
//}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else {
        
        if ([annotation isKindOfClass:[Annotation class]]){
            
            MKAnnotationView *result = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"Annotation"];
            if (result == nil) {
                result = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"Annotation"];
                
            }
            result.image = [UIImage imageNamed:@"Pin"];
            
            result.centerOffset = CGPointMake(result.centerOffset.x, -result.frame.size.height/2);
            return result;
        }
    }
    return nil;
}


//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
//    RestonItemViewController *vc = [[RestonItemViewController alloc] init];
//    if ([view.annotation isKindOfClass:[Annotation class]]){
//    Annotation* ann = (Annotation*)view.annotation;
//    currRestID = ann.restID;
//    vc.orderInfo = _orderInfo;
//    [self.navigationController pushViewController:vc
//                                         animated:YES];
//    }
//}


//- (void)openMap:(RNRestaurant *)restaurant {
//    MKPlacemark* place = [[MKPlacemark alloc] initWithCoordinate:restaurant.coordinate
//                                               addressDictionary: nil];
//    MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark: place];
//    destination.name = restaurant.title;
//    NSArray* items = [[NSArray alloc] initWithObjects: destination, nil];
//    NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:
//                             MKLaunchOptionsDirectionsModeDriving,
//                             MKLaunchOptionsDirectionsModeKey, nil];
//    [MKMapItem openMapsWithItems: items launchOptions: options];
//}


-(double) getDistanceToRest:(CLLocationCoordinate2D)restCoordinates{
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:_userLocation.latitude longitude:_userLocation.longitude];
    
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:restCoordinates.latitude longitude:restCoordinates.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
    
    NSLog(@"Distance from Annotations - %f", distance);
    NSLog(@"latitude - %f", latitude);
    NSLog(@"longitude - %f", longitude);
    
    return distance;
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if (_isAllRestons == YES && [view.annotation isKindOfClass:[Annotation class]]){
        Annotation* ann = (Annotation*)view.annotation;
        currRestID = ann.restID;
        currAnnIndex = ann.index;
        _restaurant = _restonsItems[currAnnIndex];
        [self setupValuesView];
        NSLog(@"currRestID %@",currRestID);
        NSLog(@"ann.index %ld",(long)ann.index);
        
        NSLog(@"_restaurant name %@",_restaurant.title);
        
        CGAffineTransform tr3 = CGAffineTransformMakeScale(2.0, 2.0);
        CGAffineTransform tr2 = CGAffineTransformMakeScale(1.5, 1.5);
        
        
        if (currPinView == nil){
            
            
            [UIView animateWithDuration:0.2 animations:^{
                
                view.transform = tr3;
                
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    view.transform = tr2;
                    
                    
                } completion:^(BOOL finished) {
                    
                    currPinView = view;
                    
                    [UIView animateWithDuration:0.1 animations:^{
                        
                        
                        
                        self.descripView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame)- descriptViewHeight, self.view.frame.size.width, descriptViewHeight);
                    }];
                    
                }];
                
            }];
            
        } else if (currPinView != nil){
            
            
            if ([currPinView isEqual:view]){
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    view.transform = tr3;
                    
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:0.2 animations:^{
                        
                        currPinView.transform = CGAffineTransformIdentity;
                        
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.1 animations:^{
                            currPinView = nil;
                            _restaurant = nil;
                            
                            self.descripView.frame = CGRectMake(CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame)- 114, self.view.frame.size.width, 114);
                        }];
                        
                    }];
                    
                }];
                
            } else {
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    view.transform = tr3;
                    
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:0.2 animations:^{
                        
                        currPinView.transform = CGAffineTransformIdentity;
                        view.transform = tr2;
                        
                    } completion:^(BOOL finished) {
                        
                        currPinView = view;
                        self.descripView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame)- descriptViewHeight, self.view.frame.size.width, descriptViewHeight);
                        
                    }];
                    
                }];
                
            }
        }
        
    }
}

- (IBAction)openRest:(id)sender {
    
//    RestonItemViewController * restonItemViewController = [storyboard instantiateViewControllerWithIdentifier:@"RestonItemViewController"];
//    
//    restonItemViewController.restaurantId = currRestID;
//    restonItemViewController.fromMap = YES;
//    restonItemViewController.orderInfo = _orderInfo;
//    __block UIImage* backImage;
//    if (_restaurant.images.count >0){
//        [UIImage cachedImage:_restaurant.images[0]
//               fullURLString:nil
//                withCallBack:^(UIImage *image) {
//                    backImage = image;
//                }];
//        
//    } else {
//        backImage = [UIImage imageNamed:@"restLogo"];
//    }
////    restonItemViewController.backImage = backImage;
//    
//    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:restonItemViewController];
//    
//    [self presentViewController:navVC animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    
    [self removeAllRest];
    [self.mapView removeAllAnnotations];
    [_mapView removeOverlays:_mapView.overlays];
    [self.mapView removeFromSuperview];
    self.mapView = nil;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submitButton:(id)sender {
    
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = _restaurant;
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void) setupDescripeViewForPad {
    
    if (_isAllRestons == NO){
        
        self.descripView.translatesAutoresizingMaskIntoConstraints = false;
        
    } else {
        
        
        self.descripView.frame = CGRectMake(CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame)- descriptViewHeight, self.view.frame.size.width, descriptViewHeight);
    }
    
    if (_isAllRestons == NO){
        
        [self.descripView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
        [self.descripView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
        [self.descripView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
        [self.descripView.heightAnchor  constraintEqualToConstant:descriptViewHeight].active = YES;
    }
     NSLog(@"setupDescripeViewForPad");

    
    [self.showRouteButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-16].active = YES;
    
    
    [self.showRouteButton.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:9].active = YES;
    [self.showRouteButton.widthAnchor  constraintEqualToConstant:180].active = YES;
    [self.showRouteButton.heightAnchor  constraintEqualToConstant:68].active = YES;
  
    [self.mapView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.mapView.rightAnchor  constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    
    if (_isAllRestons){
        [self.mapView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
        [self.mapView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    } else {
        [self.mapView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
        [self.mapView.bottomAnchor constraintEqualToAnchor:self.descripView.topAnchor].active = YES;
    }
    
    
    
    [self.restNamelable.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:4].active = YES;
    [self.restNamelable.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:4].active = YES;
    [self.restNamelable.rightAnchor constraintEqualToAnchor:self.showRouteButton.leftAnchor constant:-10].active = YES;
    
    if (_isAllRestons){
        [self.openRestButton.leftAnchor constraintEqualToAnchor:self.restNamelable.leftAnchor].active = YES;
        [self.openRestButton.topAnchor constraintEqualToAnchor:self.restNamelable.topAnchor].active = YES;
        [self.openRestButton.bottomAnchor constraintEqualToAnchor:self.commentView.bottomAnchor].active = YES;
        [self.openRestButton.rightAnchor constraintEqualToAnchor:self.submitButton.leftAnchor constant:-10].active = YES;
    } else {
        [self.openRestButton.leftAnchor constraintEqualToAnchor:self.view.rightAnchor constant:5].active = YES;
        [self.openRestButton.topAnchor constraintEqualToAnchor:self.restNamelable.topAnchor].active = YES;
        [self.openRestButton.bottomAnchor constraintEqualToAnchor:self.restNamelable.bottomAnchor].active = YES;
        [self.openRestButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:5].active = YES;
    }
    
    
    [self.addressView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.addressView.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:50].active = YES;
    [self.addressView.rightAnchor constraintEqualToAnchor:self.showRouteButton.leftAnchor constant:-10].active = YES;
    //    [self.descripView.heightAnchor  constraintEqualToConstant:self.containerView.bounds.size.height * 0.59].active = YES;
    
    
    [self.restType.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: 10].active = YES;
    [self.restType.rightAnchor constraintEqualToAnchor:self.addressView.rightAnchor ].active = YES;
    [self.restType.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
    
    
    [self.ratingView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:10].active = YES;
    [self.ratingView.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:76].active = YES;
    [self.ratingView.widthAnchor constraintEqualToConstant:170].active = YES;
    [self.ratingView.heightAnchor constraintEqualToConstant:26].active = YES;
    
    
    
    [self.ratingImage.leftAnchor constraintEqualToAnchor:self.ratingView.leftAnchor constant:4].active = YES;
    [self.ratingImage.centerYAnchor constraintEqualToAnchor:self.ratingView.centerYAnchor].active = YES;
    [self.ratingImage.widthAnchor constraintEqualToConstant:160].active = YES;
    [self.ratingImage.heightAnchor constraintEqualToConstant:25].active = YES;
    
    [self.commentView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.commentView.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:115].active = YES;
    [self.commentView.rightAnchor constraintEqualToAnchor:self.submitButton.leftAnchor constant:-10].active = YES;
    [self.commentView.heightAnchor constraintEqualToConstant:40].active = YES;
    
    [self.commentsIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 16].active = YES;
    [self.commentsIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.commentsIcon.widthAnchor constraintEqualToConstant:35].active = YES;
    [self.commentsIcon.heightAnchor constraintEqualToConstant:38].active = YES;
    
    
    
    [self.commentsCount.leftAnchor constraintEqualToAnchor:self.commentsIcon.rightAnchor constant: 10].active = YES;
    [self.commentsCount.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 5].active = YES;
    
    [self.avgIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 122].active = YES;
    [self.avgIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.avgIcon.widthAnchor constraintEqualToConstant:38].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:32].active = YES;
    
    
    [self.averageLabel.leftAnchor constraintEqualToAnchor:self.avgIcon.rightAnchor constant: 10].active = YES;
    [self.averageLabel.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 5].active = YES;
    
    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 245].active = YES;
    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.distansIcon.widthAnchor constraintEqualToConstant:38].active = YES;
    [self.distansIcon.heightAnchor constraintEqualToConstant:36].active = YES;
    
    
    [self.distanceRest.leftAnchor constraintEqualToAnchor:self.distansIcon.rightAnchor constant: 5].active = YES;
    [self.distanceRest.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 5].active = YES;
    
    
    [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-8].active = YES;
    
    
    [self.submitButton.bottomAnchor constraintEqualToAnchor:self.descripView.bottomAnchor constant:-8].active = YES;
    [self.submitButton.widthAnchor constraintEqualToConstant:280].active = YES;
    [self.submitButton.heightAnchor constraintEqualToConstant:68].active = YES;
    
    if (_isAllRestons == NO){
        
        [self setupValuesView];
        
    }
    
    
}

-(void) setupDescripeViewForPhone{
    
    NSLog(@"setupDescripeViewForPhone");
    
    if (_isAllRestons == NO){
        
        self.descripView.translatesAutoresizingMaskIntoConstraints = false;
        
    } else {
        
        
        self.descripView.frame = CGRectMake(CGRectGetMaxX(self.view.frame), CGRectGetMaxY(self.view.frame)- descriptViewHeight, self.view.frame.size.width, descriptViewHeight);
    }
    
    if (_isAllRestons == NO){
        
        [self.descripView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
        [self.descripView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
        [self.descripView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
        [self.descripView.heightAnchor  constraintEqualToConstant:descriptViewHeight].active = YES;
    }
    
    
//    [self.showRouteButton setTintColor:mainColor];
    
    if (screenWidth < 322) {
        
        [self.showRouteButton.rightAnchor  constraintEqualToAnchor:self.descripView.rightAnchor].active = YES;
    } else {
        [self.showRouteButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
    }
    
    [self.showRouteButton.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:9].active = YES;
    [self.showRouteButton.heightAnchor  constraintEqualToConstant:40].active = YES;
    [self.showRouteButton.widthAnchor  constraintEqualToConstant:44].active = YES;
  
    
    [self.mapView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.mapView.rightAnchor  constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    
    if (_isAllRestons){
        [self.mapView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
        [self.mapView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    } else {
        [self.mapView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
        [self.mapView.bottomAnchor constraintEqualToAnchor:self.descripView.topAnchor].active = YES;
    }
    
    
    
    [self.restNamelable.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:9].active = YES;
    [self.restNamelable.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:5].active = YES;
    [self.restNamelable.rightAnchor constraintEqualToAnchor:self.showRouteButton.leftAnchor constant:-2].active = YES;
    
    if (_isAllRestons){
        [self.openRestButton.leftAnchor constraintEqualToAnchor:self.restNamelable.leftAnchor].active = YES;
        [self.openRestButton.topAnchor constraintEqualToAnchor:self.restNamelable.topAnchor].active = YES;
        [self.openRestButton.bottomAnchor constraintEqualToAnchor:self.restNamelable.bottomAnchor].active = YES;
        [self.openRestButton.rightAnchor constraintEqualToAnchor:self.restNamelable.rightAnchor].active = YES;
    } else {
        [self.openRestButton.leftAnchor constraintEqualToAnchor:self.view.rightAnchor constant:5].active = YES;
        [self.openRestButton.topAnchor constraintEqualToAnchor:self.restNamelable.topAnchor].active = YES;
        [self.openRestButton.bottomAnchor constraintEqualToAnchor:self.restNamelable.bottomAnchor].active = YES;
        [self.openRestButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:5].active = YES;
    }
    
    
    [self.addressView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.addressView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:38].active = YES;
    [self.addressView.rightAnchor constraintEqualToAnchor:self.showRouteButton.leftAnchor constant:-2].active = YES;
    //    [self.descripView.heightAnchor  constraintEqualToConstant:self.containerView.bounds.size.height * 0.59].active = YES;
    
    
    [self.restType.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: 10].active = YES;
    [self.restType.rightAnchor constraintEqualToAnchor:self.addressView.rightAnchor ].active = YES;
    [self.restType.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
    
    
    [self.ratingView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:10].active = YES;
    [self.ratingView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:64].active = YES;
    [self.ratingView.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingView.heightAnchor constraintEqualToConstant:24].active = YES;
    
    
    
    [self.ratingImage.centerXAnchor constraintEqualToAnchor:self.ratingView.centerXAnchor].active = YES;
    [self.ratingImage.centerYAnchor constraintEqualToAnchor:self.ratingView.centerYAnchor].active = YES;
    [self.ratingImage.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingImage.heightAnchor constraintEqualToConstant:19].active = YES;
    
    [self.commentView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.commentView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:91].active = YES;
    [self.commentView.widthAnchor constraintEqualToConstant: screenWidth * 0.5].active = YES;
    [self.commentView.heightAnchor constraintEqualToConstant:28].active = YES;
    
    [self.commentsIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 12].active = YES;
    [self.commentsIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.commentsIcon.widthAnchor constraintEqualToConstant:17].active = YES;
    [self.commentsIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    
    
    [self.commentsCount.leftAnchor constraintEqualToAnchor:self.commentsIcon.rightAnchor constant: 5].active = YES;
    [self.commentsCount.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.avgIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 68].active = YES;
    [self.avgIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.avgIcon.widthAnchor constraintEqualToConstant:17].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    
    [self.averageLabel.leftAnchor constraintEqualToAnchor:self.avgIcon.rightAnchor constant: 5].active = YES;
    [self.averageLabel.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 132].active = YES;
    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.distansIcon.widthAnchor constraintEqualToConstant:18].active = YES;
    [self.distansIcon.heightAnchor constraintEqualToConstant:16].active = YES;
    
    
    [self.distanceRest.leftAnchor constraintEqualToAnchor:self.distansIcon.rightAnchor constant: 2].active = YES;
    [self.distanceRest.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor].active = YES;
    } else {
        [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
    }
    
    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:80].active = YES;
    [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.37].active = YES;
    [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
//    [self setupValuesView];
    if (_isAllRestons == NO){
        
        [self setupValuesView];
        
    }
    
}


-(void) setupView {

    self.mapView.translatesAutoresizingMaskIntoConstraints = false;
    self.openRestButton.translatesAutoresizingMaskIntoConstraints = false;
    self.addressView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.ratingView.translatesAutoresizingMaskIntoConstraints = false;
    self.commentView.translatesAutoresizingMaskIntoConstraints = false;
    self.commentsIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.avgIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.distansIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    self.restNamelable.translatesAutoresizingMaskIntoConstraints = false;
    self.restType.translatesAutoresizingMaskIntoConstraints = false;
    
    self.ratingImage.translatesAutoresizingMaskIntoConstraints = false;
    self.commentsCount.translatesAutoresizingMaskIntoConstraints = false;
    self.averageLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.distanceRest.translatesAutoresizingMaskIntoConstraints = false;
    self.showRouteButton.translatesAutoresizingMaskIntoConstraints = false;
    
    
   
    self.submitButton.layer.cornerRadius = cornerRadius;
    self.submitButton.layer.masksToBounds = true;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    
    NSString* titleButton = DPLocalizedString(@"reserv_action", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];

    if (isPhone){
        
        [self setupDescripeViewForPhone];
        
    } else {
        
        [self setupDescripeViewForPad];
    }
 
}

-(void) setupValuesView{
    
    NSString* restName = [_restaurant.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    self.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
    [self.restNamelable setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    
    NSString* type = [NSString stringWithFormat:@"%@    ",_restaurant.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",_restaurant.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:addressFontSize];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];
    
    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:addressFontSize];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];
    
    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [self.restType setAttributedText:attributedString1];
    
    
    NSUInteger rating = (int)(([_restaurant.rating doubleValue])*100);
    self.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    self.commentsCount.text = [NSString stringWithFormat:@"%@ %@",_restaurant.reviewCount, DPLocalizedString(@"comments_short", nil)];
    if (_restaurant.avgBill.integerValue == 0) {
        self.averageLabel.text = @"?";
    } else {
        self.averageLabel.text = [NSString stringWithFormat:@"%@ %@",_restaurant.avgBill, DPLocalizedString(@"avg_short", nil)];
    }
    
    CGFloat distancRest = [self getDistanceToRest:_restaurant.coordinate];
    NSLog(@"distanceRest %ld--------------------------------", (long)distancRest);
    NSLog(@"distanceRest %f--------------------------------", (floor(distancRest)));
    NSInteger numKM = (NSInteger)distancRest / 1000;
    NSLog(@" NSInteger numKM  %ld---------------------------------", (long)numKM);
    NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
    NSLog(@" NSInteger numMM  %ld---------------------------------", (long)numMM);
    NSString* strKM = @"";
    if (numKM != 0){
        strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
    }
    NSString* strMM = @"";
    if (numMM != 0){
        strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
    }
    NSString* distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
    
    if ([RNUser sharedInstance].isGeoEnabled && [RNUser sharedInstance].isThisCity == YES && numKM < 100){
        NSLog(@"[RNUser sharedInstance].isGeoEnabled = %hhu [RNUser sharedInstance].isThisCity == %hhu && numKM = %ld",[RNUser sharedInstance].isGeoEnabled, [RNUser sharedInstance].isThisCity, (long)numKM);
        [self.showRouteButton.imageView setTintColor:mainColor];
        [self.showRouteButton setUserInteractionEnabled:YES];
        self.showRouteButton.hidden = NO;
    } else {
        NSLog(@"[RNUser sharedInstance].isGeoEnabled = %hhu [RNUser sharedInstance].isThisCity == %hhu && numKM = %ld",[RNUser sharedInstance].isGeoEnabled, [RNUser sharedInstance].isThisCity, (long)numKM);
        [self.showRouteButton.imageView setTintColor:[UIColor lightGrayColor]];
        [self.showRouteButton setUserInteractionEnabled:NO];
        self.showRouteButton.hidden = YES;
    }
    
    self.distanceRest.text = distanceRest;
    //    self.distanceRest.text = _restaurant.distanceRest;
}

-(NSString*) getRating:(NSUInteger)rating{
    
    NSString* nameRating;
    
    if (rating == 0){
        nameRating = @"rating5_0";
        
    } else if (rating < 19){
        
        nameRating = @"rating5_1";
        
    } else if (rating >= 20 && rating < 40){
        
        nameRating = @"rating5_2";
        
    } else if (rating >= 40 && rating < 60){
        
        nameRating = @"rating5_3";
        
    } else if (rating >= 60 && rating < 80){
        
        nameRating = @"rating5_4";
        
    } else if (rating > 80){
        
        nameRating = @"rating5_5";
        
    } else {
        
        nameRating = @"rating5_0";
        
    }
    NSLog(@"nameRating %@", nameRating);
    return nameRating;
}


@end
