//
//  KitchenViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/4/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "KitchenViewController.h"
#import "FilterViewController.h"
#import "KitchenCell.h"
#import "GetKitchenRequest.h"
#import "GetKitchenResponse.h"
#import "Kitchen.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


static NSString *const kKitchenKey = @"KitchenKey";
NSString *const KitchenChangeNotification = @"KitchenChangeNotification";


@interface KitchenViewController (){
    NSArray<Kitchen*>* kitchens;
    NSArray<NSString*>* strKitchens;
    NSString* currentKitchenName;
    NSMutableArray* selectArr;
    NSMutableArray* sendArr;
    Kitchen* currentKitchen;
    UIColor* mainColor;
    Reachability *_reachability;
}

@end



@implementation KitchenViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.title = DPLocalizedString(@"kitchen_title", nil);
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    
    selectArr = [[NSMutableArray alloc] init];
    sendArr = [[NSMutableArray alloc] init];
    
    if (_filteredKitchens.count > 0){
        
        strKitchens = [_filteredKitchens sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kKitchenKey];
        if (setSelect.count > selectArr.count && (![setSelect.firstObject  isEqualToString: @""])){
            selectArr = [NSMutableArray arrayWithArray:setSelect];
        }
        
        [self.tableView reloadData];
    } else {
        if ([self isInternetConnect]){
            [self loadKitchens];
        }
    }
    
    self.tableView.allowsMultipleSelection = true;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)loadKitchens{
    GetKitchenRequest *request = [[GetKitchenRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    request.city = _currCity;
    NSLog(@"request.city %@", _currCity);
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetKitchenResponse *r = (GetKitchenResponse *)response;
        
        kitchens = [r.kitchens sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            Kitchen *r1 = obj1;
            Kitchen *r2 = obj2;
            
            return [r1.kitchenName compare:r2.kitchenName];
        }];
        
        if (kitchens.count == 0){
            
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        } else {
            
            NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kKitchenKey];
            if (setSelect.count > selectArr.count){
                for (Kitchen* d in kitchens){
                    for(NSString* str in setSelect){
                        if ([d.kitchenName isEqualToString:str]){
                            
                            [selectArr addObject:d];
                        }
                    }
                }
            }
            
            [self.tableView reloadData];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (IBAction)submitButton:(id)sender {
    
    
    //        for (Kitchen* rt in selectArr){
    //            [sendArr addObject:rt.kitchenName];
    for (NSString* kitch in selectArr){
        [sendArr addObject:kitch];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KitchenChangeNotification object:nil userInfo:@{@"kitchen" : sendArr}];
    
    [[NSUserDefaults standardUserDefaults] setObject:sendArr
                                              forKey:kKitchenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //    return kitchens.count;
    return strKitchens.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    KitchenCell* cell = (KitchenCell *)[tableView dequeueReusableCellWithIdentifier:@"KitchenCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"KitchenCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [cell setBackgroundView:selectedBackgroundView];
    
    //     if ([selectArr containsObject:kitchens[indexPath.row]]) {
    if ([selectArr containsObject:strKitchens[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    //
    //    Kitchen* kitchen = kitchens[indexPath.row];
    //
    //    cell.textLabel.text = kitchen.kitchenName;
    
    cell.textLabel.text = strKitchens[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    currentKitchen = kitchens[indexPath.row];
    //    currentKitchenName = currentKitchen.kitchenName;
    
    currentKitchenName = strKitchens[indexPath.row];
    NSLog(@"%@", currentKitchenName);
    
    //    if ([selectArr containsObject:kitchens[indexPath.row]] == NO) {
    //        [selectArr addObject:kitchens[indexPath.row]];
    if ([selectArr containsObject:strKitchens[indexPath.row]] == NO) {
        [selectArr addObject:strKitchens[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    } else {
        //        [selectArr removeObject:kitchens[indexPath.row]];
        [selectArr removeObject:strKitchens[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if ([selectArr containsObject:kitchens[indexPath.row]]) {
    //        [selectArr removeObject:kitchens[indexPath.row]];
    if ([selectArr containsObject:strKitchens[indexPath.row]]) {
        [selectArr removeObject:strKitchens[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    } else {
        //        [selectArr addObject:kitchens[indexPath.row]];
        [selectArr addObject:strKitchens[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end

