//
//  FilterViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "FilterViewController.h"
#import "CatalogRestViewController.h"
#import "RestoTypeTableViewController.h"
#import "DistricTableViewController.h"
#import "MetroTableViewController.h"
#import "FeaturesTableViewController.h"
#import "KitchenViewController.h"

#import "RNSubway.h"
#import "FilterType.h"

#import "NSObject+DPLocalization.h"
#import "define.h"
#import "DPLocalization.h"

#import "Reachability.h"
#import "GetReserveRequest.h"
#import "GetReserveResponse.h"
#import "OrdersViewController.h"

static NSString *const kReservCountKey = @"ReservCountKey";


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


static NSString *const kTypeKey = @"TypeKey";
static NSString *const kDistricArr = @"DistricArr";
static NSString *const kOptionsKey = @"OptionsKey";
static NSString *const kSubwayArr = @"SubwayArr";
static NSString *const kSubwayIDKey = @"SubwayIDKey";
static NSString *const kKitchenKey = @"KitchenKey";


static NSString *const kTypeIDKey = @"TypeIDKey";

NSString *const CityChangeNotification = @"CityChangeNotification";
NSString *const TypeFilterChangeNotification = @"TypeFilterChangeNotification";

@interface FilterViewController ()<UITableViewDelegate, UITableViewDataSource>{
    
    PriceSlider* mySlider;

    NSArray* options;
    NSArray* subwaysID;
    NSArray* kitchenRest;
    NSArray* currentType;
    NSArray* currentDistric;
    NSArray* currentDistricID;
    NSArray* filters;
    
    NSArray* filteredKitchens;
    NSArray* filteredOptions;
    NSArray* filteredTypeRests;
    NSArray* filteredSubways;
    NSArray* subwaysNames;
//    NSArray* filteredDistrics;
    
    FilterType* distric;
    FilterType* metro;
    FilterType* typeRest;
    FilterType* kitchen;
    FilterType* property;
    UIColor* mainColor;
    
    FilterInfo *filter;
    NSArray* arrEmpty;
    
    NSInteger searchAvg;
    Reachability *_reachability;
    UILabel* rightLabel;
    UIImageView* rightImageView;
//    UIBarButtonItem *rightBarButton;
    
    NSInteger reservsCount;
    Boolean isPhone;
    
}

@end

@implementation FilterViewController


@synthesize typeRest = _typeRest;
@synthesize distric = _distric;
@synthesize property = _property;
@synthesize metro = _metro;
@synthesize kitchen = _kitchen;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
//        addressFontSize = 10;
//        restNameFontSize = 14.0;
//        textFontSize = 12.0;
//        buttonFontSize = 17.0;
//        cornerRadius = 3.0;
        isPhone = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
//        addressFontSize = 14;
//        restNameFontSize = 20.0;
//        textFontSize = 16.0;
//        buttonFontSize = 26.0;
//        cornerRadius = 5.0;
        isPhone = NO;
        
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"filter", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];

    
    _openReservBarButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(openReservs)];
    
    [self loadReservs];


    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    arrEmpty = @[@""];
    
    [self defaultFilters];
    
    [self getFilters];
    
    [self setupView];
    
    if (_isClear == YES){
        [self cancelAllFilters];
    }
    
    [self setupTableView];
    
    _filterInfo = [[FilterInfo alloc] init];
    filter = [[FilterInfo alloc] init];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupTypeRest:)
                                                 name:TypeChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupDistricRest:)
                                                 name:DistricChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupOptionsRest:)
                                                 name:OptionsChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupSubwayRest:)
                                                 name:SubwayChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupKitchenRest:)
                                                 name:KitchenChangeNotification
                                               object:nil];
  

}

-(void) setupOpenReservsButton:(NSInteger)reservsCount{
    
    if (reservsCount < 1){
        [_openReservBarButton setEnabled:NO];
        
        UIImage* clearImage = [[UIImage alloc] init];
        [_openReservBarButton setBackgroundImage:clearImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [_openReservBarButton setTitle:@""];
        [_openReservBarButton setTintColor:[UIColor clearColor]];
        
    } else if (reservsCount >= 1){
        UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:15];
        
        UIImage* i = [UIImage imageNamed:@"bookMark"];
        //    i = [i imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        [_openReservBarButton setBackgroundImage:i forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        
        [_openReservBarButton setTitleTextAttributes:@{NSFontAttributeName : typeFont, NSForegroundColorAttributeName : mainColor} forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = _openReservBarButton;
        [_openReservBarButton setEnabled:YES];
        [_openReservBarButton setTitle:[NSString stringWithFormat:@"%ld",reservsCount]];
    }
}

- (void)openReservs{
    
    UIStoryboard* storyboard = [[UIStoryboard alloc] init];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
    }
    
    OrdersViewController * ordersViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrdersViewController"];
    ordersViewController.isMenu = NO;
    
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:ordersViewController];
    
    [self.navigationController presentViewController:navVC animated:NO completion:nil];
    
}


- (void)loadReservs{
    GetReserveRequest *request = [[GetReserveRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetReserveResponse *r = (GetReserveResponse *)response;
        
        reservsCount = r.reserves.count;
        
        [[NSUserDefaults standardUserDefaults] setInteger:reservsCount forKey:kReservCountKey];
        NSLog(@"[[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]  %ld", [[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]);
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self setupOpenReservsButton:reservsCount];
    }];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reservCountChangeNotification:)
                                                 name:OrdersCountChangeNotification
                                               object:nil];
    
}

-(void) reservCountChangeNotification:(NSNotification*) notif{
    NSLog(@"reservCountChangeNotification!!!!++++++");
    NSNumber* reservCountNumber = [notif.userInfo objectForKey:@"reservCount"];
    NSInteger reservsCount = reservCountNumber.integerValue;
    
    NSLog(@"reservCount============= %ld", reservsCount);
    
    [self setupOpenReservsButton:reservsCount];
    
    NSLog(@"reservCount reservCountChangeNotification %@",[NSString stringWithFormat:@"%ld",reservsCount]);
    
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert
                                    ];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) calculateMinAndMaxAvgBillWithArray:(NSArray*)arrRestons{
    
    NSArray* minMaxRestons = [arrRestons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        
        RNRestaurant *r1 = obj1;
        RNRestaurant *r2 = obj2;
        
        return r2.avgBill.integerValue - r1.avgBill.integerValue;
        
    }];
    RNRestaurant *maxRest = [minMaxRestons firstObject];
    self.maxAvgLabel.text = [NSString stringWithFormat:@"%ld", (long)[maxRest.avgBill integerValue]];
    
    RNRestaurant *minRest = [minMaxRestons lastObject];
    self.minAvgLabel.text = [NSString stringWithFormat:@"%ld", (long)[minRest.avgBill integerValue]];
    
    NSInteger minAvgBillVal = [minRest.avgBill integerValue];
    NSInteger maxAvgBillVal = [maxRest.avgBill integerValue];
    
    self.minAvgBill = [minRest.avgBill integerValue];
    self.maxAvgBill = [maxRest.avgBill integerValue];
    
    NSString* numAvgLabelStr = [NSString stringWithFormat:@"%ld - %ld", (long)minAvgBillVal, (long)maxAvgBillVal];
    self.numAverLabel.text = numAvgLabelStr;
    
    self.avgBill = [maxRest.avgBill integerValue] / 2;
    NSString* avgStr = DPLocalizedString(@"average_check", nil);
    self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %ld грн.",avgStr, (long)self.avgBill];
    
    self.priceSlider.minimumValue = self.minAvgBill;
    self.priceSlider.maximumValue = self.maxAvgBill;
}

-(void) getMinAndMaxAvgBill{
    
    NSLog(@"%@ == %@", metro.subtitle, DPLocalizedString(@"metro", nil));
    NSLog(@"%@ == %@", property.subtitle, DPLocalizedString(@"features", nil));
    NSLog(@"%@ == %@", distric.subtitle, DPLocalizedString(@"area", nil));
    NSLog(@"%@ == %@", typeRest.subtitle, DPLocalizedString(@"type_rest", nil));
    NSLog(@"%@ == %@", kitchen.subtitle, DPLocalizedString(@"kitchen", nil));
    
    if ( metro.subtitle == DPLocalizedString(@"metro", nil) &&
        property.subtitle == DPLocalizedString(@"features", nil) &&
        distric.subtitle == DPLocalizedString(@"area", nil) &&
        typeRest.subtitle == DPLocalizedString(@"type_rest", nil) &&
        kitchen.subtitle == DPLocalizedString(@"kitchen", nil)){
        
        NSLog(@"00000000 _allRestons.count %lu", (unsigned long)_allRestons.count);
        [self calculateMinAndMaxAvgBillWithArray:_allRestons];
        
        
//
//        NSArray* minMaxRestons = [_workRestons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//
//
//            RNRestaurant *r1 = obj1;
//            RNRestaurant *r2 = obj2;
//
//            return r2.avgBill.integerValue - r1.avgBill.integerValue;
//
//        }];
//        RNRestaurant *maxRest = [minMaxRestons firstObject];
//        self.maxAvgLabel.text = [NSString stringWithFormat:@"%ld", [maxRest.avgBill integerValue]];
//
//        RNRestaurant *minRest = [minMaxRestons lastObject];
//        self.minAvgLabel.text = [NSString stringWithFormat:@"%ld", [minRest.avgBill integerValue]];
//
//        NSInteger minAvgBillVal = [minRest.avgBill integerValue];
//        NSInteger maxAvgBillVal = [maxRest.avgBill integerValue];
//
//        self.minAvgBill = [minRest.avgBill integerValue];
//        self.maxAvgBill = [maxRest.avgBill integerValue];
//
//        NSString* numAvgLabelStr = [NSString stringWithFormat:@"%ld - %ld", minAvgBillVal, maxAvgBillVal];
//        self.numAverLabel.text = numAvgLabelStr;
//
//        self.avgBill = [maxRest.avgBill integerValue] / 2;
//        NSString* avgStr = DPLocalizedString(@"average_check", nil);
//        self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %ld грн.",avgStr, (long)self.avgBill];
//
//        self.priceSlider.minimumValue = self.minAvgBill;
//        self.priceSlider.maximumValue = self.maxAvgBill;
        
    } else {
        
        NSLog(@"1111111 _workRestons.count %lu", (unsigned long)_workRestons.count);
        
        [self calculateMinAndMaxAvgBillWithArray:_workRestons];
        
        
        
//        NSArray* minMaxRestons = [_workRestons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//
//
//            RNRestaurant *r1 = obj1;
//            RNRestaurant *r2 = obj2;
//
//            return r2.avgBill.integerValue - r1.avgBill.integerValue;
//
//        }];
//        RNRestaurant *maxRest = [minMaxRestons firstObject];
//        self.maxAvgLabel.text = [NSString stringWithFormat:@"%ld", [maxRest.avgBill integerValue]];
//
//        RNRestaurant *minRest = [minMaxRestons lastObject];
//        self.minAvgLabel.text = [NSString stringWithFormat:@"%ld", [minRest.avgBill integerValue]];
//
//        NSInteger minAvgBillVal = [minRest.avgBill integerValue];
//        NSInteger maxAvgBillVal = [maxRest.avgBill integerValue];
//
//        self.minAvgBill = [minRest.avgBill integerValue];
//        self.maxAvgBill = [maxRest.avgBill integerValue];
//
//        NSString* numAvgLabelStr = [NSString stringWithFormat:@"%ld - %ld", minAvgBillVal, maxAvgBillVal];
//        self.numAverLabel.text = numAvgLabelStr;
//
//        self.avgBill = [maxRest.avgBill integerValue] / 2;
//        NSString* avgStr = DPLocalizedString(@"average_check", nil);
//        self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %ld грн.",avgStr, (long)self.avgBill];
//
//        self.priceSlider.minimumValue = self.minAvgBill;
//        self.priceSlider.maximumValue = self.maxAvgBill;
        
    }
//    self.priceSlider.value = self.avgBill;
}

-(void) getFilters{
    
    filteredKitchens = [self getKitchens];
    filteredTypeRests = [self getTypes];
    filteredOptions = [self getOptions];
    [self getDistricsAll];
    
    
    if (_isCityHasSubway == YES) {
        
        filteredSubways = [self getSubways];
//        filteredDistrics = [self getDistrics];
        filters = @[distric, metro, typeRest, kitchen, property];
        
    } else if (_isCityHasSubway == NO) {
        
        filters = @[typeRest, kitchen, property];
    }

}

-(void) setupFilterTypeRest{

    if (filter.avgBill.length > 0){
        searchAvg = [filter.avgBill integerValue];

    } else {
        searchAvg = 1200;
    }
    
    NSMutableArray *tmp = NSMutableArray.new;
    
//    NSLog(@"_allRestons %ld", _allRestons.count);
//    NSLog(@"_workRestons %ld", _workRestons.count);
    
    for (RNRestaurant *r in _allRestons){
        if(
        
           ([self filterDistricForRest:r] == YES) &&
     
           ![tmp containsObject:r])
        {
            [tmp addObject:r];
        }
        
    }
//    _workRestons = [NSArray arrayWithArray:tmp];
    NSArray* filtArr = [NSArray arrayWithArray:tmp];
    
    [self applyAllFiltersFromArr:filtArr];
}

-(void) applyAllFiltersFromArr:(NSArray*)filtArr{
    
    NSMutableArray *tmp = NSMutableArray.new;
    
    for(RNRestaurant *r in filtArr)
    {
        BOOL isEqualSubway = NO;
        BOOL isEqualType = NO;
        BOOL isEqualKitchen = NO;
        BOOL isEqualOption = NO;
        
        if ([self filterSubwayForRest:r] == NO){
            continue;
        } else {
            isEqualSubway = YES;
        }
        
        if ([self filterTypeForRest:r] == NO){
            continue;
        } else {
            isEqualType = YES;
        }
        
        if ([self filterKitchenForRest:r] == NO){
            continue;
        } else {
            isEqualKitchen = YES;
        }
        
        if ([self filterOptionForRest:r] == NO){
            continue;
        } else {
            isEqualOption = YES;
        }
        
        NSInteger intAvg = [r.avgBill integerValue];
        
        if(
           (intAvg <= searchAvg) &&
           (isEqualSubway == YES) &&
           (isEqualType == YES) &&
           (isEqualKitchen == YES) &&
           (isEqualOption == YES) &&
           ![tmp containsObject:r])
        {
            [tmp addObject:r];
        }
//        NSLog(@"isEqualType == %d",isEqualType);
//        NSLog(@"isEqualKitchen == %d",isEqualKitchen);
//        NSLog(@"isEqualOption %d", isEqualOption);
    }
    
    _workRestons = [NSArray arrayWithArray:tmp];
    self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
    [self getFilters];
    [_tableView reloadData];
}

-(Boolean) filterDistricForRest:(RNRestaurant*)rest{
    
    NSString *strDistric = [NSString stringWithFormat:@"%@",rest.distric];
//    NSLog(@"reston.distric -----------------%@", strDistric);
    
    if (filter.distric.count == 0 || ([[filter.distric firstObject]  isEqual: @""])){
//        NSLog(@"filter.distric.count == 0");
        return YES;
    } else if (filter.distric.count > 0){
//        typeFilter = YES;
//        NSLog(@"filter.distric %@",filter.distric);
        for (NSString* str in filter.distric){
//            NSLog(@"filter.distric *strDistric -----------------%@", str);
            if([strDistric isEqualToString:str]){
                
                return YES;
            }
        }
    }
    return NO;
}

-(Boolean) filterOptionForRest:(RNRestaurant*)rest{
    
    NSArray *arrOptions = rest.options;
    
    if (filter.options.count == 0 || ([[filter.options firstObject]  isEqual: @""])){
        
        return YES;
        
    } else if (filter.options.count > 0){
//        typeFilter = YES;
        
//        NSLog(@"filter.options -----------------%@", filter.options);
        
        for (NSString* str in filter.options){
            for(NSString* strOption in arrOptions){
//                NSLog(@"str -----------------%@", str);
//                NSLog(@"strOption -----------------%@", strOption);
                if([strOption isEqualToString:str]){
                    
                    return YES;
                }
            }
        }
    }
    return NO;
}

-(Boolean) filterKitchenForRest:(RNRestaurant*)rest{
    
    NSArray *arrKitchen = rest.kitchen;
    
    if (filter.kitchen.count == 0 || ([[filter.kitchen firstObject]  isEqual: @""])){
        
        return YES;
        
    } else if (filter.kitchen.count > 0){
//        typeFilter = YES;
        
        for (NSString* str in filter.kitchen){
            for(NSString* strKitchen in arrKitchen){
//                NSLog(@"reston Kitchen -----------------%@", strKitchen);
//                NSLog(@"filter.kitchen -----------------%@", str);
                if([strKitchen isEqualToString:str]){
                    
                    return YES;
                }
            }
        }
    }
    return NO;
}

-(Boolean) filterSubwayForRest:(RNRestaurant*)rest{
    
    NSString *strSubway = rest.subwayName;
//    NSLog(@"r.subwayName %@",rest.subwayName);
    
    
    if (filter.subway.count == 0 || ([[filter.subway firstObject]  isEqual: @""])){
        return YES;
    } else if (filter.subway.count > 0){
//        typeFilter = YES;
        
        for (NSString* str in filter.subway){
            
            if([strSubway isEqualToString:str]){
                
                return YES;
            }
        }
    }
    return NO;
}

-(Boolean) filterTypeForRest:(RNRestaurant*)rest{
    
    
    NSString *strType = rest.type;
    
//    NSLog(@"filter.typeRest.count %ld",(unsigned long)filter.typeRest.count);
    
    if (filter.typeRest.count == 0 || ([[filter.typeRest firstObject]  isEqual: @""])){
        
        return YES;
    } else if (filter.typeRest.count > 0){

        
        for (NSString* tn in filter.typeRest){
            if([tn isEqualToString:strType]){
                
                return YES;
            }
        }
    }
    return NO;
}

-(NSArray*) getDistricsAll{
    NSMutableArray* districs = [NSMutableArray array];
    
//    NSLog(@"_allRestons.count %ld", (unsigned long)_allRestons.count);
    
    for (RNRestaurant* rest in _allRestons){
        
        if ((![rest.distric isEqual:[NSNull null]]) && rest.distric != nil &&(![districs containsObject:rest.distric]) && (![rest.distric isEqualToString:@"0"]) && (![rest.distric isEqualToString:@""])){
            
            [districs addObject:rest.distric];
        }
    }
    
//    NSLog(@"districsALL.count %ld", (unsigned long)districs.count);
    
    return districs;
}

-(NSArray*) getSubways{
    NSMutableArray* subways = [NSMutableArray array];
    
    for (RNRestaurant* rest in _workRestons){
        
        if ((![rest.subwayName isEqual:[NSNull null]]) && rest.subwayName != nil &&(![subways containsObject:rest.subwayName])){
   
            [subways addObject:rest.subwayName];
        }
    }
  
//    NSLog(@"subways.count %ld", (unsigned long)subways.count);
    
    return subways;
}

-(NSArray*) getTypes{
    NSMutableArray* types = [NSMutableArray array];
    
    for (RNRestaurant* rest in _workRestons){
  
                    if ((![rest.type isEqual:[NSNull null]]) && (![types containsObject:rest.type])){
                        [types addObject:rest.type];
                    }
    }

//    NSLog(@"types.count %ld", (unsigned long)types.count);
 
    return types;
}

-(NSArray*) getOptions{
    NSMutableArray* optionsArr = [NSMutableArray array];
    
    for (RNRestaurant* rest in _workRestons){
        NSArray *arrOptions = rest.options;
        
        for(NSString* strOption in arrOptions){
            if ((![strOption isEqual:[NSNull null]]) && ![optionsArr containsObject:strOption]){
                [optionsArr addObject:strOption];
            }
        }
    }
    
//    NSLog(@"options.count %ld", (unsigned long)optionsArr.count);
    return optionsArr;
}


-(NSArray*) getKitchens{
    NSMutableArray* kitchens = [NSMutableArray array];
    
    for (RNRestaurant* rest in _workRestons){
        NSArray *arrKitchens = rest.kitchen;
        
        for(NSString* strKitchen in arrKitchens){
            if ((![strKitchen isEqual:[NSNull null]]) && ![kitchens containsObject:strKitchen]){
                [kitchens addObject:strKitchen];
            }
        }
    }
    
//    NSLog(@"kitchens.count %ld", (unsigned long)kitchens.count);
    return kitchens;
}

//-(void) getAvgBill{
//
//   NSArray* avgRestons = [_workRestons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//
//
//        RNRestaurant *r1 = obj1;
//        RNRestaurant *r2 = obj2;
//
//        return r2.avgBill.integerValue - r1.avgBill.integerValue;
//
//    }];
//    RNRestaurant *firstRest = [avgRestons firstObject];
//
//    filter.avgBill = firstRest.avgBill;
//    NSString* avgStr = DPLocalizedString(@"average_check", nil);
//    self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %@ грн.",avgStr, filter.avgBill];
//}

-(void) defaultFilters{
    
    distric = [[FilterType alloc] initWithTitle:DPLocalizedString(@"area", nil) andSubtitle:DPLocalizedString(@"area", nil)];
    metro = [[FilterType alloc] initWithTitle:DPLocalizedString(@"metro", nil) andSubtitle:DPLocalizedString(@"metro", nil) ];
    typeRest = [[FilterType alloc] initWithTitle:DPLocalizedString(@"type_rest", nil)  andSubtitle:DPLocalizedString(@"type_rest", nil)];
    kitchen = [[FilterType alloc] initWithTitle:DPLocalizedString(@"kitchen", nil) andSubtitle:DPLocalizedString(@"kitchen", nil)];
    property = [[FilterType alloc] initWithTitle:DPLocalizedString(@"features", nil) andSubtitle:DPLocalizedString(@"features", nil) ];

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



-(void) setupTableView{
    kitchenRest = [[NSUserDefaults standardUserDefaults] objectForKey:kKitchenKey];
    
    NSString* kitchenName = [[NSString alloc] init];
    for (NSString* tn in kitchenRest){
        
        if (tn == kitchenRest.firstObject){
            kitchenName = [NSString stringWithFormat:@"%@", tn ];
        } else {
            kitchenName = [NSString stringWithFormat:@"%@, %@", kitchenName,tn ];
        }
        
    }
    
//    NSLog(@"NSString *kitchen %@", kitchenName);
    if (_isClear == YES || kitchenName.length == 0) {
        
        kitchen.subtitle = DPLocalizedString(@"kitchen", nil);
        _isClear = NO;
        
    } else if (kitchenName.length > 0){
        kitchen.subtitle = kitchenName;
        _isClear = NO;
    }
    
    currentType = [[NSUserDefaults standardUserDefaults] objectForKey:kTypeKey];
    
    NSString* restTypeName = [[NSString alloc] init];
    if (currentType != nil && currentType.count > 0){
        NSLog(@"arrTypeNames.count %lu", (unsigned long)currentType.count);
        for (NSString* tn in currentType){
            
            if (tn == currentType.firstObject){
                restTypeName = [NSString stringWithFormat:@"%@", tn ];
                NSLog(@"tn %@", tn);
            } else {
                restTypeName = [NSString stringWithFormat:@"%@, %@", restTypeName,tn ];
            }
        }
    }
//    NSLog(@"type: %@", restTypeName);
    if (_isClear == YES || restTypeName.length == 0) {
        
        typeRest.subtitle = DPLocalizedString(@"type_rest", nil);
        _isClear = NO;
        
    } else if (restTypeName.length > 0){
        typeRest.subtitle = restTypeName;
        _isClear = NO;
    }
    
    currentDistric = [[NSUserDefaults standardUserDefaults] objectForKey:kDistricArr];
    
    NSString* districName = [[NSString alloc] init];
    
    for (NSString* tnp in currentDistric){
        
        
        
        if (tnp == currentDistric.firstObject){
            NSString* tn = [self helperRaions:tnp.integerValue];
            districName = [NSString stringWithFormat:@"%@", tn];
        } else {
            NSString* tn = [self helperRaions:tnp.integerValue];
            districName = [NSString stringWithFormat:@"%@, %@", districName,tn];
        }
    }
//    NSLog(@"NSString *distric %@", districName);
    
    if (_isClear == YES || districName.length == 0) {
        
        distric.subtitle = DPLocalizedString(@"area", nil);
        _isClear = NO;
        
    } else if (districName.length > 0){
        distric.subtitle = districName;
        _isClear = NO;
    }
    
    options = [[NSUserDefaults standardUserDefaults] objectForKey:kOptionsKey];
    NSString* nameOptions = [[NSString alloc] init];
    for (NSString* tn in options){
        
        if (tn == options.firstObject){
            nameOptions = [NSString stringWithFormat:@"%@", tn ];
        } else {
            nameOptions = [NSString stringWithFormat:@"%@, %@", nameOptions,tn ];
        }
    }
    
    if (_isClear == YES || nameOptions.length == 0) {
        
        property.subtitle = DPLocalizedString(@"features", nil);
        _isClear = NO;
        
    } else if (nameOptions.length > 0){
        property.subtitle = nameOptions;
        _isClear = NO;
    }

    
    NSArray* subwaysNam = [[NSUserDefaults standardUserDefaults] objectForKey:kSubwayArr];
    subwaysNames = [NSArray arrayWithArray:subwaysNam];
    NSString* nameSubways = [[NSString alloc] init];
    for (NSString* tn in subwaysNames){
        
        if (tn == subwaysNames.firstObject){
            nameSubways = [NSString stringWithFormat:@"%@", tn ];
        } else {
            nameSubways = [NSString stringWithFormat:@"%@, %@", nameSubways,tn ];
        }
    }
    
    if (_isClear == YES || nameSubways.length == 0) {
        
        metro.subtitle = DPLocalizedString(@"metro", nil);
        _isClear = NO;
        
    } else if (nameSubways.length > 0){
        metro.subtitle = nameSubways;
        _isClear = NO;
    }
}


- (void)setupKitchenRest:(NSNotification*)notif {
    
    kitchenRest = [notif.userInfo objectForKey:@"kitchen"];
    
    NSString* kitchenName = [[NSString alloc] init];
    
    for (NSString* tn in kitchenRest){
        
        if (tn == kitchenRest.firstObject){
            kitchenName = [NSString stringWithFormat:@"%@", tn ];
        } else {
            kitchenName = [NSString stringWithFormat:@"%@, %@", kitchenName,tn ];
        }
    }
//    NSLog(@"type: %@", kitchenName);
    
    if (kitchenName.length > 0){
        _kitchen = kitchenName;
        filter.kitchen = kitchenRest;
         [self setupFilterTypeRest];
        
    } else {
        _kitchen = DPLocalizedString(@"kitchen", nil);
        
        [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                                  forKey:kKitchenKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        filter.kitchen = [NSArray array];
        
        _workRestons = _allRestons;
        self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
        [self getFilters];
        
        [self setupFilterTypeRest];
        
    }

    kitchen.subtitle = _kitchen;
    [self getMinAndMaxAvgBill];
    _isClear = NO;
    
    [_tableView reloadData];
}

- (void)setupTypeRest:(NSNotification*)notif {
    
    currentType = [notif.userInfo objectForKey:@"type"];
    
    NSString* restTypeName = [[NSString alloc] init];
    
    for (NSString* tn in currentType){
        
        if (tn == currentType.firstObject){
        restTypeName = [NSString stringWithFormat:@"%@", tn ];
        } else {
        restTypeName = [NSString stringWithFormat:@"%@, %@", restTypeName,tn ];
        }
    }
//    NSLog(@"type: %@", restTypeName);
    
    if (restTypeName.length > 0){
        _typeRest = restTypeName;
        filter.typeRest = currentType;
         [self setupFilterTypeRest];
       
    } else {
        _typeRest = DPLocalizedString(@"type_rest", nil);
        
        [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                                  forKey:kTypeKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        filter.typeRest = [NSArray array];
        
        _workRestons = _allRestons;
        self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
        [self getFilters];
        
        [self setupFilterTypeRest];
        
    }
    
    _typeRest = [_typeRest stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    _typeRest = [_typeRest stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];

    typeRest.subtitle = _typeRest;
    [self getMinAndMaxAvgBill];
    _isClear = NO;

    [_tableView reloadData];
}


- (void)setupDistricRest:(NSNotification*)notif {
    
    currentDistric = [notif.userInfo objectForKey:@"distric"];

   
    
    NSString* districName = [[NSString alloc] init];
    
    for (NSString* tnp in currentDistric){
        
        if (tnp == currentDistric.firstObject){
            NSString* tn = [self helperRaions:tnp.integerValue];
            districName = [NSString stringWithFormat:@"%@", tn];
        } else {
            NSString* tn = [self helperRaions:tnp.integerValue];
            districName = [NSString stringWithFormat:@"%@, %@", districName,tn ];
        }
    }
    NSLog(@"districName**********************: %@", districName);
    
    if (districName.length > 0){
        _distric = districName;
        filter.distric = currentDistric;
        
        [self setupFilterTypeRest];
    } else {
        _distric = DPLocalizedString(@"area", nil);
        
        [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                                  forKey:kDistricArr];
        [[NSUserDefaults standardUserDefaults] synchronize];
     
        filter.distric = [NSArray array];
        
        _workRestons = _allRestons;
        self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
        [self getFilters];
        
        [self setupFilterTypeRest];
    }
    
    distric.subtitle = _distric;
    
    [self getMinAndMaxAvgBill];
    _isClear = NO;
    
    [_tableView reloadData];
}

- (void)setupOptionsRest:(NSNotification*)notif {
    
    options = [notif.userInfo objectForKey:@"options"];
    
    NSString* optionName = [[NSString alloc] init];
    
    for (NSString* tn in options){
        
        if (tn == options.firstObject){
            optionName = [NSString stringWithFormat:@"%@", tn ];
        } else {
            optionName = [NSString stringWithFormat:@"%@, %@", optionName,tn ];
        }
    }

    if (optionName.length > 0){
        _property = optionName;
        filter.options = options;
         [self setupFilterTypeRest];
    } else {
        _property = DPLocalizedString(@"features", nil);
        
        [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                                  forKey:kOptionsKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        filter.options = [NSArray array];
        
        _workRestons = _allRestons;
        self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
        [self getFilters];
        
        [self setupFilterTypeRest];
    }
    
    property.subtitle = _property;
    [self getMinAndMaxAvgBill];
    _isClear = NO;
    
    [_tableView reloadData];
}

- (void)setupSubwayRest:(NSNotification*)notif {

    subwaysNames = [notif.userInfo objectForKey:@"subway"];
    
    NSString* subwayName = [[NSString alloc] init];
    
    for (NSString* tn in subwaysNames){
        
        if (tn == subwaysNames.firstObject){
            subwayName = [NSString stringWithFormat:@"%@", tn ];
        } else {
            subwayName = [NSString stringWithFormat:@"%@, %@", subwayName,tn];
        }
    }
    
    if (subwayName.length > 0){
        _metro = subwayName;
         filter.subway = subwaysNames;
         [self setupFilterTypeRest];
       
    } else {
        _metro = DPLocalizedString(@"metro", nil);
        
        [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                                  forKey:kSubwayArr];
        [[NSUserDefaults standardUserDefaults] synchronize];

        filter.subway = [NSArray array];
        
        _workRestons = _allRestons;
        self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
        [self getFilters];
        
        [self setupFilterTypeRest];
    }
    
    metro.subtitle = _metro;
    [self getMinAndMaxAvgBill];
    _isClear = NO;
    
    [_tableView reloadData];
    
}


- (void)filtersDefaults {
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kTypeKey:@"",
                                                              kDistricArr:@"",
                                                              kOptionsKey:@"",
                                                              kSubwayArr:@"",
                                                              kKitchenKey:@""
                                                              }];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return filters.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"FilterCell"];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }
    
    FilterType* filterType = filters[indexPath.row];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        cell.textLabel.font = [UIFont systemFontOfSize:24];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    }
  
    
    cell.textLabel.text = filterType.title;
    cell.detailTextLabel.text = filterType.subtitle;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [cell.detailTextLabel sizeToFit];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterType* filterType = filters[indexPath.row];
    NSString* subtStr = filterType.subtitle;
    
    CGFloat height;
    double fontSize;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        height = 50.0f;
        fontSize = 12.0f;
    } else {
        height = 80.0f;
        fontSize = 16.0f;
    }
    
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width*0.8, 16);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = subtStr;
    myTextView.font = [UIFont systemFontOfSize:fontSize];
    CGSize result = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat heightForRow;
    if (result.height > height) {
        heightForRow = result.height;
    } else {
        heightForRow = height;
    }
    return heightForRow;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FilterType* filterType = filters[indexPath.row];
    
    [self openFilterType:filterType];
    
}

-(void) openFilterType:(FilterType*)filterType{
    
    if ([filterType isEqual:distric]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        DistricTableViewController* districVC = [storyboard instantiateViewControllerWithIdentifier:@"DistricTableViewController"];
        districVC.currCity = _currCity;
        districVC.filteredDistrics = currentDistric;
        districVC.allDistrics = [self getDistricsAll];
        
        UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:districVC];
        
        [self presentViewController:navVC animated:YES completion:nil];
    } else if ([filterType isEqual:metro]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        MetroTableViewController* metroVC = [storyboard instantiateViewControllerWithIdentifier:@"MetroTableViewController"];
        metroVC.currCity = _currCity;
        metroVC.filteredSubways = filteredSubways;
        
        UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:metroVC];
        
        [self presentViewController:navVC animated:YES completion:nil];
    } else if ([filterType isEqual:typeRest]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
       RestoTypeTableViewController* typeVC = [storyboard instantiateViewControllerWithIdentifier:@"RestoTypeTableViewController"];
        typeVC.currCity = _currCity;
        typeVC.filteredTypeRests = filteredTypeRests;
        
        UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:typeVC];
        
        [self presentViewController:navVC animated:YES completion:nil];
    } else if ([filterType isEqual:kitchen]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        KitchenViewController* kitchenVC = [storyboard instantiateViewControllerWithIdentifier:@"KitchenViewController"];
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:kitchenVC];
        kitchenVC.currCity = _currCity;
        kitchenVC.filteredKitchens = filteredKitchens;
        
        [self presentViewController:navVC animated:YES completion:nil];
    } else if ([filterType isEqual:property]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FeaturesTableViewController* featuresVC = [storyboard instantiateViewControllerWithIdentifier:@"FeaturesTableViewController"];
        featuresVC.currCity = _currCity;
        featuresVC.filteredOptions = filteredOptions;
        
        UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:featuresVC];
        
        [self presentViewController:navVC animated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) cancelAllFilters{
    
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kSubwayArr];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kKitchenKey];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kDistricArr];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kTypeKey];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kOptionsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    options = [NSArray array];
    subwaysNames = [NSArray array];
    kitchenRest = [NSArray array];
    currentType = [NSArray array];
    currentDistric = [NSArray array];
    
    filter.distric = nil;
    filter.options = nil;
    filter.kitchen = nil;
    filter.subway = nil;
    filter.typeRest = nil;
    
    [self filtersDefaults];
    
    _isClear = YES;
    kitchen.subtitle = DPLocalizedString(@"kitchen", nil);
    property.subtitle = DPLocalizedString(@"features", nil);
    metro.subtitle = DPLocalizedString(@"metro", nil);
    distric.subtitle = DPLocalizedString(@"area", nil);
    typeRest.subtitle = DPLocalizedString(@"type_rest", nil);
    
    _workRestons = _allRestons;
//    NSLog(@"_workRestons1111 %ld", _workRestons.count);
    
    self.findLabel.text = [NSString stringWithFormat:@"%@ %lu", DPLocalizedString(@"find", nil), (unsigned long)_workRestons.count];
    [self getFilters];
    [self getMinAndMaxAvgBill];

    [_tableView reloadData];

}


- (IBAction)cancelAction:(id)sender {
    [self cancelAllFilters];
}

- (IBAction)sliderValueChanged:(id)sender {
    
    NSInteger val = (int)(self.priceSlider.value);
    NSString* avgStr = DPLocalizedString(@"average_check", nil);
    
        self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %ld грн.",avgStr, (long)val];
    filter.avgBill = [NSString stringWithFormat:@"%f", self.priceSlider.value + 50];
    _avgBill = self.priceSlider.value;
    [self setupFilterTypeRest];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (IBAction)backButton:(id)sender {
    
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submitButton:(id)sender {
    
    
    NSArray* currentTypeName = [[NSArray alloc] init];
    if (currentType.count > 0){
        currentTypeName = currentType;
    } else {
      currentTypeName = [[NSUserDefaults standardUserDefaults] objectForKey:kTypeKey];
    }
    
    NSArray* currentDistricId = [[NSArray alloc] init];
    if (currentDistric.count > 0){
        currentDistricId = currentDistric;
        NSLog(@"currentDistricID %@", currentDistric);
    } else {
        currentDistricId = [[NSUserDefaults standardUserDefaults] objectForKey:kDistricArr];
        NSLog(@"kDistricKey %@", currentDistricId);
    }
    
    NSArray* currentkitchenRest = [[NSArray alloc] init];
    if (kitchenRest.count > 0){
        currentkitchenRest = kitchenRest;
    } else {
        currentkitchenRest = [[NSUserDefaults standardUserDefaults] objectForKey:kKitchenKey];
    }
    
    NSArray* currentSubwayID = [[NSArray alloc] init];
    if (subwaysNames.count > 0){
        currentSubwayID = subwaysNames;
    } else {
        currentSubwayID = [[NSUserDefaults standardUserDefaults] objectForKey:kSubwayArr];
    }
    
    NSArray* currentOptionRest = [[NSArray alloc] init];
    if (options.count > 0){
        currentOptionRest = options;
    } else {
        currentOptionRest = [[NSUserDefaults standardUserDefaults] objectForKey:kOptionsKey];
    }
//
//    _filterInfo.typeRest = currentTypeName;
//    NSLog(@"_filterInfo.typeRest %@", _filterInfo.typeRest);
    _filterInfo.avgBill = [NSString stringWithFormat:@"%ld", (long)_avgBill];
    _filterInfo.workRestons = _workRestons;
//    NSLog(@"_filterInfo.avgBill %@", _filterInfo.avgBill);
//    _filterInfo.distric = currentDistricId;
//    NSLog(@"_filterInfo.distric %@", _filterInfo.distric);
//    _filterInfo.kitchen = currentkitchenRest;
//    NSLog(@"_filterInfo.kitchen %@", _filterInfo.kitchen);
//    _filterInfo.subway = currentSubwayID;
//    NSLog(@"_filterInfo.subway %@", _filterInfo.subway);
//    _filterInfo.options = currentOptionRest;
//    NSLog(@"_filterInfo.options %@", _filterInfo.options);
  
    [[NSNotificationCenter defaultCenter] postNotificationName:TypeFilterChangeNotification object:nil userInfo:@{@"typeFilter" : _filterInfo}];
//    [[NSNotificationCenter defaultCenter] postNotificationName:TypeFilterChangeNotification object:nil userInfo:@{@"typeFilter" : dicInfo}];
//    [[NSNotificationCenter defaultCenter] postNotificationName:TypeFilterChangeNotification object:nil userInfo:@{@"typeFilter" : _workRestons}];
    
   [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

-(void) setupView {
    
    self.submitButton.layer.cornerRadius = 3;
    self.submitButton.layer.masksToBounds = true;
    
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:DPLocalizedString(@"apply", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];

    self.bookMarkImage.translatesAutoresizingMaskIntoConstraints = false;
    self.bookMarkCount.translatesAutoresizingMaskIntoConstraints = false;

    self.tableView.translatesAutoresizingMaskIntoConstraints = false;
    self.setParamlabel.translatesAutoresizingMaskIntoConstraints = false;
    self.averLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.numAverLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.priceSlider.translatesAutoresizingMaskIntoConstraints = false;
    self.averagePriceLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.cancelButton.translatesAutoresizingMaskIntoConstraints = false;
    self.findLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.sliderView.translatesAutoresizingMaskIntoConstraints = false;
    self.minAvgLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.maxAvgLabel.translatesAutoresizingMaskIntoConstraints = false;
//
    CGFloat screenWidth =  [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

  

    
    [self.sliderView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.sliderView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.sliderView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
    if (screenWidth < 322) {
        [self.sliderView.heightAnchor constraintEqualToConstant:screenHeight * 0.33].active = YES;
    } else {
    [self.sliderView.heightAnchor constraintEqualToConstant:screenHeight * 0.36].active = YES;
    }
    

    if (screenWidth < 322) {
        [self.setParamlabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.setParamlabel.centerXAnchor constraintEqualToAnchor:self.sliderView.centerXAnchor].active = YES;
    [self.setParamlabel.centerYAnchor constraintEqualToAnchor:self.sliderView.topAnchor constant:screenHeight * 0.06].active = YES;
    [self.setParamlabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    [self.setParamlabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [self.averLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.averLabel.centerXAnchor constraintEqualToAnchor:self.sliderView.centerXAnchor].active = YES;
    [self.averLabel.centerYAnchor constraintEqualToAnchor:self.sliderView.topAnchor constant:screenHeight * 0.12].active = YES;
    //    [self.averLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.averLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    NSString* numAvgLabelStr = [NSString stringWithFormat:@"%ld - %ld", self.minAvgBill, self.maxAvgBill];
    self.numAverLabel.text = numAvgLabelStr;
    [self.numAverLabel.centerXAnchor constraintEqualToAnchor:self.sliderView.centerXAnchor].active = YES;
    [self.numAverLabel.topAnchor constraintEqualToAnchor:self.averLabel.bottomAnchor].active = YES;
    //    [self.numAverLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.numAverLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    self.minAvgLabel.text = [NSString stringWithFormat:@"%ld", self.minAvgBill];
    [self.minAvgLabel.leftAnchor constraintEqualToAnchor:self.sliderView.leftAnchor constant:3].active = YES;
    if (screenWidth < 322) {
        [self.minAvgLabel.centerYAnchor constraintEqualToAnchor:self.sliderView.topAnchor constant:screenHeight * 0.2].active = YES;
    } else {
        [self.minAvgLabel.centerYAnchor constraintEqualToAnchor:self.sliderView.topAnchor constant:screenHeight * 0.22].active = YES;
    }
   
    
    self.maxAvgLabel.text = [NSString stringWithFormat:@"%ld", self.maxAvgBill];
    [self.maxAvgLabel.rightAnchor constraintEqualToAnchor:self.sliderView.rightAnchor constant:-3].active = YES;
    [self.maxAvgLabel.centerYAnchor constraintEqualToAnchor:self.minAvgLabel.centerYAnchor].active = YES;
    
    
    
    self.priceSlider.minimumValue = self.minAvgBill;
    self.priceSlider.maximumValue = self.maxAvgBill;
    
    [self.priceSlider.leftAnchor constraintEqualToAnchor:self.minAvgLabel.rightAnchor].active = YES;
    [self.priceSlider.centerYAnchor constraintEqualToAnchor:self.minAvgLabel.centerYAnchor].active = YES;
    [self.priceSlider.rightAnchor constraintEqualToAnchor:self.maxAvgLabel.leftAnchor].active = YES;
    [self.priceSlider.heightAnchor constraintEqualToConstant:28].active = YES;
    

    
    if (screenWidth < 322) {
        [self.averagePriceLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    NSString* avgStr = DPLocalizedString(@"average_check", nil);
    self.averagePriceLabel.text = [NSString stringWithFormat:@"%@ < %ld грн.",avgStr, (long)self.avgBill];

    [self.averagePriceLabel.centerXAnchor constraintEqualToAnchor:self.sliderView.centerXAnchor].active = YES;
    [self.averagePriceLabel.topAnchor constraintEqualToAnchor:self.priceSlider.bottomAnchor].active = YES;
    
    [self.tableView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.tableView.topAnchor constraintEqualToAnchor:self.sliderView.bottomAnchor].active = YES;
    [self.tableView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        if (_isCityHasSubway == YES) {
            
            [self.tableView.heightAnchor constraintEqualToConstant:250].active = YES;
        } else if (_isCityHasSubway == NO) {
            
            [self.tableView.heightAnchor constraintEqualToConstant:150].active = YES;
        }
        
        [self.minAvgLabel.widthAnchor constraintEqualToConstant:36].active = YES;
        [self.minAvgLabel.heightAnchor constraintEqualToConstant:17].active = YES;
        
        [self.maxAvgLabel.widthAnchor constraintEqualToConstant:36].active = YES;
        [self.maxAvgLabel.heightAnchor constraintEqualToConstant:17].active = YES;
        
        [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
        [self.findLabel.widthAnchor constraintEqualToConstant:106].active = YES;
        [self.findLabel.heightAnchor constraintEqualToConstant:21].active = YES;
        [self.cancelButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.cancelButton.heightAnchor constraintEqualToConstant:screenHeight * 0.04].active = YES;
        
    } else {
        if (_isCityHasSubway == YES) {
            
            [self.tableView.heightAnchor constraintEqualToConstant:400].active = YES;
        } else if (_isCityHasSubway == NO) {
            
            [self.tableView.heightAnchor constraintEqualToConstant:240].active = YES;
        }
        
        [self.cancelButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [self.cancelButton.widthAnchor constraintEqualToConstant:220].active = YES;
        [self.cancelButton.heightAnchor constraintEqualToConstant:screenHeight * 0.04].active = YES;
        
        [self.minAvgLabel.widthAnchor constraintEqualToConstant:66].active = YES;
        [self.minAvgLabel.heightAnchor constraintEqualToConstant:27].active = YES;
        
        [self.maxAvgLabel.widthAnchor constraintEqualToConstant:66].active = YES;
        [self.maxAvgLabel.heightAnchor constraintEqualToConstant:27].active = YES;
        
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:26.0]];
        [self.findLabel setFont:[UIFont fontWithName:@"Thonburi" size:20.0]];
        [self.averLabel setFont:[UIFont fontWithName:@"Thonburi" size:24.0]];
        [self.setParamlabel setFont:[UIFont fontWithName:@"Thonburi" size:24.0]];
        [self.averagePriceLabel setFont:[UIFont fontWithName:@"Thonburi" size:20.0]];
        [self.minAvgLabel setFont:[UIFont fontWithName:@"Thonburi" size:20.0]];
        [self.maxAvgLabel setFont:[UIFont fontWithName:@"Thonburi" size:20.0]];
        [self.numAverLabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
        
        [self.submitButton.widthAnchor constraintEqualToConstant:240].active = YES;
        [self.submitButton.heightAnchor constraintEqualToConstant:71].active = YES;
        
        [self.findLabel.widthAnchor constraintEqualToConstant:200].active = YES;
        [self.findLabel.heightAnchor constraintEqualToConstant:21].active = YES;
    }
   
    
    if (screenWidth < 322) {
        [self.cancelButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:10.0]];
    }
    
    [self.cancelButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.cancelButton.bottomAnchor constraintEqualToAnchor:self.sliderView.bottomAnchor
                                                    constant:-5].active = YES;
    
    
    self.findLabel.text = [NSString stringWithFormat:@"%@ %ld", DPLocalizedString(@"find", nil), _workRestons.count];
    [self.findLabel.leftAnchor constraintEqualToAnchor:self.sliderView.leftAnchor constant:8].active = YES;
    [self.findLabel.bottomAnchor constraintEqualToAnchor:self.sliderView.bottomAnchor
                                                   constant:-5].active = YES;
   
    
    
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self.submitButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    
    if (screenWidth < 322) {
        [self.submitButton.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor
                                                        constant:-(screenHeight * 0.06)].active = YES;
    } else {
    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor
                                                    constant:-(screenHeight * 0.11)].active = YES;
}
   
    
}

-(NSString*) helperRaions:(NSUInteger)raion{
    
    switch (raion) {
        case 1:
            return  DPLocalizedString(@"1", nil);
            
        case 2:
            return  DPLocalizedString(@"2", nil);
            
        case 3:
            return  DPLocalizedString(@"3", nil);
            
        case 4:
            return  DPLocalizedString(@"4", nil);
            
        case 5:
            return  DPLocalizedString(@"5", nil);
            
        case 6:
            return  DPLocalizedString(@"6", nil);
            
        case 7:
            return  DPLocalizedString(@"7", nil);
            
        case 8:
            return  DPLocalizedString(@"8", nil);
            
        case 9:
            return  DPLocalizedString(@"9", nil);
            
        case 10:
            return  DPLocalizedString(@"10", nil);
            
        default:
            return [NSString stringWithFormat:@""];
    }
}


@end
