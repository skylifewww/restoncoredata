//
//  OfferTableViewCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "OfferTableViewCell.h"

@implementation OfferTableViewCell

+(NSString*)cellIdentifier{
    static NSString* cellIdentifier;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellIdentifier = @"Content2";
    });
    return cellIdentifier;
}

+ (CGFloat)heightForText:(NSString *)text{
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, 85);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = text;
    myTextView.font = [UIFont systemFontOfSize:11];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > 85 ? sz.height : 85;
    
    return result + 55;
}

- (IBAction)purchaseButtonDidTap:(id)sender
{
    //    [_delegate ExampleTableViewCellDidTapPurchaseButton:self];
}

@end
