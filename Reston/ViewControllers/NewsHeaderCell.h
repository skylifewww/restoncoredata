//
//  NewsHeaderCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNNEw.h"
//extern NSString *const kNewsHeaderCellId;
extern const CGFloat kNewsHeaderCellHeight;



@interface NewsHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (weak, nonatomic) IBOutlet UILabel *headerDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *headerShortText;

- (void)applyNew:(RNNEw *)model;
+ (CGFloat)heightForText:(NSString *)text;

@end
