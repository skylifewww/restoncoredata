//
//  RestCollectionCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 08.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface RestCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *descripView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UIView *addressView;

@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIImageView *commentsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *avgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *distansIcon;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *restNamelable;
@property (weak, nonatomic) IBOutlet UILabel *restType;

@property (weak, nonatomic) IBOutlet UIImageView *ratingImage;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;

@property (weak, nonatomic) IBOutlet UILabel *distanceRest;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;

@property (weak, nonatomic) IBOutlet UIImageView *restImage;

@property (weak, nonatomic) IBOutlet UIImageView *discountBack;
@property (weak, nonatomic) IBOutlet UILabel *restDiscount;
@property (weak, nonatomic) IBOutlet UIButton *callButton;


@end

