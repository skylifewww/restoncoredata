//
//  MetroTableViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterInfo.h"

extern NSString *const SubwayChangeNotification;

@interface MetroTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *metroTableView;
@property (strong, nonatomic) NSString* currCity;

@property (strong, nonatomic) FilterInfo* filterInfo;
@property (strong, nonatomic) NSArray* filteredSubways;

@end
