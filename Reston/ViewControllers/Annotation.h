//
//  Annotation.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/25/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface Annotation : NSObject <MKAnnotation>

- (id)initWithDictionary:(NSDictionary <NSString *, id> *)dictionary;

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subtitle;
@property(nonatomic,assign) NSNumber* restID;
@property(nonatomic,assign) NSInteger index;
@property(nonatomic,assign) Boolean isSelected;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location title:(NSString *)title subtitle:(NSString *)subtitle restID:(NSNumber*)restID index:(NSInteger)index isSelected:(Boolean)isSelected;

@end
