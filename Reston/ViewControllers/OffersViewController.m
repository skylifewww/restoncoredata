//
//  OffersViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "OffersViewController.h"
#import "GetOffersRequest.h"
#import "GetOffersResponse.h"
#import "Offer.h"
#import "UITableView+Registration.h"
#import "UIImage+Cache.h"
#import "OfferTableViewCell.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "ReservTableViewController.h"
#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end



@interface OffersViewController () <HVTableViewDataSource, HVTableViewDelegate> {
    //ExampleTableViewCellDelegate
    
    UIView* containerView;
    UILabel* _textFavlabel;
    UIImageView* _backFavIcon;
    UIStoryboard* storyboard;
    Reachability *_reachability;
    UIColor* mainColor;
    Boolean isPhone;
    double addHeight;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    double typeFontSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    double estimateHeight;
}
@property (nonatomic, copy) NSArray *offers;
@property (weak, nonatomic) IBOutlet UIButton *resrvedButton;

@end


@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        typeFontSize = 10.0;
        addHeight = 40;
        restNameFontSize = 14.0;
        textFontSize = 12.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
        isPhone = YES;
        estimateHeight = 144;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        typeFontSize = 16.0;
        addHeight = 100;
        restNameFontSize = 22.0;
        textFontSize = 16.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
        estimateHeight = 240;
    }
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    self.tableView.HVTableViewDelegate = self;
    self.tableView.HVTableViewDataSource = self;
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    if ([self isInternetConnect]){

        [self loadOffers:_restaurantID];
    }
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = estimateHeight;
    
    NSString* titleReservButton = DPLocalizedString(@"reserv_action", nil);
    [self.resrvedButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.resrvedButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleReservButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.resrvedButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.resrvedButton.layer.cornerRadius = cornerRadius;
    self.resrvedButton.layer.masksToBounds = true;
    
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)reservButtonAction:(id)sender {
  
    
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = _restaurant;
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}


- (void)loadOffers:(NSNumber *)ident {
    GetOffersRequest *request = [[GetOffersRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetOffersResponse *r = (GetOffersResponse *)response;
        _offers = r.offers;
        
        if (_offers.count == 0){
            [self setupView];
        } else {
            _offers = [_offers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                Offer *n1 = obj1;
                Offer *n2 = obj2;
                
                return -1*[n1.dateStart compare:n2.dateStart];
            }];
            
            [_tableView reloadData];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
//
//
//-(void)ExampleTableViewCellDidTapPurchaseButton:(ExampleTableViewCell *)cell
//{
//    NSString *alertTitle = [NSString stringWithFormat:@"'%@' Purchased Successfully.", cell.titlesLabel.text];
//    UIAlertController* purchaseAlert = [UIAlertController alertControllerWithTitle:nil message: alertTitle preferredStyle:UIAlertControllerStyleAlert];
//    [self presentViewController:purchaseAlert animated:YES completion:nil];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [purchaseAlert dismissViewControllerAnimated:YES completion:nil];
//    });
//}


#pragma mark HVTableViewDatasource
-(void)tableView:(UITableView *)tableView expandCell:(OfferTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    //    cell.purchaseButton.alpha = 0;
    
    [UIView animateWithDuration:.5 animations:^{
        
        Offer* offer = [_offers objectAtIndex:indexPath.row];
//        
        NSString* text = [NSString stringWithFormat:@"%@",offer.text];
        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        text = [text stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
//        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&lt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"strong&gt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"p&gt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
//        text = [text stringByReplacingOccurrencesOfString:@"/" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@":&nbsp;" withString:@":"];
//        
//        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        cell.detailLabel.text = text;
        [cell.detailLabel sizeToFit];
        
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        //        cell.purchaseButton.alpha = 1;
        cell.arrow.transform = CGAffineTransformMakeRotation(0);
    }];
    
}

-(void)tableView:(UITableView *)tableView collapseCell:(OfferTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    
    cell.arrow.transform = CGAffineTransformMakeRotation(0);
    
    
    [UIView animateWithDuration:.5 animations:^{
        cell.arrow.transform = CGAffineTransformMakeRotation(-M_PI+0.00);
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _offers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    OfferTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:[OfferTableViewCell cellIdentifier]];
    //    cell.delegate = self;
    
    if (indexPath.row %2 ==1)
        cell.backgroundColor = [UIColor colorWithRed:.96 green:.96 blue:.96 alpha:1];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    Offer* offer = [_offers objectAtIndex:indexPath.row];
    
    NSString* dateStart = [NSString stringWithFormat:@"%@",offer.dateStart];
    dateStart = [dateStart stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
    
    
    
    
    NSString* dateEnd = [NSString stringWithFormat:@"%@",offer.dateEnd];
    dateEnd = [dateEnd stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
    dateStart = [NSString stringWithFormat:@"%@ %@ по %@",DPLocalizedString(@"action_from", nil), dateStart, dateEnd];
    cell.dateStartLabel.text = dateStart;
//    cell.dateEndLabel.text = dateEnd;
    
    NSString* title = [NSString stringWithFormat:@"%@",offer.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
    cell.titlesLabel.text = title;
    
    [cell.titlesLabel sizeToFit];
    NSString* strImage = [offer.image stringByReplacingOccurrencesOfString:@"https://reston.com.ua/uploads/img/offers/" withString:@""];
    
    
    NSLog(@"strImage %@",strImage);
    
    [UIImage cachedImage:strImage
     
     fullURLString:@"https://reston.com.ua/uploads/img/offers/"
            withCallBack:^(UIImage *image) {
                
                if (image ==nil) {
                    [cell.theImageView removeFromSuperview];
                    return;
                }
                
                cell.theImageView.image = image;
            }];
    cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if (!isExpanded) {
        
        NSString* text = [NSString stringWithFormat:@"%@",offer.text];
        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        text = [text stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
//    NSAttributedString* textCon = [[NSAttributedString alloc] initWithData:[textHTML dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//        
//        NSString* text = [NSString stringWithFormat:@"%@", textCon];
    
//        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
//        text = [text stringByReplacingOccurrencesOfString:@"&lt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"strong&gt;" withString:@""];
//         text = [text stringByReplacingOccurrencesOfString:@"&nbsp" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&nb" withString:@":"];
//        text = [text stringByReplacingOccurrencesOfString:@"p&gt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"/" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text substringToIndex:NSMaxRange([text rangeOfComposedCharacterSequenceAtIndex:150])];
        cell.detailLabel.text = text;
        [cell.detailLabel sizeToFit];
        cell.arrow.transform = CGAffineTransformMakeRotation(M_PI);
        //        cell.purchaseButton.alpha = 0;
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    else
    {
        NSString* text = [NSString stringWithFormat:@"%@",offer.text];
        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        text = [text stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
//        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&lt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"strong&gt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&nbsp" withString:@":"];
//        text = [text stringByReplacingOccurrencesOfString:@"p&gt;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
//        text = [text stringByReplacingOccurrencesOfString:@"/" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
//        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        cell.detailLabel.text = text;
        
        [cell.detailLabel sizeToFit];
        cell.arrow.transform = CGAffineTransformMakeRotation(0);
        //        cell.purchaseButton.alpha = 1;
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return cell;
}

- (NSString *)stringByStrippingHTML:(NSString *)inputString
{
    NSMutableString *outString;
    
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        
        if ([inputString length] > 0)
        {
            NSRange r;
            
            while ((r = [outString rangeOfString:@"<[^>]+>|&nbsp;" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    
    return outString; 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    Offer* offer = [_offers objectAtIndex:indexPath.row];
    
    if (isexpanded){
        
        CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
        CGRect frame = CGRectMake(0, 0, width*0.6, 28);
        UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
        myTextView.text = offer.text;
        myTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
        CGSize result = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
        
        return result.height + addHeight;
        
        
    } else {
        
        return estimateHeight;
    }
}

-(void) setupView {
  
    containerView = [[UIView alloc] initWithFrame:_tableView.frame];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    
    containerView.translatesAutoresizingMaskIntoConstraints = false;
    
    _backFavIcon = [[UIImageView alloc] init];
    _backFavIcon.image = [UIImage imageNamed:@"news"];
    _backFavIcon.contentMode = UIViewContentModeScaleAspectFit;
    [containerView addSubview:_backFavIcon];
    _backFavIcon.translatesAutoresizingMaskIntoConstraints = false;
    
    
    
    _textFavlabel = [[UILabel alloc] init];
    _textFavlabel.text = DPLocalizedString(@"no_offers", nil);
    _textFavlabel.textColor = [UIColor lightGrayColor];
    _textFavlabel.numberOfLines = 2;
    _textFavlabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:_textFavlabel];
    
    
    [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:restNameFontSize]];
    _textFavlabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [containerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [containerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [containerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
    [containerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    [_backFavIcon.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_backFavIcon.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.43].active = YES;
    [_backFavIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.42].active = YES;
    [_backFavIcon.heightAnchor constraintEqualToConstant:screenHeight * 0.23].active = YES;
    
    if (screenWidth < 322) {
        [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [_textFavlabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_textFavlabel.topAnchor constraintEqualToAnchor:_backFavIcon.bottomAnchor constant:60].active = YES;
    [_textFavlabel.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    [_textFavlabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
}


@end
