//
//  MetroTableViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "MetroTableViewController.h"
#import "UIWindow+LoadingIndicator.h"
#import "UITableView+Registration.h"
#import "RNCalendarViewController.h"
#import "RNDatePickerView.h"
#import "RNEntityPickerView.h"
#import "RNOrderInfo.h"
#import "RNSubway.h"
#import "RNSearchByWordRequest.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "RNGetSubwaysRequest.h"
#import "RNGetSubwaysResponse.h"
#import "RNCountPeopleHelper.h"
#import "MetroCell.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end



static NSString *const kSubwayArr = @"SubwayArr";
static NSString *const kSubwayIDKey = @"SubwayIDKey";
NSString *const SubwayChangeNotification = @"SubwayChangeNotification";
@interface MetroTableViewController ()<UISearchBarDelegate>
{
    RNSubway* currentMetro;
    NSString* currentMetroName;
    NSString* currentMetroID;
    BOOL loading;
    NSMutableArray* selectArr;
    NSMutableArray* sendArr;
    NSArray* langAlhabet;
    //    NSMutableDictionary* sendDict;
    NSMutableArray* arrSegments;
    NSArray* subways;
    NSArray *subwaysWork;
    UIColor* mainColor;
    NSArray<NSString*>* strSubways;
    
    Reachability *_reachability;
}

//@property (nonatomic, copy) NSArray<RNSubway*> *objSubways;
@property (nonatomic, strong) RNOrderInfo *orderInfo;


@end



@implementation MetroTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"metro_title", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langAlhabet = @[@"А",@"Б",@"В",@"Г",@"Д",@"Е",@"Ж",@"З",@"И",@"К",@"Л",@"М",@"Н",@"О",@"П",@"Р",@"С",@"Т",@"У",@"Ф",@"Х",@"Ц",@"Ч",@"Ш",@"Щ",@"Э",@"Ю",@"Я"];
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langAlhabet = @[@"А",@"Б",@"В",@"Г",@"Д",@"Е",@"Ж",@"З",@"И",@"К",@"Л",@"М",@"Н",@"О",@"П",@"Р",@"С",@"Т",@"У",@"Ф",@"Х",@"Ц",@"Ч",@"Ш",@"Щ",@"Э",@"Ю",@"Я"];
    }
    
    
    selectArr = [[NSMutableArray alloc] init];
    sendArr = [[NSMutableArray alloc] init];
    arrSegments = [[NSMutableArray alloc] init];
    //    sendDict  =[[NSMutableDictionary alloc] init];
    
    if (_filteredSubways.count > 0){
        
        strSubways = [_filteredSubways sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kSubwayArr];
        if (setSelect.count > selectArr.count && (![setSelect.firstObject  isEqualToString: @""])){
            selectArr = [NSMutableArray arrayWithArray:setSelect];
        }
        
        for (NSString* ch in langAlhabet) {
            NSString *prefix = [ch uppercaseString];
            NSMutableArray *arrayCh = [[NSMutableArray alloc] init];
            //            RNSubway* charSubway = [[RNSubway alloc] init];
            //            charSubway.subwayId = @1111;
            //            charSubway.subwayName = prefix;
            
            [arrayCh addObject:prefix];
            
            for (NSString* s in strSubways){
                
                //                NSString *strName = s.subwayName;
                if ([[s uppercaseString] hasPrefix:prefix]){
                    
                    [arrayCh addObject:s];
                    
                    NSLog(@"prefix  %@",s);
                }
            }
            if (arrayCh.count > 1){
                [arrSegments addObjectsFromArray:arrayCh];
                NSLog(@"arrSegments count  %ld",arrSegments.count);
            }
            subways = arrSegments;
            subwaysWork = arrSegments;
        }
        [_metroTableView reloadData];
    } else {
        if ([self isInternetConnect]){
            [self loadSubway];
        }
    }
    
    self.metroTableView.dataSource = self;
    self.metroTableView.delegate = self;
    self.searchBar.delegate = self;
    self.searchBar.userInteractionEnabled = YES;
    self.searchBar.placeholder = DPLocalizedString(@"search_metro", nil);
    
    _metroTableView.allowsMultipleSelection = true;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)loadSubway{
    //    RNGetSubwaysRequest *request = [[RNGetSubwaysRequest alloc] init];
    //
    //    request.city = _currCity;
    //
    //    NSString* langCode = [[NSString alloc] init];
    //
    //    if ([dp_get_current_language() isEqualToString:@"ru"]){
    //        langCode = @"ru";
    //    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
    //        langCode = @"ua";
    //    }
    //    request.langCode = langCode;
    //
    //    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
    //        if (error){
    //            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
    //        }
    //        RNGetSubwaysResponse *r = (RNGetSubwaysResponse *)response;
    //
    //        _objSubways = [r.subwayArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
    //            RNSubway *r1 = obj1;
    //            RNSubway *r2 = obj2;
    //
    //            return [r1.subwayName compare:r2.subwayName];
    //        }];
    //
    //        if (_objSubways.count == 0){
    //
    //            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
    //        } else {
    //
    //            NSDictionary* dictSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kSubwayKey];
    //            NSArray* setSelect = [dictSelect allValues];
    //            if (setSelect.count > selectArr.count){
    //                for (RNSubway* d in _objSubways){
    //                    for(NSString* str in setSelect){
    //                        if ([d.subwayName isEqualToString:str]){
    //
    //                            [selectArr addObject:d];
    //                        }
    //                    }
    //                }
    //            }
    //
    //            for (NSString* ch in langAlhabet) {
    //                NSString *prefix = [ch uppercaseString];
    //                NSMutableArray *arrayCh = [[NSMutableArray alloc] init];
    //                RNSubway* charSubway = [[RNSubway alloc] init];
    //                charSubway.subwayId = @1111;
    //                charSubway.subwayName = prefix;
    //
    //                [arrayCh addObject:charSubway];
    //
    //                for (RNSubway* s in _objSubways){
    //
    //                    NSString *strName = s.subwayName;
    //                    if ([[(NSString*)strName uppercaseString] hasPrefix:prefix]){
    //
    //                        [arrayCh addObject:s];
    ////                        [arrayCh addObject:s.subwayName];
    //                        NSLog(@"prefix  %@",s.subwayName);
    //                    }
    //                }
    //                if (arrayCh.count > 1){
    //                    [arrSegments addObjectsFromArray:arrayCh];
    //                    NSLog(@"arrSegments count  %ld",arrSegments.count);
    //                }
    //                subways = arrSegments;
    //                subwaysWork = arrSegments;
    //            }
    //            [_metroTableView reloadData];
    //        }
    //    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submitButton:(id)sender {
    
    
    //        for (RNSubway* s in selectArr){
    ////            [sendArr addObject:str];
    //            [sendDict setObject:s.subwayName forKey:s.subwayId];
    for (NSString* s in selectArr){
        [sendArr addObject:s];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SubwayChangeNotification object:nil userInfo:@{@"subway" : sendArr}];
    
    [[NSUserDefaults standardUserDefaults] setObject:sendArr
                                              forKey:kSubwayArr];
    [[NSUserDefaults standardUserDefaults] setObject:currentMetroID
                                              forKey:kSubwayIDKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UISearchBarDelegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0){
        //        strSubways = subwaysWork;
        subways = subwaysWork;
        [_metroTableView reloadData];
    } else {
        
        NSMutableArray *tmp = NSMutableArray.new;
        for(NSString *s in subwaysWork)
        {
            //            NSString *strName = s.subwayName;
            
            if( ([s rangeOfString:searchText  options:NSCaseInsensitiveSearch].location != NSNotFound ) &&
               ![tmp containsObject:s])
                [tmp addObject:s];
        }
        subways = nil;
        subways = [NSArray arrayWithArray:tmp];
        [_metroTableView reloadData];
        
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"strSubways: %lu", (unsigned long)subways.count);
    
    if (subways.count > 0){
        return subways.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* subway = subways[indexPath.row];
    
    UITableViewCell* cell = UITableViewCell.new;
    
    if (subway.length > 1){
        
        cell = (MetroCell *)[tableView dequeueReusableCellWithIdentifier:@"MetroCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MetroCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
        selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [cell setBackgroundView:selectedBackgroundView];
        
        if ([selectArr containsObject:subways[indexPath.row]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundView.backgroundColor = [UIColor whiteColor];
        }
        
        subway = [subway stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        subway = [subway stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        subway = [subway stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        subway = [subway stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        
        cell.textLabel.text = subway;
        //        NSLog(@"subway.name: %@", subway);
        
    } else if (subway.length == 1){
        
        cell.textLabel.text = subway;
        cell.textLabel.textColor = mainColor;
        cell.textLabel.font = [UIFont fontWithName:@"Thonburi" size:22];
        cell.userInteractionEnabled = NO;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([selectArr containsObject:subways[indexPath.row]] == NO) {
        [selectArr addObject:subways[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    } else {
        [selectArr removeObject:subways[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([selectArr containsObject:subways[indexPath.row]]) {
        [selectArr removeObject:subways[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    } else {
        [selectArr addObject:subways[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }
    
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end
