//
//  PageViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 4/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMenuViewController.h"

@interface PageViewController : UIPageViewController <UIPageViewControllerDataSource>

//@property (weak, nonatomic) id<LeftMenuProtocol> delegate;
//@property (strong, nonatomic) NSString* currCity;
@property (nonatomic, assign) Boolean isAuth;
//@property (nonatomic, assign) Boolean isThisCity;

@end
