//
//  RNForgotPasswordViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/28/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "RNForgotPasswordViewController.h"
#import "NSObject+DPLocalization.h"
#import "define.h"
#import "DPLocalization.h"
#import "ForgotPassRequest.h"
#import "ForgotPassResponse.h"

#import "Reachability.h"
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface RNForgotPasswordViewController ()<UITextFieldDelegate>{
    Reachability *_reachability;
    double textFieldFontSize;
    double buttonFontSize;
    UIColor* mainColor;
    
}

@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;

@end



@implementation RNForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.emailView.translatesAutoresizingMaskIntoConstraints = false;
    self.firstLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.secondLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    
    self.emailTextfield.translatesAutoresizingMaskIntoConstraints = false;
    self.emailTextfield.textColor = mainColor;
    [self.emailTextfield setTintColor:mainColor];
    self.emailTextfield.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.emailTextfield.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    
    [self.sendButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.sendButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:DPLocalizedString(@"send_mess", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.sendButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.title = DPLocalizedString(@"resave_title", nil);
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    
    [self setupView];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doneClicked:)]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(doneBtnFromKeyboardClicked:)];
    [btnDoneOnKeyboard setTintColor:[UIColor lightGrayColor]];
    keyboardToolbar.items = @[flexBarButton, btnDoneOnKeyboard];
    
    _emailTextfield.inputAccessoryView = keyboardToolbar;
    _emailTextfield.delegate = self;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        alert.popoverPresentationController.sourceView = self.view;
        alert.popoverPresentationController.sourceRect = self.view.bounds;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)doneClicked:(id)sender {
    [_emailTextfield resignFirstResponder];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)doneBtnFromKeyboardClicked:(id)sender
{
    NSLog(@"Done Button Clicked.");
    
    [_emailTextfield resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_emailTextfield resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    float newVerticalPosition = -keyboardSize.height;
    if (_emailTextfield.isFirstResponder){
        if (_sendButton.frame.origin.y + _sendButton.frame.size.height + 10 > self.view.frame.size.height + newVerticalPosition){
            
            CGFloat regY = _sendButton.frame.origin.y + _sendButton.frame.size.height + 10;
            CGFloat keyY = self.view.frame.size.height + newVerticalPosition;
            
            [self moveFrameToVerticalPosition:(keyY - regY) forDuration:0.3f];
        }
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

-(void) setupView {
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    if (screenWidth < 322) {
        [self.firstLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.firstLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.firstLabel.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.248].active = YES;
    [self.firstLabel.widthAnchor constraintEqualToConstant:200].active = YES;
    //    [self.firstLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [self.secondLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.secondLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.secondLabel.topAnchor constraintEqualToAnchor:self.firstLabel.bottomAnchor constant:screenHeight * 0.001].active = YES;
    [self.secondLabel.widthAnchor constraintEqualToConstant:260].active = YES;
    //    [self.secondLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    
    
    [self.emailView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.emailView.centerYAnchor constraintEqualToAnchor:self.view.topAnchor
                                                 constant:screenHeight * 0.36].active = YES;
    [self.emailView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.emailView.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
    [self.emailTextfield.centerXAnchor constraintEqualToAnchor:self.emailView.centerXAnchor].active = YES;
    [self.emailTextfield.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor].active = YES;
    [self.emailTextfield.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    
    if (screenWidth < 322) {
        [self.sendButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self.sendButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.sendButton.centerYAnchor constraintEqualToAnchor:self.emailView.bottomAnchor
                                                  constant:screenHeight * 0.084].active = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        self.sendButton.layer.cornerRadius = 3;
        self.sendButton.layer.masksToBounds = true;
        [self.sendButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.sendButton.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.sendButton.layer.cornerRadius = 5;
        self.sendButton.layer.masksToBounds = true;
        [self.sendButton.widthAnchor constraintEqualToConstant:260].active = YES;
        [self.sendButton.heightAnchor constraintEqualToConstant:71].active = YES;
        [self.sendButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:26.0]];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButton:(id)sender {
    [_emailTextfield resignFirstResponder];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    //    [self popViewControllerAnimated:YES];
}


- (IBAction)sendEmail:(id)sender {
    [_emailTextfield resignFirstResponder];
    if ([self isInternetConnect]){
        [self forgotPassRequest];
    }
}

- (void) forgotPassRequest {
    ForgotPassRequest *request = [[ForgotPassRequest alloc]init];
    
    if (_emailTextfield.text.length != 0) {
        request.email = _emailTextfield.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_email", nil)];
        return;
    }
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        ForgotPassResponse *forgotResponse = (ForgotPassResponse *)response;
        NSLog(@"forgotResponse %@", forgotResponse);
        NSLog(@"forgotResponse %@", forgotResponse.success);
        NSLog(@"forgotResponse %@", forgotResponse.error);
        
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            
            NSLog(@"error %@", error);
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            
        } else  if ([forgotResponse.success isKindOfClass:[NSString class]] && [forgotResponse.success isEqualToString:@"success"]){
            [self showAlertWithText:DPLocalizedString(@"forgot_success", nil) andTitle:DPLocalizedString(@"new_pass", nil)];
            NSLog(@"forgotResponse %@", forgotResponse.success);
            
            [_emailTextfield resignFirstResponder];
        } else  if ([forgotResponse.error isKindOfClass:[NSString class]] && [forgotResponse.error isEqualToString:@"user not exist"]){
            [self showAlertWithText:DPLocalizedString(@"try_again", nil) andTitle:DPLocalizedString(@"forgot_error", nil)];
            NSLog(@"forgotResponse %@", forgotResponse.error);
            
            [_emailTextfield resignFirstResponder];
        } else  if ([forgotResponse.error isKindOfClass:[NSString class]] && [forgotResponse.error isEqualToString:@"email error"]){
            [self showAlertWithText:DPLocalizedString(@"try_again", nil) andTitle:DPLocalizedString(@"not_email", nil)];
            NSLog(@"forgotResponse %@", forgotResponse.error);
            
            [_emailTextfield resignFirstResponder];
        }
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    
    alert.view.tintColor = mainColor;
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0 - 105, self.view.bounds.size.height / 2.0 + 70, 1.0, 1.0);
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithText:(NSString *)text andTitle:(NSString*)title{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0 - 105, self.view.bounds.size.height / 2.0 + 70, 1.0, 1.0);
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_emailTextfield resignFirstResponder];
    
    return YES;
}


-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
    
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                 NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}

@end
