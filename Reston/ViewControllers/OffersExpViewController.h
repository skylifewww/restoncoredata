//
//  OffersExpViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HVTableView.h"
#import "RNRestaurant.h"

@interface OffersExpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSNumber* restaurantID;
@property (nonatomic, strong) RNRestaurant *restaurant;
@end
