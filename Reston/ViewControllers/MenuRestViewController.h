//
//  MenuRestViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuRestViewController : UIViewController

@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) CGPDFDocumentRef pdfDocument;

@end
