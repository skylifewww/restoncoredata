//
//  RNCalendarViewController.m
//  Reston
//
//  Created by Yurii Oliiar on 6/26/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNCalendarViewController.h"
#import "CalendarView.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end

@interface RNCalendarViewController ()<CalendarDataSource,CalendarDelegate>

@property (nonatomic, strong) CalendarView * customCalendarView;

@end

@implementation RNCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSAssert(_width > 100, @"bad width parameter");

    self.customCalendarView = [[CalendarView alloc]initWithFrame:CGRectMake(20, 0, _width, _width/8*9)];
    
    _customCalendarView.selectedDate = _selectedDate;
    _customCalendarView.delegate                    = self;
    _customCalendarView.datasource                  = self;
    _customCalendarView.calendarDate                = _selectedDate;
    _customCalendarView.monthAndDayTextColor        = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    _customCalendarView.dayBgColorWithData          = RGBCOLOR(208, 208, 214);
    _customCalendarView.dayBgColorWithoutData       = RGBCOLOR(111, 179, 199);
    _customCalendarView.dayBgColorSelected          = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    _customCalendarView.dayTxtColorWithoutData      = [UIColor whiteColor];
    _customCalendarView.dayTxtColorWithData         = [UIColor lightGrayColor];
    _customCalendarView.dayTxtColorSelected         = [UIColor whiteColor];
    _customCalendarView.borderColor                 = RGBCOLOR(159, 162, 172);
    _customCalendarView.borderWidth                 = 1;
    _customCalendarView.allowsChangeMonthByDayTap   = YES;
    _customCalendarView.allowsChangeMonthByButtons  = YES;
    _customCalendarView.keepSelDayWhenMonthChange   = YES;
    _customCalendarView.nextMonthAnimation          = UIViewAnimationOptionTransitionFlipFromRight;
    _customCalendarView.prevMonthAnimation          = UIViewAnimationOptionTransitionFlipFromLeft;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _customCalendarView.center = self.view.center;
        [self.view addSubview:_customCalendarView];
        _customCalendarView.center = _center;
    });

}

- (NSDate *)startOfTheDay:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger calendarUnits = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *components = [calendar components:calendarUnits
                                               fromDate:date];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    
    return [[calendar dateFromComponents:components] dateByAddingTimeInterval:-1];
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)backBtnPressed:(id)sender {
    [_delegate calendarPicker:self
            didCancelWithDate:_selectedDate];
}

- (IBAction)doneBtnPressed:(id)sender {
    [_delegate calendarPicker:self
                didChooseDate:_selectedDate];
}

#pragma mark - CalendarDelegate protocol conformance

- (void)dayChangedToDate:(NSDate *)selectedDate {
    self.selectedDate = selectedDate;
}

#pragma mark - CalendarDataSource protocol conformance

- (BOOL)isDataForDate:(NSDate *)date {
    return ([date compare:[[self startOfTheDay:[NSDate date]] dateByAddingTimeInterval:-1]] == NSOrderedAscending);
}

- (BOOL)canSwipeToDate:(NSDate *)date {
    NSDate *now = [[self startOfMonth:[NSDate date]] dateByAddingTimeInterval:-10];
    return ![[now laterDate:date] isEqualToDate:now];

}

- (NSDate *)startOfMonth:(NSDate *)date {
    NSInteger flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [calendar components:flags fromDate:date];
    comp.day = 1;
    return [calendar dateFromComponents:comp];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end
