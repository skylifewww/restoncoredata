//
//  FilterType.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 03.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterType : NSObject

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* subtitle;

- (id)initWithTitle:(NSString*)title andSubtitle:(NSString *)subtitle;

@end
