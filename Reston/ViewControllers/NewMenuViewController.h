//
//  NewMenuViewController.h
//  ammonitum
//
//  Created by Vladimir Nybozhinsky on 5/7/17.
//  Copyright © 2017 Dima. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RNRestaurant.h"

typedef enum : NSInteger{
    LeftCatalogue = 0,
    LeftProfile,
    LeftOrders,
    LeftFavorites,
    LeftExit,
    LeftEmpty,
    LeftEmail
} LeftMenu;

@protocol LeftMenuProtocol <NSObject>

@required
-(void)changeViewController:(LeftMenu) menu;

@end

@interface NewMenuViewController : UITableViewController <LeftMenuProtocol>

@property (nonatomic, assign) Boolean isAuth;
//@property (nonatomic, assign) Boolean isThisCity;

//@property (nonatomic, strong) NSString* currCity;
@property(strong, nonatomic) NSArray<RNRestaurant*>* restons;
@property (retain, nonatomic) NSMutableDictionary* menuItemsDict;
@property (retain, nonatomic) NSArray* menuItemsArray;
@property (retain, nonatomic) UIViewController *catalogViewController;
@property (retain, nonatomic) UIViewController *profileViewController;
@property (retain, nonatomic) UIViewController *ordersViewController;
@property (retain, nonatomic) UIViewController *favoritesViewController;
@property (retain, nonatomic) UIViewController *exitViewController;
@property (retain, nonatomic) UIViewController *emailViewController;
@property (weak, nonatomic) IBOutlet UILabel *enterLabel;
@property (weak, nonatomic) IBOutlet UIImageView *enterExitImage;

@end

