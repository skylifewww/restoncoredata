//
//  RestoTypeTableViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "RestoTypeTableViewController.h"
#import "RestoTypeCell.h"
#import "GetRestTypeRequest.h"
#import "GetRestTypeResponse.h"
#import "RestType.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


static NSString *const kTypeKey = @"TypeKey";
NSString *const TypeChangeNotification = @"TypeChangeNotification";

@interface RestoTypeTableViewController (){
    
    NSMutableArray* selectArr;
    NSMutableArray* sendArr;
    NSArray<RestType*>* types;
    NSArray<NSString*>* strTypes;
    NSString* currentTypeName;
    RestType* currentType;
    UIColor* mainColor;
    
    Reachability *_reachability;
}

@end

@implementation RestoTypeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"type_rest_title", nil);
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    selectArr = [[NSMutableArray alloc] init];
    sendArr = [[NSMutableArray alloc] init];
    
    if (_filteredTypeRests.count > 0){
        
        strTypes = [_filteredTypeRests sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kTypeKey];
        if (setSelect.count > selectArr.count && (![setSelect.firstObject  isEqualToString: @""])){
            selectArr = [NSMutableArray arrayWithArray:setSelect];
        }
        
        [self.tableView reloadData];
    } else {
        //        [self loadTypes];
    }
    
    
    self.tableView.allowsMultipleSelection = true;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)loadTypes{
    GetRestTypeRequest *request = [[GetRestTypeRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    request.city = _currCity;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetRestTypeResponse *r = (GetRestTypeResponse *)response;
        
        types = r.restTypes;
        
        types = [r.restTypes sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            RestType *r1 = (RestType*)obj1;
            RestType *r2 = (RestType*)obj2;
            
            return [r1.restTypeName compare:r2.restTypeName];
        }];
        
        if (types.count == 0){
            
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        } else {
            
            NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kTypeKey];
            if (setSelect.count > selectArr.count){
                for (RestType* d in types){
                    for(NSString* str in setSelect){
                        if ([d.restTypeName isEqualToString:str]){
                            
                            [selectArr addObject:d];
                        }
                    }
                }
            }
            [self.tableView reloadData];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (IBAction)submitButton:(id)sender {
    
    //        for (RestType* rt in selectArr){
    //            [sendArr addObject:rt.restTypeName];
    for (NSString* rt in selectArr){
        [sendArr addObject:rt];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:TypeChangeNotification object:nil userInfo:@{@"type" : sendArr}];
    
    [[NSUserDefaults standardUserDefaults] setObject:sendArr
                                              forKey:kTypeKey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (strTypes.count > 0){
        return strTypes.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RestoTypeCell* cell = (RestoTypeCell *)[tableView dequeueReusableCellWithIdentifier:@"RestoTypeCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RestoTypeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [cell setBackgroundView:selectedBackgroundView];
    
    //    if ([selectArr containsObject:types[indexPath.row]]) {
    if ([selectArr containsObject:strTypes[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    
    //    RestType* type = types[indexPath.row];
    //
    //    cell.textLabel.text = type.restTypeName;
    NSString* type = [strTypes[indexPath.row] stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    cell.textLabel.text = type;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    currentType = types[indexPath.row];
    //    currentTypeName = currentType.restTypeName;
    currentTypeName = strTypes[indexPath.row];
    NSLog(@"%@", currentTypeName);
    
    //    if ([selectArr containsObject:types[indexPath.row]] == NO) {
    //        [selectArr addObject:types[indexPath.row]];
    
    if ([selectArr containsObject:strTypes[indexPath.row]] == NO) {
        [selectArr addObject:strTypes[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    } else {
        //        [selectArr removeObject:types[indexPath.row]];
        [selectArr removeObject:strTypes[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if ([selectArr containsObject:types[indexPath.row]]) {
    //        [selectArr removeObject:types[indexPath.row]];
    
    if ([selectArr containsObject:strTypes[indexPath.row]]) {
        [selectArr removeObject:strTypes[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    } else {
        //        [selectArr addObject:types[indexPath.row]];
        [selectArr addObject:strTypes[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}


@end
