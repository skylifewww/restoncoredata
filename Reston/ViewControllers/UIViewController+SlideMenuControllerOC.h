//
//  UIViewController+SlideMenuControllerOC.h
//  ammonitum
//
//  Created by Vladimir Nybozhinsky on 13.05.17.
//  Copyright © 2017 Dima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SlideMenuControllerOC)

-(void)setNavigationBarItem;

-(void)removeNavigationBarItem;

-(void)setNavigationBarArrowLeft;

-(void)setNavigationBackBarItem;

@end
