//
//  RNRegisterViewController.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//


#import "RNRegisterViewController.h"
#import "RNRegisterRequest.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookRegisterRequest.h"
#import "FacebookRegisterResponse.h"
#import "CatalogRestViewController.h"
#import "SlideMenuController.h"
#import "NewMenuViewController.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "AFNetworking.h"
#import <AFNetworking/AFNetworking.h>
#import "RNEditProfileRequest.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end

static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kUserPhotoKey = @"UserPhotoKey";
static NSString *const kPhoneKey = @"PhoneKey";
static NSString *const kUserKey = @"UserKey";
static NSString *const kPassKey = @"PassKey";
static NSString *const kEmailFBKey = @"EmailFBKey";
static NSString *const kClientIDKey = @"ClientIDKey";
static NSString *const kStrategyKey = @"StrategyKey";


@interface RNRegisterViewController ()<UITextFieldDelegate,CLLocationManagerDelegate>{
    double latitude;
    double longitude;
    BOOL loading;
    NSString* currCity;
    NSArray<RNRestaurant*>* restons;
    UIColor* mainColor;
    BOOL keyboardIsShown;
    double textFieldFontSize;
    double buttonFontSize;
    UIStoryboard* storyboard;
    Boolean isPhone;
    NSString* filePathImage;
    Boolean isSuccessUploadImage;
   
    Reachability *_reachability;
}

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *enterFacebookButton;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation RNRegisterViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    loading = NO;
    
    [self loadUserLocation];
 
    self.title = DPLocalizedString(@"register_title", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
   
    [self setupView];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doneClicked:)]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
   
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    [doneButton setTintColor:[[UIColor darkGrayColor]colorWithAlphaComponent:0.8f]];
    _emailTextfield.inputAccessoryView = keyboardDoneButtonView;
    
    _phoneTextField.inputAccessoryView = keyboardDoneButtonView;
    
    _nameTextField.inputAccessoryView = keyboardDoneButtonView;
    _passwordTextfield.inputAccessoryView = keyboardDoneButtonView;
    _confirmPassword.inputAccessoryView = keyboardDoneButtonView;
    
    _emailTextfield.delegate = self;
    _phoneTextField.delegate = self;
    _nameTextField.delegate = self;
    _passwordTextfield.delegate = self;
    _confirmPassword.delegate = self;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void) fetchProfile{
    
    NSLog(@"fetchProfile");
    
    NSDictionary*  parameters = @{@"fields": @"id, first_name, last_name, email, picture.type(large)"};
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"me"] parameters:parameters
                                  
                                                                   HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (!error) {
            NSLog(@"result = %@",result);
            NSString* email = [result objectForKey:@"email"];
            NSString* first_name = [result objectForKey:@"first_name"];
            NSString* last_name = [result objectForKey:@"last_name"];
            NSString* name = [NSString stringWithFormat:@"%@ %@",first_name, last_name];
            
            NSString* clientID = [result objectForKey:@"id"];
            
            
            NSLog(@"%ld",(long)clientID);
            
            NSDictionary *dictionary = (NSDictionary *)[result objectForKey:@"picture"];
            NSDictionary *data = [dictionary objectForKey:@"data"];
            NSString *photoUrl = (NSString *)[data objectForKey:@"url"];
            
            NSLog(@"email is %@", [result objectForKey:@"email"]);
            
            NSData  *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoUrl]];
            
            
//            [[NSUserDefaults standardUserDefaults] setObject:dataImage forKey:kUserPhotoKey];
            [[NSUserDefaults standardUserDefaults] setObject:name
                                                      forKey:kFBNameKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self sendLoginRequestWithFacebookClientID:clientID nameFB:name emailFB:email photoFB:(NSString*)photoUrl];
        } else {
            NSLog(@"%@", error);
            return;
        }}
     ];
}


- (void)sendLoginRequestWithFacebookClientID:(NSString*)clientId nameFB:(NSString*)nameFB emailFB:(NSString*)emailFB photoFB:(NSString*)photoFB{
    FacebookRegisterRequest *request = [[FacebookRegisterRequest alloc]init];
    request.clientId = clientId;
    request.nameFB = nameFB;
    request.emailFB = emailFB;
    request.emailFB = photoFB;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        FacebookRegisterResponse *authResponse = (FacebookRegisterResponse *)response;
        //        [RNUser sharedInstance].email = email;
         if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//            NSLog(@"error: %@",error);
            return;
        }
        
//        NSLog(@"authResponse.token: %@",authResponse.token);
//        NSLog(@"authResponse.autorisation: %@",authResponse.autorisation);
        if (authResponse.token > 0) {
            
//            NSLog(@"authResponse.token > 0!!!");
            [RNUser sharedInstance].photoFB = photoFB;
//            NSLog(@"authResponse [RNUser sharedInstance].photoFB: %@",[RNUser sharedInstance].photoFB);
            [RNUser sharedInstance].token = authResponse.token;
            [RNUser sharedInstance].name = nameFB;
            [RNUser sharedInstance].email = emailFB;
            
            if ([RNUser sharedInstance].photo.length == 0){
                
                //                NSLog(@"[RNUser sharedInstance].photo.length = 0");
                
                NSData  *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoFB]];
                UIImage *userPhoto = [UIImage imageWithData:imageData];
                
                [self sendImageToServer:userPhoto];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:emailFB
                                                      forKey:kEmailFBKey];
            [[NSUserDefaults standardUserDefaults] setObject:clientId
                                                      forKey:kClientIDKey];
            [[NSUserDefaults standardUserDefaults] setObject:@"FBSTRATEGY"
                                                      forKey:kStrategyKey];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self sendSearchRequestWithCity:currCity];
            // TODO: continue
        } else {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    }];
}

- (void)sendImageToServer:(UIImage*)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[imageData length]];
    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
    
    // Init the URLRequest
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:imageData];
    //    NSMutableData *body = [NSMutableData data];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request fromData:nil progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error!!!!!!!: %@", error);
        } else {
            NSLog(@"responseObject:%@", responseObject);
            
            
            if ([responseObject objectForKey:@"token"] && [responseObject objectForKey:@"file_path"]){
                [RNUser sharedInstance].token = [responseObject objectForKey:@"token"];
                NSString* strURL = [responseObject objectForKey:@"file_path"];
                filePathImage = [strURL stringByReplacingOccurrencesOfString:@"/uploads/users/" withString:@""];
                
                NSLog(@"filePathImage:%@", filePathImage);
                isSuccessUploadImage = YES;
            }
            
            
            NSLog(@"filePath: %@", filePathImage);
            
            if (isSuccessUploadImage == YES && filePathImage.length > 0){
                NSLog(@"[self sendEditRequest]");
                [self sendEditRequest];
            }
        }
        
    }];
    [uploadTask resume];
}


- (void)sendEditRequest{
    
    RNEditProfileRequest *request = [[RNEditProfileRequest alloc]init];
    
    
    if (filePathImage.length > 0) {
        request.photo = filePathImage;
    }
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        RNAuthorizationResponse *authResponse = (RNAuthorizationResponse *)response;
        //        NSLog(@"authResponse %@",authResponse.editProfile);
        if ([authResponse.editProfile isEqualToString:@"success"]){
            
            
        } else if (error != nil || [response isKindOfClass:[NSNull class]] || [authResponse.editProfile isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    } ];
}


- (void)registerDefaults {
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kPassKey:@"",
                                                              kUserKey:@"",
                                                              kEmailFBKey:@"",
                                                              kClientIDKey:@"",
                                                              kStrategyKey:@""
                                                              }];
}


- (void) loadUserLocation
{
    
    [self registerDefaults];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    latitude = _locationManager.location.coordinate.latitude;
    longitude = _locationManager.location.coordinate.longitude;
    NSLog(@"CatalogRestViewController latitude %f longitude %f",latitude, longitude);
    
    CLLocation *locUser = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [self getAddressFromLocation:locUser];
    
    
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
}


//-(NSString *)getAddressFromLocation:(CLLocation *)location {
//
//    __block NSString *address;
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if(placemarks && placemarks.count > 0)
//         {
//             CLPlacemark *placemark= [placemarks objectAtIndex:0];
//
//             address = [NSString stringWithFormat:@"%@",[placemark locality]];
//             NSLog(@"%@",address);
//
//             if ([FBSDKAccessToken currentAccessToken]) {
//
////                 [self sendSearchRequestWithCity:address];
//
//                  NSLog(@"[FBSDKAccessToken currentAccessToken] %@",[FBSDKAccessToken currentAccessToken]);
//
//             }
//             currCity = address;
//         }
//
//     }];
//    return address;
//}

-(NSString *)getAddressFromLocation:(CLLocation *)location {
    
    __block NSString *city;
    __block NSString *administrativeArea;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             city = [NSString stringWithFormat:@"%@",[placemark locality]];
             NSLog(@"city %@",city);
             administrativeArea = [NSString stringWithFormat:@"%@",[placemark administrativeArea]];
             NSLog(@"administrativeArea %@",administrativeArea);
             
             
             if (![city isKindOfClass:[NSNull class]] || ![administrativeArea isKindOfClass:[NSNull class]]){
                 
                 NSLog(@"cityName != nil");
                 if ([city isEqualToString:@"Kiev"] || [administrativeArea isEqualToString:@"Kiev Oblast"] || [city isEqualToString:@"Киев"] || [city isEqualToString:@"Київ"]){
                     
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].realCity = @"1";
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                     //                     [self showAlertWithText:[NSString stringWithFormat:@"город?-------------------------------------------------------  1"]];
                     
                     
                 } else if ([city isEqualToString:@"Lviv"] || [administrativeArea isEqualToString:@"Lviv Oblast"] || [city isEqualToString:@"Львів"] || [city isEqualToString:@"Львов"]){
                     
                     [RNUser sharedInstance].currCity = @"6";
                     [RNUser sharedInstance].realCity = @"6";
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 } else if ([city isEqualToString:@"Ivano-Frankivs'k"] || [administrativeArea isEqualToString:@"Ivano-Frankivsk Oblast"] || [city isEqualToString:@"Івано-Франківськ"] || [city isEqualToString:@"Ивано-Франковск"]){
                     [RNUser sharedInstance].realCity = @"8";
                     [RNUser sharedInstance].currCity = @"8";
                     
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 } else {
                     [RNUser sharedInstance].realCity = @"1234567";
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].isThisCity = NO;
                     //                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 }
             } else {
                 
                 NSLog(@"cityName == nil");
                 [RNUser sharedInstance].currCity = @"1";
                 [RNUser sharedInstance].isThisCity = NO;
                 //                 [RNUser sharedInstance].isThisCity = YES;
                 [RNUser sharedInstance].realCity = @"1234567";
                 NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
             }
             
             currCity = [RNUser sharedInstance].currCity;
             
             if ([self isInternetConnect]){
//                 [self startDefaultFlowWithCity:currCity];
             }
         }
         
     }];
    return [RNUser sharedInstance].currCity;
}


- (void)sendSearchRequestWithCity:(NSString*)cityName {
    if (loading) {
        return;
    }
    loading = YES;
    [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
    RNGetRestaurantsRequest *request = [[RNGetRestaurantsRequest alloc] init];
    request.name = @"";
    request.isAuth = YES;
    
    NSLog(@"cityName************** %@", cityName);
//    if (cityName != nil){
//
//        NSLog(@"cityName != nil");
//        if ([cityName isEqualToString:@"Kiev"]){
//            cityName = @"1";
//            request.city = [cityName uppercaseString];
//        } else if ([cityName isEqualToString:@"Lviv"]){
//            cityName = @"6";
//            request.city = [cityName uppercaseString];
//        } else if ([cityName isEqualToString:@"Ivano-Frankivs'k"]){
//            cityName = @"8";
//            request.city = [cityName uppercaseString];
//        } else {
//            cityName = @"1";
//            request.city = [cityName uppercaseString];
//        }
//    } else {
//        NSLog(@"cityName == nil");
//        cityName = @"1";
//    }
    
    request.city = cityName;
//    currCity = cityName;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        loading = NO;
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        }
        RNGetRestaurantsResponse *r = (RNGetRestaurantsResponse *)response;
        [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
        
        restons = r.restrauntsArray;
        restons = [restons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            
            RNRestaurant *r1 = obj1;
            RNRestaurant *r2 = obj2;
            
            return [@(r1.intCatalogPriority) compare:@(r2.intCatalogPriority)];
            
        }];
        
        NSLog(@"launch.restons.count: %lu",(unsigned long)restons.count);
        
        [self startIPhone];
        
    }];
}

- (void)doneClicked:(id)sender {
    [_emailTextfield resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_passwordTextfield resignFirstResponder];
    [_confirmPassword resignFirstResponder];
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_emailTextfield resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_passwordTextfield resignFirstResponder];
    [_confirmPassword resignFirstResponder];
}


- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    float newVerticalPosition = -keyboardSize.height;
    NSLog(@"newVerticalPosition %f", newVerticalPosition);
    NSLog(@"newVerticalPosition %f", self.view.frame.size.height + newVerticalPosition);
    NSLog(@"_registerButton %f", _registerButton.frame.origin.y + _registerButton.frame.size.height + 10);
    
    if (_nameTextField.isFirstResponder){
        if (_nameView.frame.origin.y + _nameView.frame.size.height > self.view.frame.size.height + newVerticalPosition){
            [self moveFrameToVerticalPosition:-(_nameView.frame.origin.y + _nameView.frame.size.height - self.view.frame.size.height + newVerticalPosition) forDuration:0.3f];
        }
    }
    if (_phoneTextField.isFirstResponder){
        if (_phoneView.frame.origin.y + _phoneView.frame.size.height > self.view.frame.size.height + newVerticalPosition){
            [self moveFrameToVerticalPosition:-(_phoneView.frame.origin.y + _phoneView.frame.size.height - self.view.frame.size.height + newVerticalPosition) forDuration:0.3f];
        }
    }
    if (_emailTextfield.isFirstResponder){
        if (_emailView.frame.origin.y + _emailView.frame.size.height > self.view.frame.size.height + newVerticalPosition){
            [self moveFrameToVerticalPosition:-(_emailView.frame.origin.y + _emailView.frame.size.height - self.view.frame.size.height + newVerticalPosition) forDuration:0.3f];
        }
    }
    if (_confirmPassword.isFirstResponder || _passwordTextfield.isFirstResponder) {
        
        CGFloat regY = _registerButton.frame.origin.y + _registerButton.frame.size.height + 10;
        CGFloat keyY = self.view.frame.size.height + newVerticalPosition;
        
        NSLog(@"-(regY - keyY) %f", -(regY - keyY));
        
        [self moveFrameToVerticalPosition:-(regY - keyY) forDuration:0.3f];
    }
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}
#pragma mark - IBActions New with Storyboard


- (IBAction)enterFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
     if ([self isInternetConnect]){
    [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error)
        {
            
            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: DPLocalizedString(@"try_later", nil);
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: DPLocalizedString(@"server_error", nil);
            
            [self showAlertWithTitle:alertTitle andMessage:alertMessage];
            
        }
        else
        {
            if(result.token)
            {
                
                [self fetchProfile];
            }
        }
    }];
  }
}


- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


//#pragma mark - IBActions
//
- (IBAction)back:(id)sender {
    [_emailTextfield resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_passwordTextfield resignFirstResponder];
    [_confirmPassword resignFirstResponder];
//    [self dismissViewControllerAnimated:YES completion:nil];
     [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registerPressed:(id)sender {
    [_emailTextfield resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_passwordTextfield resignFirstResponder];
    [_confirmPassword resignFirstResponder];
     if ([self isInternetConnect]){
         [self sendRegisterRequest];
     }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)sendRegisterRequest {
    NSString *phoneText = [_phoneTextField.text stringByReplacingOccurrencesOfString:@"("
                                                                          withString:@""];
    phoneText = [phoneText stringByReplacingOccurrencesOfString:@")"
                                                     withString:@""];
    phoneText = [phoneText stringByReplacingOccurrencesOfString:@"-"
                                                     withString:@""];
    
    phoneText = [phoneText stringByReplacingOccurrencesOfString:@" "
                                                     withString:@""];
    
    RNRegisterRequest *request = [[RNRegisterRequest alloc]init];
    request.userName = _nameTextField.text;
    request.phone = phoneText;
    
    if (_emailTextfield.text.length != 0) {
        request.email = _emailTextfield.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_email", nil)];
        return;
    }
    
    if (_passwordTextfield.text.length!=0) {
        request.password = _passwordTextfield.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_pass", nil)];
        return;
    }
    if (_confirmPassword.text.length!=0 && [_confirmPassword.text isEqualToString:_passwordTextfield.text]) {
        request.password = _passwordTextfield.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_pass", nil)];
        return;
    }
    if (_phoneTextField.text.length > 0 && _phoneTextField.text.length < 12) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"format_phone", nil)
                                                                       message:DPLocalizedString(@"need_phone", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }

    if (_phoneTextField.text.length!=0) {
        request.phone = _phoneTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_phone", nil)];
        return;
    }
    if (_nameTextField.text.length!=0) {
        request.userName = _nameTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_name", nil)];
        return;
    }
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        RNAuthorizationResponse *authResponse = (RNAuthorizationResponse *)response;
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            
        } else if (authResponse.errorMessage.length > 0 && [authResponse.errorMessage isEqualToString:@"user exist"]) {
                [self showAlertWithText:DPLocalizedString(@"user_exist", nil)];
                return;
        } else {
            [RNUser sharedInstance].token = authResponse.token;
            [RNUser sharedInstance].name =_nameTextField.text;
            [RNUser sharedInstance].phone = _phoneTextField.text;
            [RNUser sharedInstance].email = _emailTextfield.text;
    
            [self sendSearchRequestWithCity:currCity];
            
        } 
    } ];
}


#pragma mark - Start

- (void)startIPhone {
    CatalogRestViewController* enterVC = [storyboard instantiateViewControllerWithIdentifier:@"CatalogRestViewController"];
    enterVC.restons = restons;
    
    UINavigationController *navVC = [[storyboard instantiateViewControllerWithIdentifier:@"CatalogNavRestViewController"] initWithRootViewController:enterVC];
    
    NewMenuViewController * menuVC = [storyboard instantiateViewControllerWithIdentifier:@"NewMenuViewController"];
    
    menuVC.restons = restons;
    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:navVC leftMenuViewController:menuVC];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:slideMenuController];
}


- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_emailTextfield resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_passwordTextfield resignFirstResponder];
    [_confirmPassword resignFirstResponder];
    if (textField == _nameTextField) {
        [_phoneTextField becomeFirstResponder];
        return YES;
    }
    if (textField == _phoneTextField) {
        [_emailTextfield becomeFirstResponder];
        return YES;
    }
    if (textField == _emailTextfield) {
        [_passwordTextfield becomeFirstResponder];
        return YES;
    }
    if (textField == _passwordTextfield) {
        [_confirmPassword becomeFirstResponder];
        return YES;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField != _phoneTextField) {
        return YES;
    }
    if (_phoneTextField.text.length == 0) {
        _phoneTextField.text = @"38";
        return NO;
    }
    if (_phoneTextField.text.length == 2 && range.length >0) {
        _phoneTextField.text = @"38";
        return NO;
    }
    if (range.length > 0) {
        return YES;
    }
    if (_phoneTextField.text.length == 18) {
        return NO;
    }
    
    if (_phoneTextField.text.length == 2) {
        _phoneTextField.text = [NSString stringWithFormat:@"%@ (",_phoneTextField.text];
    }
    if (_phoneTextField.text.length == 7) {
        _phoneTextField.text = [NSString stringWithFormat:@"%@) ",_phoneTextField.text];
    }
    if (_phoneTextField.text.length == 8) {
        _phoneTextField.text = [NSString stringWithFormat:@"%@ ",_phoneTextField.text];
    }
    if (_phoneTextField.text.length == 12) {
        _phoneTextField.text = [NSString stringWithFormat:@"%@-",_phoneTextField.text];
    }
    if (_phoneTextField.text.length == 15) {
        _phoneTextField.text = [NSString stringWithFormat:@"%@-",_phoneTextField.text];
    }
    
    return YES;
}

-(void) setupView {
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    self.emailView.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordView.translatesAutoresizingMaskIntoConstraints = false;
    self.nameView.translatesAutoresizingMaskIntoConstraints = false;
    self.confirmView.translatesAutoresizingMaskIntoConstraints = false;
    self.phoneView.translatesAutoresizingMaskIntoConstraints = false;
    
    
    self.nameTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.nameTextField.textColor = mainColor;
    [self.nameTextField setTintColor:mainColor];
    self.nameTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.nameTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"full_name", nil) withFontSize:textFieldFontSize];
    
    
    self.phoneTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.phoneTextField.textColor = mainColor;
    [self.phoneTextField setTintColor:mainColor];
    self.phoneTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.phoneTextField.attributedPlaceholder = [self placeholderString:@"38 000 000 00 00" withFontSize:textFieldFontSize];
    
    
    self.emailTextfield.translatesAutoresizingMaskIntoConstraints = false;
    self.emailTextfield.textColor = mainColor;
    [self.emailTextfield setTintColor:mainColor];
    self.emailTextfield.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.emailTextfield.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    
    
    self.passwordTextfield.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordTextfield.textColor = mainColor;
    [self.passwordTextfield setTintColor:mainColor];
    self.passwordTextfield.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.passwordTextfield.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"pass", nil) withFontSize:textFieldFontSize];
    
    
    self.confirmPassword.translatesAutoresizingMaskIntoConstraints = false;
    self.confirmPassword.textColor = mainColor;
    [self.confirmPassword setTintColor:mainColor];
    self.confirmPassword.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.confirmPassword.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"conf_pass", nil) withFontSize:textFieldFontSize];
    
    self.registerButton.translatesAutoresizingMaskIntoConstraints = false;
    
    
    
    self.faceLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.enterFacebookButton.translatesAutoresizingMaskIntoConstraints =false;
    
    [self.registerButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.registerButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:DPLocalizedString(@"register", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.registerButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
   
    [self.nameView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.nameView.centerYAnchor constraintEqualToAnchor:self.view.topAnchor
                                                constant:screenHeight * 0.26].active = YES;
    [self.nameView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.nameView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.nameTextField.centerXAnchor constraintEqualToAnchor:self.nameView.centerXAnchor].active = YES;
    [self.nameTextField.centerYAnchor constraintEqualToAnchor:self.nameView.centerYAnchor].active = YES;
    [self.nameTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    
    
    [self.phoneView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.phoneView.centerYAnchor constraintEqualToAnchor:self.nameView.centerYAnchor
                                                 constant:screenHeight * 0.08].active = YES;
    [self.phoneView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.phoneView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.phoneTextField.centerXAnchor constraintEqualToAnchor:self.phoneView.centerXAnchor].active = YES;
    [self.phoneTextField.centerYAnchor constraintEqualToAnchor:self.phoneView.centerYAnchor].active = YES;
    [self.phoneTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
 
    
    [self.emailView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.emailView.centerYAnchor constraintEqualToAnchor:self.phoneView.centerYAnchor
                                                 constant:screenHeight * 0.08].active = YES;
    [self.emailView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.emailView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.emailTextfield.centerXAnchor constraintEqualToAnchor:self.emailView.centerXAnchor].active = YES;
    [self.emailTextfield.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor].active = YES;
    [self.emailTextfield.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    
    
    [self.passwordView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.passwordView.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor
                                                    constant:screenHeight * 0.08].active = YES;
    [self.passwordView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.passwordView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    [self.passwordTextfield.centerXAnchor constraintEqualToAnchor:self.passwordView.centerXAnchor].active = YES;
    [self.passwordTextfield.centerYAnchor constraintEqualToAnchor:self.passwordView.centerYAnchor].active = YES;
    [self.passwordTextfield.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    
    
    
    [self.confirmView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.confirmView.centerYAnchor constraintEqualToAnchor:self.passwordView.centerYAnchor
                                                   constant:screenHeight * 0.08].active = YES;
    [self.confirmView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.confirmView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.confirmPassword.centerXAnchor constraintEqualToAnchor:self.confirmView.centerXAnchor].active = YES;
    [self.confirmPassword.centerYAnchor constraintEqualToAnchor:self.confirmView.centerYAnchor].active = YES;
    [self.confirmPassword.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.passwordTextField.heightAnchor constraintEqualToAnchor:self.passwordView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    
    if (screenWidth < 322) {
        [self.registerButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self.registerButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.registerButton.centerYAnchor constraintEqualToAnchor:self.confirmView.bottomAnchor
                                                      constant:screenHeight * 0.084].active = YES;
   
    
    
    
    
    if (screenWidth < 322) {
        [self.faceLabel setFont:[UIFont fontWithName:@"Thonburi" size:10.0]];
    }
    [self.faceLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.faceLabel.centerYAnchor constraintEqualToAnchor:self.registerButton.centerYAnchor
                                                 constant:screenHeight * 0.088].active = YES;
    [self.faceLabel.widthAnchor constraintEqualToConstant:260].active = YES;
    
    
    
    [self.enterFacebookButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.enterFacebookButton.centerYAnchor constraintEqualToAnchor:self.faceLabel.centerYAnchor
                                                           constant:screenHeight * 0.064].active = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        self.registerButton.layer.cornerRadius = 3;
        self.registerButton.layer.masksToBounds = true;
        
        self.enterFacebookButton.layer.cornerRadius = 15;
        self.enterFacebookButton.layer.masksToBounds = true;
        
        [self.registerButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.registerButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
        
        [self.enterFacebookButton.widthAnchor constraintEqualToConstant:screenWidth * 0.32].active = YES;
        [self.enterFacebookButton.heightAnchor constraintEqualToConstant:screenHeight * 0.05].active = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.registerButton.layer.cornerRadius = 5;
        self.registerButton.layer.masksToBounds = true;
        
        self.enterFacebookButton.layer.cornerRadius = 25;
        self.enterFacebookButton.layer.masksToBounds = true;
        
        [self.registerButton.widthAnchor constraintEqualToConstant:340].active = YES;
        [self.registerButton.heightAnchor constraintEqualToConstant:71].active = YES;
        [self.enterFacebookButton.widthAnchor constraintEqualToConstant:140].active = YES;
        [self.enterFacebookButton.heightAnchor constraintEqualToConstant:50].active = YES;
        [self.enterFacebookButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:24.0]];
//        [self.registerButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:26.0]];
        [self.faceLabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
    
    }
   
   
}


-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
    
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                 NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}
@end
