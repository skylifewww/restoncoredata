//
//  FavPadViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 10.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "FavPadViewController.h"
#import "FavRestonsViewController.h"
#import "RNRestaurant.h"
#import "RestCollectionCell.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNOrderInfo.h"
#import "UIImage+Cache.h"
#import "RNUser.h"
#import "RNCountPeopleHelper.h"
#import "RestonItemViewController.h"
#import "RNGetFavoritesRequest.h"
#import "RNGetFavoritesResponse.h"
#import "RNRemoveFromFavRequest.h"
#import "ReservTableViewController.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"


#import "Reachability.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end

static NSString * const reuseIdentifier = @"RestCollectionCell";
@interface FavPadViewController ()<CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>{
    BOOL loading;
    UIView* containerView;
    UIColor* mainColor;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UIBarButtonItem *treshButton;
    UIView* backView;
    UIView* popView;
    NSInteger currIndex;
    Boolean isTreshEditing;
    
    UIStoryboard* storyboard;
    
    Reachability *_reachability;
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation FavPadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"select_rest_title", nil);
    _myFavLabel.text = DPLocalizedString(@"my_fav_rest", nil);
    [self.myFavLabel setFont:[UIFont fontWithName:@"Thonburi" size:26]];
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    isTreshEditing = NO;
    
    [self setNavigationBarItem];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    treshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(addORDoneRows)];
    [self.navigationItem setRightBarButtonItem:treshButton];
    [treshButton setTintColor:[UIColor whiteColor]];
    
    
    self.editing = NO;
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    [self loadUserLocation];
    
    if ([self isInternetConnect]){
        [self loadData];
    }
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void) loadUserLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    _latitude = _locationManager.location.coordinate.latitude;
    _longitude = _locationManager.location.coordinate.longitude;
//    NSLog(@"CatalogRestViewController latitude %f longitude %f",_latitude, _longitude);
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
    [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
    
    if ([self isInternetConnect]){
        
        RNGetFavoritesRequest *request = [[RNGetFavoritesRequest alloc] init];
        
        NSString* langCode = [[NSString alloc] init];
        
        if ([dp_get_current_language() isEqualToString:@"ru"]){
            langCode = @"ru";
        } else if ([dp_get_current_language() isEqualToString:@"uk"]){
            langCode = @"ua";
        }
        request.langCode = langCode;
        
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
            [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
            if (error != nil || [response isKindOfClass:[NSNull class]]) {
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                return;
            }
            RNGetFavoritesResponse *r = (RNGetFavoritesResponse *)response;
            
            _restons = r.favModels;
            
            if (_restons.count == 0){
                treshButton.enabled = NO;
                [treshButton setTintColor:[UIColor clearColor]];
                
                [self setupView];
            } else if (_restons.count > 0){
                [self.view bringSubviewToFront:_collectionView];
                [treshButton setTintColor:[UIColor whiteColor]];
                treshButton.enabled = YES;
                [_collectionView reloadData];
            }
        }];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.title = DPLocalizedString(@"select_rest_title", nil);
    _myFavLabel.text = DPLocalizedString(@"my_fav_rest", nil);
    [containerView removeFromSuperview];
    isTreshEditing = NO;
    [self loadData];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    isTreshEditing = NO;
}


- (void)addORDoneRows
{
    isTreshEditing = !isTreshEditing;
//    NSLog(@"isTreshEditing == %hhu", isTreshEditing);
        [_collectionView reloadData];
}


- (void)showAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(double) getDistanceToRest:(CLLocationCoordinate2D)restCoord{
    
    
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:_latitude longitude:_longitude];
    
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:restCoord.latitude longitude:restCoord.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
    
//    NSLog(@"Distance from Annotations - %f", distance);
    
    return distance;
    
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    NSLog(@"collectionView_restons.count  %lu", (unsigned long)_restons.count);
    return _restons.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RestCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:reuseIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
  
    RNRestaurant* rest = _restons[indexPath.row];
    
    NSString* type = [NSString stringWithFormat:@"%@    ",rest.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",rest.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:12];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:10];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];
    
    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:10];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];
    
    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [cell.restType setAttributedText:attributedString1];
    
//    NSLog(@"rest.title %@---------------------------------", rest.title);
//    NSLog(@"rest.catalogpriority %@---------------------------------", rest.catalogPriority);
//    NSLog(@"rest.id %@---------------------------------", rest.restaurantId);
    
    NSUInteger rating = (int)(([rest.rating doubleValue])*100);
    cell.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    
    if ([RNUser sharedInstance].isGeoEnabled && _latitude != 0 && _longitude != 0){
    cell.distansIcon.hidden = NO;
    CGFloat distancRest = [self getDistanceToRest:rest.coordinate];
//    NSLog(@"distanceRest %ld--------------------------------", (long)distancRest);
//    NSLog(@"distanceRest %f--------------------------------", (floor(distancRest)));
    NSInteger numKM = (NSInteger)distancRest / 1000;
//    NSLog(@" NSInteger numKM  %ld---------------------------------", (long)numKM);
    NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
//    NSLog(@" NSInteger numMM  %ld---------------------------------", (long)numMM);
    NSString* strKM = @"";
    if (numKM != 0){
        strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
    }
    NSString* strMM = @"";
    if (numMM != 0){
        strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
    }
        NSString* distanceRest = @"";
        
        if (numKM < 100){
            distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
            
        } else {
            distanceRest = [NSString stringWithFormat:@"%@", strKM];
        }
    
    //    NSString* dinstStr = DPLocalizedString(@"dinst_short", nil);
    //    NSString* distanceRest = [NSString stringWithFormat:@"%0.2ld %@",(long)[self getDistanceToRest:rest.coordinate], dinstStr];
    cell.distanceRest.text = distanceRest;
//        NSLog(@"restaurant.coordinate.latitude %f restaurant.coordinate.longitude %f", rest.coordinate.latitude, rest.coordinate.longitude);
    } else {
        cell.distansIcon.hidden = YES;
        cell.distanceRest.text = @"";
    }
    
    NSString* restName = [rest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    cell.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
    
    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    cell.commentsCount.text = [NSString stringWithFormat:@"%@ %@",rest.reviewCount, commStr];
    //     cell.commentsCount.text = [NSString stringWithFormat:@"%@ %@",rest.reviewCount,[RNCountPeopleHelper returnCorrectNameFeedbackWithCount:rest.reviewCount] ];
    [UIImage cachedImage:rest.images[0]
           fullURLString:nil
            withCallBack:^(UIImage *image) {
                cell.restImage.image = image;
            }];
    //    NSString *str = [model.kitchen componentsJoinedByString:@", "];
    //    _cuisineLabel.text = str;
    
    [cell.submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.callButton addTarget:self action:@selector(callButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //    NSString *addresssStr = [rest.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
    //                                                                     withString:@""];
    //    cell.restAddress.text = addresssStr;
    if (rest.avgBill.integerValue == 0) {
        cell.averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        cell.averageLabel.text = [NSString stringWithFormat:@"%@ %@",rest.avgBill, avgStr];
    }
    //    _subwayLabel.text = [[RNUser sharedInstance] subwayNameFord:model.subway];
    
    if (rest.discounts.integerValue == 0) {
        cell.discountBack.image = nil;
        cell.restDiscount.hidden = YES;
    } else {
        cell.restDiscount.hidden = NO;
        cell.restDiscount.text = [NSString stringWithFormat:@"-%@%%",rest.discounts];
        cell.discountBack.image = [UIImage imageNamed:@"discont"];
    }
//    NSLog(@"currIndex %ld", (long)currIndex);
//    NSLog(@"indexPath.row %ld", (long)indexPath.row);
    if(currIndex == indexPath.row)
        
        //    {
        cell.callButton.hidden = NO;
    //        [self.tableView reloadData];
    //    } else {
    //        cell.callButton.hidden = YES;
    //    }
    if (isTreshEditing){
//         NSLog(@"isTreshEditing == YES");
        cell.deleteButton.hidden = NO;
        cell.deleteButton.userInteractionEnabled = YES;
        [cell.deleteButton addTarget:self action:@selector(deleteCell:) forControlEvents:UIControlEventTouchUpInside];
        [cell bringSubviewToFront:cell.deleteButton];
    } else {
        cell.deleteButton.userInteractionEnabled = NO;
        cell.deleteButton.hidden = YES;
    }
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat s = (self.view.bounds.size.width-10)/2;
    return CGSizeMake(s, s*0.61f + 100);
}



-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self isInternetConnect]){
        
        
        RNRestaurant* currentResto = _restons[indexPath.row];
        //        currentResto = [self convertToHumanRest:currentResto];
        __block UIImage* backImage;
        [UIImage cachedImage:currentResto.images[0]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    backImage = image;
                }];
        
        RestonItemViewController * restonItemViewController = [storyboard instantiateViewControllerWithIdentifier:@"RestonItemViewController"];
        
//        restonItemViewController.restaurantId = currentResto.restaurantId;
//        restonItemViewController.backImage = backImage;
//        _orderInfo.restaurant = currentResto;
//        restonItemViewController.orderInfo = _orderInfo;
        restonItemViewController.restCD = currentResto;
//        restonItemViewController.isThisCity = _isThisCity;
        
        
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:restonItemViewController];
        
        [self.navigationController presentViewController:navVC animated:NO completion:nil];
    }
    
}

- (void)deleteCell:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    UIView *containerViews = button.superview;
//    UIView* containerViews = descripView.superview;
    UIView* contentView = containerViews.superview;
    UICollectionViewCell *cell = (UICollectionViewCell *)contentView.superview;
    NSIndexPath * indexPath = [_collectionView indexPathForCell:cell];
//    NSLog(@"row containing button: %ld", (long)indexPath.row);
    
    if ([self isInternetConnect]){
        RNRestaurant *r  = _restons[indexPath.row];
        RNRequest *request;
        
        request = [[RNRemoveFromFavRequest alloc] init];
        ((RNRemoveFromFavRequest *)request).restaurantId = r.restaurantId;
        
        
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
            if (error != nil) {
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                return;
            } else {
                
                [self setupPopView];
                [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     backView.alpha = 0.0;
                                     popView.alpha = 0.0;
                                 }
                                 completion:^(BOOL finished) {
                                     [backView removeFromSuperview];
                                     [popView removeFromSuperview];
                                 }];
                [self loadData];
                
            }
        }];
        
    }
}


//- (IBAction)didLongPressCellToDelete:(UILongPressGestureRecognizer*)gesture {
//    CGPoint tapLocation = [gesture locationInView:self.collectionView];
//    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:tapLocation];
//    if (indexPath && gesture.state == UIGestureRecognizerStateBegan) {
//        NSLog(@"image with index %ld to be deleted", indexPath.item);
//        self.itemToBeDeleted = indexPath.item;
//        UIAlertView *deleteAlert = [[UIAlertView alloc]
//                                    initWithTitle:@"Delete?"
//                                    message:@"Are you sure you want to delete this image?"
//                                    delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
//        [deleteAlert show];
//        
//    }
//}
//
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    NSLog(@"selected button index = %d", buttonIndex);
//    if (buttonIndex == 1) {
//        // Do what you need to do to delete the cell
//        [self.collectionView reloadData];
//    }
//}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if ([self isInternetConnect]){
            RNRestaurant *r  = _restons[indexPath.row];
            RNRequest *request;
            
            request = [[RNRemoveFromFavRequest alloc] init];
            ((RNRemoveFromFavRequest *)request).restaurantId = r.restaurantId;
            
            
            [request sendWithcompletion:^(RNResponse *response, NSError *error) {
                if (error != nil) {
                    [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                    return;
                } else {
                    
                    [self setupPopView];
                    [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn
                                     animations:^{
                                         backView.alpha = 0.0;
                                         popView.alpha = 0.0;
                                     }
                                     completion:^(BOOL finished) {
                                         [backView removeFromSuperview];
                                         [popView removeFromSuperview];
                                     }];
                    [self loadData];
                    
                }
            }];
            
        }
        
    }
}


- (void)submitButton:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    UIView *descripView = button.superview;
    UIView* containerCellView = descripView.superview;
    UIView* contentView = containerCellView.superview;
    UICollectionViewCell *cell = (UICollectionViewCell*)contentView.superview;
    NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
//    NSLog(@"row containing button: %ld", (long)indexPath.row);
    
    RNRestaurant* currentRestourant = _restons[indexPath.row];
    
    currentRestourant = [self convertToHumanRest:currentRestourant];
    
    
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = currentRestourant;
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)callButton:(id)sender{
    //
    //    UIButton *button = (UIButton *)sender;
    //    UIView *containerView = button.superview;
    //    UIView* contentView = containerView.superview;
    //    UITableViewCell *cell = contentView.superview;
    //    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    //    NSLog(@"row containing button: %ld", (long)indexPath.row);
    //
    //    RNRestaurant* currentRestourant = _restons[indexPath.row];
    //    NSString* telephone = currentRestourant.telephone;
    NSString* telephone = @"+380509003493";
//    NSLog(@"telephone: %@ ****************************************", telephone);
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",telephone]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        //       UIAlertView* callAlert = [[UIAlertView alloc]initWithTitle:@"Звонок" message:@"в данное время не возможен!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        //        [callAlert show];
    }
    
}

-(RNRestaurant*) convertToHumanRest:(RNRestaurant*)rawRest{
    
    //    NSString* restType = rawRest.type;
    //    NSInteger type = [restType integerValue];
    //    rawRest.type = [self getType:type];
    
    NSString* restName = [rawRest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    rawRest.title = restName;
    
    NSString *addresssStr = [rawRest.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                                       withString:@""];
    rawRest.address = addresssStr;
    
    return rawRest;
}


-(NSString*) getRating:(NSUInteger)rating{
    
    NSString* nameRating;
    
    if (rating == 0){
        nameRating = @"rating5_0";
        
    } else if (rating < 19){
        
        nameRating = @"rating5_1";
        
    } else if (rating >= 20 && rating < 40){
        
        nameRating = @"rating5_2";
        
    } else if (rating >= 40 && rating < 60){
        
        nameRating = @"rating5_3";
        
    } else if (rating >= 60 && rating < 80){
        
        nameRating = @"rating5_4";
        
    } else if (rating > 80){
        
        nameRating = @"rating5_5";
        
    } else {
        
        nameRating = @"rating5_0";
        
    }
//    NSLog(@"nameRating %@", nameRating);
    return nameRating;
}


- (void)setupPopView {
    
    
    backView = [[UIView alloc]init];
    backView.backgroundColor = [UIColor whiteColor];
    backView.alpha = 0.5f;
    [self.view addSubview:backView];
    
    popView = [[UIView alloc]init];
    popView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:popView];
    
    UIImageView* popImageView = [[UIImageView alloc] init];
    [popView addSubview:popImageView];
    
    UILabel* popLabel = [[UILabel alloc] init];
    
    if (screenWidth < 322) {
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:14.0];
    } else {
        
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:18.0];
    }
    
    popLabel.numberOfLines = 2;
    popLabel.textAlignment = NSTextAlignmentCenter;
    popLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [popView addSubview:popLabel];
    
    
    popLabel.text = DPLocalizedString(@"remove_fav", nil);
    popImageView.image = [UIImage imageNamed:@"iconFav"];
    
    
    backView.translatesAutoresizingMaskIntoConstraints = false;
    popView.translatesAutoresizingMaskIntoConstraints = false;
    popImageView.translatesAutoresizingMaskIntoConstraints = false;
    popLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [backView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [backView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [backView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [backView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    [popView.centerXAnchor constraintEqualToAnchor:backView.centerXAnchor].active = YES;
    [popView.centerYAnchor constraintEqualToAnchor:backView.centerYAnchor].active = YES;
    [popView.widthAnchor constraintEqualToConstant:screenWidth * 0.33].active = YES;
    [popView.heightAnchor constraintEqualToConstant:screenWidth * 0.44].active = YES;
    
    [popImageView.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popImageView.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popImageView.topAnchor constraintEqualToAnchor:popView.topAnchor].active = YES;
    [popImageView.heightAnchor constraintEqualToConstant:screenWidth * 0.29].active = YES;
    
    [popLabel.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popLabel.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popLabel.topAnchor constraintEqualToAnchor:popImageView.bottomAnchor].active = YES;
    [popLabel.bottomAnchor constraintEqualToAnchor:popView.bottomAnchor].active = YES;
}




- (void)addFav:(id)sender {
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(changeViewController:)]) {
        
        [_delegate changeViewController:LeftCatalogue];
        _delegate = nil;
        
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    
    containerView.translatesAutoresizingMaskIntoConstraints = false;
    
    _backFavIcon = [[UIImageView alloc] init];
    _backFavIcon.image = [UIImage imageNamed:@"iconFav"];
    _backFavIcon.contentMode = UIViewContentModeScaleAspectFit;
    [containerView addSubview:_backFavIcon];
    _backFavIcon.translatesAutoresizingMaskIntoConstraints = false;
    
    _submitButton = [[UIButton alloc] init];
    _submitButton.translatesAutoresizingMaskIntoConstraints = false;
    [containerView addSubview:_submitButton];
    
    _textFavlabel = [[UILabel alloc] init];
    _textFavlabel.text = DPLocalizedString(@"no_fav", nil);
    _textFavlabel.textColor = [UIColor lightGrayColor];
    _textFavlabel.numberOfLines = 2;
    _textFavlabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:_textFavlabel];
    
    
//    [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
    _textFavlabel.translatesAutoresizingMaskIntoConstraints = false;
    
//    if (screenWidth < 322) {
//        [self.myFavLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
//    }
    
    
    
    [containerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [containerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [containerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:290].active = YES;
    [containerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    [_submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:26.0]];
    
    _submitButton.backgroundColor = mainColor;
    
    [_submitButton setTitle:DPLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _submitButton.layer.cornerRadius = 5;
    _submitButton.layer.masksToBounds = true;
    _submitButton.userInteractionEnabled = YES;
    [_submitButton addTarget:self action:@selector(addFav:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_backFavIcon.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_backFavIcon.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.43].active = YES;
    [_backFavIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.42].active = YES;
    [_backFavIcon.heightAnchor constraintEqualToConstant:screenHeight * 0.23].active = YES;
    
    if (screenWidth < 322) {
        [_submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [_submitButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_submitButton.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(screenHeight * 0.13)].active = YES;
    [_submitButton.widthAnchor constraintEqualToConstant:260].active = YES;
    [_submitButton.heightAnchor constraintEqualToConstant:71].active = YES;
    

        [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:20.0]];
    
    [_textFavlabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_textFavlabel.centerYAnchor constraintEqualToAnchor:_submitButton.topAnchor constant:-screenHeight * 0.06].active = YES;
    [_textFavlabel.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    [_textFavlabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
}

@end

