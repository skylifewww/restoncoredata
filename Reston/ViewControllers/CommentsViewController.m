//
//  CommentsViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "CommentsViewController.h"
#import "RNInfoReviewTableViewCell.h"
#import "RNGetFeedbacksRequest.h"
#import "RNGetFeedbacksResponse.h"
#import "RNFeedback.h"
#import "CommentsCell.h"
#import "UIImage+Cache.h"
#import "RNGetFavoritesRequest.h"
#import "AddFeedbacksRequest.h"
#import "AddFeedbacksResponse.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "Reachability.h"

NSString *const OpenCommentsNotification = @"OpenCommentsNotification";
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface CommentsViewController ()<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>{
    UIColor* mainColor;
    NSNumber* option;
    Reachability *_reachability;
    UIStoryboard* storyboard;
 
    Boolean isPhone;
    double addressFontSize;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    double typeFontSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    double textHeight;
    double estimatedHeight;
    UILabel* placeholderLabel;
//    CGFloat keyboardHeight;
//    Boolean isFirstTimeOpen;
   
}
@property (nonatomic, copy) NSArray *reviews;
@property (weak, nonatomic) IBOutlet UIView *containerTextView;
@end

@implementation CommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
//    isFirstTimeOpen = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        typeFontSize = 14.0;
        addressFontSize = 12.0;
        restNameFontSize = 14.0;
        textFontSize = 10.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
        isPhone = YES;
        textHeight = 60;
        estimatedHeight = 60;
//        if (screenWidth <= 320.0f && screenHeight <= 568.0f){
//            keyboardHeight = 297.0f;
//        } else if (screenWidth == 375.0f && screenHeight == 667.0f){
//            keyboardHeight = 302.0f;
//
//        } else if (screenWidth == 414.0f && screenHeight == 736.0f){
//            keyboardHeight = 315.0f;
//
//        } else if (screenWidth == 414.0f && screenHeight == 736.0f){
//            keyboardHeight = 315.0f;
//
//        }
//        keyboardHeight = 257.0f;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        typeFontSize = 16.0;
        addressFontSize = 16.0;
        restNameFontSize = 22.0;
        textFontSize = 14.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
        textHeight = 90;
        estimatedHeight = 120;
//        keyboardHeight = 315.0f;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];

        option = @1;
    
    [self.sendButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    NSString* titleSendButton = DPLocalizedString(@"send_mess", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleSendButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.sendButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.sendButton.layer.cornerRadius = cornerRadius;
    self.sendButton.layer.masksToBounds = true;
    
    [self.likeButton setTintColor:mainColor];
    
    [self setupTextView];

    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:self action:nil];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:DPLocalizedString(@"cancel", nil)
                                      style:UIBarButtonItemStyleDone target:self
                                      action:@selector(doneBtnFromKeyboardClicked:)];
//    [keyboardToolbar setTintColor:[UIColor whiteColor]];
//    [btnDoneOnKeyboard setTintColor:[UIColor blackColor]];
     [keyboardToolbar setItems:@[flexBarButton,btnDoneOnKeyboard]];
//    keyboardToolbar.items = @[flexBarButton, btnDoneOnKeyboard];
    self.textView.inputAccessoryView = keyboardToolbar;
    
//    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
//    [keyboardDoneButtonView sizeToFit];
//    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
//                                                                          target:self
//                                                                          action:nil];
//    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
//                                                                   style:UIBarButtonItemStyleDone
//                                                                  target:self
//                                                                  action:@selector(doneClicked:)];
//    [keyboardDoneButtonView setItems:@[flex,doneButton]];
//    [doneButton setTintColor:[[UIColor darkGrayColor]colorWithAlphaComponent:0.8f]];
    
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = estimatedHeight;

    if ([self isInternetConnect]){
        [self loadFeedbacks:_restaurantID];
    }
}

-(void) setupTextView{
    
    [_textView setTintColor: mainColor];
    _textView.text = @"";
    
    placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.text = DPLocalizedString(@"enter_text", nil);
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.textColor = [UIColor lightGrayColor];
    placeholderLabel.font = [UIFont fontWithName:@"Thonburi" size:restNameFontSize];
    _textView.font = [UIFont fontWithName:@"Thonburi" size:restNameFontSize];
    _textView.delegate = self;
    placeholderLabel.translatesAutoresizingMaskIntoConstraints = false;
    [_textView addSubview:placeholderLabel];
    
    [_textView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:20].active = YES;
    [_textView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-20].active = YES;
    
 
    
    [placeholderLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:22].active = YES;
    [placeholderLabel.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-22].active = YES;
    
    [placeholderLabel.topAnchor constraintEqualToAnchor:_textView.topAnchor].active = YES;
    if (screenWidth <322){
        [placeholderLabel.heightAnchor constraintEqualToConstant:24].active = YES;
    } else {
        [placeholderLabel.heightAnchor constraintEqualToConstant:24].active = YES;
    }
    
    
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)doneBtnFromKeyboardClicked:(id)sender
{
    NSLog(@"Done Button Clicked.");
    
    [_textView resignFirstResponder];
    [self setupTextView];
    option = @1;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    float newVerticalPosition = -keyboardSize.height;
    
    [placeholderLabel removeFromSuperview];
    
    [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
        
    
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self.textView resignFirstResponder];
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
    
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)loadFeedbacks:(NSNumber *)ident {
    RNGetFeedbacksRequest *request = [[RNGetFeedbacksRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        if (error || [response isKindOfClass:[NSNull class]]){
            NSLog(@"error %@",error);
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        }
        RNGetFeedbacksResponse *r = (RNGetFeedbacksResponse *)response;
        
        if (r.feedbacks != NULL){
            
            self.reviews = r.feedbacks;
            NSLog(@"r.feedbacks.count %lu",(unsigned long)r.feedbacks.count);
            
            [_tableView reloadData];
        }
    }];
}

#pragma mark - Actions

- (IBAction)backButton:(id)sender {
     [[NSNotificationCenter defaultCenter] postNotificationName:OpenCommentsNotification object:nil userInfo:@{@"isOpen" : @(_isFirstTimeOpen)}];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) addComment{
    
    if (_textView.text.length == 0) {
        return;
    }
    
    AddFeedbacksRequest *request = [[AddFeedbacksRequest alloc] init];
    request.restaurantId = _restaurantID;
    request.text = _textView.text;
    request.like = option;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        } else {
            
            AddFeedbacksResponse* r = (AddFeedbacksResponse *)response;
            NSString* responseString = r.responseString;
            NSLog(@"responseString %@",responseString);
            [self loadFeedbacks:_restaurantID];
            
            [_tableView reloadData];
            option = @1;
            _textView.text = @"";
            [_textView resignFirstResponder];
            
            [self setupTextView];
            
            [self showAlertWithTextForSuccessfully:DPLocalizedString(@"your_feedback", nil) withText:DPLocalizedString(@"success_added", nil)];
        }
    }];
}

- (IBAction)sendButton:(id)sender {
    if ([self isInternetConnect]){
        [self addComment];
    }
}

- (void)showAlertWithTextForSuccessfully:(NSString *) success withText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:success
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}



- (IBAction)likeButton:(id)sender {
    
    _likeButton.alpha = 1;
    _disLikeButton.alpha = 0.4;
    option = @1;
}

- (IBAction)disLikeButton:(id)sender {
    
    _likeButton.alpha = 0.4;
    _disLikeButton.alpha =1;
    option = @(-1);
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}





#pragma mark - UITableView


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_reviews.count > 0){
        return _reviews.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    CommentsCell* cell = (CommentsCell *)[tableView dequeueReusableCellWithIdentifier:@"CommentsCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row %2 ==1)
        cell.backgroundColor = [UIColor colorWithRed:.96 green:.96 blue:.96 alpha:1];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    RNFeedback* comment = [_reviews objectAtIndex:indexPath.row];
    
    NSString* date = [NSString stringWithFormat:@"%@",comment.date];
    date = [date stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
    
    cell.dateLabel.text = date;
    
    if (comment.name != nil && comment.name.length >0){
        NSString* userName = [NSString stringWithFormat:@"%@",comment.name];
        userName = [userName stringByReplacingOccurrencesOfString:@"<null>" withString:@"Ревизор"];
        userName = [userName stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
        userName = [userName stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        userName = [userName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        cell.userName.text = userName;
    } else if (comment.username == nil){
    cell.userName.text = @"Ревизор";
    } else {
        NSString* userName = [NSString stringWithFormat:@"%@",comment.username];
        userName = [userName stringByReplacingOccurrencesOfString:@"<null>" withString:@"Ревизор"];
        userName = [userName stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
        userName = [userName stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        userName = [userName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        cell.userName.text = userName;
    }
    
    
    [cell.userName sizeToFit];
    NSLog(@"comment.state %@",comment.state);
    
    
    if (comment.state.integerValue == 1){
        
        cell.likeIcon.image = [UIImage imageNamed:@"likeIcon"];
        
    } else if (comment.state.integerValue == 0){
        
        cell.likeIcon.hidden = YES;
        
    } else if (comment.state.integerValue == -1){
        
        cell.likeIcon.image = [UIImage imageNamed:@"disLikeIcon"];
    }
    
    
    if (comment.photo.length > 0) {
        [UIImage cachedImage:comment.photo
               fullURLString:@" "
                withCallBack:^(UIImage *image) {
                    cell.userPhoto.image = image;
                }];
    }
    if (isPhone){
        cell.userPhoto.layer.cornerRadius = 20;
    } else {
        cell.userPhoto.layer.cornerRadius = 40;
    }
    
    cell.userPhoto.layer.masksToBounds = true;
 
    cell.userPhoto.contentMode = UIViewContentModeScaleAspectFill;
    
    NSString* text = [NSString stringWithFormat:@"%@",comment.text];
    text = [text stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    
    if (text == nil || [comment.text  isEqual: @""]){
        cell.commentText.text = @"No comments No comments No comments ";
        cell.commentText.textColor = [UIColor clearColor];
        
    } else {
        cell.commentText.textColor = [UIColor darkGrayColor];
        cell.commentText.text = text;
    }
    
    
    [cell.commentText sizeToFit];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RNFeedback* comment = [_reviews objectAtIndex:indexPath.row];
    
    CGFloat addHeight = isPhone ? 20 : 40;
     CGFloat width = isPhone ? screenWidth -70 : screenWidth - 135;
        CGRect frame = CGRectMake(0, 0, width, 28);
        UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
        myTextView.text = comment.text;
        myTextView.font = [UIFont fontWithName:@"Thonburi" size:textFontSize];
        CGSize result = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    if (result.height + addHeight > estimatedHeight) {
        NSLog(@"result.height %f",result.height);
        return result.height + addHeight;
    } else {

        return estimatedHeight;
    }
}



@end
