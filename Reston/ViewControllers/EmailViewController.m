//
//  EmailViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/10/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "EmailViewController.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "WriteUSRequest.h"
#import "WriteUSResponse.h"
#import "RNUser.h"
#import "Reachability.h"

static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kEmailKey = @"EmailKey";
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface EmailViewController ()<UITextViewDelegate>{
    UIColor* mainColor;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UILabel* placeholderLabel;
    NSString* titleKeyboard;
    Reachability *_reachability;
    double textFieldFontSize;
    double buttonFontSize;
    double cornerRadius;
}

@end

@implementation EmailViewController

- (void)viewDidLoad {
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    _textView.text = @"";
    
    [self setupFields];
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    self.title = DPLocalizedString(@"email_title", nil);
    
    
    
    [self setupTextView];
    
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    NSString* titleSendButton = DPLocalizedString(@"send_mess", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleSendButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.submitButton.layer.cornerRadius = cornerRadius;
    self.submitButton.layer.masksToBounds = true;
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(doneBtnFromKeyboardClicked:)];
    [btnDoneOnKeyboard setTintColor:[UIColor lightGrayColor]];
    keyboardToolbar.items = @[flexBarButton, btnDoneOnKeyboard];
    _themeTextField.inputAccessoryView = keyboardToolbar;
    _fullNameTextField.inputAccessoryView = keyboardToolbar;
    _emailTextField.inputAccessoryView = keyboardToolbar;
    
    UIToolbar* messageToolbar = [[UIToolbar alloc] init];
    [messageToolbar sizeToFit];
    UIBarButtonItem *messageFlexBarButton = [[UIBarButtonItem alloc]
                                             initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                             target:self action:nil];
    UIBarButtonItem *messageDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                              style:UIBarButtonItemStyleDone target:self
                                                                             action:@selector(doneBtnFromKeyboardClicked:)];
//    [messageDoneOnKeyboard setTintColor:[UIColor lightGrayColor]];
    [messageDoneOnKeyboard setTintColor:mainColor];
    //    [messageToolbar setTintColor:[UIColor lightGrayColor]];
    messageToolbar.items = @[messageFlexBarButton, messageDoneOnKeyboard];
    _textView.inputAccessoryView = messageToolbar;
    
    [self setNavigationBarItem];
    // Do any additional setup after loading the view.
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) setupTextView{
    
    [_textView setTintColor: mainColor];
    
    if ([_textView.text isEqualToString: @""]){
    
    placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.text = DPLocalizedString(@"enter_text", nil);
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.textColor = [UIColor lightGrayColor];
    placeholderLabel.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    _textView.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    _textView.delegate = self;
    _textView.translatesAutoresizingMaskIntoConstraints = false;
    placeholderLabel.translatesAutoresizingMaskIntoConstraints = false;
    [_textView addSubview:placeholderLabel];
    
    [_textView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:20].active = YES;
    [_textView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-20].active = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        if (screenWidth <322){
            [_textView.topAnchor constraintEqualToAnchor:self.themeView.bottomAnchor constant:110].active = YES;
        } else {
            [_textView.topAnchor constraintEqualToAnchor:self.themeView.bottomAnchor constant:120].active = YES;
        }
        
    } else {
        
        NSLog(@"screenHeight %f", screenHeight);
        if (screenHeight == 1366){
            
            [_textView.heightAnchor constraintEqualToConstant:screenHeight - 422 -187].active = YES;
        } else {
            
            [_textView.heightAnchor constraintEqualToConstant:screenHeight - 375 -173].active = YES;
        }
        
    }
    
    
    
    [_textView.bottomAnchor constraintEqualToAnchor:self.submitButton.topAnchor constant:-16].active = YES;
    
    [placeholderLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:22].active = YES;
    [placeholderLabel.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-22].active = YES;
    
    [placeholderLabel.topAnchor constraintEqualToAnchor:_textView.topAnchor].active = YES;
    if (screenWidth <322){
        [placeholderLabel.heightAnchor constraintEqualToConstant:24].active = YES;
    } else {
        [placeholderLabel.heightAnchor constraintEqualToConstant:24].active = YES;
    }
        
  }
    
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)doneBtnFromKeyboardClicked:(id)sender
{
    NSLog(@"Done Button Clicked.");
    
    if (_textView.isFirstResponder){
        [_textView resignFirstResponder];
//        _textView.text = @"";
        [self setupTextView];
    } else {
        [_fullNameTextField resignFirstResponder];
        [_themeTextField resignFirstResponder];
        [_emailTextField resignFirstResponder];
    }
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;

    float newVerticalPosition = -keyboardSize.height;
    
    if (_textView.isFirstResponder){
        
        [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
        [placeholderLabel removeFromSuperview];
    }
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendButton:(id)sender {
    
    if (_textView.text.length == 0) {
        return;
    }
    if ([self isInternetConnect]){
        [self sendContactUsRequest];
    }
}

- (void)sendContactUsRequest {
    
    WriteUSRequest *request = [[WriteUSRequest alloc]init];
    request.userName = _fullNameTextField.text;
    request.email = _emailTextField.text;
    request.themeMessage = _themeTextField.text;
    request.message = _textView.text;
    
    if (_fullNameTextField.text.length!=0) {
        request.userName = _fullNameTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_name", nil)];
        return;
    }
    if (_emailTextField.text.length != 0) {
        request.email = _emailTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_email", nil)];
        return;
    }
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"ContactUSRequest");
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        WriteUSResponse *reservResponse = (WriteUSResponse *)response;
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            //return;
        }
        if ([reservResponse.contactus isKindOfClass:[NSString class]] && [reservResponse.contactus isEqualToString:@"success"]){
            NSLog(@"WriteUSResponse success: %@", reservResponse.contactus);
            
            _textView.text = @"";
            [_textView resignFirstResponder];
            [self setupTextView];
            [self showAlertWithTextForSuccessfully:DPLocalizedString(@"email_feedback", nil) withText:DPLocalizedString(@"success_send", nil)];
            
            
        } else {
            [self showAlertWithText:DPLocalizedString(@"unsuccess_send", nil)];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTextForSuccessfully:(NSString *) success withText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:success
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) setupFields{
    
    NSString* nameFB = [[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey];
    
    
    if ([RNUser sharedInstance].name.length > 0){
        self.fullNameTextField.text = [RNUser sharedInstance].name;
    } else if([[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey]){
        self.fullNameTextField.text = nameFB;
    }
    
    _fullNameTextField.textColor = mainColor;
    [_fullNameTextField setTintColor:mainColor];
    self.fullNameTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"full_name", nil)  withFontSize:textFieldFontSize];
    self.fullNameTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    
    _themeTextField.textColor = mainColor;
    [_themeTextField setTintColor:mainColor];
    
    self.themeTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"email_theme", nil)  withFontSize:textFieldFontSize];
    self.themeTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    
    if ([RNUser sharedInstance].email.length > 0){
        self.emailTextField.text = [RNUser sharedInstance].email;
    }
    _emailTextField.textColor = mainColor;
    [_emailTextField setTintColor:mainColor];
    _emailTextField.textColor = mainColor;
    [_emailTextField setTintColor:mainColor];
    
    self.emailTextField.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    self.emailTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
}

-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
    
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                 NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
