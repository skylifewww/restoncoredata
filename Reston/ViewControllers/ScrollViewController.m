//
//  ScrollViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "ScrollViewController.h"
#import "RNEditProfileRequest.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"
#import "RNDatePickerView.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookRegisterRequest.h"
#import "FacebookRegisterResponse.h"
#import "FacebookRegisterResponse.h"
#import "UploadIAvatarRequest.h"
#import "UploadIAvatarResponse.h"
//@import FirebaseAuth;
//@import FirebaseDatabase;
//@import FirebaseStorage;
#import "AFNetworking.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImage+Cache.h"


static NSString *const kUserKey = @"UserKey";
static NSString *const kPassKey = @"PassKey";
static NSString *const kUserPhotoKey = @"UserPhotoKey";
static NSString *const kUserBirthdayKey = @"UserBirthdayKey";
static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kPhoneKey = @"PhoneKey";
static NSString *const kEmailKey = @"EmailKey";
static NSString *const kEmailFBKey = @"EmailFBKey";
static NSString *const kClientIDKey = @"ClientIDKey";
static NSString *const kStrategyKey = @"StrategyKey";
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface ScrollViewController ()<RNDatePickerViewDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate>{
    BOOL keyboardIsShown;
    UIColor* mainColor;
    CGFloat screenWidth;
    CGFloat screenHeight;
    Boolean changedPic;
    double textFieldFontSize;
    double buttonFontSize;
    NSString* strFotoUser;
    NSString *postLength;
//    NSString* imageUrlFIR;
    UIImage* chosenImage;
    Boolean isChangePhoto;
    NSString* filePathImage;
    Boolean isSuccessUploadImage;
    
//    NSURL *documents;
//    NSURL *fileURL;
    
    Reachability *_reachability;
}
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDateFormatter *dayFormatter;
@property (strong, nonatomic) NSDateFormatter *monthFormatter;
@property (strong, nonatomic) NSDateFormatter *yearFormatter;
@property (strong, nonatomic) NSDate *dateOfBirth;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
//@property (strong, nonatomic) FIRStorageReference *storageRef;

@end

@implementation ScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    self.storageRef = [[FIRStorage storage] reference];
//
//    if (![FIRAuth auth].currentUser) {
//        [[FIRAuth auth] signInAnonymouslyWithCompletion:^(FIRUser * _Nullable user,
//                                                          NSError * _Nullable error) {
//            if (error) {
////                NSLog(@"[FIRAuth auth] error");
//            } else {
////                NSLog(@"[FIRAuth auth]");
//                [self setValuesIntoFields];
//            }
//        }];
//    }
    
    [self configureFormatters];
    
//    imageUrlFIR = [RNUser sharedInstance].photo;
    filePathImage = [RNUser sharedInstance].photo;
    isChangePhoto = NO;
    isSuccessUploadImage = NO;
    
    

    [self setValuesIntoFields];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    
    
   mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    screenWidth = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    screenHeight = [UIApplication sharedApplication].keyWindow.frame.size.height - 40;
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doneClicked:)]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
//    [self setValuesIntoFields];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    [doneButton setTintColor:[[UIColor darkGrayColor]colorWithAlphaComponent:0.8f]];
   
    _emailTextField.inputAccessoryView = keyboardDoneButtonView;

    _phoneTextField.inputAccessoryView = keyboardDoneButtonView;

    _fullNameTextField.inputAccessoryView = keyboardDoneButtonView;
    _passwordTextField.inputAccessoryView = keyboardDoneButtonView;
    _confirmTextField.inputAccessoryView = keyboardDoneButtonView;
    
    _emailTextField.delegate = self;
    _phoneTextField.delegate = self;
    _fullNameTextField.delegate = self;
    _passwordTextField.delegate = self;
    _confirmTextField.delegate = self;
    
    [self setupView];
//    NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
  

}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
  
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) fetchProfile{
    
//    NSLog(@"fetchProfile");
    
    NSDictionary*  parameters = @{@"fields": @"id, first_name, last_name, email, picture.type(large)"};
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"me"] parameters:parameters
                                  
                                                                   HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (!error) {
            NSLog(@"result = %@",result);
            NSString* email = [result objectForKey:@"email"];
            NSString* first_name = [result objectForKey:@"first_name"];
            NSString* last_name = [result objectForKey:@"last_name"];
            NSString* name = [NSString stringWithFormat:@"%@ %@",first_name, last_name];
            NSString* clientIDStr = [result objectForKey:@"id"];
            NSInteger intID = clientIDStr.integerValue;
            
            NSString* clientID = [NSString stringWithFormat:@"%ld",intID];
            
            
//            NSLog(@"clientID %@",clientID);
            
            NSDictionary *dictionary = (NSDictionary *)[result objectForKey:@"picture"];
            NSDictionary *data = [dictionary objectForKey:@"data"];
            NSString *photoUrl = (NSString *)[data objectForKey:@"url"];
            
//            NSLog(@"email is %@", [result objectForKey:@"email"]);
            
            NSData  *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoUrl]];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:dataImage forKey:kUserPhotoKey];
            [[NSUserDefaults standardUserDefaults] setObject:name
                                                      forKey:kFBNameKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [self sendLoginRequestWithFacebookClientID:clientID nameFB:name emailFB:email photoFB:(NSString*)photoUrl];
        } else {
//            NSLog(@"%@", error);
            return;
        }}
     ];
}

- (void)enterFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([self isInternetConnect]){
        [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error != nil || [result isKindOfClass:[NSNull class]])
            {
                
//                NSLog(@"Unexpected login error: %@", error);
                NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: DPLocalizedString(@"try_later", nil);
                NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: DPLocalizedString(@"server_error", nil);
                
                [self showAlertWithTitle:alertTitle andMessage:alertMessage];
                
            }
            else
            {
                if(result.token)
                {
                    
                    [self fetchProfile];
                }        
            }
        }];
    }
}

- (void)sendLoginRequestWithFacebookClientID:(NSString*)clientId nameFB:(NSString*)nameFB emailFB:(NSString*)emailFB photoFB:(NSString*)photoFB{
    FacebookRegisterRequest *request = [[FacebookRegisterRequest alloc]init];
    request.clientId = clientId;
    request.nameFB = nameFB;
    request.emailFB = emailFB;
    request.photoFB = photoFB;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        FacebookRegisterResponse *authResponse = (FacebookRegisterResponse *)response;
        //        [RNUser sharedInstance].email = email;
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
            
//            NSLog(@"error: %@",error);
         [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        }
        
//        NSLog(@"authResponse.token: %@",authResponse.token);
//        NSLog(@"authResponse.autorisation: %@",authResponse.autorisation);
        if (authResponse.token > 0) {
            
//            NSLog(@"authResponse.token > 0!!!");
            [RNUser sharedInstance].token = authResponse.token;
            [RNUser sharedInstance].name = nameFB;
            [RNUser sharedInstance].email = emailFB;
            
            [RNUser sharedInstance].photoFB = photoFB;
            
            if ([RNUser sharedInstance].photo.length == 0){
                
                NSData  *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoFB]];
                UIImage *userPhoto = [UIImage imageWithData:imageData];

                [self sendImageToServer:userPhoto];
            }
            
           
                self.fbButton.hidden = YES;
            
            
            [[NSUserDefaults standardUserDefaults] setObject:emailFB
                                                      forKey:kEmailFBKey];
            [[NSUserDefaults standardUserDefaults] setObject:clientId
                                                      forKey:kClientIDKey];
            [[NSUserDefaults standardUserDefaults] setObject:nameFB
                                                      forKey:kFBNameKey];
            [[NSUserDefaults standardUserDefaults] setObject:@"FBSTRATEGY"
                                                      forKey:kStrategyKey];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self setValuesIntoFields];
            // TODO: continue
        } else {
            self.fbButton.hidden = NO;
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    }];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}


- (IBAction)selectPhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    self.userPhotoImage.image = chosenImage;
    isChangePhoto = YES;
  
    NSData *pngImageData = UIImageJPEGRepresentation(chosenImage, 1.0);
    strFotoUser = [[NSString alloc] initWithData:pngImageData encoding:NSASCIIStringEncoding];

    [[NSUserDefaults standardUserDefaults] setObject:pngImageData forKey:kUserPhotoKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//     [self setValuesIntoFields];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_fullNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmTextField resignFirstResponder];
}

- (void)configureFormatters {
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.dateFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
        
        self.dayFormatter = [[NSDateFormatter alloc] init];
        [self.dayFormatter setDateFormat:@"dd"];
        self.dayFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
        
        self.monthFormatter = [[NSDateFormatter alloc] init];
        [self.monthFormatter setDateFormat:@"MMMM"];
        self.monthFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
        
        self.yearFormatter = [[NSDateFormatter alloc] init];
        [self.yearFormatter setDateFormat:@"yyyy"];
        self.yearFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.dateFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
        
        self.dayFormatter = [[NSDateFormatter alloc] init];
        [self.dayFormatter setDateFormat:@"dd"];
        self.dayFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
        
        self.monthFormatter = [[NSDateFormatter alloc] init];
        [self.monthFormatter setDateFormat:@"MMMM"];
        self.monthFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
        
        self.yearFormatter = [[NSDateFormatter alloc] init];
        [self.yearFormatter setDateFormat:@"yyyy"];
        self.yearFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
    }
    

}

- (void)showDatePicker {
    if ([self.view viewWithTag:0xDEADBEEF]) {
        return;
    }
    
        RNDatePickerView *picker = [RNDatePickerView viewWithDelegate:self];
    if ([[RNUser sharedInstance].birthDate isKindOfClass:[NSDate class]] && [RNUser sharedInstance].birthDate != nil){
        
        picker.datePicker.date = [_dateFormatter dateFromString:[RNUser sharedInstance].birthDate];
        
        
    } else if([[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey] && ![[[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey] isEqual: @""]){
        picker.datePicker.date = [[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey];
    } else {
        
        picker.datePicker.date = [NSDate date];
    }
        picker.datePicker.datePickerMode = UIDatePickerModeDate;
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        picker.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        picker.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"uk"];
    }
        picker.tag = 0xDEADBEEF;
        picker.identifier= 1;
//        picker.datePicker.minimumDate = [NSDate date];
        [picker showInView:self.view];
}

#pragma mark - RNDatePickerViewDelegate

- (void)datePicker:(RNDatePickerView *)picker didCancelWithDate:(NSDate *)date {
    [picker dismissFromView:self.view
                 completion:NULL];
}

- (void)datePicker:(RNDatePickerView *)picker didChooseDate:(NSDate *)date {
 
    [picker dismissFromView:self.view
                 completion:NULL];
    [RNUser sharedInstance].birthDate = [_dateFormatter stringFromDate:date];
    _dateOfBirth = date;
    self.numDayLabel.text = [_dayFormatter stringFromDate:date];
    self.numMonthLabel.text = [_monthFormatter stringFromDate:date];
    self.numYearLabel.text = [_yearFormatter stringFromDate:date];
    
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:kUserBirthdayKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)dateChoiseButton:(id)sender {
    [self showDatePicker];
}
- (void)doneClicked:(id)sender {
    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_fullNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmTextField resignFirstResponder];
}

- (IBAction)submitButton:(id)sender {
    [_passwordTextField resignFirstResponder];
    [_confirmTextField resignFirstResponder];
    if ([self isInternetConnect]){
        if (isChangePhoto == YES){
            [self sendImageToServer:chosenImage];
            
        } else {
        
            [self sendEditRequest];
        }
    }
}

- (IBAction)enterWithFB:(id)sender {
    [self enterFacebook];
}

- (void)sendImageToServer:(UIImage*)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[imageData length]];
    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
    
    // Init the URLRequest
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:imageData];
    //    NSMutableData *body = [NSMutableData data];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request fromData:nil progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error!!!!!!!: %@", error);
        } else {
            NSLog(@"responseObject:%@", responseObject);
            
            
            if ([responseObject objectForKey:@"token"] && [responseObject objectForKey:@"file_path"]){
                [RNUser sharedInstance].token = [responseObject objectForKey:@"token"];
                NSString* strURL = [responseObject objectForKey:@"file_path"];
                filePathImage = [strURL stringByReplacingOccurrencesOfString:@"/uploads/img/user/" withString:@""];
                //                filePathImage = [strURL substringFromIndex: [strURL length] - 16];
                NSLog(@"filePathImage:%@", filePathImage);
                isSuccessUploadImage = YES;
            }
            
            
            NSLog(@"filePath: %@", filePathImage);
            
            if (isSuccessUploadImage == YES && filePathImage.length > 0){
                NSLog(@"[self sendEditRequest]");
                [self sendEditRequest];
            }
        }
        
    }];
    [uploadTask resume];
}



- (void)sendEditRequest{

    NSString *phoneText = _phoneTextField.text;
    
    RNEditProfileRequest *request = [[RNEditProfileRequest alloc]init];
    request.userName = _fullNameTextField.text;

    request.birthDate = [_dateFormatter stringFromDate:self.dateOfBirth];
    request.email = _emailTextField.text;
    

    if (_passwordTextField.text.length!=0 && [_passwordTextField.text isEqual:_confirmTextField.text]) {
        request.password = _passwordTextField.text;
    }
    
    if (phoneText.length > 0 && phoneText.length < 12) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"format_phone", nil)
                                                                       message:DPLocalizedString(@"need_phone", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }

    if (phoneText.length !=0) {
        request.phone = phoneText;

        [[NSUserDefaults standardUserDefaults] setObject:phoneText forKey:kPhoneKey];
    }
    
    if (_emailTextField.text.length !=0) {

        [[NSUserDefaults standardUserDefaults] setObject:_emailTextField.text forKey:kEmailKey];
    }

//    if (imageUrlFIR.length == 36) {
//        request.photo = imageUrlFIR;
//    }
    
//    if (filePathImage.length > 0) {
    NSLog(@"request.photo = filePathImage %@", filePathImage);
        request.photo = filePathImage;
//    }

    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        RNAuthorizationResponse *authResponse = (RNAuthorizationResponse *)response;
        NSLog(@"response %@",response);
        NSLog(@"authResponse %@",authResponse.editProfile);
        if ([authResponse.editProfile isEqualToString:@"success"]){
            [RNUser sharedInstance].name =_fullNameTextField.text;
            [RNUser sharedInstance].phone = _phoneTextField.text;
            [RNUser sharedInstance].email = _emailTextField.text;
            
            _passwordTextField.text = @"";
            _confirmTextField.text = @"";
            [_emailTextField resignFirstResponder];
            [_phoneTextField resignFirstResponder];
            [_fullNameTextField resignFirstResponder];
            [_passwordTextField resignFirstResponder];
            [_confirmTextField resignFirstResponder];
            
//            [self showAlertWithTitle:@"RestOn" andMessage:DPLocalizedString(@"save_details", nil)];
            [self showAlertWithAnimationForSuccessfully:@"RestOn" withText:DPLocalizedString(@"save_details", nil)];
            
        } else
//            if (error != nil || [response isKindOfClass:[NSNull class]] || [authResponse.editProfile isKindOfClass:[NSNull class]])
        {
             [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    } ];
}

- (void)showAlertWithAnimationForSuccessfully:(NSString *) success withText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:success
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:^{
        [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             alert.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             [alert dismissViewControllerAnimated:YES completion:^{
                             }];
                         }];
    }];
}
    
- (void)showAlertWithText:(NSString *)text {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                       message:text
                                                                preferredStyle:UIAlertControllerStyleAlert]; // 1
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel]; // 4
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGFloat keyboardHeight = 0.0f;
    CGFloat heightKey = [[NSUserDefaults standardUserDefaults] floatForKey:kKeyboardHeightKey];
    if (keyboardSize.height > 0){
        if (heightKey > 0 && heightKey == keyboardSize.height){
            keyboardHeight = [[NSUserDefaults standardUserDefaults] floatForKey:kKeyboardHeightKey];
//            NSLog(@" floatForKey:kKeyboardHeightKey %f", keyboardHeight);
        } else {
            
            keyboardHeight = keyboardSize.height;
            [[NSUserDefaults standardUserDefaults] setFloat:keyboardHeight forKey:kKeyboardHeightKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
//            NSLog(@" keyboardHeight = keyboardSize.height %f", keyboardHeight);
        }
    } else {
        if (heightKey > 0){
            keyboardHeight = [[NSUserDefaults standardUserDefaults] floatForKey:kKeyboardHeightKey];
//            NSLog(@" floatForKey:kKeyboardHeightKey %f", keyboardHeight);
            
        }
    }
    
    
//    NSLog(@"screenWidth %f", screenWidth);
//    NSLog(@"screenHeight %f", screenHeight);
//    NSLog(@"keyboardSize.height %f", keyboardSize.height);
//    NSLog(@"newVerticalPosition %f", keyboardHeight);
    
    float newVerticalPosition = -keyboardHeight;
    CGFloat keyY = self.view.frame.size.height + newVerticalPosition;
//    CGFloat reservY = self.reservedView.frame.origin.y;
    CGFloat textY = _phone2View.frame.origin.y + _phone2View.frame.size.height + 10;
    

    if (_confirmTextField.isFirstResponder || _passwordTextField.isFirstResponder) {
        
        [self moveFrameToVerticalPosition:newVerticalPosition forDuration:0.3f];
    }
    if (_fullNameTextField.isFirstResponder){
        if (textY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
        }
    }
    if (_emailTextField.isFirstResponder){
        if (textY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
        }
    }
    if (_phoneTextField.isFirstResponder){
        if (textY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
        }
    }
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}


- (void)setValuesIntoFields{
    
    if ([RNUser sharedInstance].token.length > 0){
        self.fbButton.hidden = YES;
    } else {
        self.fbButton.hidden = NO;
    }

    if ([RNUser sharedInstance].photo.length > 0){

        NSString* urlImage = [NSString stringWithFormat:@"%@", [RNUser sharedInstance].photo];
//        NSString* urlImage1 = [NSString stringWithFormat:@"http://reston.com.ua/uploads/users/%@", [RNUser sharedInstance].photo];
//        NSLog(@"urlImage from server--------------------------------------------------------------------------- %@", urlImage);
//
        if (urlImage.length > 0){
            [UIImage cachedImage:urlImage
                   fullURLString:@"http://reston.com.ua/uploads/img/user/"
                    withCallBack:^(UIImage *image) {

                        self.userPhotoImage.image = image;
                    }];

        } else {

            self.userPhotoImage.image = [UIImage imageNamed:@"userPhoto"];
            //        cell.backgroundColor = mainColor;
        }
        
//        NSData  *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlImage1]];
//        UIImage *userPhoto = [UIImage imageWithData:imageData];
//        //         [self uploadImageToServer:userPhoto];
//        //        [self uploadImage:userPhoto];
////        [self uploadImageToFIRStore:userPhoto];
//        self.userPhotoImage.image = userPhoto;
//    }
//    else if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserPhotoKey] != nil){
////        NSLog(@"kUserPhotoKey] != nil");
//        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:kUserPhotoKey];
//        UIImage *userPhoto = [UIImage imageWithData:imageData];
////         [self uploadImageToServer:userPhoto];
////        [self uploadImage:userPhoto];
////        [self uploadImageToFIRStore:userPhoto];
//        self.userPhotoImage.image = userPhoto;
    } else {
//        NSLog(@"kUserPhotoKey] == nil");
        self.userPhotoImage.image = [UIImage imageNamed:@"userPhoto"];
    }
    
    NSString* nameFB = [[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey];
    if ([RNUser sharedInstance].name.length >= nameFB.length){
        self.fullNameTextField.text = [RNUser sharedInstance].name;
    } else if([[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey]){
        self.fullNameTextField.text = nameFB;
    }
    
    if ([RNUser sharedInstance].phone.length > 0 && [RNUser sharedInstance].phone.integerValue != 0){
        self.phoneTextField.text = [RNUser sharedInstance].phone;
//        NSLog(@"[RNUser sharedInstance].phone %@", [RNUser sharedInstance].phone);
    } else if ([[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey]){
        self.phoneTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey];
//        NSLog(@"objectForKey:kPhoneKey %@", [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey]);
    }
    
    if ([RNUser sharedInstance].email.length > 0){
        self.emailTextField.text = [RNUser sharedInstance].email;
//        NSLog(@"[RNUser sharedInstance].email %@", [RNUser sharedInstance].email);
    } else if ([[NSUserDefaults standardUserDefaults] objectForKey:kEmailKey]){
        self.emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kEmailKey];
//        NSLog(@"objectForKey:kEmailKey %@", [[NSUserDefaults standardUserDefaults] objectForKey:kEmailKey]);
    }

    if ([[RNUser sharedInstance].birthDate isKindOfClass:[NSString class]] && [RNUser sharedInstance].birthDate != nil){
//         NSLog(@"[RNUser sharedInstance].birthDate != nil======== %@", [RNUser sharedInstance].birthDate);
        
        _dateOfBirth = [_dateFormatter dateFromString:[RNUser sharedInstance].birthDate];
//        NSLog(@"_dateOfBirth %@", _dateOfBirth);
        self.numDayLabel.text = [_dayFormatter stringFromDate:self.dateOfBirth];
        self.numMonthLabel.text = [_monthFormatter stringFromDate:self.dateOfBirth];
        self.numYearLabel.text = [_yearFormatter stringFromDate:self.dateOfBirth];
        
    } else if([[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey] && ![[[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey] isEqual: @""]){
        
//        NSLog(@"[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey %@",[[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey]);
        _dateOfBirth = [[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey];
        self.numDayLabel.text = [_dayFormatter stringFromDate:self.dateOfBirth];
        self.numMonthLabel.text = [_monthFormatter stringFromDate:self.dateOfBirth];
        self.numYearLabel.text = [_yearFormatter stringFromDate:self.dateOfBirth];
    } else {
        
//        NSLog(@"DPLocalizedString(");
        self.numDayLabel.text = DPLocalizedString(@"day", nil);
        self.numMonthLabel.text = DPLocalizedString(@"month", nil);
        self.numYearLabel.text = DPLocalizedString(@"year", nil);
    }
//    NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
}

//- (NSString *)formattedNumber {
//    NSString *result = [RNUser sharedInstance].phone;
//    if (result.length != 12) {
//        return result;
//    }
//    NSString *l2 = [result substringToIndex:2];
//    NSString *l3 = [result substringWithRange:NSMakeRange(2, 3)];
//    NSString *l32 = [result substringWithRange:NSMakeRange(5, 3)];
//    NSString *l22 = [result substringWithRange:NSMakeRange(8, 2)];
//    NSString *l23 = [result substringWithRange:NSMakeRange(10, 2)];
//
//    return [NSString stringWithFormat:@"%@ (%@) %@-%@-%@",l2,l3,l32,l22,l23];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_fullNameTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmTextField resignFirstResponder];
    
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//        if (textField != _phoneTextField) {
//            return YES;
//        }
//        if (_phoneTextField.text.length == 0) {
//            _phoneTextField.text = @"38";
//            return NO;
//        }
//        if (_phoneTextField.text.length == 2 && range.length >0) {
//            _phoneTextField.text = @"38";
//            return NO;
//        }
//        if (range.length > 0) {
//            return YES;
//        }
//        if (_phoneTextField.text.length == 18) {
//            return NO;
//        }
//
//        if (_phoneTextField.text.length == 2) {
//            _phoneTextField.text = [NSString stringWithFormat:@"%@ (",_phoneTextField.text];
//        }
//        if (_phoneTextField.text.length == 7) {
//            _phoneTextField.text = [NSString stringWithFormat:@"%@) ",_phoneTextField.text];
//        }
//        if (_phoneTextField.text.length == 8) {
//            _phoneTextField.text = [NSString stringWithFormat:@"%@ ",_phoneTextField.text];
//        }
//        if (_phoneTextField.text.length == 12) {
//            _phoneTextField.text = [NSString stringWithFormat:@"%@-",_phoneTextField.text];
//        }
//        if (_phoneTextField.text.length == 15) {
//            _phoneTextField.text = [NSString stringWithFormat:@"%@-",_phoneTextField.text];
//        }
//
//        return YES;
//    }
//
//

////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    self.fullNameView.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordView.translatesAutoresizingMaskIntoConstraints = false;
    self.phone1View.translatesAutoresizingMaskIntoConstraints = false;
    self.confirmView.translatesAutoresizingMaskIntoConstraints = false;
    self.phone2View.translatesAutoresizingMaskIntoConstraints = false;
   
    self.photoContainer.translatesAutoresizingMaskIntoConstraints = false;
    self.userPhotoImage.translatesAutoresizingMaskIntoConstraints = false;
    self.addphotoLabel.translatesAutoresizingMaskIntoConstraints  =false;
    
    self.dateContainerView.translatesAutoresizingMaskIntoConstraints = false;
    self.passwContainerView.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordlabel.translatesAutoresizingMaskIntoConstraints = false;
    self.birthDateLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.fbButton.translatesAutoresizingMaskIntoConstraints = false;
    self.dayLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.monthLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.yearLabel.translatesAutoresizingMaskIntoConstraints = false;
//    self.cityNamelabel.translatesAutoresizingMaskIntoConstraints = false;
    self.numDayLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.dayTreanglImage.translatesAutoresizingMaskIntoConstraints = false;
    self.numMonthLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.monthTrenglImage.translatesAutoresizingMaskIntoConstraints = false;
   
//    self.cityNamelabel.translatesAutoresizingMaskIntoConstraints = false;
//    self.city.translatesAutoresizingMaskIntoConstraints = false;
//    self.cityLabel.translatesAutoresizingMaskIntoConstraints = false;
//    self.cityTreanglImage.translatesAutoresizingMaskIntoConstraints = false;
    self.numYearLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.yearTreanglImage.translatesAutoresizingMaskIntoConstraints = false;
    self.dateChoiseButton.translatesAutoresizingMaskIntoConstraints = false;
    self.photoButton.translatesAutoresizingMaskIntoConstraints = false;
    
    
    self.fullNameTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.fullNameTextField.textColor = [UIColor darkGrayColor];
    [self.fullNameTextField setTintColor:mainColor];

    self.fullNameTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.fullNameTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"full_name", nil) withFontSize:textFieldFontSize];
    
    
    self.phoneTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.phoneTextField.textColor = [UIColor darkGrayColor];
    [self.phoneTextField setTintColor:mainColor];
    self.phoneTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.phoneTextField.attributedPlaceholder = [self placeholderString:@"38 000 000 00 00" withFontSize:textFieldFontSize];

    
    self.emailTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.emailTextField.textColor = [UIColor darkGrayColor];
    [self.emailTextField setTintColor:mainColor];
    self.emailTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.emailTextField.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    
    
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordTextField.textColor = [UIColor darkGrayColor];
    [self.passwordTextField setTintColor:mainColor];
    self.passwordTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.passwordTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"pass", nil) withFontSize:textFieldFontSize];
    
    
    self.confirmTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.confirmTextField.textColor = [UIColor darkGrayColor];
    [self.confirmTextField setTintColor:mainColor];
    self.confirmTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.confirmTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"conf_pass", nil) withFontSize:textFieldFontSize];

    
    
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.submitButton.titleLabel.text = DPLocalizedString(@"apply", nil);
    
    

    /////////////////////////////////////////////////////////////////////////////////////////////
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        
        self.submitButton.layer.cornerRadius = 3;
        self.submitButton.layer.masksToBounds = true;
        
        
        
        [self.photoContainer.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor].active = YES;
        [self.photoContainer.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor].active = YES;
        [self.photoContainer.topAnchor constraintEqualToAnchor:self.containerView.topAnchor].active = YES;
        [self.photoContainer.heightAnchor constraintEqualToConstant:130].active = YES;
        
        self.userPhotoImage.layer.cornerRadius = 50.0f;
        self.userPhotoImage.layer.masksToBounds = YES;
        
        [self.userPhotoImage.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor  constant:16].active = YES;
        [self.userPhotoImage.centerYAnchor constraintEqualToAnchor:self.photoContainer.centerYAnchor].active = YES;
        [self.userPhotoImage.widthAnchor constraintEqualToConstant:100].active = YES;
        [self.userPhotoImage.heightAnchor constraintEqualToConstant:100].active = YES;
        
        [self.addphotoLabel.leftAnchor constraintEqualToAnchor:self.userPhotoImage.rightAnchor constant:16].active = YES;
        [self.addphotoLabel.centerYAnchor constraintEqualToAnchor:self.userPhotoImage.centerYAnchor].active = YES;
        [self.addphotoLabel.widthAnchor constraintEqualToConstant:101].active = YES;
        [self.addphotoLabel.heightAnchor constraintEqualToConstant:30].active = YES;
        
        [self.photoButton.leftAnchor constraintEqualToAnchor:self.userPhotoImage.leftAnchor].active = YES;
        [self.photoButton.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor].active = YES;
        [self.photoButton.topAnchor constraintEqualToAnchor:self.userPhotoImage.topAnchor].active = YES;
        [self.photoButton.bottomAnchor constraintEqualToAnchor:self.userPhotoImage.bottomAnchor].active = YES;
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        [self.fullNameView.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:14].active = YES;
        [self.fullNameView.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-14].active = YES;
        [self.fullNameView.topAnchor constraintEqualToAnchor:self.photoContainer.bottomAnchor
                                                    constant:screenHeight * 0.0].active = YES;
        [self.fullNameView.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.fullNameTextField.leftAnchor constraintEqualToAnchor:self.fullNameView.leftAnchor constant:8].active = YES;
        [self.fullNameTextField.rightAnchor constraintEqualToAnchor:self.fullNameView.rightAnchor constant:-8].active = YES;
        [self.fullNameTextField.centerYAnchor constraintEqualToAnchor:self.fullNameView.centerYAnchor].active = YES;
        
        
        [self.phone1View.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
        [self.phone1View.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
        [self.phone1View.topAnchor constraintEqualToAnchor:self.fullNameView.bottomAnchor
                                                  constant:16].active = YES;
        [self.phone1View.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.emailTextField.leftAnchor constraintEqualToAnchor:self.phone1View.leftAnchor constant:8].active = YES;
        [self.emailTextField.rightAnchor constraintEqualToAnchor:self.phone1View.rightAnchor constant:-8].active = YES;
        [self.emailTextField.centerYAnchor constraintEqualToAnchor:self.phone1View.centerYAnchor].active = YES;
        
        
        [self.phone2View.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
        [self.phone2View.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
        [self.phone2View.topAnchor constraintEqualToAnchor:self.phone1View.bottomAnchor
                                                  constant:16].active = YES;
        [self.phone2View.heightAnchor constraintEqualToConstant:44].active = YES;
        
        [self.phoneTextField.leftAnchor constraintEqualToAnchor:self.phone2View.leftAnchor constant:8].active = YES;
        [self.phoneTextField.rightAnchor constraintEqualToAnchor:self.phone2View.rightAnchor constant:-8].active = YES;
        [self.phoneTextField.centerYAnchor constraintEqualToAnchor:self.phone2View.centerYAnchor].active = YES;
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        if (screenWidth < 322) {
            
            [self.dateContainerView.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:14].active = YES;
            [self.dateContainerView.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-14].active = YES;
            [self.dateContainerView.topAnchor constraintEqualToAnchor:self.phone2View.bottomAnchor].active = YES;
            [self.dateContainerView.heightAnchor constraintEqualToConstant:90].active = YES;
            
            
            [self.birthDateLabel.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor constant:0].active = YES;
            [self.birthDateLabel.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant: 16].active = YES;
            [self.birthDateLabel.widthAnchor constraintEqualToConstant:128].active = YES;
            [self.birthDateLabel.heightAnchor constraintEqualToConstant:21].active = YES;
            
            
            [self.yearLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: 0].active = YES;
            [self.yearLabel.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant: 49].active = YES;
            [self.yearLabel.widthAnchor constraintEqualToConstant:57].active = YES;
            [self.yearLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numYearLabel.leftAnchor constraintEqualToAnchor:self.yearLabel.leftAnchor constant: 2].active = YES;
            [self.numYearLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.numYearLabel.widthAnchor constraintEqualToConstant:32].active = YES;
            [self.numYearLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.yearTreanglImage.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: -2].active = YES;
            [self.yearTreanglImage.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.yearTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.yearTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.monthLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: -63].active = YES;
            [self.monthLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.monthLabel.widthAnchor constraintEqualToConstant:82].active = YES;
            [self.monthLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numMonthLabel.leftAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: 2].active = YES;
            [self.numMonthLabel.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.numMonthLabel.widthAnchor constraintEqualToConstant:56].active = YES;
            [self.numMonthLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.monthTrenglImage.rightAnchor constraintEqualToAnchor:self.monthLabel.rightAnchor constant: -2].active = YES;
            [self.monthTrenglImage.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.monthTrenglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.monthTrenglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.dayLabel.rightAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: -10].active = YES;
            [self.dayLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            
            [self.dayLabel.widthAnchor constraintEqualToConstant:42].active = YES;
            [self.dayLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numDayLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: 2].active = YES;
            [self.numDayLabel.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.numDayLabel.widthAnchor constraintEqualToConstant:30].active = YES;
            [self.numDayLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.dayTreanglImage.rightAnchor constraintEqualToAnchor:self.dayLabel.rightAnchor constant: -2].active = YES;
            [self.dayTreanglImage.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.dayTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.dayTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            [self.dateChoiseButton.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: -2].active = YES;
            [self.dateChoiseButton.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: 2].active = YES;
            [self.dateChoiseButton.topAnchor constraintEqualToAnchor:self.dayLabel.topAnchor constant: -5].active = YES;
            [self.dateChoiseButton.bottomAnchor constraintEqualToAnchor:self.dayLabel.bottomAnchor constant: 5].active = YES;
            
            
            //        [self.city.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor].active = YES;
            //        [self.city.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant:90].active = YES;
            //        [self.city.widthAnchor constraintEqualToConstant:100].active = YES;
            //        [self.city.heightAnchor constraintEqualToConstant:21].active = YES;
            //
            //
            //
            //        [self.cityLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor].active = YES;
            //        [self.cityLabel.centerYAnchor constraintEqualToAnchor:self.city.centerYAnchor].active = YES;
            //        [self.cityLabel.widthAnchor constraintEqualToConstant:156].active = YES;
            //        [self.cityLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            //
            //        [self.cityNamelabel.leftAnchor constraintEqualToAnchor:self.cityLabel.leftAnchor constant: 2].active = YES;
            //        [self.cityNamelabel.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //        [self.cityNamelabel.widthAnchor constraintEqualToConstant:140].active = YES;
            //        [self.cityNamelabel.heightAnchor constraintEqualToConstant:16].active = YES;
            //
            //        [self.cityTreanglImage.rightAnchor constraintEqualToAnchor:self.cityLabel.rightAnchor constant: -2].active = YES;
            //        [self.cityTreanglImage.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //        [self.cityTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            //        [self.cityTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
        } else {
            
            [self.dateContainerView.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:14].active = YES;
            [self.dateContainerView.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-14].active = YES;
            [self.dateContainerView.topAnchor constraintEqualToAnchor:self.phone2View.bottomAnchor].active = YES;
            [self.dateContainerView.heightAnchor constraintEqualToConstant:90].active = YES;
            
            
            [self.birthDateLabel.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor constant:0].active = YES;
            [self.birthDateLabel.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant: 34].active = YES;
            [self.birthDateLabel.widthAnchor constraintEqualToConstant:128].active = YES;
            [self.birthDateLabel.heightAnchor constraintEqualToConstant:21].active = YES;
            
            
            [self.yearLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: 0].active = YES;
            [self.yearLabel.centerYAnchor constraintEqualToAnchor:self.birthDateLabel.centerYAnchor].active = YES;
            [self.yearLabel.widthAnchor constraintEqualToConstant:57].active = YES;
            [self.yearLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numYearLabel.leftAnchor constraintEqualToAnchor:self.yearLabel.leftAnchor constant: 2].active = YES;
            [self.numYearLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.numYearLabel.widthAnchor constraintEqualToConstant:32].active = YES;
            [self.numYearLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.yearTreanglImage.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: -2].active = YES;
            [self.yearTreanglImage.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.yearTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.yearTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.monthLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: -63].active = YES;
            [self.monthLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.monthLabel.widthAnchor constraintEqualToConstant:82].active = YES;
            [self.monthLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numMonthLabel.leftAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: 2].active = YES;
            [self.numMonthLabel.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.numMonthLabel.widthAnchor constraintEqualToConstant:56].active = YES;
            [self.numMonthLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.monthTrenglImage.rightAnchor constraintEqualToAnchor:self.monthLabel.rightAnchor constant: -2].active = YES;
            [self.monthTrenglImage.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.monthTrenglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.monthTrenglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.dayLabel.rightAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: -10].active = YES;
            [self.dayLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            
            [self.dayLabel.widthAnchor constraintEqualToConstant:42].active = YES;
            [self.dayLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            
            [self.numDayLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: 2].active = YES;
            [self.numDayLabel.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.numDayLabel.widthAnchor constraintEqualToConstant:30].active = YES;
            [self.numDayLabel.heightAnchor constraintEqualToConstant:16].active = YES;
            
            [self.dayTreanglImage.rightAnchor constraintEqualToAnchor:self.dayLabel.rightAnchor constant: -2].active = YES;
            [self.dayTreanglImage.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.dayTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.dayTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            [self.dateChoiseButton.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: -2].active = YES;
            [self.dateChoiseButton.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: 2].active = YES;
            [self.dateChoiseButton.topAnchor constraintEqualToAnchor:self.dayLabel.topAnchor constant: -5].active = YES;
            [self.dateChoiseButton.bottomAnchor constraintEqualToAnchor:self.dayLabel.bottomAnchor constant: 5].active = YES;
            
            
            
            //
            //        [self.city.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor].active = YES;
            //    [self.city.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant:80].active = YES;
            //    [self.city.widthAnchor constraintEqualToConstant:100].active = YES;
            //    [self.city.heightAnchor constraintEqualToConstant:21].active = YES;
            //
            //
            //
            //        [self.cityLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor].active = YES;
            //        [self.cityLabel.centerYAnchor constraintEqualToAnchor:self.city.centerYAnchor].active = YES;
            //    [self.cityLabel.widthAnchor constraintEqualToConstant:156].active = YES;
            //    [self.cityLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            //
            //    [self.cityNamelabel.leftAnchor constraintEqualToAnchor:self.cityLabel.leftAnchor constant: 2].active = YES;
            //    [self.cityNamelabel.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //    [self.cityNamelabel.widthAnchor constraintEqualToConstant:140].active = YES;
            //    [self.cityNamelabel.heightAnchor constraintEqualToConstant:16].active = YES;
            //
            //    [self.cityTreanglImage.rightAnchor constraintEqualToAnchor:self.cityLabel.rightAnchor constant: -2].active = YES;
            //    [self.cityTreanglImage.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //    [self.cityTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            //    [self.cityTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
        }
        
        [self.passwContainerView.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor].active = YES;
        [self.passwContainerView.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor].active = YES;
        [self.passwContainerView.bottomAnchor constraintEqualToAnchor:self.containerView.bottomAnchor].active = YES;
        [self.passwContainerView.heightAnchor constraintEqualToConstant:290].active = YES;
        
        
        [self.passwordlabel.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.passwordlabel.topAnchor constraintEqualToAnchor:self.passwContainerView.topAnchor constant: 0].active = YES;
        [self.passwordlabel.widthAnchor constraintEqualToConstant:100].active = YES;
        
        
        [self.passwordView.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.passwordView.rightAnchor constraintEqualToAnchor:self.passwContainerView.rightAnchor constant:-14].active = YES;
        [self.passwordView.topAnchor constraintEqualToAnchor:self.passwordlabel.bottomAnchor
                                                    constant:16].active = YES;
        [self.passwordView.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.passwordTextField.leftAnchor constraintEqualToAnchor:self.passwordView.leftAnchor constant:8].active = YES;
        [self.passwordTextField.rightAnchor constraintEqualToAnchor:self.passwordView.rightAnchor constant:-8].active = YES;
        [self.passwordTextField.centerYAnchor constraintEqualToAnchor:self.passwordView.centerYAnchor].active = YES;
        
        
        [self.confirmView.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.confirmView.rightAnchor constraintEqualToAnchor:self.passwContainerView.rightAnchor constant:-14].active = YES;
        [self.confirmView.topAnchor constraintEqualToAnchor:self.passwordView.bottomAnchor
                                                   constant:16].active = YES;
        [self.confirmView.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.confirmTextField.leftAnchor constraintEqualToAnchor:self.confirmView.leftAnchor constant:8].active = YES;
        [self.confirmTextField.rightAnchor constraintEqualToAnchor:self.confirmView.rightAnchor constant:-8].active = YES;
        [self.confirmTextField.centerYAnchor constraintEqualToAnchor:self.confirmView.centerYAnchor].active = YES;
        
        
        if (screenWidth < 322) {
            [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        }
        [self.submitButton.centerXAnchor constraintEqualToAnchor:self.passwContainerView.centerXAnchor].active = YES;
        [self.submitButton.centerYAnchor constraintEqualToAnchor:self.passwContainerView.bottomAnchor constant:-(screenHeight * 0.06)].active = YES;
        [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
        
        self.fbButton.layer.cornerRadius = 15;
        self.fbButton.layer.masksToBounds = true;
        
        [self.fbButton.centerXAnchor constraintEqualToAnchor:self.passwContainerView.centerXAnchor].active = YES;
        [self.fbButton.centerYAnchor constraintEqualToAnchor:self.submitButton.topAnchor constant:-(screenHeight * 0.06)].active = YES;
        [self.fbButton.widthAnchor constraintEqualToConstant:screenWidth * 0.29].active = YES;
        [self.fbButton.heightAnchor constraintEqualToConstant:screenHeight * 0.05].active = YES;
        
     

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
//        self.containerView.translatesAutoresizingMaskIntoConstraints = false;
//        [self.containerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:screenWidth * 0.0].active = YES;
//        [self.containerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-screenWidth * 0.4].active = YES;
//        [self.containerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenWidth * 0].active = YES;
//        [self.containerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:screenWidth * 0].active = YES;
        
        
        self.submitButton.layer.cornerRadius = 5;
        self.submitButton.layer.masksToBounds = true;
 
        
        [self.photoContainer.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.2].active = YES;
        [self.photoContainer.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.2].active = YES;
        [self.photoContainer.topAnchor constraintEqualToAnchor:self.containerView.topAnchor].active = YES;
        [self.photoContainer.heightAnchor constraintEqualToConstant:130].active = YES;
        
        self.userPhotoImage.layer.cornerRadius = 50.0f;
        self.userPhotoImage.layer.masksToBounds = YES;
        
        [self.userPhotoImage.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor  constant:16].active = YES;
        [self.userPhotoImage.centerYAnchor constraintEqualToAnchor:self.photoContainer.centerYAnchor].active = YES;
        [self.userPhotoImage.widthAnchor constraintEqualToConstant:100].active = YES;
        [self.userPhotoImage.heightAnchor constraintEqualToConstant:100].active = YES;
        
        self.addphotoLabel.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
        
        [self.addphotoLabel.leftAnchor constraintEqualToAnchor:self.userPhotoImage.rightAnchor constant:16].active = YES;
        [self.addphotoLabel.centerYAnchor constraintEqualToAnchor:self.userPhotoImage.centerYAnchor].active = YES;
        [self.addphotoLabel.widthAnchor constraintEqualToConstant:220].active = YES;
        [self.addphotoLabel.heightAnchor constraintEqualToConstant:30].active = YES;
        
        [self.photoButton.leftAnchor constraintEqualToAnchor:self.userPhotoImage.leftAnchor].active = YES;
        [self.photoButton.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor].active = YES;
        [self.photoButton.topAnchor constraintEqualToAnchor:self.userPhotoImage.topAnchor].active = YES;
        [self.photoButton.bottomAnchor constraintEqualToAnchor:self.userPhotoImage.bottomAnchor].active = YES;
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        [self.fullNameView.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
        [self.fullNameView.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
        [self.fullNameView.topAnchor constraintEqualToAnchor:self.photoContainer.bottomAnchor
                                                    constant:screenHeight * 0.0].active = YES;
        [self.fullNameView.heightAnchor constraintEqualToConstant:44].active = YES;
        

        
        [self.fullNameTextField.leftAnchor constraintEqualToAnchor:self.fullNameView.leftAnchor constant:8].active = YES;
        [self.fullNameTextField.rightAnchor constraintEqualToAnchor:self.fullNameView.rightAnchor constant:-8].active = YES;
        [self.fullNameTextField.centerYAnchor constraintEqualToAnchor:self.fullNameView.centerYAnchor].active = YES;
        
        
        [self.phone1View.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
        [self.phone1View.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
        [self.phone1View.topAnchor constraintEqualToAnchor:self.fullNameView.bottomAnchor
                                                  constant:16].active = YES;
        [self.phone1View.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.emailTextField.leftAnchor constraintEqualToAnchor:self.phone1View.leftAnchor constant:8].active = YES;
        [self.emailTextField.rightAnchor constraintEqualToAnchor:self.phone1View.rightAnchor constant:-8].active = YES;
        [self.emailTextField.centerYAnchor constraintEqualToAnchor:self.phone1View.centerYAnchor].active = YES;
        
        
        [self.phone2View.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
        [self.phone2View.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
        [self.phone2View.topAnchor constraintEqualToAnchor:self.phone1View.bottomAnchor
                                                  constant:16].active = YES;
        [self.phone2View.heightAnchor constraintEqualToConstant:44].active = YES;
        
        [self.phoneTextField.leftAnchor constraintEqualToAnchor:self.phone2View.leftAnchor constant:8].active = YES;
        [self.phoneTextField.rightAnchor constraintEqualToAnchor:self.phone2View.rightAnchor constant:-8].active = YES;
        [self.phoneTextField.centerYAnchor constraintEqualToAnchor:self.phone2View.centerYAnchor].active = YES;
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
            [self.dateContainerView.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor constant:14].active = YES;
            [self.dateContainerView.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor constant:-14].active = YES;
            [self.dateContainerView.topAnchor constraintEqualToAnchor:self.phone2View.bottomAnchor].active = YES;
            [self.dateContainerView.heightAnchor constraintEqualToConstant:90].active = YES;
            
            self.birthDateLabel.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
            [self.birthDateLabel.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor constant:0].active = YES;
            [self.birthDateLabel.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant: 34].active = YES;
            [self.birthDateLabel.widthAnchor constraintEqualToConstant:228].active = YES;
            [self.birthDateLabel.heightAnchor constraintEqualToConstant:21].active = YES;
            
            
            [self.yearLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: 0].active = YES;
            [self.yearLabel.centerYAnchor constraintEqualToAnchor:self.birthDateLabel.centerYAnchor].active = YES;
            [self.yearLabel.widthAnchor constraintEqualToConstant:77].active = YES;
            [self.yearLabel.heightAnchor constraintEqualToConstant:35].active = YES;
        
            self.numYearLabel.font = [UIFont fontWithName:@"Thonburi" size:16];
            [self.numYearLabel.leftAnchor constraintEqualToAnchor:self.yearLabel.leftAnchor constant: 5].active = YES;
            [self.numYearLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.numYearLabel.widthAnchor constraintEqualToConstant:52].active = YES;
            [self.numYearLabel.heightAnchor constraintEqualToConstant:20].active = YES;
            
            [self.yearTreanglImage.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: -2].active = YES;
            [self.yearTreanglImage.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.yearTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.yearTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.monthLabel.rightAnchor constraintEqualToAnchor:self.dateContainerView.rightAnchor constant: -85].active = YES;
            [self.monthLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            [self.monthLabel.widthAnchor constraintEqualToConstant:102].active = YES;
            [self.monthLabel.heightAnchor constraintEqualToConstant:35].active = YES;
        
            self.numMonthLabel.font = [UIFont fontWithName:@"Thonburi" size:16];
            [self.numMonthLabel.leftAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: 5].active = YES;
            [self.numMonthLabel.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.numMonthLabel.widthAnchor constraintEqualToConstant:86].active = YES;
            [self.numMonthLabel.heightAnchor constraintEqualToConstant:20].active = YES;
            
            [self.monthTrenglImage.rightAnchor constraintEqualToAnchor:self.monthLabel.rightAnchor constant: -2].active = YES;
            [self.monthTrenglImage.centerYAnchor constraintEqualToAnchor:self.monthLabel.centerYAnchor].active = YES;
            [self.monthTrenglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.monthTrenglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            
            
            [self.dayLabel.rightAnchor constraintEqualToAnchor:self.monthLabel.leftAnchor constant: -10].active = YES;
            [self.dayLabel.centerYAnchor constraintEqualToAnchor:self.yearLabel.centerYAnchor].active = YES;
            
            [self.dayLabel.widthAnchor constraintEqualToConstant:62].active = YES;
            [self.dayLabel.heightAnchor constraintEqualToConstant:35].active = YES;
        
        
            self.numDayLabel.font = [UIFont fontWithName:@"Thonburi" size:16];
            [self.numDayLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: 2].active = YES;
            [self.numDayLabel.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.numDayLabel.widthAnchor constraintEqualToConstant:50].active = YES;
            [self.numDayLabel.heightAnchor constraintEqualToConstant:20].active = YES;
            
            [self.dayTreanglImage.rightAnchor constraintEqualToAnchor:self.dayLabel.rightAnchor constant: -2].active = YES;
            [self.dayTreanglImage.centerYAnchor constraintEqualToAnchor:self.dayLabel.centerYAnchor].active = YES;
            [self.dayTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            [self.dayTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
            
            [self.dateChoiseButton.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor constant: -2].active = YES;
            [self.dateChoiseButton.rightAnchor constraintEqualToAnchor:self.yearLabel.rightAnchor constant: 2].active = YES;
            [self.dateChoiseButton.topAnchor constraintEqualToAnchor:self.dayLabel.topAnchor constant: -5].active = YES;
            [self.dateChoiseButton.bottomAnchor constraintEqualToAnchor:self.dayLabel.bottomAnchor constant: 5].active = YES;
            
            
            
            //
            //        [self.city.leftAnchor constraintEqualToAnchor:self.dateContainerView.leftAnchor].active = YES;
            //    [self.city.topAnchor constraintEqualToAnchor:self.dateContainerView.topAnchor constant:80].active = YES;
            //    [self.city.widthAnchor constraintEqualToConstant:100].active = YES;
            //    [self.city.heightAnchor constraintEqualToConstant:21].active = YES;
            //
            //
            //
            //        [self.cityLabel.leftAnchor constraintEqualToAnchor:self.dayLabel.leftAnchor].active = YES;
            //        [self.cityLabel.centerYAnchor constraintEqualToAnchor:self.city.centerYAnchor].active = YES;
            //    [self.cityLabel.widthAnchor constraintEqualToConstant:156].active = YES;
            //    [self.cityLabel.heightAnchor constraintEqualToConstant:25].active = YES;
            //
            //    [self.cityNamelabel.leftAnchor constraintEqualToAnchor:self.cityLabel.leftAnchor constant: 2].active = YES;
            //    [self.cityNamelabel.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //    [self.cityNamelabel.widthAnchor constraintEqualToConstant:140].active = YES;
            //    [self.cityNamelabel.heightAnchor constraintEqualToConstant:16].active = YES;
            //
            //    [self.cityTreanglImage.rightAnchor constraintEqualToAnchor:self.cityLabel.rightAnchor constant: -2].active = YES;
            //    [self.cityTreanglImage.centerYAnchor constraintEqualToAnchor:self.cityLabel.centerYAnchor].active = YES;
            //    [self.cityTreanglImage.widthAnchor constraintEqualToConstant:9].active = YES;
            //    [self.cityTreanglImage.heightAnchor constraintEqualToConstant:9].active = YES;
        
        [self.passwContainerView.leftAnchor constraintEqualToAnchor:self.photoContainer.leftAnchor].active = YES;
        [self.passwContainerView.rightAnchor constraintEqualToAnchor:self.photoContainer.rightAnchor].active = YES;
        [self.passwContainerView.bottomAnchor constraintEqualToAnchor:self.containerView.bottomAnchor].active = YES;
        [self.passwContainerView.heightAnchor constraintEqualToConstant:290].active = YES;
        
        self.passwordlabel.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
        [self.passwordlabel.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.passwordlabel.topAnchor constraintEqualToAnchor:self.passwContainerView.topAnchor constant: 0].active = YES;
        [self.passwordlabel.widthAnchor constraintEqualToConstant:150].active = YES;
        
        
        [self.passwordView.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.passwordView.rightAnchor constraintEqualToAnchor:self.passwContainerView.rightAnchor constant:-14].active = YES;
        [self.passwordView.topAnchor constraintEqualToAnchor:self.passwordlabel.bottomAnchor
                                                    constant:16].active = YES;
        [self.passwordView.heightAnchor constraintEqualToConstant:44].active = YES;
    

        [self.passwordTextField.leftAnchor constraintEqualToAnchor:self.passwordView.leftAnchor constant:8].active = YES;
        [self.passwordTextField.rightAnchor constraintEqualToAnchor:self.passwordView.rightAnchor constant:-8].active = YES;
        [self.passwordTextField.centerYAnchor constraintEqualToAnchor:self.passwordView.centerYAnchor].active = YES;
        
        
        [self.confirmView.leftAnchor constraintEqualToAnchor:self.passwContainerView.leftAnchor constant:14].active = YES;
        [self.confirmView.rightAnchor constraintEqualToAnchor:self.passwContainerView.rightAnchor constant:-14].active = YES;
        [self.confirmView.topAnchor constraintEqualToAnchor:self.passwordView.bottomAnchor
                                                   constant:16].active = YES;
        [self.confirmView.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        [self.confirmTextField.leftAnchor constraintEqualToAnchor:self.confirmView.leftAnchor constant:8].active = YES;
        [self.confirmTextField.rightAnchor constraintEqualToAnchor:self.confirmView.rightAnchor constant:-8].active = YES;
        [self.confirmTextField.centerYAnchor constraintEqualToAnchor:self.confirmView.centerYAnchor].active = YES;
        
        self.fbButton.layer.cornerRadius = 25;
        self.fbButton.layer.masksToBounds = true;
        
        [self.fbButton.centerXAnchor constraintEqualToAnchor:self.passwContainerView.centerXAnchor].active = YES;
        [self.fbButton.topAnchor constraintEqualToAnchor:self.confirmView.bottomAnchor constant:32].active = YES;
        [self.fbButton.widthAnchor constraintEqualToConstant:140].active = YES;
        [self.fbButton.heightAnchor constraintEqualToConstant:50].active = YES;
        
 
        [self.submitButton.centerXAnchor constraintEqualToAnchor:self.passwContainerView.centerXAnchor].active = YES;
        [self.submitButton.topAnchor constraintEqualToAnchor:self.fbButton.bottomAnchor constant:32].active = YES;
        [self.submitButton.widthAnchor constraintEqualToConstant:220].active = YES;
        [self.submitButton.heightAnchor constraintEqualToConstant:71].active = YES;
        
        
 
        
    }
    
}

-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
 
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                                                                      NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}

@end

//-(void) uploadImageToFIRStore:(UIImage*)image{
////    NSData *data = [NSData dataWithContentsOfFile:@"rivers.jpg"];
//
////    FIRStorage *storage = [FIRStorage storage];
////    FIRStorageReference *storageRef = [storage reference];
////    imageUrlFIR = [NSString stringWithFormat:@"gs://reston-2e06b.appspot.com/images/%@.jpg",[RNUser sharedInstance].idUser];
//    //    FIRStorageReference *imagesRef = [storageRef child:@"images"];
//    FIRStorageReference *spaceRef = [self.storageRef child:[NSString stringWithFormat:@"%@.jpg",[RNUser sharedInstance].idUser]];
////    spaceRef = [storage referenceForURL:imageUrlFIR];
////    NSString *path = spaceRef.fullPath;
////    NSLog(@"imageUrlFIR: %@  path: %@", imageUrlFIR, path);
//
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
//    FIRStorageMetadata *metadata = [FIRStorageMetadata new];
//    metadata.contentType = @"image/jpeg";
//    [spaceRef putData:imageData metadata:metadata
//                                completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
//                                    if (error) {
////                                        NSLog(@"Error uploading: %@", error);
////                                        _urlTextView.text = @"Upload Failed";
//                                        return;
//                                    }
////                                    [self uploadSuccess:metadata storagePath:imagePath];
////                                    NSURL *downloadURL = metadata.downloadURL;
//                                    NSString* strURL = metadata.downloadURL.absoluteString;
//                                    imageUrlFIR = [strURL substringFromIndex: [strURL length] - 36];
////                                    NSLog(@"imageUrlFIR %@", imageUrlFIR);
////                                    NSLog(@"metadata.downloadURL.absoluteString %@", metadata.downloadURL.absoluteString);
//                                    if (imageUrlFIR.length > 0){
//                                        [self sendEditRequest];
//                                    }
//
//                                }];
//}
//
//    // Create a reference to the file you want to upload
//    FIRStorageReference *riversRef = [storageRef child:@"images/rivers.jpg"];
//
//    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
//    metadata.contentType = @"image/jpeg";
//
//    // Upload the file to the path "images/rivers.jpg"
//    FIRStorageUploadTask *uploadTask = [riversRef putData:imageData
//                                                 metadata:metadata
//                                               completion:^(FIRStorageMetadata *metadata,
//                                                            NSError *error) {
//                                                   if (error != nil) {
//                                                       // Uh-oh, an error occurred!
//                                                   } else {
//                                                       // Metadata contains file metadata such as size, content-type, and download URL.
//                                                       NSURL *downloadURL = metadata.downloadURL;
//                                                       NSLog(@"downloadURL %@", downloadURL);
//                                                   }
//                                               }];
//    // Listen for state changes, errors, and completion of the upload.
//    [uploadTask observeStatus:FIRStorageTaskStatusResume handler:^(FIRStorageTaskSnapshot *snapshot) {
//        // Upload resumed, also fires when the upload starts
//    }];
//
//    [uploadTask observeStatus:FIRStorageTaskStatusPause handler:^(FIRStorageTaskSnapshot *snapshot) {
//        // Upload paused
//    }];
//
//    [uploadTask observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot) {
//        // Upload reported progress
//        double percentComplete = 100.0 * (snapshot.progress.completedUnitCount) / (snapshot.progress.totalUnitCount);
//    }];
//
//    [uploadTask observeStatus:FIRStorageTaskStatusSuccess handler:^(FIRStorageTaskSnapshot *snapshot) {
//        // Upload completed successfully
//    }];
//
//    // Errors only occur in the "Failure" case
//    [uploadTask observeStatus:FIRStorageTaskStatusFailure handler:^(FIRStorageTaskSnapshot *snapshot) {
//        if (snapshot.error != nil) {
//            switch (snapshot.error.code) {
//                case FIRStorageErrorCodeObjectNotFound:
//                    // File doesn't exist
//                    break;
//                case FIRStorageErrorCodeUnauthorized:
//                    // User doesn't have permission to access file
//                    break;
//
//                case FIRStorageErrorCodeCancelled:
//                    // User canceled the upload
//                    break;
//
//                    /* ... */
//
//                case FIRStorageErrorCodeUnknown:
//                    // Unknown error occurred, inspect the server response
//                    break;
//            }
//        }
//    }];0683319646   0667650373

//}
//-(void) uploadAvatarToServerReston:(UIImage*)image{
//    NSData *imageData = UIImagePNGRepresentation(image);
//
//    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
//
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    [request setHTTPMethod:@"POST"];
//
//    NSString *boundary = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//    NSMutableData *body = [NSMutableData data];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"avatar\"; filename=\"test.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[NSData dataWithData:imageData]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [request setHTTPBody:body];
//
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", [NSString stringWithFormat:@"Image Return String: %@", returnString]);
//}



//-(void) uploadAvatarImageBase64:(UIImage*)image{
//
//    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
//
//    NSString* FileParamConstant = @"avatar";
//    NSData *data = UIImageJPEGRepresentation(image, 1.0);
//    NSString *base64StringJJJ = [data base64EncodedStringWithOptions:kNilOptions];
//    NSString *base64String = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//     NSString *imageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
////    NSString* imageDataStr = [NSString stringWithFormat:@"data:image/jpeg;base64,%@", imageData];
////    NSLog(@"base64String: %@", base64String);
////    NSLog(@"base64StringJJJ: %@", base64StringJJJ);
////    NSLog(@"imageData: %@", imageData);
////    NSLog(@"imageDataStr: %@", imageDataStr);
//
//    //Make a request
//    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSError *error;
////        NSDictionary *dic = @{@"avatardata:image/jpeg;base64," : imageDataStr};
//        NSDictionary *dic = @{@"avatardata:image/jpeg;base64," : imageData};
//        NSData *imageJsonData = [NSJSONSerialization dataWithJSONObject:dic
//                                                                options:NSJSONWritingPrettyPrinted
//                                                                  error:&error];
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",BoundaryConstant];
//
//        NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//         NSMutableData *body = [NSMutableData data];
//        [request setURL:[NSURL URLWithString:urlString]];
//                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//                if (imageData != nil)
//
//                {
//                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//                    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//                     [body appendData:[[NSString stringWithFormat:@"%@",imageData] dataUsingEncoding:NSUTF8StringEncoding]];
//                    [body appendData:data];
//
//                }
//        [request setHTTPMethod:@"POST"];
//        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)imageJsonData.length] forHTTPHeaderField:@"Content-Length"];
//        [request setHTTPBody:imageJsonData];
//
//        NSLog(@"Image upload request: %@", request);
//        NSLog(@"Image upload body: %@", body);
//
//        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//
//        NSLog(@"Image upload response: %@",returnString);
//
////        [RNUser sharedInstance].token = [responseObject objectForKey:@"token"];
////        NSString* strURL = [responseObject objectForKey:@"file_path"];
////        filePathImage = [strURL stringByReplacingOccurrencesOfString:@"/uploads/users/" withString:@""];
////        //                filePathImage = [strURL substringFromIndex: [strURL length] - 16];
////        NSLog(@"filePathImage:%@", filePathImage);
////        isSuccessUploadImage = YES;
////        //            }
////
////
////        NSLog(@"filePath: %@", filePathImage);
////
////        if (isSuccessUploadImage == YES && filePathImage.length > 0){
////            NSLog(@"[self sendEditRequest]");
////            [self sendEditRequest];
//    });
//}


//-(void) uploadAvatarImage:(UIImage*)image{
//
//
//
//    NSData *imageData = UIImagePNGRepresentation(image);
//
//    NSString *base64String = [imageData base64EncodedStringWithOptions:kNilOptions];
//    NSString *strImageData = [base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
// NSLog(@"strImageData %@",strImageData);
//    UploadIAvatarRequest *request = [[UploadIAvatarRequest alloc]init];
//    request.photoAvatar = strImageData;
//
//    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
//        UploadIAvatarResponse *uploadResponse = (UploadIAvatarResponse *)response;
//        NSLog(@"response %@",response);
//        NSLog(@"uploadResponse %@",uploadResponse.pathAvatar);
//        if (uploadResponse.pathAvatar.length > 0){
//
//            filePathImage = uploadResponse.pathAvatar;
//            isSuccessUploadImage = YES;
//            [self showAlertWithTitle:@"RestOn" andMessage:DPLocalizedString(@"save_details", nil)];
//
//        } else
//            //            if (error != nil || [response isKindOfClass:[NSNull class]] || [authResponse.editProfile isKindOfClass:[NSNull class]])
//        {
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//        }
//    } ];
//}

//-(void) uploadAvatAFNetw:(UIImage*)image{
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
//    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
////    NSMutableURLRequest *request1 = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
////        [formData appendPartWithFileData:imageData name:@"avatar" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
////    } error:nil];
//
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        [formData appendPartWithFormData:imageData name:@"file"];
//    } error:nil];
//
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//
//    NSURLSessionUploadTask *uploadTask;
//    uploadTask = [manager
//                  uploadTaskWithStreamedRequest:request
//                  progress:^(NSProgress * _Nonnull uploadProgress) {
//                      // This is not called back on the main queue.
//                      // You are responsible for dispatching to the main queue for UI updates
//                      dispatch_async(dispatch_get_main_queue(), ^{
//                          //Update the progress view
////                          [progressView setProgress:uploadProgress.fractionCompleted];
//                      });
//                  }
//                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                      if (error) {
//                          NSLog(@"Error: %@", error);
//                      } else {
//                          NSLog(@"%@ %@", response, responseObject);
//                      }
//                  }];
//
//    [uploadTask resume];
//}

//-(void) uploadImageWithAFNet:(UIImage*)image{
//
//    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
////
////    NSString* FileParamConstant = @"avatar";
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
//    NSURL *URL = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
//
//    NSMutableData *body = [NSMutableData data];
//
//    [request setHTTPMethod:@"POST"];
//
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",BoundaryConstant];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//    if (imageData != nil)
//
//    {
//        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"avatar\"; filename=\"image.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:imageData];
//        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    }
//
//    [request setHTTPBody:body];
//
//    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request fromData:body progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        if (error) {
//            NSLog(@"Error!!!!!!!: %@", error);
//        } else {
//            NSLog(@"responseObject:%@", responseObject);
//
//
//            if ([responseObject objectForKey:@"token"] && [responseObject objectForKey:@"file_path"]){
//                [RNUser sharedInstance].token = [responseObject objectForKey:@"token"];
//                NSString* strURL = [responseObject objectForKey:@"file_path"];
//                filePathImage = [strURL stringByReplacingOccurrencesOfString:@"/uploads/users/" withString:@""];
////                filePathImage = [strURL substringFromIndex: [strURL length] - 16];
//            NSLog(@"filePathImage:%@", filePathImage);
//                isSuccessUploadImage = YES;
//            }
//
//
//            NSLog(@"filePath: %@", filePathImage);
//
//            if (isSuccessUploadImage == YES && filePathImage.length > 0){
//                NSLog(@"[self sendEditRequest]");
//                [self sendEditRequest];
//            }
//        }
//
//    }];
//    [uploadTask resume];
//
//}


//-(void)uploadImageToPHP:(UIImage*)image{
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
//    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
//
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    [request setHTTPMethod:@"POST"];
//
//    NSString *boundary = @"---------------------------14737809831466499882746641449"
//    ;
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//    NSMutableData *body = [NSMutableData data];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Disposition: form-data; name=\"file\"; filename=\"avatar.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[NSData dataWithData:imageData]];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [request setHTTPBody:body];
//
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    NSLog(@"returnString %@", returnString);
//}




//
//<?php
////$firstName = $_POST["firstName"];
////$lastName = $_POST["lastName"];
////$userId = $_POST["userId"];
//
//$target_dir = "/uploads/users";
//if(!file_exists($target_dir)) {
//    mkdir($target_dir, 0777, true);
//}
//
//$target_dir = $target_dir . "/" . basename($_FILES["avatar"]["name"]);
//
//if (move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile)) {
//                echo $file;
//
//                echo json_encode((object)array('upload' => 'success', 'file_path' => $uploadfile, 'token' => $this->SetToken()), JSON_UNESCAPED_UNICODE);
//                die();
//            } else {
//                $this->errorDie('cannot catch file from request');
//                }
//
//        } else {
//            $this->SetToken(true);
//            $this->errorDie('user error');
//        }
//
//}
//?>


//function uploadUserPhoto(){
//    $this->GetHeaders();
//    $this->CheckToken();
//    $user_id = $_SESSION['user_id'];
//    if($user_id > 0){
//
//        var_dump($_REQUEST);
//
//        $uploaddir = '/uploads/users/';
//        $file = basename($_FILES['avatar']['name']);
//        $uploadfile = $uploaddir . $file;
//
//        echo "file=".$file; //is empty, but shouldn't
//
//        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile)) {
//            echo $file;
//
//            echo json_encode((object)array('upload' => 'success', 'file_path' => $uploadfile, 'token' => $this->SetToken()), JSON_UNESCAPED_UNICODE);
//            die();
//        }
//        else {
//            $this->errorDie('cannot catch file from request');
//        }
//
//    } else {
//        $this->SetToken(true);
//        $this->errorDie('user error');
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


