//
//  FullScreenViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/18/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenViewController : UIViewController

@property (nonatomic,strong) NSArray *images;
@property (nonatomic) NSInteger selectIndex;

@end
