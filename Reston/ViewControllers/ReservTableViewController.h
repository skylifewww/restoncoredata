//
//  ReservTableViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"
#import "RNRestaurant.h"
#import "NewMenuViewController.h"

extern NSString *const ReservCountChangeNotification;
extern NSString *const OrdersCountChangeNotification;

@class ReservTableViewController;

@protocol ReservTableViewControllerDelegate <NSObject>

-(void)changeReservesCountDelegate: (ReservTableViewController *) controller;

@end




@interface ReservTableViewController : UIViewController

//@property (weak, nonatomic) id<LeftMenuProtocol> delegate;

@property (nonatomic, weak) id<ReservTableViewControllerDelegate> delegate;


@property (nonatomic, strong) RNOrderInfo *orderInfo;

@property (weak, nonatomic) RNRestaurant *restourant;
@property (weak, nonatomic) NSNumber *restourantID;
@property (strong, nonatomic) UIBarButtonItem *openReservBarButton;

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIImageView *tailInage;

@property (weak, nonatomic) IBOutlet UILabel *dateChoisLabel;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dateArrow;

@property (weak, nonatomic) IBOutlet UILabel *timeChoiselabel;
@property (weak, nonatomic) IBOutlet UILabel *noReservedLabel;

@property (weak, nonatomic) IBOutlet UILabel *bookMark;
@property (weak, nonatomic) IBOutlet UILabel *restNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restAddressLabel;
@property (weak, nonatomic) IBOutlet UIButton *dataAction;
@property (weak, nonatomic) IBOutlet UIButton *timeAction;

@property (weak, nonatomic) IBOutlet UIButton *dateChoiseButton;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timeArrow;


@property (weak, nonatomic) IBOutlet UILabel *personsChioseLabel;
@property (weak, nonatomic) IBOutlet UIView *personsView;
@property (weak, nonatomic) IBOutlet UILabel *personsCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;



@property (weak, nonatomic) IBOutlet UIButton *timeChoiseButton;

@property (weak, nonatomic) IBOutlet UIImageView *personsIcon;

@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@end
