//
//  AllNewsPhoneExpCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 04.02.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllNewsPhoneExpCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriplabel;
@property (weak, nonatomic) IBOutlet UIButton *reservButton;

@end
