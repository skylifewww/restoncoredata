//
//  FilterViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PriceSlider.h"
#import "FilterInfo.h"

extern NSString *const TypeFilterChangeNotification;
extern NSString *const CityChangeNotification;
extern NSString *const OrdersCountFilterNotification;

@interface FilterViewController : UIViewController

@property (strong, nonatomic) UIBarButtonItem *openReservBarButton;

@property (strong, nonatomic) NSArray* allRestons;
@property (strong, nonatomic) NSArray* workRestons;

@property (strong, nonatomic) FilterInfo* filterInfo;
@property (assign, nonatomic) NSInteger avgBill;
@property (assign, nonatomic) NSInteger minAvgBill;
@property (assign, nonatomic) NSInteger maxAvgBill;

@property (weak, nonatomic) IBOutlet UIImageView *bookMarkImage;
@property (weak, nonatomic) IBOutlet UILabel *bookMarkCount;
@property (weak, nonatomic) IBOutlet UILabel *averagePriceLabel;
@property (weak, nonatomic) IBOutlet PriceSlider *priceSlider;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *setParamlabel;
@property (weak, nonatomic) IBOutlet UILabel *averLabel;
@property (weak, nonatomic) IBOutlet UILabel *numAverLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *minAvgLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxAvgLabel;

@property (strong, nonatomic)NSString* currCity;
@property (strong, nonatomic)NSString* typeRest;
@property (strong, nonatomic)NSString* distric;
@property (strong, nonatomic)NSString* property;
@property (strong, nonatomic)NSString* metro;
@property (strong, nonatomic)NSString* kitchen;
@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet UILabel *findLabel;

@property (assign, nonatomic) BOOL isCityHasSubway;
@property (assign, nonatomic) BOOL isClear;

@end
