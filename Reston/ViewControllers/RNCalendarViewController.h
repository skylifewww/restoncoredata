//
//  RNCalendarViewController.h
//  Reston
//
//  Created by Yurii Oliiar on 6/26/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RNCalendarViewController;

@protocol RNCalendarViewControllerDelegate <NSObject>

- (void)calendarPicker:(RNCalendarViewController *)picker didChooseDate:(NSDate *)date;
- (void)calendarPicker:(RNCalendarViewController *)picker didCancelWithDate:(NSDate *)date;

@end

@interface RNCalendarViewController : UIViewController

@property (nonatomic, assign) CGPoint center;
@property (nonatomic, assign) CGFloat width;

@property (nonatomic, copy) NSDate *selectedDate;
@property (nonatomic, assign) id<RNCalendarViewControllerDelegate> delegate;

@end
