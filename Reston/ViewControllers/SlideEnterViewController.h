//
//  SlideEnterViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 4/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SlideEnterViewController : UIViewController

@property (strong, nonatomic) NSString* imageName;
@property (strong, nonatomic) NSString* iconName;
@property (strong, nonatomic) NSString* text;

//@property (nonatomic, assign) Boolean isThisCity;

//@property (strong, nonatomic) NSString* currCity;

@property (strong, nonatomic) NSAttributedString* enterText;
@property (strong, nonatomic) NSAttributedString* registerText;
@property (strong, nonatomic) NSAttributedString* buttonText;

@property (assign, nonatomic) NSUInteger pageIndex;

@property (weak, nonatomic) IBOutlet UIImageView *slideImage;

@property (weak, nonatomic) IBOutlet UIImageView *slideIcon;
//@property (weak, nonatomic) IBOutlet UIButton *passButton;


@property (weak, nonatomic) IBOutlet UILabel *slideLabel;


@property (weak, nonatomic) IBOutlet UIButton *choiseTextButton;


@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UIView *divideView;



//- (IBAction)passButton:(id)sender;

- (IBAction)choiseButton:(id)sender;

- (IBAction)enterButton:(id)sender;


@end
