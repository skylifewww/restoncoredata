//
//  PageViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 4/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "PageViewController.h"
#import "SlideEnterViewController.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "UIViewController+SlideMenuControllerOC.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
//        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
//    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
//    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
//    {
//        BOOL autoRotate = (BOOL)[self.topViewController
//                                 performSelector:@selector(shouldAutorotate)
//                                 withObject:nil];
//        return autoRotate;
//        
//    }
    return NO;
}
@end


@interface PageViewController () {
    
    NSArray* slides;
    NSArray* icons;
    NSArray* texts;
    CGFloat sizeFont;
    NSAttributedString* enterButtonText;
    NSAttributedString* registerButtonText;
    NSAttributedString* choiseButtonText;
    UIColor* mainColor;
//    NSArray* buttonTexts;
}

@end

@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    mainColor = [UIColor colorWithRed:0.26 green:0.5 blue:0.0 alpha:1.0];
    
    CGFloat screenWidth = self.view.bounds.size.width;
//    CGFloat screenHeight = self.view.bounds.size.height;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        if (screenWidth < 322) {
          sizeFont = 14.0f;
        } else {
          sizeFont = 16.0f;
        }
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        sizeFont = 26.0f;
    }
   
   
    slides = @[@"slideEnter1", @"slideEnter2", @"slideEnter3", @"slideEnter4"];
    icons = @[@"iconEnter1", @"iconEnter2", @"iconEnter3", @"iconEnter4"];
    texts = @[DPLocalizedString(@"search_rests", nil), DPLocalizedString(@"reserv_by_click", nil), DPLocalizedString(@"save_fav", nil), DPLocalizedString(@"enjoy", nil)];
   
//    buttonTexts = @[[self setAtrString:DPLocalizedString(@"go_to_restaraunts", nil) withColor:[UIColor whiteColor]],
//                    [self setAtrString:DPLocalizedString(@"register_action", nil) withColor:[UIColor whiteColor]],
//                    [self setAtrString:DPLocalizedString(@"register_action", nil) withColor:[UIColor whiteColor]],
//                    [self setAtrString:DPLocalizedString(@"list_rests", nil) withColor:[UIColor whiteColor]]
//                    ];
    enterButtonText = [self setAtrString:DPLocalizedString(@"enter", nil) withColor:mainColor];
    registerButtonText = [self setAtrString:DPLocalizedString(@"register_action", nil) withColor:mainColor];
    choiseButtonText = [self setAtrString:DPLocalizedString(@"go_to_restaraunts", nil) withColor:[UIColor whiteColor]];
    self.dataSource = self;
    
    SlideEnterViewController* initialVC = (SlideEnterViewController*) [self viewControllerAtIndex:0];
    NSArray* slideViewControllers = [NSArray arrayWithObjects:initialVC, nil];
    
    [self setViewControllers:slideViewControllers direction:UIPageViewControllerNavigationDirectionForward animated:true completion:nil];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (BOOL)shouldAutorotate{
    return NO;
}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
//    return UIInterfaceOrientationPortrait |
//    UIInterfaceOrientationPortraitUpsideDown;
//}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    [self removeNavigationBarItem];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
   
}

//-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
//    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
//        UIViewController *vc = ((UINavigationController *)self.slideMenuController.mainViewController).topViewController;
//        if (vc != nil) {
//            if ([vc isKindOfClass:[PageViewController class]]) {
//                [[self slideMenuController] removeLeftGestures];
//                [[self slideMenuController] removeRightGestures];
//            }
//        }
//    }];
//}

- (NSAttributedString*)setAtrString:(NSString*)string withColor:(UIColor*) color{
    
   
    
    UIFont *font = [UIFont fontWithName:@"Thonburi" size:sizeFont];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    NSDictionary *attributes = @{NSFontAttributeName:font,
                                 NSForegroundColorAttributeName:color,
                                 NSBackgroundColorAttributeName:[UIColor clearColor],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 };
    
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraphStyle setLineSpacing:8];
    [paragraphStyle setHeadIndent:1];
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:string  attributes:attributes]];

    return attString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger) index{
    
    SlideEnterViewController* slideViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideEnterViewController"];
    
    slideViewController.imageName = slides[index];
    slideViewController.iconName = icons[index];
    slideViewController.text = texts[index];
    slideViewController.buttonText = choiseButtonText;
    slideViewController.pageIndex = index;
    slideViewController.enterText = enterButtonText;
    slideViewController.registerText = registerButtonText;
//    slideViewController.currCity = _currCity;
//    slideViewController.isThisCity = _isThisCity;
//    NSLog(@"slideViewController.currCity %@", slideViewController.currCity);
    
    return slideViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((SlideEnterViewController*) viewController).pageIndex;
    
    if (index == 0 || index == NSNotFound) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController  viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((SlideEnterViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == slides.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleDefault;
}

@end
