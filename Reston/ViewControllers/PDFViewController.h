//
//  PDFViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property(assign, nonatomic) CGPDFDocumentRef pdfDocument;
@property(assign, nonatomic) size_t pageCount;
@property(strong, nonatomic) NSURL* menuPDF;
@property (strong, nonatomic) NSString* titleNav;

@end
