//
//  RNForgotPasswordViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/28/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;

@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
