//
//  RestoTypeTableViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterInfo.h"

static NSString *const kTypeKey;
extern NSString *const TypeChangeNotification;


@interface RestoTypeTableViewController : UITableViewController
@property (strong, nonatomic) FilterInfo* filterInfo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *typeRestBarButtom;
@property (strong, nonatomic) NSString* currCity;
@property (strong, nonatomic) NSArray* filteredTypeRests;

@end
