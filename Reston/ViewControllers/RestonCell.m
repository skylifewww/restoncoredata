//
//  RestonCell.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "RestonCell.h"

@implementation RestonCell

@synthesize submitButton = _submitButton;
@synthesize restNamelable = _restNamelable;
@synthesize restType = _restType;
@synthesize restAddress = _restAddress;
@synthesize ratingImage = _ratingImage;
@synthesize commentsCount = _commentsCount;
@synthesize averageLabel = _averageLabel;
@synthesize distanceRest = _distanceRest;
@synthesize restImage = _restImage;
@synthesize discountBack = _discountBack;
@synthesize restDiscount = _restDiscount;
@synthesize callButton = _callButton;

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.contentView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.containerView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.descripView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.addressView.translatesAutoresizingMaskIntoConstraints = false;
    self.mapIconImage.translatesAutoresizingMaskIntoConstraints = false;
    self.ratingView.translatesAutoresizingMaskIntoConstraints = false;
    self.commentView.translatesAutoresizingMaskIntoConstraints = false;
    self.commentsIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.avgIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.distansIcon.translatesAutoresizingMaskIntoConstraints = false;
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    self.restNamelable.translatesAutoresizingMaskIntoConstraints = false;
    self.restType.translatesAutoresizingMaskIntoConstraints = false;
    self.restAddress.translatesAutoresizingMaskIntoConstraints = false;
    self.ratingImage.translatesAutoresizingMaskIntoConstraints = false;
    self.commentsCount.translatesAutoresizingMaskIntoConstraints = false;
    self.averageLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.distanceRest.translatesAutoresizingMaskIntoConstraints = false;
    
    self.restImage.translatesAutoresizingMaskIntoConstraints = false;
    self.restImage.contentMode = UIViewContentModeScaleAspectFill;
    
    self.discountBack.translatesAutoresizingMaskIntoConstraints = false;
    self.restDiscount.translatesAutoresizingMaskIntoConstraints = false;
    self.callButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.submitButton.layer.cornerRadius = 3;
    self.submitButton.layer.masksToBounds = true;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:@"Забронировать" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    
    [self setupView];
}

-(void) setupView {
    
    CGFloat screenWidth =  [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    
   
    
    [self.containerView.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor].active = YES;
    [self.containerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
    [self.containerView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
    
    if (screenWidth < 322) {
        [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.964].active = YES;
    } else {
        
        [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    }
    
    if (screenWidth < 322) {
       [self.containerView.heightAnchor constraintEqualToConstant:289].active = YES;
    } else {
        
       [self.containerView.heightAnchor constraintEqualToConstant:299].active = YES;
    }
    
    
    
    
    
    
    if (screenWidth < 322) {
        [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.05].active = YES;
        
        [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.05].active = YES;
    } else {
        
        [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.005].active = YES;
        
        [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.005].active = YES;
    }

    [self.restImage.topAnchor constraintEqualToAnchor:self.containerView.topAnchor].active = YES;
    
//    [self.restImage.widthAnchor constraintEqualToAnchor:self.containerView.widthAnchor].active = YES;
    
    if (screenWidth < 322) {
        [self.restImage.heightAnchor  constraintEqualToConstant:156].active = YES;
    } else {
        
       [self.restImage.heightAnchor  constraintEqualToConstant:160].active = YES;
    }
    
    
    if (screenWidth < 322) {
         [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:12].active = YES;
    } else {
        
         [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:18].active = YES;
    }
    
    
   
    [self.callButton.centerYAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:116].active = YES;
      [self.callButton.widthAnchor  constraintEqualToConstant:56].active = YES;
    [self.callButton.heightAnchor  constraintEqualToConstant:56].active = YES;
    
    
    if (screenWidth < 322) {
        [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-2].active = YES;
    } else {
        
        [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-8].active = YES;
    }
    
    
    [self.discountBack.topAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:11].active = YES;
    [self.discountBack.widthAnchor  constraintEqualToConstant:60].active = YES;
    [self.discountBack.heightAnchor  constraintEqualToConstant:34].active = YES;
    
    [self.restDiscount.rightAnchor constraintEqualToAnchor:self.discountBack.rightAnchor constant:-4].active = YES;
    [self.restDiscount.centerYAnchor constraintEqualToAnchor:self.discountBack.centerYAnchor].active = YES;
//    [self.restDiscount.widthAnchor  constraintEqualToConstant:60].active = YES;
//    [self.restDiscount.heightAnchor  constraintEqualToConstant:34].active = YES;
    
    
    
    [self.descripView.centerXAnchor constraintEqualToAnchor:self.containerView.centerXAnchor].active = YES;
    [self.descripView.topAnchor constraintEqualToAnchor:self.restImage.bottomAnchor].active = YES;
    [self.descripView.bottomAnchor constraintEqualToAnchor:self.containerView.bottomAnchor].active = YES;
    [self.descripView.widthAnchor constraintEqualToAnchor:self.containerView.widthAnchor].active = YES;

    
    [self.restNamelable.centerXAnchor constraintEqualToAnchor:self.descripView.centerXAnchor].active = YES;
    [self.restNamelable.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:5].active = YES;
    [self.restNamelable.widthAnchor constraintEqualToAnchor:self.descripView.widthAnchor].active = YES;
//    [self.descripView.heightAnchor  constraintEqualToConstant:self.containerView.bounds.size.height * 0.59].active = YES;
    
    
    [self.addressView.centerXAnchor constraintEqualToAnchor:self.descripView.centerXAnchor].active = YES;
    [self.addressView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:38].active = YES;
    [self.addressView.widthAnchor constraintEqualToAnchor:self.descripView.widthAnchor].active = YES;
    //    [self.descripView.heightAnchor  constraintEqualToConstant:self.containerView.bounds.size.height * 0.59].active = YES;
    
    [self.restType.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: 10].active = YES;
    [self.restType.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
//    [self.restType.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.restType.heightAnchor constraintEqualToConstant:18].active = YES;
    
    [self.mapIconImage.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: screenWidth * 0.37].active = YES;
    [self.mapIconImage.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
    [self.mapIconImage.widthAnchor constraintEqualToConstant:9].active = YES;
        [self.mapIconImage.heightAnchor constraintEqualToConstant:11].active = YES;
    
    [self.restAddress.leftAnchor constraintEqualToAnchor:self.mapIconImage.rightAnchor constant: 5].active = YES;
    [self.restAddress.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
//    [self.restAddress.widthAnchor constraintEqualToConstant:9].active = YES;
//    [self.mapIconImage.heightAnchor constraintEqualToConstant:11].active = YES;
    
    
    [self.ratingView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:10].active = YES;
    [self.ratingView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:64].active = YES;
    [self.ratingView.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingView.heightAnchor constraintEqualToConstant:24].active = YES;
    
    
    [self.ratingImage.centerXAnchor constraintEqualToAnchor:self.ratingView.centerXAnchor].active = YES;
    [self.ratingImage.centerYAnchor constraintEqualToAnchor:self.ratingView.centerYAnchor].active = YES;
    [self.ratingImage.widthAnchor constraintEqualToConstant:115].active = YES;
    [self.ratingImage.heightAnchor constraintEqualToConstant:19].active = YES;
    
    [self.commentView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
    [self.commentView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:91].active = YES;
    [self.commentView.widthAnchor constraintEqualToConstant: screenWidth * 0.5].active = YES;
    [self.commentView.heightAnchor constraintEqualToConstant:28].active = YES;
    
    [self.commentsIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 12].active = YES;
    [self.commentsIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
        [self.commentsIcon.widthAnchor constraintEqualToConstant:17].active = YES;
        [self.commentsIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    [self.commentsCount.leftAnchor constraintEqualToAnchor:self.commentsIcon.rightAnchor constant: 5].active = YES;
    [self.commentsCount.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.avgIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 68].active = YES;
    [self.avgIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.avgIcon.widthAnchor constraintEqualToConstant:17].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    [self.averageLabel.leftAnchor constraintEqualToAnchor:self.avgIcon.rightAnchor constant: 5].active = YES;
    [self.averageLabel.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 122].active = YES;
    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.distansIcon.widthAnchor constraintEqualToConstant:18].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:16].active = YES;
    
    [self.distanceRest.leftAnchor constraintEqualToAnchor:self.distansIcon.rightAnchor constant: 5].active = YES;
    [self.distanceRest.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
    
    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 136].active = YES;
    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
    [self.distansIcon.widthAnchor constraintEqualToConstant:18].active = YES;
    [self.avgIcon.heightAnchor constraintEqualToConstant:16].active = YES;
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
         [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
    } else {
         [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:0].active = YES;
    }
    
    [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:80].active = YES;
    [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.37].active = YES;
    [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
//    [self.commentsIcon.widthAnchor constraintEqualToConstant:17].active = YES;
//    [self.commentsIcon.heightAnchor constraintEqualToConstant:18].active = YES;
    
    
//    [self.filterContainerView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//    [self.filterContainerView.topAnchor constraintEqualToAnchor:self.topContainerView.topAnchor].active = YES;
//    [self.filterContainerView.widthAnchor constraintEqualToAnchor:self.topContainerView.widthAnchor].active = YES;
//    [self.filterContainerView.heightAnchor constraintEqualToConstant:30].active = YES;
//    
//    
//    [self.restonsButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.leftAnchor constant: screenWidth * 0.07].active = YES;
//    [self.restonsButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.restonsButton.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.restonsButton.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.filterButton.centerXAnchor constraintEqualToAnchor:self.restonsButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
//    [self.filterButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.filterButton.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.filterButton.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.sortButton.centerXAnchor constraintEqualToAnchor:self.filterButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
//    [self.sortButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.sortButton.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.sortButton.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.mapButton.centerXAnchor constraintEqualToAnchor:self.sortButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
//    [self.mapButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.mapButton.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.mapButton.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.gridButton.centerXAnchor constraintEqualToAnchor:self.mapButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
//    [self.gridButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.gridButton.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.gridButton.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.langButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.08)].active = YES;
//    [self.langButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: -(4)].active = YES;
//    [self.langButton.widthAnchor constraintEqualToConstant:26].active = YES;
//    [self.langButton.heightAnchor constraintEqualToConstant:15].active = YES;
//    
//    
//    [self.cityLabel.centerXAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.28)].active = YES;
//    [self.cityLabel.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: 4].active = YES;
//    [self.cityLabel.widthAnchor constraintEqualToConstant:31].active = YES;
//    [self.cityLabel.heightAnchor constraintEqualToConstant:13].active = YES;
//    
//    [self.markerImage.rightAnchor constraintEqualToAnchor:self.cityLabel.leftAnchor constant:-5].active = YES;
//    [self.markerImage.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
//    [self.markerImage.widthAnchor constraintEqualToConstant:10].active = YES;
//    [self.markerImage.heightAnchor constraintEqualToConstant:16].active = YES;
//    
//    [self.searchField.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//    [self.searchField.topAnchor constraintEqualToAnchor:self.filterContainerView.bottomAnchor].active = YES;
//    [self.searchField.widthAnchor constraintEqualToConstant:screenWidth * 0.968].active = YES;
//    [self.searchField.heightAnchor constraintEqualToConstant:44].active = YES;
//    
//    [self.tableCellContainer.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//    [self.tableCellContainer.topAnchor constraintEqualToAnchor:self.topContainerView.bottomAnchor].active = YES;
//    [self.tableCellContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
//    [self.tableCellContainer.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
//    //    [self.tableCellContainer.heightAnchor constraintEqualToConstant:92].active = YES;
//    
//    [self.collectionContainer.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//    [self.collectionContainer.topAnchor constraintEqualToAnchor:self.topContainerView.bottomAnchor].active = YES;
//    [self.collectionContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
//    [self.collectionContainer.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
    
}


- (IBAction)submitButton:(id)sender {
    
}

//- (IBAction)callActionButton:(id)sender {
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
