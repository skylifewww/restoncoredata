//
//  CommentsViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/27/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const OpenCommentsNotification;

@interface CommentsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleNav;
@property (strong, nonatomic) NSNumber* restaurantID;
@property (strong, nonatomic) NSString* titleNavString;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *disLikeButton;
@property (assign, nonatomic) Boolean isFirstTimeOpen;

@end
