//
//  MapViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RNOrderInfo.h"
#import "RNRestaurant.h"
#import "TSClusterMapView.h"

extern NSString *const OrdersCountChangeNotification;


@interface MapViewController : UIViewController <TSClusterMapViewDelegate>

//@property (weak) IBOutlet NSProgressIndicator *buildProgress;

@property (strong, nonatomic) UIBarButtonItem *openReservBarButton;

@property (nonatomic, assign) Boolean isAllRestons;
@property (nonatomic, assign) Boolean isFiltered;
@property (nonatomic, strong) NSString* cityName;

//@property (nonatomic, assign) Boolean isUserLocation;
@property (nonatomic, assign) CLLocationCoordinate2D userLocation;
//@property (nonatomic, copy) NSArray *mapItems;
@property (nonatomic, copy) NSArray<RNRestaurant*> *restonsItems;
@property (nonatomic, assign) BOOL showInfo;

@property (nonatomic, copy) NSNumber *restaurantId;
@property (nonatomic, strong) RNOrderInfo *orderInfo;

@property (nonatomic, strong) RNRestaurant *restaurant;


@property (strong, nonatomic) IBOutlet TSClusterMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UIView *buttonsView;

@property (weak, nonatomic) IBOutlet UIButton *showRouteButton;


@property (weak, nonatomic) IBOutlet UIView *descripView;

@property (weak, nonatomic) IBOutlet UIView *addressView;

@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIImageView *commentsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *avgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *distansIcon;

@property (weak, nonatomic) IBOutlet UIButton *openRestButton;

@property (weak, nonatomic) IBOutlet UILabel *restNamelable;
@property (weak, nonatomic) IBOutlet UILabel *restType;

@property (weak, nonatomic) IBOutlet UIImageView *ratingImage;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceRest;



@end
