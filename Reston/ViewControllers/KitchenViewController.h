//
//  KitchenViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/4/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "FilterInfo.h"

static NSString *const kKitchenKey;
extern NSString *const KitchenChangeNotification;

@interface KitchenViewController : UITableViewController
@property (strong, nonatomic) FilterInfo* filterInfo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *typeRestBarButtom;
@property (strong, nonatomic) NSString* currCity;
@property (strong, nonatomic) NSArray* filteredKitchens;

@end

