//
//  CatalogRestViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"
#import "RNRestaurant.h"
extern NSString *const SetupRestonsNotification;
extern NSString *const RestonsChangeNotification;


@interface CatalogRestViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong, nonatomic) NSArray<RNRestaurant*>* restons;

//@property (nonatomic, strong) NSString* currCity;

@property (nonatomic, assign) Boolean isAuth;
//@property (nonatomic, assign) Boolean isThisCity;

@property (nonatomic, strong) RNRestaurant *restaurant;

@property (nonatomic, strong) RNOrderInfo *orderInfo;

@property (weak, nonatomic) IBOutlet UIView *topContainerView;

@property (weak, nonatomic) IBOutlet UIView *filterContainerView;

@property (weak, nonatomic) IBOutlet UIButton *restonsButton;

@property (weak, nonatomic) IBOutlet UIButton *filterButton;

@property (weak, nonatomic) IBOutlet UIButton *sortButton;

@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIButton *newsButton;

@property (weak, nonatomic) IBOutlet UIButton *gridButton;
@property (weak, nonatomic) IBOutlet UIButton *filterCancelButton;

@property (weak, nonatomic) IBOutlet UIImageView *markerImage;

@property (weak, nonatomic) IBOutlet UIButton *cityButton;

@property (weak, nonatomic) IBOutlet UIView *collectionContainer;

@property (weak, nonatomic) IBOutlet UIView *tableCellContainer;

//@property (weak, nonatomic) IBOutlet UITableView *catalogTableView;

@property (weak, nonatomic) IBOutlet UIButton *langButton;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UISearchBar *searchField;

@property (weak, nonatomic) IBOutlet UILabel *bookMarkLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end
