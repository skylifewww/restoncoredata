//
//  RNRegisterViewController.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNRegisterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;


@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *passwordView;

@property (weak, nonatomic) IBOutlet UIView *confirmView;


@property (weak, nonatomic) IBOutlet UILabel *faceLabel;


@end
