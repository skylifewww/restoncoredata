//
//  FavRestonsViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/2/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "FavRestonsViewController.h"
#import "RNRestaurant.h"
#import "RestonCell.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNOrderInfo.h"
#import "UIImage+Cache.h"
#import "RNUser.h"
#import "RNCountPeopleHelper.h"
#import "RestonItemViewController.h"
#import "RNGetFavoritesRequest.h"
#import "RNGetFavoritesResponse.h"
#import "RNRemoveFromFavRequest.h"
#import "ReservTableViewController.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "CDRestaurant+CoreDataProperties.h"
#import "CoreDataManager.h"

#import "Reachability.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface FavRestonsViewController ()<CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>{
    BOOL loading;
    UIView* containerView;
    UIColor* mainColor;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UIBarButtonItem *treshButton;
    UIView* backView;
    UIView* popView;
    UIView* backGeoView;
    NSString* realCity;
    
    Reachability *_reachability;
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSFetchRequest* fetchRequest;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) NSSortDescriptor* descriptor;
@property (strong, nonatomic) NSPredicate* predicateCity;
@property (strong, nonatomic) NSArray<NSSortDescriptor*>* descriptorsArray;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation FavRestonsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"select_rest_title", nil);
    _myFavLabel.text = DPLocalizedString(@"my_fav_rest", nil);
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    if (screenWidth < 322) {
        
        [self.myFavLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self setNavigationBarItem];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    treshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(addORDoneRows)];
    [self.navigationItem setRightBarButtonItem:treshButton];
    [treshButton setTintColor:[UIColor whiteColor]];
  
    self.editing = NO;
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([RNUser sharedInstance].isGeoEnabled){
        
        [self loadUserLocation];
        
        if ([self isInternetConnect]){
            [self loadData];
        }
    }
}

#pragma mark - NSFetchedResultsController methods

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    self.managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
    self.fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"CDRestaurant"
                inManagedObjectContext:self.managedObjectContext];
    
    [self.fetchRequest setEntity:description];
    
    self.descriptorsArray = @[];
    NSSortDescriptor * intCatalogPriorityDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"intCatalogPriority" ascending:YES];
    NSSortDescriptor * viewsPriorityDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"views" ascending:NO];
    self.descriptorsArray = @[intCatalogPriorityDescriptor, viewsPriorityDescriptor];
    
    [self.fetchRequest setSortDescriptors:self.descriptorsArray];
    [self.fetchRequest setFetchBatchSize:12];
    
    
    self.predicateCity = [NSPredicate predicateWithFormat:@"city == %@", [RNUser sharedInstance].currCity];
    [self.fetchRequest setPredicate:self.predicateCity];
    
    //    NSError *fetchError = nil;
    //    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
    //    NSLog(@"fetchedObjects.count %ld",fetchedObjects.count);
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
   
        return YES;
    }
}


-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void) loadUserLocation
{
    
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    _latitude = _locationManager.location.coordinate.latitude;
    _longitude = _locationManager.location.coordinate.longitude;
    NSLog(@"CatalogRestViewController latitude %f longitude %f",_latitude, _longitude);
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
    [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
    
    if ([self isInternetConnect]){
        
    RNGetFavoritesRequest *request = [[RNGetFavoritesRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
        if (error != nil || [response isKindOfClass:[NSNull class]]) {
                        [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                        return;
                    }
        RNGetFavoritesResponse *r = (RNGetFavoritesResponse *)response;
        
        _restons = r.favModels;
    
        if (_restons.count == 0){
            treshButton.enabled = NO;
            [treshButton setTintColor:[UIColor clearColor]];
            
        [self setupView];
            
            
        } else if (_restons.count > 0){
            [self.view bringSubviewToFront:_tableView];
            [treshButton setTintColor:[UIColor whiteColor]];
            treshButton.enabled = YES;
            [_tableView reloadData];
        }
    }];
  }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.title = DPLocalizedString(@"select_rest_title", nil);
    _myFavLabel.text = DPLocalizedString(@"my_fav_rest", nil);
    [containerView removeFromSuperview];
    [self loadData];
    
}


- (void)addORDoneRows
{
    if(self.editing)
    {
        [super setEditing:NO animated:NO];
        [_tableView setEditing:NO animated:NO];
        [_tableView reloadData];
 
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
    }
    else
    {
        [super setEditing:YES animated:YES];
        [_tableView setEditing:YES animated:YES];
        [_tableView reloadData];
      
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}


- (void)showAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(double) getDistanceToRest:(CLLocationCoordinate2D)restCoord{
    
    
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:_latitude longitude:_longitude];
    
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:restCoord.latitude longitude:restCoord.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
    
//    NSLog(@"Distance from Annotations - %f", distance);
    
    return distance;
    
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger rowHeight = 299;
    
    if (screenWidth < 322) {
        rowHeight = 289;
    }
    
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _restons.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    RestonCell *cell = (RestonCell *)[tableView dequeueReusableCellWithIdentifier:@"RestonCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RestonCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];

    }
    
    RNRestaurant* rest = _restons[indexPath.row];
    
    _restaurant = rest;
    
    cell.restType.text = rest.type;
    
//    NSLog(@"_restaurant.type %@", rest.type);
    
//    NSLog(@"_restaurant.title %@", rest.title);
    
    NSString* type = [NSString stringWithFormat:@"%@    ",rest.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",rest.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:12];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:10];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];
    
    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:10];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];
    
    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [cell.restType setAttributedText:attributedString1];
    
    NSUInteger rating = (int)(([rest.rating doubleValue])*100);
    cell.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    
    
    if ([RNUser sharedInstance].isGeoEnabled && _latitude != 0 && _longitude != 0){
    cell.distansIcon.hidden = NO;
    CGFloat distancRest = [self getDistanceToRest:rest.coordinate];
//    NSLog(@"distanceRest %ld--------------------------------", (long)distancRest);
//    NSLog(@"distanceRest %f--------------------------------", (floor(distancRest)));
    NSInteger numKM = (NSInteger)distancRest / 1000;
//    NSLog(@" NSInteger numKM  %ld---------------------------------", (long)numKM);
    NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
//    NSLog(@" NSInteger numMM  %ld---------------------------------", (long)numMM);
    NSString* strKM = @"";
    if (numKM != 0){
        strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
    }
    NSString* strMM = @"";
    if (numMM != 0){
        strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
    }
        NSString* distanceRest = @"";
        
        if (numKM < 100){
            distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
            
        } else {
            distanceRest = [NSString stringWithFormat:@"%@", strKM];
        }

    cell.distanceRest.text = distanceRest;
//        NSLog(@"restaurant.coordinate.latitude %f restaurant.coordinate.longitude %f", rest.coordinate.latitude,rest.coordinate.longitude);
    } else {
        cell.distansIcon.hidden = YES;
        cell.distanceRest.text = @"";
    }
    
    NSString* restName = [rest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    cell.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
    
    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    cell.commentsCount.text = [NSString stringWithFormat:@"%@ %@",rest.reviewCount, commStr];

    [UIImage cachedImage:rest.images[0]
           fullURLString:nil
            withCallBack:^(UIImage *image) {
                cell.restImage.image = image;
            }];
    cell.submitButton.tag = indexPath.row;
    [cell.submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.callButton addTarget:self action:@selector(callButton:) forControlEvents:UIControlEventTouchUpInside];
    
    if (rest.avgBill.integerValue == 0) {
        cell.averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        cell.averageLabel.text = [NSString stringWithFormat:@"%@ %@",rest.avgBill, avgStr];
    }
    //    _subwayLabel.text = [[RNUser sharedInstance] subwayNameFord:model.subway];
    
    if (rest.discounts.integerValue == 0) {
        cell.discountBack.image = nil;
        cell.restDiscount.hidden = YES;
    } else {
        cell.restDiscount.hidden = NO;
        cell.restDiscount.text = [NSString stringWithFormat:@"-%@%%",rest.discounts];
        cell.discountBack.image = [UIImage imageNamed:@"discont"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
if ([self isInternetConnect]){
    RNRestaurant* currentResto = _restons[indexPath.row];
    __block UIImage* backImage;
    [UIImage cachedImage:currentResto.images[0]
           fullURLString:nil
            withCallBack:^(UIImage *image) {
                backImage = image;
            }];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RestonItemViewController * restonItemViewController = [storyboard instantiateViewControllerWithIdentifier:@"RestonItemViewController"];

    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:restonItemViewController];
 
    [self.navigationController presentViewController:navVC animated:NO completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
     if ([self isInternetConnect]){
            RNRestaurant *r  = _restons[indexPath.row];
         RNRequest *request;
         
         request = [[RNRemoveFromFavRequest alloc] init];
         ((RNRemoveFromFavRequest *)request).restaurantId = r.restaurantId;
         
         
         [request sendWithcompletion:^(RNResponse *response, NSError *error) {
             if (error != nil) {
                 [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                 return;
             } else {
                 
                 [self setupPopView];
                 [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn
                                  animations:^{
                                      backView.alpha = 0.0;
                                      popView.alpha = 0.0;
                                  }
                                  completion:^(BOOL finished) {
                                      [backView removeFromSuperview];
                                      [popView removeFromSuperview];
                                  }];
                 [self loadData];
                 
             }
         }];
         
        }
        
    }
}

- (void)setupPopView {
    
    
    backView = [[UIView alloc]init];
    backView.backgroundColor = [UIColor whiteColor];
    backView.alpha = 0.5f;
    [self.view addSubview:backView];
    
    popView = [[UIView alloc]init];
    popView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:popView];
    
    UIImageView* popImageView = [[UIImageView alloc] init];
    [popView addSubview:popImageView];
    
    UILabel* popLabel = [[UILabel alloc] init];
    
    if (screenWidth < 322) {
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:14.0];
    } else {
        
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:18.0];
    }
    
    popLabel.numberOfLines = 2;
    popLabel.textAlignment = NSTextAlignmentCenter;
    popLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [popView addSubview:popLabel];
    

        popLabel.text = DPLocalizedString(@"remove_fav", nil);
        popImageView.image = [UIImage imageNamed:@"iconFav"];
    
    
    backView.translatesAutoresizingMaskIntoConstraints = false;
    popView.translatesAutoresizingMaskIntoConstraints = false;
    popImageView.translatesAutoresizingMaskIntoConstraints = false;
    popLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [backView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [backView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [backView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [backView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    [popView.centerXAnchor constraintEqualToAnchor:backView.centerXAnchor].active = YES;
    [popView.centerYAnchor constraintEqualToAnchor:backView.centerYAnchor].active = YES;
    [popView.widthAnchor constraintEqualToConstant:screenWidth * 0.33].active = YES;
    [popView.heightAnchor constraintEqualToConstant:screenWidth * 0.44].active = YES;
    
    [popImageView.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popImageView.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popImageView.topAnchor constraintEqualToAnchor:popView.topAnchor].active = YES;
    [popImageView.heightAnchor constraintEqualToConstant:screenWidth * 0.29].active = YES;
    
    [popLabel.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popLabel.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popLabel.topAnchor constraintEqualToAnchor:popImageView.bottomAnchor].active = YES;
    [popLabel.bottomAnchor constraintEqualToAnchor:popView.bottomAnchor].active = YES;
}


-(NSString*) getRating:(NSUInteger)rating{
    
    NSString* nameRating;
    
    if (rating == 0){
        nameRating = @"rating5_0";
        
    } else if (rating < 19){
        
        nameRating = @"rating5_1";
        
    } else if (rating >= 20 && rating < 40){
        
        nameRating = @"rating5_2";
        
    } else if (rating >= 40 && rating < 60){
        
        nameRating = @"rating5_3";
        
    } else if (rating >= 60 && rating < 80){
        
        nameRating = @"rating5_4";
        
    } else if (rating > 80){
        
        nameRating = @"rating5_5";
        
    } else {
        
        nameRating = @"rating5_0";
        
    }
//    NSLog(@"nameRating %@", nameRating);
    return nameRating;
}





- (void)submitButton:(id)sender{
    
    UIButton *button = (UIButton *)sender;
//    UIView *descripView = button.superview;
//    UIView* containerViews = descripView.superview;
//    UIView* contentView = containerViews.superview;
//    UITableViewCell *cell = (UITableViewCell *)contentView.superview;
//    NSIndexPath * indexPath = [_tableView indexPathForCell:cell];
//    NSLog(@"row containing button: %ld", (long)indexPath.row);
    RNRestaurant* currentRestourant = _restons[button.tag];
//    RNRestaurant* currentRestourant = _restons[indexPath.row];
    
    //    currentRestourant = [self convertToHumanRest:currentRestourant];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = currentRestourant;
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)callButton:(id)sender{
    
    NSString* telephone = @"+380509003493";
//    NSLog(@"telephone: %@ ****************************************", telephone);
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",telephone]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
//        UIAlertView* calert = [[UIAlertView alloc]initWithTitle:@"Звонок" message:@"в данное время не возможен!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//        [calert show];
    }
    
}


- (void)addFav:(id)sender {
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(changeViewController:)]) {
        
        [_delegate changeViewController:LeftCatalogue];
        _delegate = nil;
        
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    
    containerView.translatesAutoresizingMaskIntoConstraints = false;
    
    _backFavIcon = [[UIImageView alloc] init];
    _backFavIcon.image = [UIImage imageNamed:@"iconFav"];
    _backFavIcon.contentMode = UIViewContentModeScaleAspectFit;
    [containerView addSubview:_backFavIcon];
    _backFavIcon.translatesAutoresizingMaskIntoConstraints = false;
    
    _submitButton = [[UIButton alloc] init];
    _submitButton.translatesAutoresizingMaskIntoConstraints = false;
    [containerView addSubview:_submitButton];
    
    _textFavlabel = [[UILabel alloc] init];
    _textFavlabel.text = DPLocalizedString(@"no_fav", nil);
    _textFavlabel.textColor = [UIColor lightGrayColor];
    _textFavlabel.numberOfLines = 2;
    _textFavlabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:_textFavlabel];
 
    
    [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
    _textFavlabel.translatesAutoresizingMaskIntoConstraints = false;
    
    if (screenWidth < 322) {
        [self.myFavLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
   
    
    
    [containerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [containerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [containerView.topAnchor constraintEqualToAnchor:self.myFavLabel.bottomAnchor].active = YES;
    [containerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    [_submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
    
    _submitButton.backgroundColor = mainColor;

    [_submitButton setTitle:DPLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _submitButton.layer.cornerRadius = 3;
    _submitButton.layer.masksToBounds = true;
    _submitButton.userInteractionEnabled = YES;
    [_submitButton addTarget:self action:@selector(addFav:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_backFavIcon.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_backFavIcon.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.43].active = YES;
    [_backFavIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.42].active = YES;
    [_backFavIcon.heightAnchor constraintEqualToConstant:screenHeight * 0.23].active = YES;
    
    if (screenWidth < 322) {
        [_submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [_submitButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_submitButton.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(screenHeight * 0.13)].active = YES;
    [_submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
    [_submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
    
    if (screenWidth < 322) {
        [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [_textFavlabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_textFavlabel.centerYAnchor constraintEqualToAnchor:_submitButton.topAnchor constant:-screenHeight * 0.06].active = YES;
    [_textFavlabel.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    [_textFavlabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
}

@end
