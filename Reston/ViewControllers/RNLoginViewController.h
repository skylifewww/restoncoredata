//
//  RNViewController.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *passwordView;

@property (weak, nonatomic) IBOutlet UILabel *faceLabel;

@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UILabel *forgotPassLabel1;
@property (weak, nonatomic) IBOutlet UILabel *forgotpassLabel2;

@end
