//
//  RestonItemViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//
#import "AppDelegate.h"
#import "RestonItemViewController.h"
#import "iCarousel.h"
#import "CarouselCell.h"
#import "CockCell.h"
#import "CockPadCell.h"
#import "MenuPadCell.h"
#import "CockMenuCell.h"
#import "UIImage+Cache.h"
#import "MapViewController.h"


#import "RNGetRestaurantRequest.h"
#import "RNGetRestaurantResponse.h"
#import "RNGetFavoritesRequest.h"
#import "RNRemoveFromFavRequest.h"
#import "RNUser.h"

#import "UITableView+Registration.h"
#import "RNInfoDescriptionCell.h"
#import "RNInfoDetailCell.h"
#import "RNNewsTableViewCell.h"
#import "RNInfoReviewTableViewCell.h"
#import "RNPromptView.h"

#import "RNGetNewsRequest.h"
#import "RNGetNewsResponse.h"
#import "RNNEw.h"

#import "RNGetFeedbacksRequest.h"
#import "RNGetFeedbacksResponse.h"
#import "RNFeedback.h"


#import "RNAddToFavRequest.h"
#import "RNAddToFavResponse.h"
#import "ReservTableViewController.h"

#import "CommentsViewController.h"
#import "News1ViewController.h"
#import "NewsExpandViewController.h"
#import "OffersViewController.h"
#import "OffersExpViewController.h"
#import "FullScreenViewController.h"
#import "UIViewController+Orientation.h"
#import "Telprompt.h"
#import "PDFViewController.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import <AVFoundation/AVFoundation.h>

#import "AddLikeRequest.h"
#import "AddLikeResponse.h"

#import "Reachability.h"
#import "UIColor+Hexadecimal.h"

#import "RNGetFeedbacksRequest.h"
#import "RNGetFeedbacksResponse.h"
#import "RNGetNewsRequest.h"
#import "RNGetNewsResponse.h"
#import "GetOffersRequest.h"
#import "GetOffersResponse.h"
#import "Offer.h"


@interface RestonItemViewController ()<UITableViewDataSource,UITableViewDelegate,iCarouselDataSource,iCarouselDelegate, CLLocationManagerDelegate> {
    BOOL descriptionResized;
    BOOL menuResized;
    BOOL isAdded;
    UIColor* mainColor;
    NSLayoutConstraint *tableViewHeightAnchorConstraint;
    CGFloat heightForDescriptionRow;
    CGFloat heightForCockRow;
    CGFloat heightForCockMenuRow;
    
    double latitude;
    double longitude;
    
    NSString* distanceRestString;
    MKMapView *myMapView;
    
    UIView* backGeoView;
    
    UIView* backView;
    UIView* popView;
    
    UIView* timeView;
    UIView* backTimeView;
    UIView* phonePopView;
    UIView* backPhoneView;
    
    UIImageView* backRestImage;

    CGFloat screenWidth;
    NSInteger currDayNumber;
    UISwipeGestureRecognizer* swipeDown;
    
    NSInteger feedsCount;
    NSInteger newsCount;
    NSInteger offersCount;
    
    Boolean isGreen;
    UIStoryboard* storyboard;
    
    Boolean isPhone;
    double addHeight;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    double typeFontSize;
    double addressFontSize;
//    NSString* realCity;
    
    CGFloat screenHeight;
    double estimateHeight;
    Boolean isFirstCommentOpen;
    
    MenuPadCell *menuPadCell;
    CockMenuCell *menuPhoneCell;
    
    CGPDFDocumentRef pdfDocument;
    size_t pageCount;
    Boolean isGetPDF;
    
     Reachability *_reachability;
    Boolean isReservedDay;
    
//    UIActivityIndicatorView *loadIndicator;
}

//@property (nonatomic, retain) AVAudioPlayer *audioPlayer;

//@property (nonatomic, strong) RNRestaurant *restaurant;
@property (weak, nonatomic) IBOutlet iCarousel *carousel;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *feedsButton;
@property (weak, nonatomic) IBOutlet UIButton *newsButton;
@property (weak, nonatomic) IBOutlet UIButton *offersButton;

@property (weak, nonatomic) IBOutlet UIView *descripView;

@property (weak, nonatomic) IBOutlet UIImageView *favIcon;

@property (weak, nonatomic) IBOutlet UILabel *imagesCountlabel;

@property (weak, nonatomic) IBOutlet UIButton *addRatingButton;

@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIImageView *mapIconImage;
@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIImageView *commentsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *avgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *distansIcon;

@property (weak, nonatomic) IBOutlet UIButton *resrvedButton;
@property (weak, nonatomic) IBOutlet UILabel *restNamelable;
@property (weak, nonatomic) IBOutlet UILabel *restType;
@property (weak, nonatomic) IBOutlet UILabel *restAddress;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImage;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceRest;


@property (weak, nonatomic) IBOutlet UIImageView *discountBack;
@property (weak, nonatomic) IBOutlet UILabel *restDiscount;
@property (weak, nonatomic) IBOutlet UIButton *callButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;

@property (weak, nonatomic) IBOutlet UILabel *likeCount;
@property (weak, nonatomic) IBOutlet UIButton *onMapButton;

@property (nonatomic, copy) NSArray *news;
@property (nonatomic, copy) NSArray *reviews;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UIView *gradientView;

@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDown;
@property (weak, nonatomic) IBOutlet UIImageView *dateIcon;
@property (weak, nonatomic) IBOutlet UILabel *dateNumber;

@property (weak, nonatomic) IBOutlet UIView *phoneView;

@property (weak, nonatomic) IBOutlet UIImageView *phoneIcon;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDownPhone;

@property (weak, nonatomic) IBOutlet UILabel *cockStyle;
@property (weak, nonatomic) IBOutlet UILabel *cockDescrLabel;
@property (weak, nonatomic) IBOutlet UILabel *featuresLabel;
@property (weak, nonatomic) IBOutlet UILabel *featuresDescrLabel;
//@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) UIButton *favorBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDownBottom;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageGradient;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptViewHC;
@property (nonatomic, assign) UIInterfaceOrientation interfaceOrientation;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gradientTopAnchor;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gradientViewHeight;


@end

@implementation RestonItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    self.discountBack.hidden = YES;
    self.restDiscount.hidden = YES;
    isGetPDF = NO;
    
    isReservedDay = YES;
    
    isFirstCommentOpen = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        typeFontSize = 12.0;
        addressFontSize = 10.0;
        addHeight = 40;
        restNameFontSize = 15.0;
        textFontSize = 12.0;
        if (screenWidth < 322){
            buttonFontSize = 14.0;
        } else {
            buttonFontSize = 17.0;
        }
        
        if (screenHeight > 811){
            _gradientTopAnchor.constant = -88;
            _gradientViewHeight.constant = 46;
        } else {
            _gradientTopAnchor.constant = -52;
            _gradientViewHeight.constant = 0;
        }
        
        cornerRadius = 3.0;
        isPhone = YES;
        estimateHeight = 120;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        typeFontSize = 18.0;
        addressFontSize = 14.0;
        addHeight = 100;
        restNameFontSize = 24.0;
        textFontSize = 16.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
        estimateHeight = 320;
    }
    
//    [self setupBackImage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    [self restrictRotation:NO];
    

     _imageGradient.image = [self drawGradientWithFrame:CGRectMake(0, 0, screenWidth, 65)];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    if ([RNUser sharedInstance].isGeoEnabled){
    
        [self loadUserLocation];
        if ([self isInternetConnect]){
            [self loadData];
        }
    } else {
        
        [self checkLocationServicesAndStartUpdates];
    }

    
    UIImage* clearImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:clearImage forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.shadowImage = clearImage;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    UIButton* backButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 45, 21)];
    [backButton1 addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [backButton1 setTintColor:[UIColor whiteColor]];
    [backButton1 setImage:[UIImage imageNamed:@"arrowLeft.png"]  forState:UIControlStateNormal];
    
    [self.navigationController.navigationBar addSubview:backButton1];
   
    descriptionResized = false;
    menuResized = false;

    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    [self configureTable];
    
    heightForDescriptionRow = [RNInfoDescriptionCell sizeForText:_restCD.descriptionRestaurant
                                                    shouldResize:descriptionResized];
    heightForCockMenuRow = [CockMenuCell sizeForText:_restCD.descriptionRestaurant
                                        shouldResize:menuResized] + 50;

    _carousel.decelerationRate = 0.1;
    
//    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
//    [_carousel addGestureRecognizer:swipeDown];
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = estimateHeight;

    [self setupView];
    if ([self isInternetConnect]){
        NSNumber *restaurantId = [NSNumber numberWithInt:(int)[self.restCD.restaurantId integerValue]];
        [self loadFeedbacks:restaurantId];
        [self loadNews:restaurantId];
        [self loadOffers:restaurantId];
    }
}


-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {

        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) restrictRotation:(BOOL) restriction{
   AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.restrictRotation = restriction;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.interfaceOrientation = UIInterfaceOrientationMaskPortrait;
    
//    _segmentControl.selectedSegmentIndex = 0;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = estimateHeight;
    
    
    UIImage* clearImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:clearImage forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.shadowImage = clearImage;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:YES];

}
-(BOOL) prefersStatusBarHidden{
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(double) getDistance{

    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLLocationCoordinate2D coordinate= CLLocationCoordinate2DMake(_restCD.lat, _restCD.lon);
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
    
//    NSLog(@"Distance from Annotations - %f", distance);
    
    return distance;
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{

    [self showAlertWithTextForSuccessfully:@"Ошибка" withText:@"определения вашей геолокации"];
//    NSLog(@"Error: %@",error.description);
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CLLocationCoordinate2D coordinate= CLLocationCoordinate2DMake(_restCD.lat, _restCD.lon);
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    CLLocationDistance distance = [loc1 distanceFromLocation:loc2];

//        NSLog(@"Distance  - %f", distance);

    [_locationManager stopUpdatingLocation];
}

-(void)updateDistanceToAnnotation:(id<MKAnnotation>)annotation
{
    if (annotation == nil)
    {
        distanceRestString = @"";
        return;
    }
    
    if (myMapView.userLocation.location == nil)
    {
        distanceRestString = @"User location is unknown";
        return;
    }
    
    CLLocation *pinLocation = [[CLLocation alloc]
                               initWithLatitude:annotation.coordinate.latitude
                               longitude:annotation.coordinate.longitude];
    
    CLLocation *userLocation = [[CLLocation alloc]
                                initWithLatitude:latitude
                                longitude:longitude];
    
    CLLocationDistance distance = [pinLocation distanceFromLocation:userLocation];
    
    distanceRestString = [NSString stringWithFormat:@"Distance to point %4.0f m.", distance];
//    NSLog(@"distanceRestString %@  ////////////////////////////////////////////////////////////////////////////////", distanceRestString);
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (mapView.selectedAnnotations.count == 0)
        //no annotation is currently selected
        [self updateDistanceToAnnotation:nil];
    else
        //first object in array is currently selected annotation
        [self updateDistanceToAnnotation:[mapView.selectedAnnotations objectAtIndex:0]];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{    
    [self updateDistanceToAnnotation:view.annotation];
}

- (IBAction)mapButton:(id)sender {
    
//    NSLog(@"Map!!!!");
    
    MapViewController * mapViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    mapViewController.isAllRestons = NO;
//    mapViewController.restaurantId = _restaurant.restaurantId;
//    _orderInfo.restaurant = _restaurant;
//    mapViewController.orderInfo = _orderInfo;
////    mapViewController.mapItems = @[_restaurant];
//    mapViewController.orderInfo = _orderInfo;
    mapViewController.showInfo = NO;
//    mapViewController.restaurant = _restaurant;
    mapViewController.cityName = _cityName;
    
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:mapViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

-(void) checkLocationServicesAndStartUpdates
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    //Checking authorization status
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
    
        if (![CLLocationManager locationServicesEnabled])
        {
       
        }
       
        else
        {
       
        }
   
        return;
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [_locationManager startUpdatingLocation];
    }
}


- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {

        case kCLAuthorizationStatusDenied: {
//            NSLog(@"User denied location access request!!");
            [self setupBackGeoView];
            [self showGeoAlertWithTitle:DPLocalizedString(@"for_geo", nil) andMessage:DPLocalizedString(@"forward_to_settings_for_geo", nil)];
            
            [_locationManager stopUpdatingLocation];
            //            [loadingView stopLoading];
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            
            [RNUser sharedInstance].isGeoEnabled = YES;
            [self loadUserLocation];
            if ([self isInternetConnect]){
                [self loadData];
            }
            [backGeoView removeFromSuperview];
            
//            NSLog(@"Userlocation!!!!");
            
        } break;
        case kCLAuthorizationStatusNotDetermined:{
            
            [RNUser sharedInstance].isGeoEnabled = YES;
            [self loadUserLocation];
            if ([self isInternetConnect]){
                [self loadData];
            }
            [backGeoView removeFromSuperview];
//            NSLog(@"Userlocation!!!!!");
            
        } break;
        case kCLAuthorizationStatusAuthorizedAlways: {
            [RNUser sharedInstance].isGeoEnabled = YES;
            [self loadUserLocation];
            if ([self isInternetConnect]){
                [self loadData];
            }
            [backGeoView removeFromSuperview];
//            NSLog(@"Userlocation!!!!!");
            
        } break;
        default:
            break;
    }
}

- (void)showGeoAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSString* url = SYSTEM_VERSION_LESS_THAN(@"10.0") ? @"prefs:root=LOCATION_SERVICES" : @"App-Prefs:root=Privacy&path=LOCATION";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
        
    }];
    
    UIAlertAction* continueWithoutLocation = [UIAlertAction actionWithTitle:DPLocalizedString(@"pass_action", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [RNUser sharedInstance].isGeoEnabled = NO;
        [backGeoView removeFromSuperview];
        
        if ([self isInternetConnect]){
            [self loadData];
        }
    }];
    
    [alert addAction:actionCancel]; // 4
    [alert addAction:continueWithoutLocation]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void) loadUserLocation
{

    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    latitude = _locationManager.location.coordinate.latitude;
    longitude = _locationManager.location.coordinate.longitude;
//    NSLog(@"CatalogRestViewController latitude %f longitude %f",latitude, longitude);
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [self getAddressFromLocation:userLoc];
    
//    NSLog(@"realCity %@ _cityName %@",realCity, _cityName);
//    if ([realCity isEqualToString:@"Kiev"]){
//        
//        realCity = @"1";
//        
//        
//    } else if ([realCity isEqualToString:@"Lviv"]){
//        
//        realCity = @"6";
//        
//    } else if ([realCity isEqualToString:@"Ivano-Frankivs'k"]){
//        realCity = @"8";
//        
//    } else {
//        realCity = @"10000";
//    }
//    
//    if ([realCity isEqualToString:_cityName]){
//        NSLog(@"realCity %@ Equal  cityName %@", realCity, _cityName);
//        _isThisCity = YES;
//    } else {
//        NSLog(@"realCity %@ not Equal  cityName %@", realCity, _cityName);
//        _isThisCity = NO;
//    }
    
//    NSLog(@"realCity %@ _cityName %@",realCity, _cityName);
    
    [_locationManager stopUpdatingLocation];
}

-(NSString *)getAddressFromLocation:(CLLocation *)location {
    
    __block NSString *city;
    __block NSString *administrativeArea;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             city = [NSString stringWithFormat:@"%@",[placemark locality]];
             NSLog(@"city %@",city);
             administrativeArea = [NSString stringWithFormat:@"%@",[placemark administrativeArea]];
             NSLog(@"administrativeArea %@",administrativeArea);
             
             
             if (![city isKindOfClass:[NSNull class]] || ![administrativeArea isKindOfClass:[NSNull class]]){
                 
                 NSLog(@"cityName != nil");
                 if ([city isEqualToString:@"Kiev"] || [administrativeArea isEqualToString:@"Kiev Oblast"] || [city isEqualToString:@"Киев"] || [city isEqualToString:@"Київ"]){

                     [RNUser sharedInstance].realCity = @"1";

                 } else if ([city isEqualToString:@"Lviv"] || [administrativeArea isEqualToString:@"Lviv Oblast"] || [city isEqualToString:@"Львів"] || [city isEqualToString:@"Львов"]){

                     [RNUser sharedInstance].realCity = @"6";

                 } else if ([city isEqualToString:@"Ivano-Frankivs'k"] || [administrativeArea isEqualToString:@"Ivano-Frankivsk Oblast"] || [city isEqualToString:@"Івано-Франківськ"] || [city isEqualToString:@"Ивано-Франковск"]){
                     [RNUser sharedInstance].realCity = @"8";
                     
                 } else if ([city isEqualToString:@"Kharkiv"] || [administrativeArea isEqualToString:@"Kharkiv Oblast"] || [city isEqualToString:@"Харків"] || [city isEqualToString:@"Харьков"]){
                     [RNUser sharedInstance].realCity = @"9";
                     [RNUser sharedInstance].currCity = @"9";

                 } else {
                     [RNUser sharedInstance].realCity = @"1234567";
                 }
             } else {

                 [RNUser sharedInstance].realCity = @"1234567";
             }
             
             if ([[RNUser sharedInstance].realCity isEqualToString:[RNUser sharedInstance].currCity]){

                 [RNUser sharedInstance].isThisCity = YES;
 
             } else {

                 [RNUser sharedInstance].isThisCity = NO;

             }

         }
         
     }];
    return [RNUser sharedInstance].currCity;
}


- (void)startLocationManager {
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestWhenInUseAuthorization];
}

- (void)loadData {
//    RNGetRestaurantRequest *request = [[RNGetRestaurantRequest alloc] init];
//    request.restaurantId = _restaurantId != nil ? _restaurantId : _orderInfo.restaurant.restaurantId;
    
//    NSString* langCode = [[NSString alloc] init];
//
//    if ([dp_get_current_language() isEqualToString:@"ru"]){
//        langCode = @"ru";
//    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
//        langCode = @"ua";
//    }
//    request.langCode = langCode;
//
//    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
//
//        if (error != nil || [response isKindOfClass:[NSNull class]])
//        {
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//            return ;
//
//        }
//        RNGetRestaurantResponse *r = (RNGetRestaurantResponse *)response;
//        _restaurant = r.restaurant;
    
    
         [_carousel reloadData];
//        [backRestImage removeFromSuperview];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"setupRestaurant");
            [self setupRestaurant];
        });

        if (isPhone && !isGetPDF){
            NSLog(@"isPhone && !isGetPDF");
            [menuPhoneCell.loadingIndicator startAnimating];
            [self performSelectorInBackground:@selector(getPDF) withObject:nil];
        } else if (!isPhone && !isGetPDF){
            NSLog(@"isPhone == NO  && !isGetPDF");
            [menuPadCell.loadingIndicator startAnimating];
            [self performSelectorInBackground:@selector(getPDF) withObject:nil];
        }
        
//    }];
}

-(void) setupRestaurant{
    
    
    
    NSString* type = [NSString stringWithFormat:@"%@    ",self.restCD.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    
    NSString *addressStr = [NSString stringWithFormat:@" %@",self.restCD.address];
    
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Україна, Київ, "
                                                       withString:@""];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Україна, Ивано-Франковск, "
                                                       withString:@""];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Україна, Івано-Франківськ, "
                                                       withString:@""];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Україна, Львов, "
                                                       withString:@""];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Україна, Львів, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:addressFontSize];
    NSLog(@"setupRestaurant");
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];

    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:addressFontSize];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];

    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [_restType setAttributedText:attributedString1];

    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 60;
    CGRect frame = CGRectMake(0, 0, width, 14);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = [NSString stringWithFormat:@"%@",attributedString1];
    myTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    
    
            CGSize result = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    
    
            CGFloat height = result.height;
            NSLog(@"height %f",height);
    
    NSString* onMap = DPLocalizedString(@"on_map", nil);
    NSString* arrowRightString = @"\uf0da";
    
    UIFont *onMapFont = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    NSMutableAttributedString *attributeMap =
    [[NSMutableAttributedString alloc] initWithString:onMap attributes:@{
                                                                         NSFontAttributeName : onMapFont}];
    UIFont *arrowFont = [UIFont fontWithName:@"FontAwesome" size:addressFontSize];
    NSMutableAttributedString *attributeMap1 =
    [[NSMutableAttributedString alloc] initWithString:arrowRightString attributes:@{NSFontAttributeName : arrowFont, NSForegroundColorAttributeName : mainColor}];
    
    [attributeMap appendAttributedString:attributeMap1];
    
    [_onMapButton setAttributedTitle:attributeMap forState:UIControlStateNormal];
    
    if (_fromMap == YES){
        _onMapButton.hidden = YES;
        _onMapButton.userInteractionEnabled = NO;
    }
    NSLog(@"setupRestaurant");
    if ([RNUser sharedInstance].isGeoEnabled && latitude != 0 && longitude != 0){
        CGFloat distancRest = [self getDistance];
        
        NSInteger numKM = (NSInteger)distancRest / 1000;
        
        NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
        
        NSString* strKM = @"";
        if (numKM != 0){
            strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
        }
        NSString* strMM = @"";
        if (numMM != 0){
            strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
        }
        NSString* distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
        
        _distanceRest.text = distanceRest;
        _restCD.distanceRest = distanceRest;
    } else {
        
        _distanceRest.text = @"";
        _restCD.distanceRest = @"";
        _distansIcon.hidden = YES;
    }
    
    NSTimeInterval utcoffset = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSTimeInterval interval = ([[NSDate date] timeIntervalSinceReferenceDate]+utcoffset)/(60.0*60.0*24.0);
    long dayix=((long)interval+8) % 7;
    
    if (dayix == 0){
        
        currDayNumber = 6;
        
    } else {
        currDayNumber = dayix - 1;
    }
    NSArray *timeWorkArr = (NSArray*)_restCD.workTimes;
    NSDictionary *dic = timeWorkArr[currDayNumber];
    //        NSLog(@"DAY_OF_WEEK %@",dic[@"DAY_OF_WEEK"]);
    NSString* strDay = [self getDay:[dic[@"DAY_OF_WEEK"] integerValue]];
    NSString* strTimeFrom = dic[@"TIME_FROM"];
    
    NSMutableArray* arrNoReservedDays = [NSMutableArray array];

    for (NSDictionary *dicDays in timeWorkArr){
        if ([dicDays[@"ON"] isEqualToString:@"0"]){
            NSString* strDate = dicDays[@"DATE"];
            if (![strDate isEqualToString:@"0000-00-00"]){
                [arrNoReservedDays addObject:strDate];
            }
        }
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStrNow = [dateFormatter stringFromDate:[NSDate date]];
    
    for (NSString* strDate in arrNoReservedDays){
        if ([strDate isEqualToString:dateStrNow]){
            isReservedDay = NO;
            break;
        }
    }
    
    
    strTimeFrom = [strTimeFrom substringToIndex:NSMaxRange([strTimeFrom rangeOfComposedCharacterSequenceAtIndex:4])];
    NSString* strTimeTo = dic[@"TIME_TO"];
    strTimeTo = [strTimeTo stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
    NSInteger timeToInt = strTimeTo.integerValue;
    
    if (timeToInt > 24) {
        timeToInt = timeToInt - 24;
    }
    NSString* strTimeToPrint = [[NSString alloc] init];
    if (timeToInt < 10) {
        strTimeToPrint = [NSString stringWithFormat:@"0%ld:00",(long)timeToInt];
    } else {
        strTimeToPrint = [NSString stringWithFormat:@"%ld:00",(long)timeToInt];
    }

    _dateLabel.text = [NSString stringWithFormat:@"%@",strDay];
    _dateNumber.text = [NSString stringWithFormat:@"%@ - %@",strTimeFrom, strTimeToPrint];
    _dateLabel.font = [UIFont fontWithName:@"Thonburi" size:restNameFontSize];
    _dateLabel.numberOfLines = 1;
    _dateLabel.textAlignment = NSTextAlignmentLeft;
    _dateLabel.textColor = [UIColor darkGrayColor];
    
    UIView* greenCircle = [[UIView alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH"];
    NSString *dateNow = [formatter stringFromDate:[NSDate date]];
    
    NSLog(@"dateNow.integerValue %ld > strTimeFrom.integerValue %ld",dateNow.integerValue, strTimeFrom.integerValue);
    
    if (isReservedDay){
        if (dateNow.integerValue >= strTimeFrom.integerValue && dateNow.integerValue < strTimeTo.integerValue){
            greenCircle.backgroundColor = mainColor;
            isGreen = YES;
        } else {
            greenCircle.backgroundColor = [UIColor redColor];
            isGreen = NO;
        }
    } else {
        greenCircle.backgroundColor = [UIColor redColor];
        isGreen = NO;
    }
    
    
    //        NSLog(@"dateNow %@ strTimeFrom %@ strTimeTo %@", dateNow,strTimeFrom, strTimeTo);
    greenCircle.layer.cornerRadius = 4;
    greenCircle.layer.masksToBounds = YES;
    [_dateView addSubview:greenCircle];
    
    greenCircle.translatesAutoresizingMaskIntoConstraints = false;
    
    [greenCircle.centerYAnchor constraintEqualToAnchor:_dateLabel.centerYAnchor constant:1].active = YES;
    [greenCircle.rightAnchor constraintEqualToAnchor:_dateLabel.leftAnchor constant:-3].active = YES;
    [greenCircle.widthAnchor constraintEqualToConstant:8].active = YES;
    [greenCircle.heightAnchor constraintEqualToConstant:8].active = YES;
    
    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    _commentsCount.text = [NSString stringWithFormat:@"%lld %@",_restCD.reviewCount, commStr];
    
    NSUInteger rating = (int)(self.restCD.rating*100);
    
    _ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    
    if (self.restCD.isFavorited == YES){
        
        [_likeBtn setImage:[UIImage imageNamed:@"favActive"]
                  forState:UIControlStateNormal];
    } else {
        
        [_likeBtn setImage:[UIImage imageNamed:@"favInactive"]
                  forState:UIControlStateNormal];
    }
    
    
    if (self.restCD.avgBill == 0) {
        _averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        _averageLabel.text = [NSString stringWithFormat:@"%d %@",self.restCD.avgBill, avgStr];
    }
    
    if (self.restCD.discounts == 0) {
        _discountBack.image = nil;
        _restDiscount.hidden = YES;
    } else {
        self.discountBack.hidden = NO;
        self.restDiscount.hidden = NO;
        
        _restDiscount.text = [NSString stringWithFormat:@"-%hd%%",self.restCD.discounts];
        _discountBack.image = [UIImage imageNamed:@"discont"];
    }
    //        NSLog(@"_restaurant.position %@", _restaurant.position);
    //        NSLog(@"menuFile %@", _restaurant.menuFile);
    //        NSLog(@"menuFileUrl %@", _restaurant.menuFileUrl);
    //        NSLog(@"menuFileType %@", _restaurant.menuFileType);
    
    if (self.restCD.isLiked == 1){
        
        [_addRatingButton setImage:[UIImage imageNamed:@"star"]
                          forState:UIControlStateNormal];
    } else {
        
        [_addRatingButton setImage:[UIImage imageNamed:@"ratInactive"]
                          forState:UIControlStateNormal];
    }
    
    NSString* likeStr = DPLocalizedString(@"likes", nil);
    _likeCount.text = [NSString stringWithFormat:@"%@: %lld",likeStr, self.restCD.likeCount];
//    if (self.restCD.discounts == 0) {
//        _discountBack.image = nil;
//        _restDiscount.hidden = YES;
//    } else {
//        _discountBack.hidden = NO;
//        _restDiscount.text = [NSString stringWithFormat:@"-%@%%",self.restCD.discounts];
//        _discountBack.hidden = NO;
//        _discountBack.image = [UIImage imageNamed:@"discont"];
//    }
    [_tableView reloadData];
    
    NSArray *images = (NSArray*)self.restCD.images;
    //    NSLog(@"CDRestaurant.images: %@",imagesArr.firstObject);
    NSString* photoStr = DPLocalizedString(@"photo", nil);
    NSString* imagesCount = [[NSString stringWithFormat:@"%zi %@",images.count, photoStr] stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    
    _imagesCountlabel.text = imagesCount;
    
    
    NSString* restName = [self.restCD.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    _restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
}


-(NSString*) getRating:(NSUInteger)rating{
    
    NSString* nameRating;
    
    if (rating == 0){
        nameRating = @"rating5_0";
        
    } else if (rating < 19){
        
        nameRating = @"rating5_1";
        
    } else if (rating >= 20 && rating < 40){
        
        nameRating = @"rating5_2";
        
    } else if (rating >= 40 && rating < 60){
        
        nameRating = @"rating5_3";
        
    } else if (rating >= 60 && rating < 80){
        
        nameRating = @"rating5_4";
        
    } else if (rating > 80){
        
        nameRating = @"rating5_5";
        
    } else {
        
        nameRating = @"rating5_0";
        
    }
//    NSLog(@"nameRating %@", nameRating);
    return nameRating;
}

- (void)configureTable {
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFromTop];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [animation setFillMode:kCAFillModeBoth];
    [animation setDuration:.7];
    [[_tableView layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    
    [_tableView registerCellWithReuseId:kInfoDescriptionCellId
                                 useXIB:YES];
    [_tableView registerCellWithReuseId:kCockCellId
                                 useXIB:YES];
    [_tableView registerCellWithReuseId:kCockPadCellId
                                 useXIB:YES];
    [_tableView registerCellWithReuseId:kCockMenuCellId
                                 useXIB:YES];
    [_tableView registerCellWithReuseId:kMenuPadCellId
                                 useXIB:YES];

}

-(void) favAddAction{
    if ([RNUser sharedInstance].token.length > 0) {
        
        RNRequest *request;
         NSNumber *restaurantId = [NSNumber numberWithInt:(int)[_restCD.restaurantId integerValue]];
        if (_restCD.isFavorited) {
            request = [[RNRemoveFromFavRequest alloc] init];
           
            ((RNRemoveFromFavRequest *)request).restaurantId = restaurantId;
        } else {
            request = [[RNAddToFavRequest alloc] init];
            ((RNAddToFavRequest *)request).restaurantId = restaurantId;
        }
        
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
            if (error != nil || [response isKindOfClass:[NSNull class]]){
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
                return;
            } else {
                if (!_restCD.isFavorited) {
                    _restCD.isFavorited = YES;
                    [_likeBtn setImage:[UIImage imageNamed:@"favActive"]
                              forState:UIControlStateNormal];
                    isAdded = YES;
                    [self setupPopView];
                    [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn
                                     animations:^{
                                         backView.alpha = 0.0;
                                         popView.alpha = 0.0;
                                     }
                                     completion:^(BOOL finished) {
                                         [backView removeFromSuperview];
                                         [popView removeFromSuperview];
                                     }];
                    
                } else {
                    _restCD.isFavorited = NO;
                    isAdded = NO;
                    [_likeBtn setImage:[UIImage imageNamed:@"favInactive"]
                              forState:UIControlStateNormal];
                    [self setupPopView];
                    [UIView animateWithDuration:1.0 delay:2.0 options:UIViewAnimationOptionCurveEaseIn
                                     animations:^{
                                         backView.alpha = 0.0;
                                         popView.alpha = 0.0;
                                     }
                                     completion:^(BOOL finished) {
                                         [backView removeFromSuperview];
                                         [popView removeFromSuperview];
                                     }];
                }
            }
        }];
    } else {
        
        [self showAlertWithTextForSuccessfully:DPLocalizedString(@"for_add_fav", nil) withText:DPLocalizedString(@"need_register", nil)];
    }

}


- (IBAction)favoritePressed:(id)sender {
    if ([self isInternetConnect]){
        [self favAddAction];
    }
}

//-(void) likePressedAction{
//
//     if ([RNUser sharedInstance].token.length > 0) {
//
//    if (_restaurant.isLiked.boolValue) {
//        return;
//    }
//    AddLikeRequest *request = [[AddLikeRequest alloc] init];
//    request.restaurantId = _restaurant.restaurantId;
//
//    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
//        AddLikeResponse* r = (AddLikeResponse *)response;
//        if (error != nil || [response isKindOfClass:[NSNull class]]){
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//            return;
//        } else if ([r.success isKindOfClass:[NSString class]] && [r.success isEqualToString:@"success"]){
//            [self loadData];
//            if (!_restaurant.isLiked.boolValue) {
//                NSString* restName = [_restaurant.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
//                restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
//                restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
//                restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
//                restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
//                [self showAlertWithAnimationForSuccessfully:restName withText:DPLocalizedString(@"success_added_like", nil)];
//                _restaurant.isLiked = @1;
//                [_addRatingButton setImage:[UIImage imageNamed:@"star"]
//                                  forState:UIControlStateNormal];
//            } else {
//                _restaurant.isLiked = @0;
//                [_addRatingButton setImage:[UIImage imageNamed:@"ratInactive"]
//                                  forState:UIControlStateNormal];
//            }
//
//        }
//    }];
//     } else {
//
//         [self showAlertWithAnimationForSuccessfully:DPLocalizedString(@"for_add_like", nil) withText:DPLocalizedString(@"need_register", nil)];
//
//     }
//
//
//}

- (IBAction)likePressed:(id)sender {
    if ([self isInternetConnect]){
    
//        [self likePressedAction];
    }
}

- (IBAction)timeButton:(id)sender {
    
    [self setupTimeView];
}

-(void) timeBack:(id) sender{
    [backTimeView removeFromSuperview];
    _arrowDown.alpha = 1;
}

- (IBAction)phoneButton:(id)sender {
    
    [self setupPhoneView];
}

-(void) callButton1Action:(id) sender{
    
    NSString* telephone = @"+380509003493";
    
//    [Telprompt callWithString:telephone];
    
    
//    NSLog(@"telephone: %@ ****************************************", telephone);
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",telephone]];
    
    UIApplication *application = [UIApplication sharedApplication];
   
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:phoneUrl options:@{}
           completionHandler:^(BOOL success) {
               if (success){
                   NSLog(@"Ok success");
               } else {
                   
//                    [self showAlertWithTextForSuccessfully:@"Звонок" withText:@"в данное время не возможен!!!"];
               }
           }];
    } else {
        if ( [application openURL:phoneUrl]) {
             [application openURL:phoneUrl];
        } else
        {
//            [self showAlertWithTextForSuccessfully:@"Звонок" withText:@"в данное время не возможен!!!"];
        }

    }
}


-(void) callButton2Action:(id) sender{
    
    NSString* telephone = @"+380443844934";
//    NSLog(@"telephone: %@ ****************************************", telephone);
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",telephone]];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:phoneUrl options:@{}
           completionHandler:^(BOOL success) {
               if (success){
//                   NSLog(@"Ok success");
               } else {
                   
//                   [self showAlertWithTextForSuccessfully:@"Звонок" withText:@"в данное время не возможен!!!"];
               }
           }];
    } else {
        if ( [application openURL:phoneUrl]) {
            [application openURL:phoneUrl];
        } else
        {
//            [self showAlertWithTextForSuccessfully:@"Звонок" withText:@"в данное время не возможен!!!"];
        }
        
    }
}


//- (IBAction)segmentedIndexChanged:(UISegmentedControl *)sender {
//if ([self isInternetConnect]){
//    switch (sender.selectedSegmentIndex) {
//        case 0:
//            
//            [self showComments];
//
//            break;
//        case 1:
//            
//            [self showNews];
//
//            break;
//        case 2:
//            
//            [self showActions];
//            
//            break;
//            
//        default:
//            break;
//    }
//}
//}
- (IBAction)offersButtonAction:(id)sender {
    [self showActions];
}

-(void) showActions{

    OffersExpViewController * offersViewController = [storyboard instantiateViewControllerWithIdentifier:@"OffersExpViewController"];

    offersViewController.restaurantID = _restCD.restaurantId;

    NSString* title = [NSString stringWithFormat:@"%@",_restCD.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    offersViewController.title = title;
    offersViewController.restaurant = _restCD;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:offersViewController];

    [self presentViewController:navVC animated:YES completion:nil];

}
- (IBAction)feedsButtonAction:(id)sender {
    [self showComments];
}

-(void) showComments{

    CommentsViewController * newsVC = [storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];

    newsVC.restaurantID = _restCD.restaurantId;
    newsVC.isFirstTimeOpen = isFirstCommentOpen;

    NSString* title = [NSString stringWithFormat:@"%@",_restCD.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    newsVC.title = title;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:newsVC];

    [self presentViewController:navVC animated:YES completion:nil];
}
- (IBAction)newsButtonAction:(id)sender {
    [self showNews];
}

-(void) showNews{

    NewsExpandViewController * newsVC = [storyboard instantiateViewControllerWithIdentifier:@"NewsExpandViewController"];

    newsVC.restaurantID = _restCD.restaurantId;

    NSString* title = [NSString stringWithFormat:@"%@",_restCD.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    newsVC.title = title;
    newsVC.restaurant = _restCD;
    newsVC.isAllRests = NO;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:newsVC];

    [self presentViewController:navVC animated:YES completion:nil];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat heightRow = estimateHeight;
    
    if (indexPath.row == 0) {
        heightRow = [self tableView:tableView heightForDescriptionRowAtIndexPath:indexPath];
    }
    if (indexPath.row == 1) {
        
        heightRow = UITableViewAutomaticDimension;

//        heightRow = [self tableView:tableView heightForCockRowAtIndexPath:indexPath];
    }
    
    if (indexPath.row == 2) {
        if (!menuResized){
            heightRow = 76;
        } else {
            heightRow = [self tableView:tableView heightForCockMenuRowAtIndexPath:indexPath];
        }
    }
    return heightRow;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForCockRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    NSString* kitchenRest = [_restaurant.kitchen componentsJoinedByString:@","];
//    NSString* optionRest = [_restaurant.options componentsJoinedByString:@", "];
//    optionRest = [optionRest stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
//    
//    heightForCockRow = [CockCell sizeForKitchen:kitchenRest
//                                  andOptions:optionRest];
//    
//    
//    return heightForCockRow;
//}


- (CGFloat)tableView:(UITableView *)tableView heightForDescriptionRowAtIndexPath:(NSIndexPath *)indexPath {
    
    heightForDescriptionRow = [RNInfoDescriptionCell sizeForText:_restCD.descriptionRestaurant
                                                            shouldResize:descriptionResized];
    
    
    return heightForDescriptionRow;
}

- (CGFloat)tableView:(UITableView *)tableView heightForCockMenuRowAtIndexPath:(NSIndexPath *)indexPath {
    
    heightForCockMenuRow = [CockMenuCell sizeForText:_restCD.descriptionRestaurant
                                                    shouldResize:menuResized];
    
    
    return heightForCockMenuRow + 50;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = cell.contentView.backgroundColor;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        RNInfoDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:kInfoDescriptionCellId];
         NSString* descriptionRest = [_restCD.descriptionRestaurant stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"  " withString:@"\n     "];
//        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\n\n     "];
        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
//        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"дров&#039;яній" withString:@"дров'яній"];
        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
        
        descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
         descriptionRest = [descriptionRest stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
        cell.titleLabel.text = [descriptionRest stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        [cell.disclosureBtn addTarget:self
                               action:@selector(descriptionDisclose:)
                     forControlEvents:UIControlEventTouchUpInside];
        
        NSString* wrapStr = DPLocalizedString(@"wrap", nil);
        NSString* expandStr = DPLocalizedString(@"expand", nil);
        
        NSString *titleButton = descriptionResized ? wrapStr : expandStr;
        [cell.disclosureBtn setTitle:titleButton
                            forState:UIControlStateNormal];
        [cell.disclosureBtn setTitleColor:mainColor forState:UIControlStateNormal];
        
        return cell;
    }
    if (indexPath.row == 1) {
        
        NSArray *kitchensArr = (NSArray*)_restCD.kitchen;
        NSArray *optionsArr = (NSArray*)_restCD.options;
        
        if (isPhone){
            CockCell *cell = [tableView dequeueReusableCellWithIdentifier:kCockCellId];
            
            cell.cockLabel.text = [kitchensArr componentsJoinedByString:@","];
            NSString* optionRest = [optionsArr componentsJoinedByString:@", "];
            optionRest = [optionRest stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
            cell.optionLabel.text = optionRest;
            return cell;
        } else {
            CockPadCell *cell = [tableView dequeueReusableCellWithIdentifier:kCockPadCellId];
            
            
            cell.cockLabel.text = [kitchensArr componentsJoinedByString:@","];
            NSString* optionRest = [optionsArr componentsJoinedByString:@", "];
            optionRest = [optionRest stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
            cell.optionLabel.text = optionRest;
            return cell;
        }
    
    }
    if (indexPath.row == 2) {
        
         if (isPhone){
    menuPhoneCell = [tableView dequeueReusableCellWithIdentifier:kCockMenuCellId];

        [menuPhoneCell.menuButton addTarget:self
                               action:@selector(menuPDFOpen:)
                     forControlEvents:UIControlEventTouchUpInside];
             
             if (isGetPDF == YES){
                 
                 menuPhoneCell.menuButton.enabled = YES;
                 menuPhoneCell.menuButton.hidden = NO;
                 [menuPhoneCell.loadingIndicator stopAnimating];
                 
             } else {
             
                 menuPhoneCell.menuButton.enabled = NO;
                 menuPhoneCell.menuButton.hidden = YES;
                 
             }
    
             return menuPhoneCell;
         } else {
             
             menuPadCell = [tableView dequeueReusableCellWithIdentifier:kMenuPadCellId];
             
             [menuPadCell.menuButton addTarget:self
                                 action:@selector(menuPDFOpen:)
                       forControlEvents:UIControlEventTouchUpInside];
             if (isGetPDF == YES){
                 NSLog(@"isGetPDF == YES");
                 
                 menuPadCell.menuButton.enabled = YES;
                 menuPadCell.menuButton.hidden = NO;
                 [menuPhoneCell.loadingIndicator stopAnimating];
                 
             } else {
                 NSLog(@"isGetPDF == NO");
                 menuPadCell.menuButton.enabled = NO;
                 menuPadCell.menuButton.hidden = YES;
                 
             }
             
             return menuPadCell;
         }
    }
    
     return UITableViewCell.new;
}

- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CarouselCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier:@"CarouselCell" forIndexPath:indexPath];
    
    headerView.backgroundColor = [UIColor clearColor];
    
    _carousel.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width * 0.54);
    
   
    _carousel.delegate = self;
    _carousel.dataSource = self;
    [headerView addSubview:_carousel];
    return headerView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    NSUInteger contY = (NSUInteger)(scrollView.contentOffset.y);
//    NSLog(@"scrollView.contentOffset.y %ld",(unsigned long)contY);
    
    NSUInteger height = (NSUInteger)(_tableView.contentSize.height - _tableView.bounds.size.height);
//    NSLog(@"height %ld",(unsigned long)height);
    if (descriptionResized){
        height = height;
    }
    
    _arrowDownBottom.transform = CGAffineTransformMakeRotation(0);
    
    if (contY == height){
    
        [UIView animateWithDuration:.5 animations:^{
            _arrowDownBottom.transform = CGAffineTransformMakeRotation(-M_PI+0.00);
        }];
//         NSLog(@"At the top");
    } else {
        
        [UIView animateWithDuration:.5 animations:^{
            _arrowDownBottom.transform = CGAffineTransformIdentity;
        }];
    }
}


- (void)descriptionDisclose:(UIButton *)sender {
    descriptionResized = !descriptionResized;
    [_tableView reloadData];
}

-(void) getPDF{
    
    NSString* urlStr;

    if ([self isInternetConnect]){


//        NSLog(@"_restaurant.menuFileUrl %@",_restaurant.menuFileUrl);
        if ([_restCD.menuFileType isEqualToString:@"url"] && [NSString stringWithFormat:@"%@",_restCD.menuFileUrl].length > 0) {
            urlStr = [NSString stringWithFormat:@"%@",_restCD.menuFileUrl];

//            [self openPDFWithURL:urlStr];
        } else if ([_restCD.menuFileType isEqualToString:@"file"] && [NSString stringWithFormat:@"%@",_restCD.menuFile].length > 0){

            urlStr = [NSString stringWithFormat:@"%@",_restCD.menuFile];

//            [self openPDFWithURL:urlStr];
        }
    }

    NSURL* pdfURL = [NSURL URLWithString:urlStr];

    NSData *pdfData = [[NSData alloc] initWithContentsOfURL:pdfURL];

    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef) pdfData);

    pdfDocument = CGPDFDocumentCreateWithProvider(dataProvider);
    pageCount = CGPDFDocumentGetNumberOfPages(pdfDocument);
//    NSLog(@"pdfDocument %@",pdfDocument);
//
//    NSLog(@"pageCount %ld",pageCount);

    dispatch_async(dispatch_get_main_queue(), ^{



        if (isPhone){

            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
//                                 menuPhoneCell.loadingIndicator.hidden = YES;
                                 menuPhoneCell.menuButton.enabled = YES;
                                 menuPhoneCell.menuButton.hidden = NO;
                             }
                             completion:^(BOOL finished) {
                                 isGetPDF = YES;
                                 [menuPhoneCell.loadingIndicator stopAnimating];
                                 [menuPhoneCell.loadingIndicator removeFromSuperview];

                             }];

        } else if (isPhone == NO){

            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
//                                 menuPadCell.loadingIndicator.hidden = YES;
                                 menuPadCell.menuButton.enabled = YES;
                                 menuPadCell.menuButton.hidden = NO;
                             }
                             completion:^(BOOL finished) {
                                 isGetPDF = YES;
                                 [menuPadCell.loadingIndicator stopAnimating];
                                 [menuPadCell.loadingIndicator removeFromSuperview];
                             }];
        }
    });
}

-(void) openPDFWithDoc{
    
    PDFViewController * pdfViewController = [storyboard instantiateViewControllerWithIdentifier:@"PDFViewController"];

//    NSURL* pdfURL = [NSURL URLWithString:urlStr];

    pdfViewController.pdfDocument = pdfDocument;
    pdfViewController.pageCount = pageCount;

    NSString* title = [NSString stringWithFormat:@"%@",_restCD.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    pdfViewController.titleNav = title;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:pdfViewController];

    [self presentViewController:navVC animated:YES completion:nil];
}


-(void) openPDFWithURL:(NSString*)urlStr{

    PDFViewController * pdfViewController = [storyboard instantiateViewControllerWithIdentifier:@"PDFViewController"];

    NSURL* pdfURL = [NSURL URLWithString:urlStr];

    pdfViewController.menuPDF = pdfURL;

    NSString* title = [NSString stringWithFormat:@"%@",_restCD.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    pdfViewController.titleNav = title;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:pdfViewController];

    [self presentViewController:navVC animated:YES completion:nil];
}


- (void)menuPDFOpen:(UIButton *)sender {
    if (pdfDocument != nil){
        [self openPDFWithDoc];
    }
    if ([self isInternetConnect]){
    NSString* urlStr;

    NSLog(@"_restaurant.menuFileUrl %@",_restCD.menuFileUrl);
    if ([_restCD.menuFileType isEqualToString:@"url"] && [NSString stringWithFormat:@"%@",_restCD.menuFileUrl].length > 0) {
        urlStr = [NSString stringWithFormat:@"%@",_restCD.menuFileUrl];

        [self openPDFWithURL:urlStr];
    } else if ([_restCD.menuFileType isEqualToString:@"file"] && [NSString stringWithFormat:@"%@",_restCD.menuFile].length > 0){

        urlStr = [NSString stringWithFormat:@"%@",_restCD.menuFile];

        [self openPDFWithURL:urlStr];
    }
  }
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForNewRowAtIndexPath:(NSIndexPath *)indexPath {
//    RNNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kNewsTableViewCellId];
//    RNNEw *n = _news[indexPath.row];
//    [cell applyNew:n];
//    +7928 4316952
//    return cell;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForReviewRowAtIndexPath:(NSIndexPath *)indexPath {
//    RNInfoReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kInfoReviewTableViewCellId];
//    RNFeedback *fb = _reviews[indexPath.row];
//    [cell applyReview:fb];
//    return cell;
//}



- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTextForSuccessfully:(NSString *) success withText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:success
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithAnimationForSuccessfully:(NSString *) success withText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:success
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:^{
        [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             alert.view.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             [alert dismissViewControllerAnimated:YES completion:^{
                             }];
                         }];
    }];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)reservedButton:(id)sender {

    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
//    reservTableViewController.restourant = _restaurant;

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}
- (IBAction)callAction:(id)sender {
}
//
//- (IBAction)menuButton:(id)sender {
//    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    PDFViewController * pdfViewController = [storyboard instantiateViewControllerWithIdentifier:@"PDFViewController"];
//    
//    NSString* urlStr = [NSString stringWithFormat:@"%@/%@",_restaurant.menuFileUrl,_restaurant.menuFile];
//    
//    NSURL* pdfURL = [NSURL URLWithString:urlStr];
//    
//    pdfViewController.menuPDF = pdfURL;
//    
//    [self presentViewController:pdfViewController animated:YES completion:nil];
//
//}

#pragma mark - iCarousel

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    NSArray *images = (NSArray*)_restCD.images;
    NSInteger count = [images count];
    
    NSLog(@"count %ld",(long)count);
    return count > 0 ? count : 1;
}

- (UIView *)carousel:(iCarousel *)carousel
  viewForItemAtIndex:(NSInteger)index
         reusingView:(UIView *)view {
    UIImageView *imageView = (UIImageView *)view;
    if (imageView == nil) {
        imageView = [[UIImageView alloc] initWithFrame:_carousel.frame];
    }
    
    UISwipeGestureRecognizer* recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:forIndex:)];
    [recognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    [view addGestureRecognizer:recognizer];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    NSArray *images = (NSArray*)_restCD.images;
    if (images.count == 0) {
        [UIImage cachedImage:_restCD.logo
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    imageView.image = image;
                }];
        return imageView;
    } else {
        
        [UIImage cachedImage:images[index]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    imageView.image = image;

                }];
        
        UISwipeGestureRecognizer* recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:forIndex:)];
        [recognizer setDirection:UISwipeGestureRecognizerDirectionDown];
        [imageView addGestureRecognizer:recognizer];
        return imageView;
    }
}

- (UIView *)carousel:(iCarousel *)carousel
placeholderViewAtIndex:(NSInteger)index
         reusingView:(UIView *)view {
    UIImageView *imageView = (UIImageView *)view;
    if (imageView == nil) {
        imageView = [[UIImageView alloc] initWithFrame:_carousel.frame];
    }
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    NSArray *images = (NSArray*)_restCD.images;
    if (images.count == 0) {
        [UIImage cachedImage:_restCD.logo
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    imageView.image = image;
                }];
        return imageView;
    } else {
        
        [UIImage cachedImage:images[index]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    imageView.image = image;
                }];
        return imageView;
    }
}

- (CATransform3D)carousel:(iCarousel *)carousel
   itemTransformForOffset:(CGFloat)offset
            baseTransform:(CATransform3D)transform {
    transform = CATransform3DRotate(transform, M_PI / 6.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel {
    return _carousel.frame.size.width;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{

        FullScreenViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"FullScreenViewController"];
        NSArray *images = (NSArray*)_restCD.images;
        VC.images = images;
        VC.selectIndex = index;
  
        [self.navigationController presentViewController:VC animated:YES completion:^{}];
}

-(void)handleSwipe:(UISwipeGestureRecognizer *) sender forIndex:(NSInteger)index
{
    
//    NSLog(@"handleSwipe");
    if (sender.direction == UISwipeGestureRecognizerDirectionDown)
    {
        
//         NSLog(@"handleSwipe UISwipeGestureRecognizerDirectionDown");
        FullScreenViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"FullScreenViewController"];
        NSArray *images = (NSArray*)_restCD.images;
        VC.images = images;
        VC.selectIndex = index;
        
        [self.navigationController presentViewController:VC animated:YES completion:^{}];
    }
    else //if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        //do something
    }
}

-(void)handleSwipe:(UISwipeGestureRecognizer *) sender
{
    
//    NSLog(@"handleSwipe");
    if (sender.direction == UISwipeGestureRecognizerDirectionDown)
    {
        
//        NSLog(@"handleSwipe UISwipeGestureRecognizerDirectionDown");
        FullScreenViewController *VC = [storyboard instantiateViewControllerWithIdentifier:@"FullScreenViewController"];
        NSArray *images = (NSArray*)_restCD.images;
        VC.images = images;
        VC.selectIndex = 1;
        
        [self.navigationController presentViewController:VC animated:YES completion:^{}];
    }
    else //if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        //do something
    }
}


- (void) backButton:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(NSString*) getType:(NSInteger)type{
    
    NSString* typeRest;
    
    switch (type) {
        case 1:
            typeRest = @"Ресторан";
            break;
        case 2:
            typeRest = @"Кафе";
            break;
        case 3:
            typeRest = @"Бар";
            break;
        case 4:
            typeRest = @"Кафе";
            break;
        case 6:
            typeRest = @"Паб";
            break;
        case 15:
            typeRest = @"Лаунж-Паб";
            break;
        case 19:
            typeRest = @"Пив-Бар";
            break;
        case 21:
            typeRest = @"Загородный";
            break;
        case 22:
            typeRest = @"Комплекс";
            break;
        case 23:
            typeRest = @"Бургер-Паб";
            break;
        case 25:
            typeRest = @"Кондитерский";
            break;
        case 34:
            typeRest = @"Кальян-Бар";
            break;
        case 38:
            typeRest = @"Ресто-Клаб";
            break;
        case 42:
            typeRest = @"Гриль-Бар";
            break;
        case 43:
            typeRest = @"Чача-Бар";
            break;
        case 45:
            typeRest = @"Ресто-Бар";
            break;
            
        default:
            break;
    }
//    NSLog(@"typeRest %@", typeRest);
    return typeRest;
}

- (void)setupBackGeoView {
    
    backGeoView = [[UIView alloc]initWithFrame:self.view.frame];
    backGeoView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backGeoView];
}

//-(void) setupBackImage{
//    backRestImage = [[UIImageView alloc] init];
//    backRestImage.image = self.backImage;
//    backRestImage.contentMode = UIViewContentModeScaleAspectFill;
//    [self.carousel addSubview:backRestImage];
//
//    backRestImage.translatesAutoresizingMaskIntoConstraints = false;
//
//    [backRestImage.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
//    [backRestImage.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
//    [backRestImage.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
//    [backRestImage.heightAnchor constraintEqualToConstant:screenWidth * 247/374].active = YES;
//}



- (void)setupView {

NSString* titleReservButton = DPLocalizedString(@"reserv_action", nil);
    [self.resrvedButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.resrvedButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleReservButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.resrvedButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.resrvedButton.layer.cornerRadius = cornerRadius;
    self.resrvedButton.layer.masksToBounds = true;
    
    NSString* titleFeedsButton = DPLocalizedString(@"comments_segment", nil);
    [self.feedsButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.feedsButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strFeeds = [[NSAttributedString alloc] initWithString:titleFeedsButton attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    [self.feedsButton setAttributedTitle:strFeeds forState:UIControlStateNormal];
    
    NSString* titleNewsButton = DPLocalizedString(@"news_segment", nil);
    [self.newsButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.newsButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strNews = [[NSAttributedString alloc] initWithString:titleNewsButton attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    [self.newsButton setAttributedTitle:strNews forState:UIControlStateNormal];
    
    NSString* titleOffersButton = DPLocalizedString(@"actions_segment", nil);
    [self.offersButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.offersButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strOffers = [[NSAttributedString alloc] initWithString:titleOffersButton attributes:@{ NSForegroundColorAttributeName : [UIColor darkGrayColor]}];
    [self.offersButton setAttributedTitle:strOffers forState:UIControlStateNormal];
}

-(void) setupFeddsBadgeWithTag:(NSInteger)tag{
    
    NSString* fakeStr;
    CGFloat centerXFake = 0;
    NSInteger count = 0;
    
    if (tag == 0){
        fakeStr = DPLocalizedString(@"comments_segment", nil);
        centerXFake = screenWidth/6*1;
        count = feedsCount;
    } else if (tag == 1){
        fakeStr = DPLocalizedString(@"news_segment", nil);
        centerXFake = screenWidth/6*3;
        count = newsCount;
    } else if (tag == 2){
        fakeStr = DPLocalizedString(@"actions_segment", nil);
        centerXFake = screenWidth/6*5;
        count = offersCount;
    }

    UILabel* feedsFake = [[UILabel alloc] init];
    [_segmentControl addSubview:feedsFake];
    feedsFake.backgroundColor = [UIColor clearColor];
    feedsFake.text = fakeStr;
    feedsFake.textColor = [UIColor clearColor];
    if (screenWidth < 322) {
        feedsFake.font = [UIFont fontWithName:@"Thonburi" size:11];
    } else {
        
        feedsFake.font = [UIFont fontWithName:@"Thonburi" size:restNameFontSize];
    }
   
    feedsFake.textAlignment = NSTextAlignmentCenter;
    [feedsFake sizeToFit];
    feedsFake.translatesAutoresizingMaskIntoConstraints = false;
    [feedsFake.centerYAnchor constraintEqualToAnchor:_segmentControl.centerYAnchor].active = YES;
    [feedsFake.centerXAnchor constraintEqualToAnchor:_segmentControl.leftAnchor constant:centerXFake].active = YES;
    [feedsFake.heightAnchor constraintEqualToConstant:20].active = YES;
  
    
    UIView* feedsBagde = [[UIView alloc] init];
    [feedsFake addSubview:feedsBagde];
    if (count != 0){
        
        feedsBagde.backgroundColor = mainColor;
    } else if (count == 0){
        
        feedsBagde.backgroundColor = [UIColor colorWithHexString:@"#D1D1D1"];
    }
 
    feedsBagde.layer.cornerRadius = 10;
    feedsBagde.layer.masksToBounds = YES;

    UILabel* feedsCountL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [feedsBagde addSubview:feedsCountL];
    feedsCountL.backgroundColor = [UIColor clearColor];
    feedsCountL.text = [NSString stringWithFormat:@"%ld",(long)count];
    feedsCountL.textColor = [UIColor whiteColor];
    feedsCountL.font = [UIFont fontWithName:@"Thonburi" size:8];
    feedsCountL.textAlignment = NSTextAlignmentCenter;
    
    feedsBagde.translatesAutoresizingMaskIntoConstraints = false;
    [feedsBagde.centerXAnchor constraintEqualToAnchor:feedsFake.rightAnchor constant:2].active = YES;
    [feedsBagde.centerYAnchor constraintEqualToAnchor:feedsFake.topAnchor].active = YES;
    [feedsBagde.heightAnchor constraintEqualToConstant:20].active = YES;
    [feedsBagde.widthAnchor constraintEqualToConstant:20].active = YES;
}

-(void) setupFeddsBadgeForButton:(UIButton*)button{
    
    NSString* fakeStr;
//    CGFloat centerXFake = 0;
    NSInteger count = 0;
    
    if (button.tag == 1){
        fakeStr = DPLocalizedString(@"comments_segment", nil);
//        centerXFake = screenWidth/6*1;
        count = feedsCount;
    } else if (button.tag == 2){
        fakeStr = DPLocalizedString(@"news_segment", nil);
//        centerXFake = screenWidth/6*3;
        count = newsCount;
    } else if (button.tag == 3){
        fakeStr = DPLocalizedString(@"actions_segment", nil);
//        centerXFake = screenWidth/6*5;
        count = offersCount;
    }
    
    UILabel* feedsFake = [[UILabel alloc] init];
    [button addSubview:feedsFake];
    feedsFake.backgroundColor = [UIColor clearColor];
    feedsFake.text = fakeStr;
    feedsFake.textColor = [UIColor clearColor];
    if (screenWidth < 322) {
        feedsFake.font = [UIFont fontWithName:@"Thonburi" size:buttonFontSize];
    } else {
        
        feedsFake.font = [UIFont fontWithName:@"Thonburi" size:buttonFontSize];
    }
    
    feedsFake.textAlignment = NSTextAlignmentCenter;
    [feedsFake sizeToFit];
    feedsFake.translatesAutoresizingMaskIntoConstraints = false;
    [feedsFake.centerYAnchor constraintEqualToAnchor:button.centerYAnchor].active = YES;
    [feedsFake.centerXAnchor constraintEqualToAnchor:button.centerXAnchor].active = YES;
    [feedsFake.heightAnchor constraintEqualToConstant:buttonFontSize].active = YES;
    
    
    UIView* feedsBagde = [[UIView alloc] init];
    [feedsFake addSubview:feedsBagde];
    if (count != 0){
        
        feedsBagde.backgroundColor = mainColor;
    } else if (count == 0){
        
        feedsBagde.backgroundColor = [UIColor colorWithHexString:@"#D1D1D1"];
    }
    
    feedsBagde.layer.cornerRadius = 10;
    feedsBagde.layer.masksToBounds = YES;

    UILabel* feedsCountL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [feedsBagde addSubview:feedsCountL];
    feedsCountL.backgroundColor = [UIColor clearColor];
    feedsCountL.text = [NSString stringWithFormat:@"%ld",(long)count];
    feedsCountL.textColor = [UIColor whiteColor];
    feedsCountL.font = [UIFont fontWithName:@"Thonburi" size:8];
    feedsCountL.textAlignment = NSTextAlignmentCenter;
    
    feedsBagde.translatesAutoresizingMaskIntoConstraints = false;
    [feedsBagde.centerXAnchor constraintEqualToAnchor:feedsFake.rightAnchor constant:3].active = YES;
    [feedsBagde.centerYAnchor constraintEqualToAnchor:feedsFake.topAnchor constant:-2].active = YES;
    [feedsBagde.heightAnchor constraintEqualToConstant:20].active = YES;
    [feedsBagde.widthAnchor constraintEqualToConstant:20].active = YES;
}



- (void)loadFeedbacks:(NSNumber *)ident {
    RNGetFeedbacksRequest *request = [[RNGetFeedbacksRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            return;
        }
        RNGetFeedbacksResponse *r = (RNGetFeedbacksResponse *)response;
//        NSLog(@"r.feedbacks.count %lu",(unsigned long)r.feedbacks.count);

            feedsCount = r.feedbacks.count;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setupFeddsBadgeForButton:self.feedsButton];
            self.feedsButton.userInteractionEnabled = YES;
        });
    }];
}

- (void)loadNews:(NSNumber *)ident {
    RNGetNewsRequest *request = [[RNGetNewsRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        RNGetNewsResponse *r = (RNGetNewsResponse *)response;
        
        if (r.news.count > 0){
            
            newsCount = r.news.count;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupFeddsBadgeForButton:self.newsButton];
                self.feedsButton.userInteractionEnabled = YES;
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupFeddsBadgeForButton:self.newsButton];
                self.feedsButton.userInteractionEnabled = NO;
            });
        }
    }];
}

- (void)loadOffers:(NSNumber *)ident {
    GetOffersRequest *request = [[GetOffersRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
       if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
           return ;
        }
        GetOffersResponse *r = (GetOffersResponse *)response;
//        NSLog(@"r.offers.count: %lu", (unsigned long)r.offers.count);
        if (r.offers.count > 0){

            offersCount = r.offers.count;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupFeddsBadgeForButton:self.offersButton];
                self.feedsButton.userInteractionEnabled = YES;
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupFeddsBadgeForButton:self.offersButton];
                self.feedsButton.userInteractionEnabled = NO;
            });
        }
    }];
}


- (void)setupPopView {
    
    backView = [[UIView alloc]init];
    backView.backgroundColor = [UIColor whiteColor];
    backView.alpha = 0.5f;
    [self.view addSubview:backView];
    
    popView = [[UIView alloc]init];
    popView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:popView];
    
    UIImageView* popImageView = [[UIImageView alloc] init];
    [popView addSubview:popImageView];
    
    UILabel* popLabel = [[UILabel alloc] init];
    
    if (screenWidth < 322) {
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:14.0];
    } else {
        
        popLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:18.0];
    }
    
    popLabel.numberOfLines = 2;
    popLabel.textAlignment = NSTextAlignmentCenter;
    popLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [popView addSubview:popLabel];
    
    if (isAdded){
        popLabel.text = DPLocalizedString(@"add_fav", nil);
        popImageView.image = [UIImage imageNamed:@"heartBig"];
    } else {
        popLabel.text = DPLocalizedString(@"remove_fav", nil);
        popImageView.image = [UIImage imageNamed:@"iconFav"];
    }
    
    backView.translatesAutoresizingMaskIntoConstraints = false;
    popView.translatesAutoresizingMaskIntoConstraints = false;
    popImageView.translatesAutoresizingMaskIntoConstraints = false;
    popLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [backView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [backView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [backView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [backView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    
    [popView.centerXAnchor constraintEqualToAnchor:backView.centerXAnchor].active = YES;
    [popView.centerYAnchor constraintEqualToAnchor:backView.centerYAnchor].active = YES;
    [popView.widthAnchor constraintEqualToConstant:screenWidth * 0.33].active = YES;
    [popView.heightAnchor constraintEqualToConstant:screenWidth * 0.44].active = YES;
    
    [popImageView.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popImageView.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popImageView.topAnchor constraintEqualToAnchor:popView.topAnchor].active = YES;
    [popImageView.heightAnchor constraintEqualToConstant:screenWidth * 0.29].active = YES;
    
    [popLabel.leftAnchor constraintEqualToAnchor:popView.leftAnchor].active = YES;
    [popLabel.rightAnchor constraintEqualToAnchor:popView.rightAnchor].active = YES;
    [popLabel.topAnchor constraintEqualToAnchor:popImageView.bottomAnchor].active = YES;
    [popLabel.bottomAnchor constraintEqualToAnchor:popView.bottomAnchor].active = YES;
}

- (void)setupTimeView {
    
    _arrowDown.alpha = 0;
    
    backTimeView = [[UIView alloc]initWithFrame:self.view.frame];
    backTimeView.backgroundColor = [UIColor clearColor];
 
    [self.view addSubview:backTimeView];
    
    
    timeView = [[UIView alloc]init];
    timeView.backgroundColor = [UIColor whiteColor];
    
    [timeView.layer setShadowColor: [UIColor grayColor].CGColor];
    [timeView.layer setShadowOpacity:0.8];
    [timeView.layer setShadowRadius:3.0];
    [timeView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backTimeView addSubview:timeView];
    
    
    CGFloat heightRow = 21.0;
    CGFloat widthRow = 116.0;
    CGFloat fontSize = 15;
    CGFloat heightTimeV = 147;
    CGFloat widthTimeView = 228;
    CGFloat leftMargin = 15;
    
    if (!isPhone) {
 
        heightRow = 38.0;
        widthRow = 170.0;
        fontSize = 22;
        heightTimeV = 266;
        widthTimeView = 350;
        leftMargin = 22;
    }
NSArray *workTimes = (NSArray*)_restCD.workTimes;
    for (int i = 0; i < workTimes.count && i < 7; i++) {
        
        
        UILabel* dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 0 + heightRow*i, widthRow, heightRow)];
        dayLabel.font = [UIFont fontWithName:@"Thonburi" size:fontSize];
        dayLabel.numberOfLines = 1;
        dayLabel.textAlignment = NSTextAlignmentLeft;
        dayLabel.textColor = [UIColor darkGrayColor];
        [timeView addSubview:dayLabel];
        
        UILabel* timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(widthRow, 0 + heightRow*i, widthRow, heightRow)];
        timeLabel.font = [UIFont fontWithName:@"Thonburi" size:fontSize];
        timeLabel.numberOfLines = 1;
        timeLabel.textAlignment = NSTextAlignmentLeft;
        timeLabel.textColor = [UIColor darkGrayColor];
        [timeView addSubview:timeLabel];
        
        NSDictionary *dic = workTimes[i];
        NSString* strDay = [self getDay:[dic[@"DAY_OF_WEEK"] integerValue]];
        NSString* strTimeFrom = dic[@"TIME_FROM"];
        NSLog(@"strTimeFrom 1: %@", strTimeFrom);
        strTimeFrom = [strTimeFrom substringToIndex:NSMaxRange([strTimeFrom rangeOfComposedCharacterSequenceAtIndex:4])];
        NSLog(@"strTimeFrom 2: %@", strTimeFrom);
        NSString* strTimeTo = dic[@"TIME_TO"];
        strTimeTo = [strTimeTo stringByReplacingOccurrencesOfString:@":00:00" withString:@""];
        NSInteger timeToInt = strTimeTo.integerValue;
        if (timeToInt > 24) {
            timeToInt = timeToInt - 24;
        }
        NSString* strTimeToPrint = [[NSString alloc] init];
        if (timeToInt < 10) {
            strTimeToPrint = [NSString stringWithFormat:@"0%ld:00",(long)timeToInt];
        } else {
            strTimeToPrint = [NSString stringWithFormat:@"%ld:00",(long)timeToInt];
        }
        
        dayLabel.text = [NSString stringWithFormat:@"%@",strDay];
        timeLabel.text = [NSString stringWithFormat:@"%@ - %@",strTimeFrom, strTimeToPrint];
         NSLog(@"timeLabel: %@", timeLabel.text);
        
        if (currDayNumber == i){
            
            UIView* greenCircle = [[UIView alloc] init];
            if (isGreen == YES){
            greenCircle.backgroundColor = mainColor;
            } else if (isGreen == NO){
            greenCircle.backgroundColor = [UIColor redColor];
            }
            
            greenCircle.layer.cornerRadius = 4;
            greenCircle.layer.masksToBounds = YES;
            [timeView addSubview:greenCircle];
            
            greenCircle.translatesAutoresizingMaskIntoConstraints = false;
            
            [greenCircle.centerYAnchor constraintEqualToAnchor:dayLabel.centerYAnchor constant:1].active = YES;
            [greenCircle.rightAnchor constraintEqualToAnchor:dayLabel.leftAnchor constant:-3].active = YES;
            [greenCircle.widthAnchor constraintEqualToConstant:8].active = YES;
            [greenCircle.heightAnchor constraintEqualToConstant:8].active = YES;
        
        }
    }
    
    
    UIButton* backButton = [[UIButton alloc] init];
    
    timeView.translatesAutoresizingMaskIntoConstraints = false;
    
    backButton.translatesAutoresizingMaskIntoConstraints = false;
    [backTimeView addSubview:backButton];
    
    CGFloat heightTimeView = workTimes.count * heightRow;
    if (heightTimeView > heightTimeV) {
        
        heightTimeView = heightTimeV;
    }
    
    [backButton.topAnchor constraintEqualToAnchor:backTimeView.topAnchor].active = YES;
    [backButton.leftAnchor constraintEqualToAnchor:backTimeView.leftAnchor].active = YES;
    [backButton.widthAnchor constraintEqualToAnchor:backTimeView.widthAnchor].active = YES;
    [backButton.heightAnchor constraintEqualToAnchor:backTimeView.heightAnchor].active = YES;
    
    [timeView.topAnchor constraintEqualToAnchor:_dateLabel.topAnchor constant:-1].active = YES;
    [timeView.leftAnchor constraintEqualToAnchor:_dateLabel.leftAnchor constant:-15].active = YES;
    [timeView.widthAnchor constraintEqualToConstant:widthTimeView].active = YES;
    [timeView.heightAnchor constraintEqualToConstant:heightTimeView].active = YES;

    [backButton.topAnchor constraintEqualToAnchor:backTimeView.topAnchor].active = YES;
    [backButton.leftAnchor constraintEqualToAnchor:backTimeView.leftAnchor].active = YES;
    [backButton.widthAnchor constraintEqualToAnchor:backTimeView.widthAnchor].active = YES;
    [backButton.heightAnchor constraintEqualToAnchor:backTimeView.heightAnchor].active = YES;
    
   
    
    [backButton addTarget:self action:@selector(timeBack:) forControlEvents:UIControlEventTouchUpInside];
}


-(NSString*) getDay:(NSInteger)type{

    NSString* dayOfWeek;
    
    switch (type) {
        case 1:
            dayOfWeek = DPLocalizedString(@"monday", nil);
            break;
        case 2:
            dayOfWeek = DPLocalizedString(@"tuesday", nil);
            break;
        case 3:
            dayOfWeek = DPLocalizedString(@"wednesday", nil);
            break;
        case 4:
            dayOfWeek = DPLocalizedString(@"thursday", nil);
            break;
        case 5:
            dayOfWeek = DPLocalizedString(@"friday", nil);
            break;
        case 6:
            dayOfWeek = DPLocalizedString(@"saturday", nil);
            break;
        case 7:
            dayOfWeek = DPLocalizedString(@"sunday", nil);
            break;
        
        default:
            break;
    }
//    NSLog(@"dayOfWeek %@", dayOfWeek);
    return dayOfWeek;
}


- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    [backPhoneView removeFromSuperview];
}

- (void)setupPhoneView {
    
    backPhoneView = [[UIView alloc]initWithFrame:self.view.frame];
    backPhoneView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [backPhoneView addGestureRecognizer:singleFingerTap];
    [self.view addSubview:backPhoneView];
    
    
    phonePopView = [[UIView alloc]init];
    phonePopView.backgroundColor = [UIColor whiteColor];
    
    [phonePopView.layer setShadowColor: [UIColor grayColor].CGColor];
    [phonePopView.layer setShadowOpacity:0.8];
    [phonePopView.layer setShadowRadius:3.0];
    [phonePopView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backPhoneView addSubview:phonePopView];
    
    UIView* separLine = [[UIView alloc] init];
    separLine.backgroundColor = [UIColor lightGrayColor];

    [phonePopView addSubview:separLine];
    
    
    CGFloat heightRow = 38.0;
    CGFloat widthRow = 164;
    CGFloat fontSize = 13;
    CGFloat heightPhoneV = 80;

    if (!isPhone) {
        
        heightRow = 58.0;
        widthRow = 266;
        fontSize = 22;
        heightPhoneV = 124;
    }

        UIButton* callButton1 = [[UIButton alloc] init];
                                 
        [callButton1.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        [callButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [callButton1.titleLabel setTintColor:[UIColor darkGrayColor]];
        [callButton1 setTitle:@"+38 (050) 900 34 93" forState:UIControlStateNormal];
        [phonePopView addSubview:callButton1];
        
        UIButton* callButton2 = [[UIButton alloc] init];
        [callButton2.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        [callButton2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [callButton2.titleLabel setTintColor:[UIColor darkGrayColor]];
        [callButton2 setTitle:@"+38 (044) 384 49 34" forState:UIControlStateNormal];
        [phonePopView addSubview:callButton2];
        

    separLine.translatesAutoresizingMaskIntoConstraints = false;
    phonePopView.translatesAutoresizingMaskIntoConstraints = false;
    callButton1.translatesAutoresizingMaskIntoConstraints = false;
    callButton2.translatesAutoresizingMaskIntoConstraints = false;

    [phonePopView.topAnchor constraintEqualToAnchor:_phoneLabel.topAnchor constant:-1].active = YES;
    [phonePopView.leftAnchor constraintEqualToAnchor:_phoneLabel.leftAnchor constant:-10].active = YES;
    [phonePopView.widthAnchor constraintEqualToConstant:widthRow].active = YES;
    [phonePopView.heightAnchor constraintEqualToConstant:heightPhoneV].active = YES;
    
    [separLine.centerYAnchor constraintEqualToAnchor:phonePopView.centerYAnchor].active = YES;
    [separLine.leftAnchor constraintEqualToAnchor:phonePopView.leftAnchor].active = YES;
    [separLine.widthAnchor constraintEqualToConstant:widthRow].active = YES;
    [separLine.heightAnchor constraintEqualToConstant:2].active = YES;
    
    [callButton1.topAnchor constraintEqualToAnchor:phonePopView.topAnchor].active = YES;
    [callButton1.leftAnchor constraintEqualToAnchor:phonePopView.leftAnchor].active = YES;
    [callButton1.widthAnchor constraintEqualToAnchor:phonePopView.widthAnchor].active = YES;
    [callButton1.heightAnchor constraintEqualToConstant:heightRow].active = YES;
    
    [callButton2.bottomAnchor constraintEqualToAnchor:phonePopView.bottomAnchor].active = YES;
    [callButton2.leftAnchor constraintEqualToAnchor:phonePopView.leftAnchor].active = YES;
    [callButton2.widthAnchor constraintEqualToAnchor:phonePopView.widthAnchor].active = YES;
    [callButton2.heightAnchor constraintEqualToConstant:heightRow].active = YES;
    
    [callButton1 addTarget:self action:@selector(callButton1Action:) forControlEvents:UIControlEventTouchUpInside];
    [callButton2 addTarget:self action:@selector(callButton2Action:) forControlEvents:UIControlEventTouchUpInside];
}

- (UIImage*)drawGradientWithFrame: (CGRect)frame;
{
    UIImage*image;
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, [UIScreen mainScreen].scale);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Gradient Declarations
    CGFloat gradientLocations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace,(__bridge CFArrayRef)@[(id)[UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.85].CGColor, (id)UIColor.clearColor.CGColor],gradientLocations);
    //// Rectangle Drawing
    CGRect rectangleRect = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.00000 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.00000 + 0.5), floor(CGRectGetWidth(frame) * 1.00000 + 0.5) - floor(CGRectGetWidth(frame) * 0.00000 + 0.5), floor(CGRectGetHeight(frame) * 1.00000 + 0.5) - floor(CGRectGetHeight(frame) * 0.00000 + 0.5));
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: rectangleRect];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, gradient,
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMinY(rectangleRect)),
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMaxY(rectangleRect)),
                                0);
    CGContextRestoreGState(context);
    
    
    //// Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
