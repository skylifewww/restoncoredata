//
//  ProfileViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface ProfileViewController (){
    Reachability *_reachability;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"profile", nil);
    
    [self setNavigationBarItem];
   
    [self setupView];
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    CGFloat screenWidth = self.view.bounds.size.width;
//    CGFloat screenHeight = self.view.bounds.size.height;
   
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
    self.textBottomLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.scrollViewContainer.translatesAutoresizingMaskIntoConstraints = false;
    self.topView.translatesAutoresizingMaskIntoConstraints = false;
    self.myProfileLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    [self.topView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.topView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant: 65].active = YES;
    [self.topView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
    [self.topView.heightAnchor constraintEqualToConstant:82].active = YES;
    
    if (screenWidth < 322) {
        [self.myProfileLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.myProfileLabel.centerXAnchor constraintEqualToAnchor:self.topView.centerXAnchor].active = YES;
    [self.myProfileLabel.centerYAnchor constraintEqualToAnchor:self.topView.topAnchor constant:11].active = YES;
    [self.myProfileLabel.widthAnchor constraintEqualToConstant:300].active = YES;
    [self.myProfileLabel.heightAnchor constraintEqualToConstant:21].active = YES;
    
    [self.textBottomLabel.centerXAnchor constraintEqualToAnchor:self.topView.centerXAnchor].active = YES;
    [self.textBottomLabel.centerYAnchor constraintEqualToAnchor:self.topView.bottomAnchor constant:-20].active = YES;
    [self.textBottomLabel.widthAnchor constraintEqualToConstant:300].active = YES;
    [self.textBottomLabel.heightAnchor constraintEqualToConstant:14].active = YES;
    
    [self.scrollViewContainer.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.scrollViewContainer.topAnchor constraintEqualToAnchor:self.topView.bottomAnchor].active = YES;
    [self.scrollViewContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.scrollViewContainer.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
//        [self.topView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//        [self.topView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant: 65.0].active = YES;
//        [self.topView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
//        [self.topView.heightAnchor constraintEqualToConstant:80].active = YES;
//
//        [self.topView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//        [self.topView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
//        [self.topView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
//        [self.topView.heightAnchor constraintEqualToConstant:420].active = YES;
        
        self.myProfileLabel.font = [UIFont fontWithName:@"Thonburi" size:24];
//        [self.myProfileLabel.centerXAnchor constraintEqualToAnchor:self.topView.centerXAnchor].active = YES;
//        [self.myProfileLabel.centerYAnchor constraintEqualToAnchor:self.topView.topAnchor constant:40].active = YES;
//        [self.myProfileLabel.widthAnchor constraintEqualToConstant:300].active = YES;
//        [self.myProfileLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
        
        self.textBottomLabel.font = [UIFont fontWithName:@"Thonburi" size:16];
//        [self.textBottomLabel.centerXAnchor constraintEqualToAnchor:self.topView.centerXAnchor].active = YES;
//        [self.textBottomLabel.centerYAnchor constraintEqualToAnchor:self.myProfileLabel.centerYAnchor constant:30].active = YES;
//        [self.textBottomLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//        [self.textBottomLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
//        
//        [self.scrollViewContainer.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
//        [self.scrollViewContainer.topAnchor constraintEqualToAnchor:self.topView.bottomAnchor].active = YES;
//        [self.scrollViewContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
//        [self.scrollViewContainer.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    }
}

@end
