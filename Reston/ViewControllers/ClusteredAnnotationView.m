//
//  ClusteredAnnotationView.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/26/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "ClusteredAnnotationView.h"

@implementation ClusteredAnnotationView

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UIColor* mainColor;
        mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
        
        self.image = [UIImage imageNamed:@"bigCluster"];
        self.frame = CGRectMake(0, 0, self.image.size.width, self.image.size.height);
        
        CGRect frame = self.frame;
        frame.origin.y += 63;
        self.textLayer.frame = frame;
        self.textLayer.font = (__bridge CFTypeRef _Nullable)([UIFont fontWithName:@"Thonburi-Bold" size:18]);
        self.textLayer.fontSize = 18;
        self.textLayer.contentsScale = [[UIScreen mainScreen] scale];
        [self.textLayer setAlignmentMode:@"center"];
        self.textLayer.foregroundColor = mainColor.CGColor;
        
        self.centerOffset = CGPointMake(0, -self.frame.size.height/2);
        
        [self.layer addSublayer:self.textLayer];
        
        self.canShowCallout = YES;
        
        [self clusteringAnimation];
    }
    return self;
}

- (void)clusteringAnimation {
    
}

- (CATextLayer *)textLayer {
    if (!_textLayer) {
        _textLayer = [[CATextLayer alloc] init];
    }
    return _textLayer;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    [super setAnnotation:annotation];
    
    ADClusterAnnotation *clusterAnnotation = (ADClusterAnnotation *)annotation;
    if (clusterAnnotation) {
        NSUInteger count = clusterAnnotation.clusterCount;
//        if (count < 100){
//        self.image = [UIImage imageNamed:@"smallCluster"];
//        }
        
        // Removes rid of the animation
        self.textLayer.actions = @{@"contents": [NSNull null]};
        
        self.textLayer.string = [self numberLabelText:count];
    }
}

- (NSString *)numberLabelText:(float)count {
    
    if (!count) {
        return nil;
    }
    
    if (count > 1000) {
        float rounded;
        if (count < 10000) {
            rounded = ceilf(count/100)/10;
            return [NSString stringWithFormat:@"%.1fk", rounded];
        }
        else {
            rounded = roundf(count/1000);
            return [NSString stringWithFormat:@"%luk", (unsigned long)rounded];
        }
    }
    
    return [NSString stringWithFormat:@"%lu", (unsigned long)count];
}


@end
