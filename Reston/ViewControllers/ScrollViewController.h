//
//  ScrollViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *photoContainer;
@property (weak, nonatomic) IBOutlet UIImageView *userPhotoImage;
@property (weak, nonatomic) IBOutlet UILabel *addphotoLabel;
@property (weak, nonatomic) IBOutlet UIView *fullNameView;
@property (weak, nonatomic) IBOutlet UIView *phone1View;
@property (weak, nonatomic) IBOutlet UIView *phone2View;

@property (weak, nonatomic) IBOutlet UIView *dateContainerView;
@property (weak, nonatomic) IBOutlet UILabel *birthDateLabel;

@property (weak, nonatomic) IBOutlet UIButton *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *numDayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dayTreanglImage;




@property (weak, nonatomic) IBOutlet UIButton *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *numMonthLabel;

@property (weak, nonatomic) IBOutlet UIImageView *monthTrenglImage;

@property (weak, nonatomic) IBOutlet UIButton *yearLabel;

@property (weak, nonatomic) IBOutlet UIButton *dateChoiseButton;

//@property (weak, nonatomic) IBOutlet UILabel *city;
//@property (weak, nonatomic) IBOutlet UIButton *cityLabel;

//@property (weak, nonatomic) IBOutlet UILabel *cityNamelabel;
//
//@property (weak, nonatomic) IBOutlet UIImageView *cityTreanglImage;

@property (weak, nonatomic) IBOutlet UIButton *fbButton;


@property (weak, nonatomic) IBOutlet UILabel *numYearLabel;
@property (weak, nonatomic) IBOutlet UIImageView *yearTreanglImage;


@property (weak, nonatomic) IBOutlet UIView *passwContainerView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UILabel *passwordlabel;

@end
