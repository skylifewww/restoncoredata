//
//  NewsTableViewCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "NewsTableViewCell.h"
#import "UIImage+Cache.h"

NSString *const kNewsExpandedCellId = @"NewsExpandedCell";
const CGFloat kNewsExpandedCellHeight = 640;

@interface NewsTableViewCell ()

@end

@implementation NewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setPhoto:(UIImage *)image {
    if (image ==nil) {
        [_expandImage removeFromSuperview];
        return;
    }
  
    _expandImage.image = image;
}

- (void)applyNew:(RNNEw *)model {

    _expandText.text = model.text;
    [UIImage cachedImage:model.image
           fullURLString:@"http://reston.com.ua/uploads/img/news/original/"
            withCallBack:^(UIImage *image) {
                [self setPhoto:image];
            }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
