//
//  OfferTableViewCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@class OfferTableViewCell;


@interface OfferTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *theImageView;
@property (weak, nonatomic) IBOutlet UILabel *titlesLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
//@property (weak, nonatomic) IBOutlet UIButton *purchaseButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
//- (IBAction)purchaseButtonDidTap:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *dateStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateEndLabel;

//@property (weak, nonatomic) id<ExampleTableViewCellDelegate> delegate;


+(NSString*)cellIdentifier;

@end
