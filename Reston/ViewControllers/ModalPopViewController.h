//
//  ModalPopViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNOrderInfo.h"
#import "RNRestaurant.h"

extern NSString *const ReservCountChangeNotification;

@interface ModalPopViewController : UIViewController

@property (nonatomic, strong) RNOrderInfo *orderInfo;

@property (weak, nonatomic) RNRestaurant *restourant;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
//@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextView *conditionTextView;
@property (weak, nonatomic) IBOutlet UILabel *enterCommentslabel;

@property (weak, nonatomic) IBOutlet UIView *reservedView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel1;
@property (weak, nonatomic) IBOutlet UILabel *topLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomlabel1;
@property (weak, nonatomic) IBOutlet UILabel *bottomlabel2;
@property (weak, nonatomic) IBOutlet UIView *fullNameView;
//@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
