//
//  PDFViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "PDFViewController.h"
#import "MenuRestViewController.h"
#import "Reachability.h"

@interface PDFViewController (){
    
    NSArray* slideViewControllers;
    NSArray* arrayPages;
    NSInteger pageIndex;
//    CGPDFDocumentRef pdfDocument;
//    size_t pageCount;
    UIPageControl* pageControl;
    UIColor* mainColor;
    
    
}
@property (strong, nonatomic) UIPageViewController *pageViewController;
@end

@implementation PDFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
//    NSLog(@"_menuPDF %@",_menuPDF);
    
//    NSData *pdfData = [[NSData alloc] initWithContentsOfURL:_menuPDF];
//
//    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef) pdfData);
//
//    pdfDocument = CGPDFDocumentCreateWithProvider(dataProvider);
//    pageCount = CGPDFDocumentGetNumberOfPages(pdfDocument);
//    NSLog(@"pdfDocument %@",pdfDocument);
//
//    NSLog(@"pageCount %ld",pageCount);
    
    self.title = [NSString stringWithFormat:@"Меню \"%@\"",self.titleNav];
   
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    self.pageViewController = [[UIPageViewController alloc]
                               initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
                               navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                               options:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false;
    
    [self.pageViewController.view.leadingAnchor constraintEqualToAnchor:self.view .leadingAnchor].active = YES;
    [self.pageViewController.view.trailingAnchor constraintEqualToAnchor:self.view .trailingAnchor].active = YES;
    [self.pageViewController.view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.pageViewController.view.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:65].active = YES;
    
    
    [self.pageViewController didMoveToParentViewController:self];
    
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    MenuRestViewController* initialVC = (MenuRestViewController*) [self viewControllerAtIndex:1];
    
    initialVC.pageIndex = 1;
    initialVC.pdfDocument = self.pdfDocument;
    
    slideViewControllers = [NSArray arrayWithObjects:initialVC, nil];

    [self.pageViewController setViewControllers:slideViewControllers direction:UIPageViewControllerNavigationDirectionForward animated:true completion:nil];
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake(CGRectGetMidX(self.view.frame) - 50, CGRectGetMaxY(self.view.frame) - 40, 100, 21);
    pageControl.numberOfPages = self.pageCount - 1;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = mainColor;
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    if (self.pageCount - 1 <= 1) {
        pageControl.alpha = 0;
    }

    [self.view addSubview:pageControl];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger) index{
    
    slideViewControllers = @[];
    
    MenuRestViewController* slideViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuRestViewController"];
    
    slideViewController.pageIndex = index;
    slideViewController.pdfDocument = self.pdfDocument;
    [pageControl setCurrentPage:index - 1];
   
    NSLog(@"[self viewControllerAtIndex:] %lu",(unsigned long)index);
    
    return slideViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index = ((MenuRestViewController*) viewController).pageIndex;
    
    if (index == NSNotFound || index == 0)
    {
        return nil;
    }
    
    if (index > 1)
    {
        index--;
    }
    else
    {
        return nil;
    }
    slideViewControllers = @[];
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = ((MenuRestViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    
    if (index < self.pageCount - 1)
    {
        index++;
    }else
    {
        return nil;
    }
    slideViewControllers = @[];
    return [self viewControllerAtIndex:index];
}


- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers{
    pageIndex = (int)((MenuRestViewController*) pendingViewControllers.firstObject).pageIndex;
}


-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    if (completed) {

        pageIndex = ((MenuRestViewController *)self.pageViewController.viewControllers.firstObject).pageIndex;
    }
}

-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return  pageIndex;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(showAlertSaveProduct:)
    //                                                 name:SaveProductNotification
    //                                               object:nil];
    
    //    [slideViewController.collectionView removeFromSuperview];
    
}

- (IBAction)backButton:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
