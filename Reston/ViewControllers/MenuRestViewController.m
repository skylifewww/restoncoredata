//
//  MenuRestViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "MenuRestViewController.h"
#import "PDFPageView.h"
#import "Reachability.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

@interface MenuRestViewController ()<UIScrollViewDelegate>{
    Reachability *_reachability;
}
@property (weak, nonatomic) IBOutlet PDFPageView *page;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MenuRestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    _scrollView.delegate = self;
    _scrollView.bounces = NO;
    
    _scrollView.minimumZoomScale = 1.0;
    _scrollView.maximumZoomScale = 5.0;
    [_scrollView setZoomScale:_scrollView.minimumZoomScale];
//    self.view = _scrollView;
    [self preparePDFPage];
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
//        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) preparePDFPage{
    
    _page.pdfDocument = self.pdfDocument;
    _page.pageIndex = self.pageIndex;
    
    [_page setNeedsDisplay];

}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _page;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
