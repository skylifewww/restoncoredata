//
//  News1ViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HVTableView.h"
#import "RNRestaurant.h"


@interface News1ViewController : UIViewController

@property (strong, nonatomic) NSString* titleNav;
@property (weak, nonatomic) IBOutlet HVTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) NSNumber* restaurantID;
@property (nonatomic, strong) RNRestaurant *restaurant;
@property (nonatomic, strong) NSArray *newsAll;
@end
