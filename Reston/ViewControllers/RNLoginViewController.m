//
//  RNViewController.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNLoginViewController.h"
#import "RNRegisterViewController.h"
#import "RNAuthRequest.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"
#import "RNEditProfileRequest.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookRegisterRequest.h"
#import "FacebookRegisterResponse.h"
#import "CatalogRestViewController.h"
#import "SlideMenuController.h"
#import "NewMenuViewController.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "AFNetworking.h"
#import <AFNetworking/AFNetworking.h>
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
//@import FirebaseAuth;
//@import FirebaseDatabase;
//@import FirebaseStorage;

#import "Reachability.h"
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;

    }
    return NO;
}
@end

static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kUserPhotoKey = @"UserPhotoKey";
static NSString *const kUserKey = @"UserKey";
static NSString *const kPassKey = @"PassKey";
static NSString *const kEmailFBKey = @"EmailFBKey";
static NSString *const kClientIDKey = @"ClientIDKey";
static NSString *const kStrategyKey = @"StrategyKey";

@interface RNLoginViewController ()<UITextFieldDelegate,CLLocationManagerDelegate>{
    double latitude;
    double longitude;
    BOOL loading;
    NSString* currCity;
    NSArray<RNRestaurant*>* restons;
    UIColor* mainColor;
    Boolean isAuth;
    double textFieldFontSize;
    double buttonFontSize;
    UIStoryboard* storyboard;
    Boolean isPhone;
//    NSString* imageUrlFIR;
    NSString* filePathImage;
    Boolean isSuccessUploadImage;
 
    
    Reachability *_reachability;
    
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;
@property (weak, nonatomic) IBOutlet UIButton *faceButton;
//@property (strong, nonatomic) FIRStorageReference *storageRef;
//@property (weak, nonatomic) IBOutlet FBSDKLoginButton *faceButton;


@end

@implementation RNLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSuccessUploadImage = NO;
    
//    self.storageRef = [[FIRStorage storage] reference];

//    if (![FIRAuth auth].currentUser) {
//        [[FIRAuth auth] signInAnonymouslyWithCompletion:^(FIRUser * _Nullable user,
//                                                          NSError * _Nullable error) {
//            if (error) {
////                NSLog(@"[FIRAuth auth] error");
//            } else {
////                NSLog(@"[FIRAuth auth]");
//            }
//        }];
//    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    loading = NO;
    
    [self loadUserLocation];
    
    self.title = DPLocalizedString(@"enter_title", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];

    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doneClicked:)]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    [doneButton setTintColor:[[UIColor darkGrayColor]colorWithAlphaComponent:0.8f]];
    
    _emailTextField.inputAccessoryView = keyboardDoneButtonView;
    _passwordTextField.inputAccessoryView = keyboardDoneButtonView;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self setupView];
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)doneClicked:(id)sender {
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    float newVerticalPosition = -keyboardSize.height;

  
    if (_emailTextField.isFirstResponder){
        if (_emailView.frame.origin.y + _emailView.frame.size.height > self.view.frame.size.height + newVerticalPosition){
            [self moveFrameToVerticalPosition:-(_emailView.frame.origin.y + _emailView.frame.size.height - self.view.frame.size.height + newVerticalPosition) forDuration:0.3f];
        }
    }
    
    if (_passwordTextField.isFirstResponder){
        if (_passwordView.frame.origin.y + _passwordView.frame.size.height > self.view.frame.size.height + newVerticalPosition){
            [self moveFrameToVerticalPosition:-(_passwordView.frame.origin.y + _passwordView.frame.size.height - self.view.frame.size.height + newVerticalPosition) forDuration:0.3f];
        }
    }
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)registerDefaults {
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kPassKey:@"",
                                                              kUserKey:@"",
                                                              kEmailFBKey:@"",
                                                              kClientIDKey:@"",
                                                              kStrategyKey:@""
                                                              }];
}

- (void) loadUserLocation
{
    
    [self registerDefaults];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
    
    latitude = _locationManager.location.coordinate.latitude;
    longitude = _locationManager.location.coordinate.longitude;
//    NSLog(@"CatalogRestViewController latitude %f longitude %f",latitude, longitude);
    
    CLLocation *locUser = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [self getAddressFromLocation:locUser];
    
    
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
}
//
//
//-(NSString *)getAddressFromLocation:(CLLocation *)location {
//
//    __block NSString *address;
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if(placemarks && placemarks.count > 0)
//         {
//             CLPlacemark *placemark= [placemarks objectAtIndex:0];
//
//             address = [NSString stringWithFormat:@"%@",[placemark locality]];
////             NSLog(@"%@",address);
//
//             if ([FBSDKAccessToken currentAccessToken]) {
////
////                 [self sendSearchRequestWithCity:address];
////
////                  NSLog(@"[FBSDKAccessToken currentAccessToken] %@",[FBSDKAccessToken currentAccessToken]);
//             }
//             currCity = address;
//         }
//
//     }];
//    return address;
//}

-(NSString *)getAddressFromLocation:(CLLocation *)location {
    
    __block NSString *city;
    __block NSString *administrativeArea;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             city = [NSString stringWithFormat:@"%@",[placemark locality]];
             NSLog(@"city %@",city);
             administrativeArea = [NSString stringWithFormat:@"%@",[placemark administrativeArea]];
             NSLog(@"administrativeArea %@",administrativeArea);
             
             
             if (![city isKindOfClass:[NSNull class]] || ![administrativeArea isKindOfClass:[NSNull class]]){
                 
                 NSLog(@"cityName != nil");
                 if ([city isEqualToString:@"Kiev"] || [administrativeArea isEqualToString:@"Kiev Oblast"] || [city isEqualToString:@"Киев"] || [city isEqualToString:@"Київ"]){
                     
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].realCity = @"1";
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                     //                     [self showAlertWithText:[NSString stringWithFormat:@"город?-------------------------------------------------------  1"]];
                     
                     
                 } else if ([city isEqualToString:@"Lviv"] || [administrativeArea isEqualToString:@"Lviv Oblast"] || [city isEqualToString:@"Львів"] || [city isEqualToString:@"Львов"]){
                     
                     [RNUser sharedInstance].currCity = @"6";
                     [RNUser sharedInstance].realCity = @"6";
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 } else if ([city isEqualToString:@"Ivano-Frankivs'k"] || [administrativeArea isEqualToString:@"Ivano-Frankivsk Oblast"] || [city isEqualToString:@"Івано-Франківськ"] || [city isEqualToString:@"Ивано-Франковск"]){
                     [RNUser sharedInstance].realCity = @"8";
                     [RNUser sharedInstance].currCity = @"8";
                     
                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 } else {
                     [RNUser sharedInstance].realCity = @"1234567";
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].isThisCity = NO;
                     //                     [RNUser sharedInstance].isThisCity = YES;
                     NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
                 }
             } else {
                 
                 NSLog(@"cityName == nil");
                 [RNUser sharedInstance].currCity = @"1";
                 [RNUser sharedInstance].isThisCity = NO;
                 //                 [RNUser sharedInstance].isThisCity = YES;
                 [RNUser sharedInstance].realCity = @"1234567";
                 NSLog(@"[RNUser sharedInstance].isThisCity = %d", [RNUser sharedInstance].isThisCity);
             }
             
             if ([FBSDKAccessToken currentAccessToken]) {
                 //
                 //                 [self sendSearchRequestWithCity:address];
                 //
                 //                  NSLog(@"[FBSDKAccessToken currentAccessToken] %@",[FBSDKAccessToken currentAccessToken]);
             }
             
             currCity = [RNUser sharedInstance].currCity;
             
//             if ([self isInternetConnect]){
//                 [self startDefaultFlowWithCity:[RNUser sharedInstance].currCity];
//             }
         }
         
     }];
    return [RNUser sharedInstance].currCity;
}


////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) fetchProfile{
    
//    NSLog(@"fetchProfile");

    NSDictionary*  parameters = @{@"fields": @"id, first_name, last_name, email, picture.type(large)"};
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"me"] parameters:parameters

                                                                   HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if (!error) {
//            NSLog(@"result = %@",result);
            NSString* email = [result objectForKey:@"email"];
            NSString* first_name = [result objectForKey:@"first_name"];
            NSString* last_name = [result objectForKey:@"last_name"];
            NSString* name = [NSString stringWithFormat:@"%@ %@",first_name, last_name];
            NSString* clientIDStr = [result objectForKey:@"id"];
            NSInteger intID = clientIDStr.integerValue;

            NSString* clientID = [NSString stringWithFormat:@"%ld",(long)intID];
           
            
//            NSLog(@"clientID %@",clientID);

            NSDictionary *dictionary = (NSDictionary *)[result objectForKey:@"picture"];
            NSDictionary *data = [dictionary objectForKey:@"data"];
            NSString *photoUrl = (NSString *)[data objectForKey:@"url"];
            
//            NSLog(@"email is %@", [result objectForKey:@"email"]);
            
            NSData  *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoUrl]];
     
            
            [[NSUserDefaults standardUserDefaults] setObject:dataImage forKey:kUserPhotoKey];
            [[NSUserDefaults standardUserDefaults] setObject:name
                                                      forKey:kFBNameKey];
           [[NSUserDefaults standardUserDefaults] synchronize];
 
            
            [self sendLoginRequestWithFacebookClientID:clientID nameFB:name emailFB:email photoFB:(NSString*)photoUrl];
        } else {
//            NSLog(@"%@", error);
            return;
        }}
     ];
}


- (IBAction)backButtonTape:(id)sender {
    
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
//     [self dismissViewControllerAnimated:YES completion:nil];
     [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
   
        return UIStatusBarStyleLightContent;
    }

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}

#pragma mark - IBActions New (with main.Storyboard)
- (IBAction)enterPressed:(id)sender {
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    if ([self isInternetConnect]){
        [self sendLoginRequest];
    }
//    NSLog(@"Login!!!");
}


- (IBAction)enterFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    if ([self isInternetConnect]){
    [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    
        if (error != nil || [result isKindOfClass:[NSNull class]])
        {
            
//            NSLog(@"Unexpected login error: %@", error);
            NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: DPLocalizedString(@"try_later", nil);
            NSString *alertTitle = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: DPLocalizedString(@"server_error", nil);
            
            [self showAlertWithTitle:alertTitle andMessage:alertMessage];
            
        }
        else
        {
            if(result.token)
            {
          
                [self fetchProfile];
            }        
        }
    }];
    }
}
- (IBAction)forgotPassword:(id)sender {
    
    UIViewController * forgotViewController = [storyboard instantiateViewControllerWithIdentifier:@"RNForgotPasswordViewController"];
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:forgotViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
//    [self.navigationController pushViewController:forgotViewController animated:YES];
}

#pragma mark - IBActions


- (IBAction)signUpPressed:(id)sender {
    RNRegisterViewController *vc = [[RNRegisterViewController alloc] init];
    [self.navigationController pushViewController:vc
                                         animated:YES];
}


- (void)sendLoginRequest {
    RNAuthRequest *request = [[RNAuthRequest alloc]init];
    
    if (_emailTextField.text.length != 0) {
        request.username = _emailTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_email", nil)];
        return;
    }
    
    if (_passwordTextField.text.length!=0) {
        request.password = _passwordTextField.text;
    } else {
        [self showAlertWithText:DPLocalizedString(@"less_pass", nil)];
        return;
    }
    
//    request.username = @"skylife_2014@ukr.net";
//    request.password = @"skywww222";

    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            return;
        }
        
        RNAuthorizationResponse *authResponse = (RNAuthorizationResponse *)response;
        [RNUser sharedInstance].email = _emailTextField.text;// TODO: v.1.0
        
//        NSLog(@"authResponse.token: %@",authResponse.token);
        if (authResponse.token > 0) {
            
//             NSLog(@"authResponse.userData > 0!!!");
            [RNUser sharedInstance].token = authResponse.token;
            [[NSUserDefaults standardUserDefaults] setObject:_emailTextField.text
                                                      forKey:kUserKey];
            [[NSUserDefaults standardUserDefaults] setObject:_passwordTextField.text
                                                      forKey:kPassKey];
            [[NSUserDefaults standardUserDefaults] setObject:@"LOGINSTRATEGY"
                                                      forKey:kStrategyKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self sendSearchRequestWithCity:currCity];
            // TODO: continue
        } else {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    }];
}

- (void)sendLoginRequestWithFacebookClientID:(NSString*)clientId nameFB:(NSString*)nameFB emailFB:(NSString*)emailFB photoFB:(NSString*)photoFB{
    FacebookRegisterRequest *request = [[FacebookRegisterRequest alloc]init];
    request.clientId = clientId;
    request.nameFB = nameFB;
    request.emailFB = emailFB;
    request.photoFB = photoFB;
    
//    request.password = password;

    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        FacebookRegisterResponse *authResponse = (FacebookRegisterResponse *)response;
//        [RNUser sharedInstance].email = email;
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            
            NSLog(@"error: %@",error);
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//            return;
        }
        
//        NSLog(@"authResponse.token: %@",authResponse.token);
//        NSLog(@"authResponse.autorisation: %@",authResponse.autorisation);
        if (authResponse.token > 0) {
            
//            NSLog(@"authResponse.token > 0!!!");
            [RNUser sharedInstance].token = authResponse.token;
            [RNUser sharedInstance].name = nameFB;
            [RNUser sharedInstance].email = emailFB;
            [RNUser sharedInstance].photoFB = photoFB;
//            NSLog(@"[RNUser sharedInstance].photo.length %ld", [RNUser sharedInstance].photo.length);
            if ([RNUser sharedInstance].photo.length == 0){
                
//                NSLog(@"[RNUser sharedInstance].photo.length = 0");
                
                NSData  *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoFB]];
                UIImage *userPhoto = [UIImage imageWithData:imageData];
                
                [self sendImageToServer:userPhoto];
            }
            
//            NSLog(@"authResponse [RNUser sharedInstance].photoFB: %@",[RNUser sharedInstance].photoFB);
            [[NSUserDefaults standardUserDefaults] setObject:emailFB
                                                      forKey:kEmailFBKey];
            [[NSUserDefaults standardUserDefaults] setObject:clientId
                                                      forKey:kClientIDKey];
            [[NSUserDefaults standardUserDefaults] setObject:nameFB
                                                      forKey:kFBNameKey];
            [[NSUserDefaults standardUserDefaults] setObject:@"FBSTRATEGY"
                                                      forKey:kStrategyKey];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self sendSearchRequestWithCity:currCity];
            // TODO: continue
        } else {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    }];
}

- (void)sendImageToServer:(UIImage*)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[imageData length]];
    NSString *urlString = [NSString stringWithFormat:@"http://api.reston.com.ua/api.php?action=uploadUserPhoto&token=%@", [RNUser sharedInstance].token];
    
    // Init the URLRequest
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:imageData];
    //    NSMutableData *body = [NSMutableData data];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request fromData:nil progress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error!!!!!!!: %@", error);
        } else {
            NSLog(@"responseObject:%@", responseObject);
            
            
            if ([responseObject objectForKey:@"token"] && [responseObject objectForKey:@"file_path"]){
                [RNUser sharedInstance].token = [responseObject objectForKey:@"token"];
                NSString* strURL = [responseObject objectForKey:@"file_path"];
                filePathImage = [strURL stringByReplacingOccurrencesOfString:@"/uploads/users/" withString:@""];
  
                NSLog(@"filePathImage:%@", filePathImage);
                isSuccessUploadImage = YES;
            }
            
            
            NSLog(@"filePath: %@", filePathImage);
            
            if (isSuccessUploadImage == YES && filePathImage.length > 0){
                NSLog(@"[self sendEditRequest]");
                [self sendEditRequest];
            }
        }
        
    }];
    [uploadTask resume];
}

//-(void) uploadImageToFIRStore:(UIImage*)image{
    //    NSData *data = [NSData dataWithContentsOfFile:@"rivers.jpg"];
    
    //    FIRStorage *storage = [FIRStorage storage];
    //    FIRStorageReference *storageRef = [storage reference];
    //    imageUrlFIR = [NSString stringWithFormat:@"gs://reston-2e06b.appspot.com/images/%@.jpg",[RNUser sharedInstance].idUser];
    //    FIRStorageReference *imagesRef = [storageRef child:@"images"];
//    FIRStorageReference *spaceRef = [self.storageRef child:[NSString stringWithFormat:@"%@.jpg",[RNUser sharedInstance].idUser]];
    //    spaceRef = [storage referenceForURL:imageUrlFIR];
    //    NSString *path = spaceRef.fullPath;
    //    NSLog(@"imageUrlFIR: %@  path: %@", imageUrlFIR, path);
    
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
//    FIRStorageMetadata *metadata = [FIRStorageMetadata new];
//    metadata.contentType = @"image/jpeg";
//    [spaceRef putData:imageData metadata:metadata
//           completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
//               if (error) {
//                   NSLog(@"Error uploading: %@", error);
                   //                                        _urlTextView.text = @"Upload Failed";
//                   return;
//               }
               //                                    [self uploadSuccess:metadata storagePath:imagePath];
               //                                    NSURL *downloadURL = metadata.downloadURL;
//               NSString* strURL = metadata.downloadURL.absoluteString;
//               imageUrlFIR = [strURL substringFromIndex: [strURL length] - 36];
//               NSLog(@"imageUrlFIR %@", imageUrlFIR);
//               NSLog(@"metadata.downloadURL.absoluteString %@", metadata.downloadURL.absoluteString);
//               if (imageUrlFIR.length > 0){
//                   [self sendEditRequest];
//               }
//
//           }];
    //}
    //
    //    // Create a reference to the file you want to upload
    //    FIRStorageReference *riversRef = [storageRef child:@"images/rivers.jpg"];
    //
    //    FIRStorageMetadata *metadata = [[FIRStorageMetadata alloc] init];
    //    metadata.contentType = @"image/jpeg";
    //
    //    // Upload the file to the path "images/rivers.jpg"
    //    FIRStorageUploadTask *uploadTask = [riversRef putData:imageData
    //                                                 metadata:metadata
    //                                               completion:^(FIRStorageMetadata *metadata,
    //                                                            NSError *error) {
    //                                                   if (error != nil) {
    //                                                       // Uh-oh, an error occurred!
    //                                                   } else {
    //                                                       // Metadata contains file metadata such as size, content-type, and download URL.
    //                                                       NSURL *downloadURL = metadata.downloadURL;
    //                                                       NSLog(@"downloadURL %@", downloadURL);
    //                                                   }
    //                                               }];
    //    // Listen for state changes, errors, and completion of the upload.
    //    [uploadTask observeStatus:FIRStorageTaskStatusResume handler:^(FIRStorageTaskSnapshot *snapshot) {
    //        // Upload resumed, also fires when the upload starts
    //    }];
    //
    //    [uploadTask observeStatus:FIRStorageTaskStatusPause handler:^(FIRStorageTaskSnapshot *snapshot) {
    //        // Upload paused
    //    }];
    //
    //    [uploadTask observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot) {
    //        // Upload reported progress
    //        double percentComplete = 100.0 * (snapshot.progress.completedUnitCount) / (snapshot.progress.totalUnitCount);
    //    }];
    //
    //    [uploadTask observeStatus:FIRStorageTaskStatusSuccess handler:^(FIRStorageTaskSnapshot *snapshot) {
    //        // Upload completed successfully
    //    }];
    //
    //    // Errors only occur in the "Failure" case
    //    [uploadTask observeStatus:FIRStorageTaskStatusFailure handler:^(FIRStorageTaskSnapshot *snapshot) {
    //        if (snapshot.error != nil) {
    //            switch (snapshot.error.code) {
    //                case FIRStorageErrorCodeObjectNotFound:
    //                    // File doesn't exist
    //                    break;
    //                case FIRStorageErrorCodeUnauthorized:
    //                    // User doesn't have permission to access file
    //                    break;
    //
    //                case FIRStorageErrorCodeCancelled:
    //                    // User canceled the upload
    //                    break;
    //
    //                    /* ... */
    //
    //                case FIRStorageErrorCodeUnknown:
    //                    // Unknown error occurred, inspect the server response
    //                    break;
    //            }
    //        }
    //    }];
    
//}

- (void)sendEditRequest{
    
    RNEditProfileRequest *request = [[RNEditProfileRequest alloc]init];

    
    if (filePathImage.length > 0) {
        request.photo = filePathImage;
    }
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        RNAuthorizationResponse *authResponse = (RNAuthorizationResponse *)response;
//        NSLog(@"authResponse %@",authResponse.editProfile);
        if ([authResponse.editProfile isEqualToString:@"success"]){

            
        } else if (error != nil || [response isKindOfClass:[NSNull class]] || [authResponse.editProfile isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
    } ];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
 
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString*)message {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Start

- (void)sendSearchRequestWithCity:(NSString*)cityName {
    if (loading) {
        return;
    }
    loading = YES;
    [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
    RNGetRestaurantsRequest *request = [[RNGetRestaurantsRequest alloc] init];
    request.name = @"";
    request.isAuth = YES;
    isAuth = YES;
    
//    NSLog(@"cityName************** %@", cityName);
//    if (cityName != nil){
//
////        NSLog(@"cityName != nil");
//        if ([cityName isEqualToString:@"Kiev"]){
//            cityName = @"1";
//            request.city = [cityName uppercaseString];
//        } else if ([cityName isEqualToString:@"Lviv"]){
//            cityName = @"6";
//            request.city = [cityName uppercaseString];
//        } else if ([cityName isEqualToString:@"Ivano-Frankivs'k"]){
//            cityName = @"8";
//            request.city = [cityName uppercaseString];
//        } else {
//            cityName = @"1";
//            request.city = [cityName uppercaseString];
//        }
//    } else {
////        NSLog(@"cityName == nil");
//        cityName = @"1";
//    }
    
    request.city = cityName;
//    currCity = cityName;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        
        loading = NO;
         if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"pass", nil)];
            return;
        }
        RNGetRestaurantsResponse *r = (RNGetRestaurantsResponse *)response;
        [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
        
        restons = r.restrauntsArray;
        restons = [restons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            
            RNRestaurant *r1 = obj1;
            RNRestaurant *r2 = obj2;

            return [@(r1.intCatalogPriority) compare:@(r2.intCatalogPriority)];
            
        }];
        
//        NSLog(@"launch.restons.count: %lu",(unsigned long)restons.count);
        
        [self startIPhone];      
        
    }];
}


- (void)startIPhone {
    CatalogRestViewController* enterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CatalogRestViewController"];
    enterVC.restons = restons;
    enterVC.isAuth = isAuth;
    
    UINavigationController *navVC = [[self.storyboard instantiateViewControllerWithIdentifier:@"CatalogNavRestViewController"] initWithRootViewController:enterVC];
    
    NewMenuViewController * menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewMenuViewController"];
    
    menuVC.restons = restons;
    menuVC.isAuth = isAuth;
    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:navVC leftMenuViewController:menuVC];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:slideMenuController];
    [self.view removeFromSuperview];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    return YES;
}


-(void) setupView {
    
    self.emailView.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordView.translatesAutoresizingMaskIntoConstraints = false;
    
    
    self.emailTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.emailTextField.textColor = mainColor;
    [self.emailTextField setTintColor:mainColor];
    self.emailTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.emailTextField.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    
    
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.passwordTextField.textColor = mainColor;
    [self.passwordTextField setTintColor:mainColor];
    self.passwordTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.passwordTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"pass", nil) withFontSize:textFieldFontSize];
    
    self.enterButton.translatesAutoresizingMaskIntoConstraints = false;
    
    
    
    self.forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false;
    self.forgotPassLabel1.translatesAutoresizingMaskIntoConstraints = false;
    self.forgotpassLabel2.translatesAutoresizingMaskIntoConstraints = false;
    self.faceLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.faceButton.translatesAutoresizingMaskIntoConstraints =false;
    
    [self.enterButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.enterButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:DPLocalizedString(@"enter", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.enterButton setAttributedTitle:strEnter forState:UIControlStateNormal];

    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;

    
    [self.emailView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.emailView.centerYAnchor constraintEqualToAnchor:self.view.topAnchor
                                                 constant:screenHeight * 0.26].active = YES;
    [self.emailView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.emailView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.emailTextField.centerXAnchor constraintEqualToAnchor:self.emailView.centerXAnchor].active = YES;
    [self.emailTextField.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor].active = YES;
    [self.emailTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    [self.passwordView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.passwordView.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor
                                                    constant:screenHeight * 0.08].active = YES;
    [self.passwordView.widthAnchor constraintEqualToConstant:screenWidth * 0.9].active = YES;
    [self.passwordView.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.passwordTextField.centerXAnchor constraintEqualToAnchor:self.passwordView.centerXAnchor].active = YES;
    [self.passwordTextField.centerYAnchor constraintEqualToAnchor:self.passwordView.centerYAnchor].active = YES;
    [self.passwordTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.86].active = YES;
    //    [self.passwordTextField.heightAnchor constraintEqualToAnchor:self.passwordView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    if (screenWidth < 322) {
        [self.enterButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self.enterButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.enterButton.centerYAnchor constraintEqualToAnchor:self.passwordView.bottomAnchor
                                                   constant:screenHeight * 0.084].active = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){

        
        self.enterButton.layer.cornerRadius = 3;
        [self.enterButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [self.enterButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
       
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.enterButton.layer.cornerRadius = 5;
        [self.enterButton.widthAnchor constraintEqualToConstant:220].active = YES;
        [self.enterButton.heightAnchor constraintEqualToConstant:71].active = YES;
//        [self.enterButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:26.0]];
    }
    
    
    self.faceLabel.text = DPLocalizedString(@"or_facebook", nil);
    
    if (screenWidth < 322) {
        [self.faceLabel setFont:[UIFont fontWithName:@"Thonburi" size:10.0]];
    }
    [self.faceLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.faceLabel.centerYAnchor constraintEqualToAnchor:self.enterButton.centerYAnchor
                                                 constant:screenHeight * 0.088].active = YES;
    [self.faceLabel.widthAnchor constraintEqualToConstant:260].active = YES;
    
    
    [self.faceButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.faceButton.centerYAnchor constraintEqualToAnchor:self.faceLabel.centerYAnchor
                                                           constant:screenHeight * 0.064].active = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        self.faceButton.layer.cornerRadius = 15;
        self.faceButton.layer.masksToBounds = true;
        
        self.enterButton.layer.masksToBounds = true;
        [self.faceButton.widthAnchor constraintEqualToConstant:screenWidth * 0.29].active = YES;
        [self.faceButton.heightAnchor constraintEqualToConstant:screenHeight * 0.05].active = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        self.faceButton.layer.cornerRadius = 25;
        self.faceButton.layer.masksToBounds = true;
        
        self.enterButton.layer.masksToBounds = true;
        [self.faceButton.widthAnchor constraintEqualToConstant:140].active = YES;
        [self.faceButton.heightAnchor constraintEqualToConstant:50].active = YES;
        [self.faceButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:30.0]];
          [self.faceLabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
        [self.forgotPassLabel1 setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
        [self.forgotpassLabel2 setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
    }
    
    
    self.forgotPassLabel1.text = DPLocalizedString(@"forgot_pass", nil);
    
    self.forgotpassLabel2.text = DPLocalizedString(@"press", nil);

    [self.forgotPassLabel1.rightAnchor constraintEqualToAnchor:self.view.centerXAnchor constant:-2].active = YES;
    [self.forgotPassLabel1.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor
                                                            constant:-(screenHeight * 0.082)].active = YES;
    [self.forgotPassLabel1.widthAnchor constraintEqualToConstant:140].active = YES;
    [self.forgotPassLabel1.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    [self.forgotpassLabel2.leftAnchor constraintEqualToAnchor:self.view.centerXAnchor constant:2].active = YES;
    [self.forgotpassLabel2.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor
                                                        constant:-(screenHeight * 0.082)].active = YES;
    [self.forgotpassLabel2.widthAnchor constraintEqualToConstant:140].active = YES;
    [self.forgotpassLabel2.heightAnchor constraintEqualToConstant:screenHeight * 0.06].active = YES;
    
    
    [self.forgotPasswordButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.forgotPasswordButton.centerYAnchor constraintEqualToAnchor:self.forgotpassLabel2.centerYAnchor].active = YES;
    [self.forgotPasswordButton.widthAnchor constraintEqualToConstant:300].active = YES;
    [self.forgotPasswordButton.heightAnchor constraintEqualToConstant:60].active = YES;
}


-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
    
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                 NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}

@end
