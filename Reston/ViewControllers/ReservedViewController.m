//
//  ReservedViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "ReservedViewController.h"

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end



@interface ReservedViewController ()

@end

@implementation ReservedViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.closeButton.translatesAutoresizingMaskIntoConstraints = false;
    self.dearLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.fullNameLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.reservedLabel1.translatesAutoresizingMaskIntoConstraints = false;
    self.reservedLabel2.translatesAutoresizingMaskIntoConstraints = false;
    self.reservedImage.translatesAutoresizingMaskIntoConstraints = false;
    
    [self setupView];
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(NSString *)getAddressFromLocation:(CLLocation *)location {
    
    __block NSString *address;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             address = [NSString stringWithFormat:@"%@",[placemark locality]];
             NSLog(@"%@",address);
             
         }
         
     }];
    return address;
}

- (void)showAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка сервера"
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}



- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (IBAction)closeButton:(id)sender {
   
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    
    }];
   
}


-(void) setupView {
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    
    [self.closeButton.centerXAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-screenWidth * 0.09].active = YES;
    [self.closeButton.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.09].active = YES;
    [self.closeButton.widthAnchor constraintEqualToConstant:20].active = YES;
    [self.closeButton.heightAnchor constraintEqualToConstant:20].active = YES;
    
    if (screenWidth < 322) {
        [self.dearLabel setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [self.dearLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.dearLabel.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.16].active = YES;
    [self.dearLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//    [self.dearLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [self.fullNameLabel setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    self.fullNameLabel.text = _userName;
    [self.fullNameLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.fullNameLabel.topAnchor constraintEqualToAnchor:self.dearLabel.bottomAnchor constant:0].active = YES;
    [self.fullNameLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//    [self.fullNameLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [self.reservedLabel1 setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [self.reservedLabel1.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.reservedLabel1.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.29].active = YES;
    [self.reservedLabel1.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//    [self.reservedLabel1.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [self.reservedLabel2 setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [self.reservedLabel2.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.reservedLabel2.topAnchor constraintEqualToAnchor:self.reservedLabel1.bottomAnchor constant:0].active = YES;
    [self.reservedLabel2.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//    [self.reservedLabel2.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    [self.reservedImage.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.reservedImage.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.46].active = YES;
    [self.reservedImage.widthAnchor constraintEqualToConstant:screenWidth * 0.44].active = YES;
    [self.reservedImage.heightAnchor constraintEqualToConstant:screenWidth * 0.84].active = YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
