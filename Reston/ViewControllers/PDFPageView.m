//
//  PDFPageView.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "PDFPageView.h"

@implementation PDFPageView

- (void)drawRect:(CGRect)rect {
    

    if (_pdfDocument != nil) {
        
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
        
        CGContextClearRect(ctx, rect);
       
        CGContextFillRect(ctx, rect);
   
        CGContextScaleCTM(ctx, 1, -1);
        
        CGContextTranslateCTM(ctx, 0, -rect.size.height);
        
        
        
        CGPDFPageRef page = CGPDFDocumentGetPage(_pdfDocument, _pageIndex);
        
        CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
        
        CGFloat ratioW = rect.size.width / (pageRect.size.width);
        CGFloat ratioH = rect.size.height / (pageRect.size.height);
        
        CGFloat ratio = MIN(ratioW, ratioH);
        
        CGFloat newWidth = (pageRect.size.width) * ratio;
        CGFloat newHeight = (pageRect.size.height) * ratio;
        
        CGFloat offsetX = (rect.size.width - newWidth);
        CGFloat offsetY = (rect.size.height - newHeight);
        
        
        CGContextScaleCTM(ctx, newWidth / (pageRect.size.width), newHeight / (pageRect.size.height));
        
        CGContextTranslateCTM(ctx, -(pageRect.origin.x) + offsetX, -(pageRect.origin.y) + offsetY);
        
        CGContextDrawPDFPage(ctx, page);
        
//        CGContextRelease(ctx);
//        CGImageRelease(result);
//        CGPDFDocumentRelease(_pdfDocument);
    }
}

@end
