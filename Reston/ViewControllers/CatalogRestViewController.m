//
//  CatalogRestViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//
#import "LaunchViewController.h"
#import "CatalogRestViewController.h"
#import "PageViewController.h"
#import "RNRestaurant.h"
#import "RNUser.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"
#import "UIWindow+LoadingIndicator.h"
#import "RNSearchByWordRequest.h"
#import "MapViewController.h"
#import "FilterViewController.h"
#import "RestoTypeTableViewController.h"
#import "RestType.h"
#import "FilterInfo.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "NSObject+DPLocalization.h"
#import "define.h"
#import "DPLocalization.h"
#import "UIColor+Hexadecimal.h"
#import "RestCollectionCell.h"
#import "RestonCell.h"
#import "UIImage+Cache.h"
#import "RestonItemViewController.h"
#import "ReservTableViewController.h"
#import "RNGetNewsRequest.h"
#import "RNGetNewsResponse.h"
#import "GetOffersRequest.h"
#import "GetOffersResponse.h"
#import "Reachability.h"
#import "NewsExpandViewController.h"
#import "Offer.h"
#import "RNNEw.h"
#import "RestontableCell.h"

#import "CDRestaurant+CoreDataProperties.h"
#import "CoreDataManager.h"

#import <INTULocationManager/INTULocationManager.h>
#import "LocationManager.h"

static NSString * const reuseIdentifierRestCollectionCell = @"RestCollectionCell";
static NSString * const reuseIdentifierRestTableCell = @"RestonCell";
//static NSString * const reuseIdentifierRestTableCell = @"RestontableCell";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}

@end

NSString *const SetupRestonsNotification = @"SetupRestonsNotification";
NSString *const RestonsChangeNotification = @"RestonsChangeNotification";

typedef NS_ENUM(NSInteger, RNFilterType) {
    RNFilterTypePopular = 0,
    RNFilterTypeAlphabet,
    RNFilterTypeAVGBillDescending,
    RNFilterTypeAVGBillAscending,
    RNFilterTypeNews,
    RNFilterTypeDiscount
};

@interface CatalogRestViewController ()<UISearchBarDelegate, CLLocationManagerDelegate, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    RNFilterType filterType;
    RNFilterType currentFilterType;
    
    CLLocation *currentUserLocation;
    
    double latitude;
    double longitude;
    BOOL loading;
    BOOL isSearch;
    UIColor* mainColor;
    
    NSString* searchCity;
    
    NSInteger currIndex;
    NSInteger exCurrIndex;
    
    NSArray* sortTypes;
    UIView* backSortView;
    UIView* sortView;
    UIView* sortButtonView;
    
    NSArray* arrCity;
    NSArray* arrCityNumber;
    UIButton* cityBackButton;
    UIView* backCityView;
    UIView* cityView;
    UIView* cityButtonView;
    
    UIView* backLangView;
    UIView* langView;
    UIView* langButtonView;
    UIButton* langBackButton;
    
    NSArray* filterActionArr;
    UIView* backFilterView;
    UIView* filtersView;
    UIView* filterButtonView;
    UIButton* filterBackButton;
    NSString* filterActionName;
    
    UIView* newsAllView;
    UIImageView* newsAllImageView;
    UIView* feedsBagde;
    UILabel* feedsCountL;
    UIButton* newsPhoneButton;
    NSInteger newsCount;
    NSInteger offersCount;
    
    NSInteger currentTag;
    
    BOOL isCityHasSubway;
    
    NSArray* workRestons;
    NSString* cityName;
    
    NSString* currentResto;
    BOOL typeFilter;
    
    Boolean isFirst;
    Boolean isPhone;
    NSString* currLang;
    
    NSInteger minAvgBill;
    NSInteger maxAvgBill;
    
    NSMutableArray *lang;
    
    FilterInfo *filter;
    Boolean isClear;
    
    Reachability *_reachability;
    
    UIStoryboard *storyboard;
    
    NSInteger avgBill;
    NSArray* newsAll;
    
    NSMutableArray* arrayOffers;
    
    CGFloat screenWidth;
    
}

@property (nonatomic, strong) dispatch_group_t dispatchGroupForGetNewsFndOffers;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSFetchRequest* fetchRequest;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) NSSortDescriptor* descriptor;
@property (strong, nonatomic) NSPredicate* predicateCity;
@property (strong, nonatomic) NSArray<NSSortDescriptor*>* descriptorsArray;
@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;
@property (assign, nonatomic) NSTimeInterval timeout;
@property (assign, nonatomic) INTULocationRequestID locationRequestID;
@end


@implementation CatalogRestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.desiredAccuracy = INTULocationAccuracyBlock;
    self.timeout = 10.0;
    self.locationRequestID = NSNotFound;
    
//    self.managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
    }
    
    screenWidth = self.view.bounds.size.width;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.newsButton.enabled = NO;
    self.newsButton.hidden = YES;
    
    arrayOffers = [NSMutableArray array];
    
    avgBill = 950;
    
    isClear = YES;
    
    cityName = [RNUser sharedInstance].currCity;
    
    workRestons = _restons;
    
    [self getMinAndMaxAvgBill];

    lang = [[DPLocalizationManager supportedLanguages] mutableCopy];
    [lang removeObject:@"Base"];
    
    lang = [@[@"ru",
              @"uk"] mutableCopy];
    
    [self setNavigationBarItem];
    
    self.title = DPLocalizedString(@"catalogue_rest", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    sortTypes = @[@"by_popularity", @"by_name", @"expensive_cheap", @"cheap_expensive", @"by_novelty", @"at_discount"];
    arrCity = @[@"Kiev", @"Lviv", @"Ivano-Frankivsk", @"Kharkiv"];
    arrCityNumber = @[@"1", @"6", @"8", @"9"];
    filterActionArr = @[@"to_filters", @"cancel_filters"];
    
//    _locationManager = [[CLLocationManager alloc] init];
//    _locationManager.delegate = self;
//    _locationManager.distanceFilter = kCLDistanceFilterNone;
//    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
//    if ([RNUser sharedInstance].isGeoEnabled){
//
//        [self loadUserLocation];
//    } else {
//
//        [self checkLocationServicesAndStartUpdates];
//    }
    
    [self startSingleLocationRequest];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupFilterTypeRest:)
                                                 name:TypeFilterChangeNotification
                                               object:nil];
    
    if (typeFilter == NO){
        
    }
    
    self.orderInfo = [[RNOrderInfo alloc] init];
    if (_restaurant != nil) {
        _orderInfo.restaurant = _restaurant;
    }
    
    
    
    if ([cityName isEqualToString:@"1"] || [cityName isEqualToString:@"9"]){
        isCityHasSubway = YES;
    } else {
        isCityHasSubway = NO;
    }
    
    loading = NO;
    isSearch = NO;

    
    UIBarButtonItem *brandBackButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [brandBackButton setTintColor:[UIColor whiteColor]];
    
    self.navigationController.navigationBar.topItem.backBarButtonItem = brandBackButton;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    isFirst = YES;
    
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    [doneButton setTintColor:[[UIColor darkGrayColor]colorWithAlphaComponent:0.8f]];
    
    _searchField.inputAccessoryView = keyboardDoneButtonView;
    _searchField.delegate = self;
    
    
    [self setupView];
    
    [self getOffersAndNews];
    
    [self processData];
}


#pragma mark - CLLocation methods

- (void)startSingleLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:self.desiredAccuracy
                                                                timeout:self.timeout
                                                   delayUntilAuthorized:YES
                                                                  block:
                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                  __typeof(weakSelf) strongSelf = weakSelf;
                                  
                                  if (status == INTULocationStatusSuccess) {
                                      NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                      currentUserLocation = currentLocation;
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request successful!. Current address:\n%@", address);
                                          [self sendSearchRequest];
                                      }];
                                  }
                                  else if (status == INTULocationStatusTimedOut) {
                                      
                                      NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      currentUserLocation = currentLocation;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request timed out. Current address:\n%@", address);
                                          [self sendSearchRequest];
                                      }];
                                  }
                                  else {
                                      
                                      [RNUser sharedInstance].isGeoEnabled = NO;

                                  }
                                  
                                  strongSelf.locationRequestID = NSNotFound;
                              }];
}

-(double) getDistanceToRest:(CLLocationCoordinate2D)restCoord{

    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:restCoord.latitude longitude:restCoord.longitude];
    
    CLLocationDistance distance = [currentUserLocation distanceFromLocation:loc2];
    
    return distance;
}


#pragma mark - NSFetchedResultsController methods

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    self.managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
    self.fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"CDRestaurant"
                inManagedObjectContext:self.managedObjectContext];
    
    [self.fetchRequest setEntity:description];
    
    self.descriptorsArray = @[];
    NSSortDescriptor * intCatalogPriorityDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"intCatalogPriority" ascending:YES];
    NSSortDescriptor * viewsPriorityDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"views" ascending:NO];
    self.descriptorsArray = @[intCatalogPriorityDescriptor, viewsPriorityDescriptor];

    [self.fetchRequest setSortDescriptors:self.descriptorsArray];
    [self.fetchRequest setFetchBatchSize:12];
    
    
    self.predicateCity = [NSPredicate predicateWithFormat:@"city == %@", [RNUser sharedInstance].currCity];
    [self.fetchRequest setPredicate:self.predicateCity];
    
//    NSError *fetchError = nil;
//    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&fetchError];
//    NSLog(@"fetchedObjects.count %ld",fetchedObjects.count);
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}


- (void) getOffersAndNews{
    if ([self isInternetConnect]){
        
        [arrayOffers removeAllObjects];

        if (arrayOffers.count == 0){
            
            [self fetchNewsWithCompletion:^(bool nextOper) {
                if (nextOper == YES) {
                    [self fetchNewsAndOffersWithCompletion:^(NSInteger count) {
                        [self sortNewsAndOffersByDateAdd];
                        [self setCountNewsAndOffers:count];
                    }];
                }
            }];
        }
    }
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)doneClicked:(id)sender {
    [_searchField resignFirstResponder];
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [_searchField resignFirstResponder];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    if (workRestons.count == _restons.count){
        
        [self.filterButton setTintColor:[UIColor colorWithHexString:@"#8E8E93"]];
        
        isClear = YES;
    } else {
        
        [self.filterButton setTintColor:mainColor];
        
        isClear = NO;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void) setupFilterTypeRest:(NSNotification*)notif{
    typeFilter = YES;
    
    if (isPhone){
        
        if ([notif.userInfo objectForKey:@"typeFilter"]){
        FilterInfo* info = [notif.userInfo objectForKey:@"typeFilter"];
        avgBill = [info.avgBill integerValue];
        workRestons = info.workRestons;
        [self getMinAndMaxAvgBill];
        [self setupColorForFilter];
        if (_restons.count > 0){
            [self.tableView reloadData];
        }
      }
    } else {

        if ([notif.userInfo objectForKey:@"typeFilter"]){
        FilterInfo* info = [notif.userInfo objectForKey:@"typeFilter"];
        workRestons = info.workRestons;
        [self getMinAndMaxAvgBill];
        avgBill = [info.avgBill integerValue];
        [self setupColorForFilter];
        if (_restons.count > 0){
            [self.collectionView reloadData];
        }
      }
    }
}

-(void) setupColorForFilter{
    
    if (workRestons.count == _restons.count){
        
        [self.filterButton setTintColor:[UIColor colorWithHexString:@"#8E8E93"]];
        
        isClear = YES;
    } else {
        
        [self.filterButton setTintColor:mainColor];
        
        isClear = NO;
    }
}

-(Boolean) filterDistricForRest:(RNRestaurant*)rest{
    
    NSString *strDistric = [NSString stringWithFormat:@"%@",rest.distric];
//    NSLog(@"reston.distric -----------------%@", strDistric);
    
    if (filter.distric.count == 0 || ([[filter.distric firstObject]  isEqual: @""])){
//        NSLog(@"filter.distric.count == 0");
        return YES;
    } else if (filter.distric.count > 0){
        typeFilter = YES;
//        NSLog(@"filter.distric %@",filter.distric);
        for (NSString* str in filter.distric){
//            NSLog(@"filter.distric *strDistric -----------------%@", str);
            if([strDistric isEqualToString:str]){
                
                return YES;
            }
        }
    }
    return NO;
}

-(Boolean) filterOptionForRest:(RNRestaurant*)rest{
    
    NSArray *arrOptions = rest.options;
    
    if (filter.options.count == 0 || ([[filter.options firstObject]  isEqual: @""])){
        
        return YES;
        
    } else if (filter.options.count > 0){
        typeFilter = YES;
        
//        NSLog(@"filter.options -----------------%@", filter.options);
        
        for (NSString* str in filter.options){
            for(NSString* strOption in arrOptions){
//                NSLog(@"str -----------------%@", str);
//                NSLog(@"strOption -----------------%@", strOption);
                if([strOption isEqualToString:str]){
                    
                    return YES;
                }
            }
        }
    }
    return NO;
}

-(Boolean) filterKitchenForRest:(RNRestaurant*)rest{
    
    NSArray *arrKitchen = rest.kitchen;
    
    if (filter.kitchen.count == 0 || ([[filter.kitchen firstObject]  isEqual: @""])){
        
        return YES;
        
    } else if (filter.kitchen.count > 0){
        typeFilter = YES;
        
        for (NSString* str in filter.kitchen){
            for(NSString* strKitchen in arrKitchen){
//                NSLog(@"reston Kitchen -----------------%@", strKitchen);
//                NSLog(@"filter.kitchen -----------------%@", str);
                if([strKitchen isEqualToString:str]){
                    
                    return YES;
                }
            }
        }
    }
    return NO;
}

-(Boolean) filterSubwayForRest:(RNRestaurant*)rest{
    
    NSString *strSubway = rest.subwayName;

    if (filter.subway.count == 0 || ([[filter.subway firstObject]  isEqual: @""])){
        return YES;
    } else if (filter.subway.count > 0){
        typeFilter = YES;
        
        for (NSString* str in filter.subway){
            
            if([strSubway isEqualToString:str]){
                
                return YES;
            }
        }
    }
    return NO;
}

-(Boolean) filterTypeForRest:(RNRestaurant*)rest{
    
    
    NSString *strType = rest.type;

    if (filter.typeRest.count == 0 || ([[filter.typeRest firstObject]  isEqual: @""])){
        
        return YES;
    } else if (filter.typeRest.count > 0){
        typeFilter = YES;
        
        for (NSString* tn in filter.typeRest){
            if([tn isEqualToString:strType]){
                
                return YES;
            }
        }
    }
    return NO;
}


- (void)sendSearchRequestWithCity:(NSString*)cityNum andLang:(NSString*)language{
//    if (loading) {
//        return;
//    }
//    loading = YES;
//    [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
//    RNGetRestaurantsRequest *request = [[RNGetRestaurantsRequest alloc] init];
//    request.name = @"";
//    request.isAuth = _isAuth;
//
//    request.city = cityNum;
//
//    NSString* langCode = [[NSString alloc] init];
//
//    if ([language isEqualToString:@"ru"]){
//        langCode = @"ru";
//    } else if ([language isEqualToString:@"uk"]){
//        langCode = @"ua";
//    }
//    request.langCode = langCode;
//
//    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
//
//        loading = NO;
//        if (error != nil || [response isKindOfClass:[NSNull class]]) {
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//            return;
//        }
//        RNGetRestaurantsResponse *r = (RNGetRestaurantsResponse *)response;
//        [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];
//
//        NSArray* tempArr = r.restrauntsArray;
//
//        _restons = [tempArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//
//
//            RNRestaurant *r1 = obj1;
//            RNRestaurant *r2 = obj2;
//
//            return [@(r1.intCatalogPriority) compare:@(r2.intCatalogPriority)];
//
//        }];
//
//        workRestons = _restons;
//
        [self getOffersAndNews];
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:RestonsChangeNotification object:nil userInfo:@{@"restons" : _restons}];
//
//        isSearch = YES;

        [self setupCityName];
        self.searchField.placeholder = DPLocalizedString(@"search_place", nil);
    
//        [self reloadTableAndCollection];
        if (backCityView){
            [backCityView removeFromSuperview];
        }
        if (backLangView){
            [backLangView removeFromSuperview];
        }
}

-(void) searchReloadTableAndCollectionWithArray:(NSArray*)workArray{
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        _restons = workArray;

        if (_restons > 0){
            [self.tableView reloadData];
        }
    } else {

        _restons = workArray;
        if (_restons > 0){
            [self.collectionView reloadData];
        }
    }
}

#pragma mark - UISearchBarDelegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    [self searchReloadTableAndCollectionWithArray:workRestons];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if (searchBar.text.length == 0){
        
    } else {
        
        NSMutableArray *tmp = NSMutableArray.new;
        if (workRestons.count > 0){
            for(RNRestaurant *r in workRestons)
            {
                NSString *strName = r.title;
                
                if( ([strName rangeOfString:searchBar.text  options:NSCaseInsensitiveSearch].location != NSNotFound ) &&
                   ![tmp containsObject:r])
                    [tmp addObject:r];
            }
        }
        
        if (tmp.count > 0){
            
            [self searchReloadTableAndCollectionWithArray:[NSArray arrayWithArray:tmp]];
        }
    }}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0){
        
        [self searchReloadTableAndCollectionWithArray:workRestons];
    } else {
        
        NSMutableArray *tmp = NSMutableArray.new;
        if (workRestons.count > 0){
            for(RNRestaurant *r in workRestons)
            {
                NSString *strName = r.title;
                
                if( ([strName rangeOfString:searchText  options:NSCaseInsensitiveSearch].location != NSNotFound ) &&
                   ![tmp containsObject:r])
                    [tmp addObject:r];
            }
        }
        
        if (tmp.count > 0){
            
            [self searchReloadTableAndCollectionWithArray:[NSArray arrayWithArray:tmp]];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

-(void) reloadTableAndCollection{
    
    if (isPhone){

        isSearch = YES;

        [self setupColorForFilter];
        if (_restons.count > 0){
            [self.tableView reloadData];
        }
    } else {

        isSearch = YES;

        [self setupColorForFilter];
        if (_restons.count > 0){
            [self.collectionView reloadData];
        }
    }
}

- (void)reloadData{
    if (isPhone){
        [self.tableView reloadData];
    } else {
        [self.collectionView reloadData];
    }
}

- (void)sendSearchRequest {
    
    [self reloadTableAndCollection];
}

- (void)showAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) openFilters{
    
//    NSLog(@"Filters!!!!");
    
    [self.filterButton setTintColor:mainColor];
    
    FilterViewController * filterViewController = [storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    filterViewController.isCityHasSubway = isCityHasSubway;
    filterViewController.currCity = cityName;
    filterViewController.allRestons = _restons;
    filterViewController.workRestons = workRestons;
    filterViewController.isClear = isClear;
//    NSLog(@"filterViewController.avgBill %ld",avgBill);
    filterViewController.avgBill = avgBill;
    filterViewController.minAvgBill = minAvgBill;
    filterViewController.maxAvgBill = maxAvgBill;
    
//    NSLog(@"allRestons.count %ld workRestons.count %ld",(unsigned long)_restons.count, (long)workRestons.count);
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:filterViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}


- (IBAction)filtrButton:(id)sender {
    if ([self isInternetConnect]){
        if (isClear == YES){
            
            [self openFilters];
            
        } else if (isClear == NO){
            [self setupFilterView];
        }
    }
}


- (void)processData {
    if (filterType == RNFilterTypePopular || filterType == RNFilterTypeAVGBillDescending || filterType == RNFilterTypeAVGBillAscending || filterType == RNFilterTypeDiscount || filterType == RNFilterTypeAlphabet || filterType == RNFilterTypeNews) {
        
        [self sortData];
    }
}

-(void) setupFilterType:(id)sender{
    
    filterType = [sender tag];
    
    [sortButtonView removeFromSuperview];
    
    currentFilterType = [sender tag];
    
    [self setupSortButtons];
    
    [self processData];
    [backSortView removeFromSuperview];
}

- (IBAction) sortButtonAction:(id)sender {
    if ([self isInternetConnect]){
        [self setupSortView];
    }
}


-(void) sortBack:(id) sender{
    [backSortView removeFromSuperview];
}

- (void)setupSortView {
    
    backSortView = [[UIView alloc]initWithFrame:self.view.frame];
    backSortView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:backSortView];
    
    UIButton* backButton = [[UIButton alloc] init];
    
    backButton.translatesAutoresizingMaskIntoConstraints = false;
    [backSortView addSubview:backButton];
    
    sortView = [[UIView alloc]init];
    sortView.backgroundColor = [UIColor whiteColor];
    
    [sortView.layer setShadowColor: [UIColor grayColor].CGColor];
    [sortView.layer setShadowOpacity:0.8];
    [sortView.layer setShadowRadius:3.0];
    [sortView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backSortView addSubview:sortView];
    
    sortView.translatesAutoresizingMaskIntoConstraints = false;
    
    [self setupSortButtons];
    CGFloat heightSortView = 31.0f;
    
    if (isPhone) {
        heightSortView = sortTypes.count * 31;
        if (heightSortView > 186) {
            
            heightSortView = 186;
        }
        
        [sortView.widthAnchor constraintEqualToConstant:190].active = YES;
        [sortView.heightAnchor constraintEqualToConstant:heightSortView].active = YES;
        
    } else {
        heightSortView = sortTypes.count * 48;
        if (heightSortView > 288) {
            
            heightSortView = 288;
        }
        
        [sortView.widthAnchor constraintEqualToConstant:290].active = YES;
        [sortView.heightAnchor constraintEqualToConstant:heightSortView].active = YES;
    }
    
    [backButton.topAnchor constraintEqualToAnchor:backSortView.topAnchor].active = YES;
    [backButton.leftAnchor constraintEqualToAnchor:backSortView.leftAnchor].active = YES;
    [backButton.widthAnchor constraintEqualToAnchor:backSortView.widthAnchor].active = YES;
    [backButton.heightAnchor constraintEqualToAnchor:backSortView.heightAnchor].active = YES;
    
    [sortView.topAnchor constraintEqualToAnchor:_sortButton.topAnchor constant:-1].active = YES;
    [sortView.leftAnchor constraintEqualToAnchor:_sortButton.leftAnchor constant:-15].active = YES;
    
    [backButton addTarget:self action:@selector(sortBack:) forControlEvents:UIControlEventTouchUpInside];
}


-(void) setupSortButtons{
    
    sortButtonView = [[UIView alloc]init];
    sortButtonView.backgroundColor = [UIColor whiteColor];
    
    [sortView addSubview:sortButtonView];
    
    sortButtonView.translatesAutoresizingMaskIntoConstraints = false;
    
    CGFloat heightRow = 31.0;
    CGFloat widthRow = 190.0;
    CGFloat fontSize = 15;
    CGFloat topMargin = 20;
    
    if (isPhone) {
        heightRow = 31.0;
        widthRow = 190.0;
        fontSize = 15;
        topMargin = 20;
        
    } else {
        heightRow = 48.0;
        widthRow = 290.0;
        fontSize = 22;
        topMargin = 36;
    }
    
    
    for (int i = 0; i < sortTypes.count; i++) {
        
        
        UIButton* sortButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0 + heightRow*i, widthRow, topMargin)];
        [sortButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        sortButton.tag = i;
        if (currentFilterType == i){
            [sortButton setTitleColor:mainColor forState:UIControlStateNormal];
        } else {
            [sortButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        [sortButton setTitle:DPLocalizedString(sortTypes[i], nil)  forState:UIControlStateNormal];
        sortButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        
        sortButton.userInteractionEnabled = YES;
        [sortButtonView addSubview:sortButton];
        
        [sortButton addTarget:self action:@selector(setupFilterType:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [sortButtonView.topAnchor constraintEqualToAnchor:sortView.topAnchor].active = YES;
    [sortButtonView.leftAnchor constraintEqualToAnchor:sortView.leftAnchor].active = YES;
    [sortButtonView.widthAnchor constraintEqualToAnchor:sortView.widthAnchor].active = YES;
    [sortButtonView.heightAnchor constraintEqualToAnchor:sortView.heightAnchor].active = YES;
}

-(void) setupCityName{
    
    if ([[RNUser sharedInstance].realCity isEqualToString:[RNUser sharedInstance].currCity]){
//        NSLog(@"realCity %@ Equal++++++++  cityName %@", [RNUser sharedInstance].realCity, [RNUser sharedInstance].currCity);
        [RNUser sharedInstance].isThisCity = YES;
        [RNUser sharedInstance].isGeoEnabled = YES;
//        NSLog(@"realCity %@ Equal++++++++  cityName %@  [RNUser sharedInstance].isThisCity %d", [RNUser sharedInstance].realCity, [RNUser sharedInstance].currCity, [RNUser sharedInstance].isThisCity);
    } else {
//        NSLog(@"realCity %@ not Equal++++++++  cityName %@", [RNUser sharedInstance].realCity, [RNUser sharedInstance].currCity);
        [RNUser sharedInstance].isThisCity = NO;
        [RNUser sharedInstance].isGeoEnabled = NO;
//        NSLog(@"realCity %@ not Equal++++++++  cityName %@  [RNUser sharedInstance].isThisCity %d", [RNUser sharedInstance].realCity, [RNUser sharedInstance].currCity, [RNUser sharedInstance].isThisCity);
    }
    
    if ([cityName isEqualToString:@"1"]){
        [self.cityButton setTitle:DPLocalizedString(@"Kiev", nil) forState:UIControlStateNormal];
        isCityHasSubway = YES;
    } else if ([cityName isEqualToString:@"9"]){
        [self.cityButton setTitle:DPLocalizedString(@"Kharkiv", nil) forState:UIControlStateNormal];
        isCityHasSubway = YES;
    } else if ([cityName isEqualToString:@"6"]){
        [self.cityButton setTitle:DPLocalizedString(@"Lviv", nil) forState:UIControlStateNormal];
        isCityHasSubway = NO;
    } else if ([cityName isEqualToString:@"8"]){
        [self.cityButton setTitle:DPLocalizedString(@"Ivano-Frankivsk", nil) forState:UIControlStateNormal];
        isCityHasSubway = NO;
    } else {
        [self.cityButton setTitle:DPLocalizedString(@"Kiev", nil) forState:UIControlStateNormal];
        isCityHasSubway = YES;
    }
}


-(void) setupCurrCity:(id)sender{
    
    if (currentTag != [sender tag]){
        
        cityName = arrCityNumber[[sender tag]];
        [RNUser sharedInstance].currCity = arrCityNumber[[sender tag]];
        
        self.predicateCity = [NSPredicate predicateWithFormat:@"city == %@", cityName];
        [self prepareRestaurantsFetchRequest];
        
        currentTag = [sender tag];
        [self setupCityName];
        
        [cityButtonView removeFromSuperview];
        [self setupCityButtonsIsEnable:NO];
        cityBackButton.userInteractionEnabled = NO;
        if (backCityView){
            [backCityView removeFromSuperview];
        }
    }
}


- (IBAction) cityButtonAction:(id)sender {
        [self setupCityView];
}


-(void) cityBack:(id) sender{
    [backCityView removeFromSuperview];
}

- (void)setupCityView {
    
    backCityView = [[UIView alloc]initWithFrame:self.view.frame];
    backCityView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:backCityView];
    
    cityBackButton = [[UIButton alloc] init];
    cityBackButton.userInteractionEnabled = YES;
    cityBackButton.translatesAutoresizingMaskIntoConstraints = false;
    [backCityView addSubview:cityBackButton];
    
    
    cityView = [[UIView alloc]init];
    cityView.backgroundColor = [UIColor whiteColor];
    
    [cityView.layer setShadowColor: [UIColor grayColor].CGColor];
    [cityView.layer setShadowOpacity:0.8];
    [cityView.layer setShadowRadius:3.0];
    [cityView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backCityView addSubview:cityView];
    
    cityView.translatesAutoresizingMaskIntoConstraints = false;
    
    [self setupCityButtonsIsEnable:YES];
    
    CGFloat heightCityView = arrCity.count * 31 + 10;
    
    if (isPhone) {
        heightCityView = arrCity.count * 31 + 10;
        
        [cityView.widthAnchor constraintEqualToConstant:190].active = YES;
        [cityView.heightAnchor constraintEqualToConstant:heightCityView].active = YES;
        
    } else {
        heightCityView = arrCity.count * 48 + 10;
        
        [cityView.widthAnchor constraintEqualToConstant:290].active = YES;
        [cityView.heightAnchor constraintEqualToConstant:heightCityView].active = YES;
    }
    
    
    [cityBackButton.topAnchor constraintEqualToAnchor:backCityView.topAnchor].active = YES;
    [cityBackButton.leftAnchor constraintEqualToAnchor:backCityView.leftAnchor].active = YES;
    [cityBackButton.widthAnchor constraintEqualToAnchor:backCityView.widthAnchor].active = YES;
    [cityBackButton.heightAnchor constraintEqualToAnchor:backCityView.heightAnchor].active = YES;
    
    [cityView.topAnchor constraintEqualToAnchor:_cityButton.topAnchor constant:-1].active = YES;
    [cityView.rightAnchor constraintEqualToAnchor:_cityButton.rightAnchor constant:15].active = YES;
    
    [cityBackButton addTarget:self action:@selector(cityBack:) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setupCityButtonsIsEnable:(BOOL)isEnable{
    
    cityButtonView = [[UIView alloc]init];
    cityButtonView.backgroundColor = [UIColor whiteColor];
    
    [cityView addSubview:cityButtonView];
    
    cityButtonView.translatesAutoresizingMaskIntoConstraints = false;
    
    CGFloat heightRow = 31.0;
    CGFloat widthRow = 190.0;
    CGFloat fontSize = 15;
    CGFloat topMargin = 20;
    
    if (isPhone) {
        heightRow = 31.0;
        widthRow = 190.0;
        fontSize = 15;
        topMargin = 20;
        
    } else {
        heightRow = 48.0;
        widthRow = 290.0;
        fontSize = 22;
        topMargin = 36;
    }
    
    for (int i = 0; i < arrCity.count; i++) {
        
        
        UIButton* cityButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 10 + heightRow*i, widthRow, topMargin)];
        [cityButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        cityButton.tag = i;
        if (cityName == arrCityNumber[i]){
            [cityButton setTitleColor:mainColor forState:UIControlStateNormal];
            currentTag = i;
        } else {
            [cityButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        [cityButton setTitle:DPLocalizedString(arrCity[i], nil) forState:UIControlStateNormal];
        cityButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        if (isEnable){
            cityButton.userInteractionEnabled = YES;
        } else {
            cityButton.userInteractionEnabled = NO;
        }
        
        
        [cityButtonView addSubview:cityButton];
        
        [cityButton addTarget:self action:@selector(setupCurrCity:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [cityButtonView.topAnchor constraintEqualToAnchor:cityView.topAnchor].active = YES;
    [cityButtonView.leftAnchor constraintEqualToAnchor:cityView.leftAnchor].active = YES;
    [cityButtonView.widthAnchor constraintEqualToAnchor:cityView.widthAnchor].active = YES;
    [cityButtonView.heightAnchor constraintEqualToAnchor:cityView.heightAnchor].active = YES;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupCurrLang:(id)sender{
    
    if (currentTag != [sender tag]){
        
        currentTag = [sender tag];
        currLang = lang[currentTag];
        
        langBackButton.userInteractionEnabled = NO;
        
        [langButtonView removeFromSuperview];
        [self setupLangButtonsIsEnable:NO];
        
        dp_set_current_language(currLang);
        [[NSUserDefaults standardUserDefaults] setObject:currLang forKey:languageOnApp];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString* langName = [NSString string];
        
        if ([currLang isEqualToString:@"ru"]){
            langName = @"РУС";
        } else if ([currLang isEqualToString:@"uk"]){
            langName = @"УКР";
        }
        [self.langButton setTitle:langName forState:UIControlStateNormal];
        self.title = DPLocalizedString(@"catalogue_rest", nil);
        
        if ([self isInternetConnect]){
            [self sendSearchRequestWithCity:cityName andLang:currLang];
        }
    }
}


- (IBAction) langButtonAction:(id)sender {
    if ([self isInternetConnect]){
        [self setupLangView];
    }
}


-(void) langBack:(id) sender{
    [backLangView removeFromSuperview];
}

- (void)setupLangView {
    
    backLangView = [[UIView alloc]initWithFrame:self.view.frame];
    backLangView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:backLangView];
    
    langBackButton = [[UIButton alloc] init];
    
    langBackButton.translatesAutoresizingMaskIntoConstraints = false;
    [backLangView addSubview:langBackButton];
    
    
    langView = [[UIView alloc]init];
    langView.backgroundColor = [UIColor whiteColor];
    
    [langView.layer setShadowColor: [UIColor grayColor].CGColor];
    [langView.layer setShadowOpacity:0.8];
    [langView.layer setShadowRadius:3.0];
    [langView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backLangView addSubview:langView];
    
    langView.translatesAutoresizingMaskIntoConstraints = false;
    
    [self setupLangButtonsIsEnable:YES];
    
    CGFloat heightLangView = lang.count * 31 + 10;
    
    if (isPhone) {
        heightLangView = lang.count * 31 + 10;
        
        [langView.widthAnchor constraintEqualToConstant:120].active = YES;
        [langView.heightAnchor constraintEqualToConstant:heightLangView].active = YES;
        
    } else {
        heightLangView = lang.count * 48 + 10;
        
        [langView.widthAnchor constraintEqualToConstant:190].active = YES;
        [langView.heightAnchor constraintEqualToConstant:heightLangView].active = YES;
    }
    
    
    [langBackButton.topAnchor constraintEqualToAnchor:backLangView.topAnchor].active = YES;
    [langBackButton.leftAnchor constraintEqualToAnchor:backLangView.leftAnchor].active = YES;
    [langBackButton.widthAnchor constraintEqualToAnchor:backLangView.widthAnchor].active = YES;
    [langBackButton.heightAnchor constraintEqualToAnchor:backLangView.heightAnchor].active = YES;
    
    [langView.topAnchor constraintEqualToAnchor:_langButton.topAnchor constant:-1].active = YES;
    [langView.rightAnchor constraintEqualToAnchor:_langButton.rightAnchor constant:2].active = YES;
    
    [langBackButton addTarget:self action:@selector(langBack:) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setupLangButtonsIsEnable:(BOOL)isEnable{
    
    langButtonView = [[UIView alloc]init];
    langButtonView.backgroundColor = [UIColor whiteColor];
    
    [langView addSubview:langButtonView];
    
    langButtonView.translatesAutoresizingMaskIntoConstraints = false;
    
    CGFloat heightRow = 31.0;
    CGFloat widthRow = 120.0;
    CGFloat fontSize = 15;
    CGFloat topMargin = 20;
    
    if (isPhone) {
        heightRow = 31.0;
        widthRow = 120.0;
        fontSize = 15;
        topMargin = 20;
        
    } else {
        heightRow = 48.0;
        widthRow = 190.0;
        fontSize = 22;
        topMargin = 36;
    }
    
    for (int i = 0; i < lang.count; i++) {
        
        UIButton* langButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 10 + heightRow*i, widthRow, topMargin)];
        [langButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        langButton.tag = i;
        if ([lang[i] isEqualToString:dp_get_current_language()]){
            [langButton setTitleColor:mainColor forState:UIControlStateNormal];
            currentTag = i;
            
        } else {
            [langButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        NSString *buttonTitle = [[NSString alloc] init];
        buttonTitle = [NSString stringWithFormat:@"language%@",[lang[i] uppercaseString]];
        buttonTitle = DPLocalizedString(buttonTitle, nil);
        [langButton setTitle:buttonTitle forState:UIControlStateNormal];
        langButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        if (isEnable){
            langButton.userInteractionEnabled = YES;
        } else {
            langButton.userInteractionEnabled = NO;
        }
        
        
        [langButtonView addSubview:langButton];
        
        [langButton addTarget:self action:@selector(setupCurrLang:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [langButtonView.topAnchor constraintEqualToAnchor:langView.topAnchor].active = YES;
    [langButtonView.leftAnchor constraintEqualToAnchor:langView.leftAnchor].active = YES;
    [langButtonView.widthAnchor constraintEqualToAnchor:langView.widthAnchor].active = YES;
    [langButtonView.heightAnchor constraintEqualToAnchor:langView.heightAnchor].active = YES;
}

////////////////////////////////////////////////////////////////////////////////////////

-(void) setupCurrFilterAction:(id)sender{
    
    if ([sender tag] == 0){
        
        [self openFilters];
        [filtersView removeFromSuperview];
        //        [filterButtonView removeFromSuperview];
        //        filterBackButton.userInteractionEnabled = NO;
        
    } else if ([sender tag] == 1){
        
        [filtersView removeFromSuperview];
        
        //        filterBackButton.userInteractionEnabled = NO;
        
        isClear = YES;
        
        workRestons = _restons;
        [self getMinAndMaxAvgBill];
        avgBill = 950;
        
        [self.filterButton setTintColor:[UIColor colorWithHexString:@"#8E8E93"]];
        
        if (isPhone){
            
            [self.tableView reloadData];

        } else {
            [self.collectionView reloadData];
        }
        
    }
    [filterButtonView removeFromSuperview];
}


-(void) filterBack:(id) sender{
    [filtersView removeFromSuperview];
    [backFilterView removeFromSuperview];
}

- (void)setupFilterView {
    
    backFilterView = [[UIView alloc]initWithFrame:self.view.frame];
    backFilterView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:backFilterView];
    
    filterBackButton = [[UIButton alloc] init];
    filterBackButton.userInteractionEnabled = YES;
    filterBackButton.translatesAutoresizingMaskIntoConstraints = false;
    [backFilterView addSubview:filterBackButton];
    
    
    filtersView = [[UIView alloc]init];
    filtersView.backgroundColor = [UIColor whiteColor];
    
    [filtersView.layer setShadowColor: [UIColor grayColor].CGColor];
    [filtersView.layer setShadowOpacity:0.8];
    [filtersView.layer setShadowRadius:3.0];
    [filtersView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [backFilterView addSubview:filtersView];
    
    filtersView.translatesAutoresizingMaskIntoConstraints = false;
    
    [self setupFilterButtons];
    
    CGFloat heightFilterView = filterActionArr.count * 31 + 10;
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        heightFilterView = filterActionArr.count * 31 + 10;
        
        [filtersView.widthAnchor constraintEqualToConstant:190].active = YES;
        [filtersView.heightAnchor constraintEqualToConstant:heightFilterView].active = YES;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        heightFilterView = filterActionArr.count * 48 + 10;
        
        [filtersView.widthAnchor constraintEqualToConstant:290].active = YES;
        [filtersView.heightAnchor constraintEqualToConstant:heightFilterView].active = YES;
    }
    
    
    [filterBackButton.topAnchor constraintEqualToAnchor:backFilterView.topAnchor].active = YES;
    [filterBackButton.leftAnchor constraintEqualToAnchor:backFilterView.leftAnchor].active = YES;
    [filterBackButton.widthAnchor constraintEqualToAnchor:backFilterView.widthAnchor].active = YES;
    [filterBackButton.heightAnchor constraintEqualToAnchor:backFilterView.heightAnchor].active = YES;
    
    [filtersView.topAnchor constraintEqualToAnchor:_filterButton.topAnchor constant:-1].active = YES;
    [filtersView.leftAnchor constraintEqualToAnchor:_filterButton.leftAnchor constant:-5].active = YES;
    
    
    
    [filterBackButton addTarget:self action:@selector(filterBack:) forControlEvents:UIControlEventTouchUpInside];
}

-(void) setupFilterButtons{
    
    filterButtonView = [[UIView alloc]init];
    filterButtonView.backgroundColor = [UIColor whiteColor];
    
    [filtersView addSubview:filterButtonView];
    
    filterButtonView.translatesAutoresizingMaskIntoConstraints = false;
    
    CGFloat heightRow = 31.0;
    CGFloat widthRow = 190.0;
    CGFloat fontSize = 15;
    CGFloat topMargin = 20;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        heightRow = 31.0;
        widthRow = 190.0;
        fontSize = 15;
        topMargin = 20;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        heightRow = 48.0;
        widthRow = 290.0;
        fontSize = 22;
        topMargin = 36;
    }
    
    for (int i = 0; i < filterActionArr.count; i++) {
        
        
        UIButton* filtButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 10 + heightRow*i, widthRow, topMargin)];
        [filtButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:fontSize]];
        filtButton.tag = i;
        //        if (filterActionName == filterActionArr[i]){
        [filtButton setTitleColor:mainColor forState:UIControlStateNormal];
        //            currentTag = i;
        //        } else {
        //            [filtButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        //        }
        [filtButton setTitle:DPLocalizedString(filterActionArr[i], nil) forState:UIControlStateNormal];
        filtButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        [filterButtonView addSubview:filtButton];
        
        [filtButton addTarget:self action:@selector(setupCurrFilterAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [filterButtonView.topAnchor constraintEqualToAnchor:filtersView.topAnchor].active = YES;
    [filterButtonView.leftAnchor constraintEqualToAnchor:filtersView.leftAnchor].active = YES;
    [filterButtonView.widthAnchor constraintEqualToAnchor:filtersView.widthAnchor].active = YES;
    [filterButtonView.heightAnchor constraintEqualToAnchor:filtersView.heightAnchor].active = YES;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)newsButton:(id)sender {
    NSLog(@"newsButton");
    [self newsButtonAction];
}

- (void)newsButtonOver:(id)sender {
    NSLog(@"newsButtonOver");
    [self newsButtonAction];
}

-(void) newsButtonAction{
    
    NewsExpandViewController * newsVC = [storyboard instantiateViewControllerWithIdentifier:@"NewsExpandViewController"];
    
    newsVC.title = DPLocalizedString(@"news_and_offers", nil);;
    newsVC.isAllRests = YES;
//    newsAll = [arrayOffers copy];
    newsVC.newsAll = newsAll;
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:newsVC];
    
    [self presentViewController:navVC animated:YES completion:nil];
}


- (IBAction)offersButton:(id)sender {
    [self offersButtonAction];
}

-(void) offersButtonAction{
    
    
}

-(void)configureRequest:(NSMutableURLRequest *)request
{
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
}

-(void)fetchNewsWithCompletion:(void (^)(bool nextOper))completion
{
    
    // Create the dispatch group
    dispatch_group_t serviceGroup = dispatch_group_create();
    
    // Start the first service
    
    NSLog(@"workRestons.count: %lu", workRestons.count);
    
    for (int i = 0; i < workRestons.count; i++) {
        RNRestaurant* rest = workRestons[i];
        
        RNGetNewsRequest *requestNews = [[RNGetNewsRequest alloc] init];
        requestNews.restaurantId = rest.restaurantId;
        dispatch_group_enter(serviceGroup);
        [requestNews sendWithcompletion:^(RNResponse *response, NSError *error) {
     
            RNGetNewsResponse *r = (RNGetNewsResponse *)response;
            newsCount = r.news.count;
            
            
            if (r.news.count > 0){
//                NSLog(@"r.news.count: %lu", (unsigned long)r.news.count);
                for (RNNEw* new in r.news){
                    
                    new.restaurant = rest;
                    new.imageFullString = @"http://reston.com.ua/uploads/img/news/original/";
                    [arrayOffers addObject: new];
//                    new.restID = rest.restaurantId;
                }
            }
     
//            NSLog(@"arrayOffers.count: %ld", arrayOffers.count);

            dispatch_group_leave(serviceGroup);
        }];
    }

    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{
        
        completion(YES);
    });
}

-(void)fetchNewsAndOffersWithCompletion:(void (^)(NSInteger count))completion
{


    // Create the dispatch group
    dispatch_group_t serviceGroup = dispatch_group_create();

    // Start the first service
    
    for (int i = 0; i < workRestons.count; i++) {
        RNRestaurant* rest = (RNRestaurant*)workRestons[i];
        
        GetOffersRequest *request = [[GetOffersRequest alloc] init];
        request.restaurantId = rest.restaurantId;
        dispatch_group_enter(serviceGroup);
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {

            GetOffersResponse *r = (GetOffersResponse *)response;

            if (r.offers.count > 0){

                for (Offer* offer in r.offers){
                    RNNEw* new = [[RNNEw alloc] init];
                    
                    new.date = offer.dateStart;

                    new.image = [offer.image stringByReplacingOccurrencesOfString:@"https://reston.com.ua/uploads/img/offers/" withString:@""];

                    new.imageFullString = @"https://reston.com.ua/uploads/img/offers/";
                    new.text = offer.text;
                    if (new.text.length >= 60){
                        new.text = [new.text substringToIndex:NSMaxRange([new.text rangeOfComposedCharacterSequenceAtIndex:60])];
                        new.shortText = [NSString stringWithFormat:@"%@...", new.text];
                    } else {
                        new.shortText = offer.text;
                    }
                    
                    new.title = offer.title;

                    new.restaurant = rest;
                    [arrayOffers addObject: new];

                }
            }

            dispatch_group_leave(serviceGroup);
        }];
    }

    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{

        completion(arrayOffers.count);
    });
}

- (void)sortNewsAndOffersByDateAdd {
    
    
    
    newsAll = [arrayOffers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        RNNEw *r1 = obj1;
        RNNEw *r2 = obj2;
        
        return -1*[r1.date compare:r2.date];
    }];
}

- (void) setCountNewsAndOffers:(NSInteger)count {
    
    if (count > 0){
        NSLog(@"setCountNewsAndOffers count > 0");
        feedsBagde.backgroundColor = mainColor;
        
    } else if (count == 0){
        NSLog(@"setCountNewsAndOffers count == 0");
        feedsBagde.backgroundColor = [UIColor colorWithHexString:@"#D1D1D1"];
    }
    feedsCountL.text = [NSString stringWithFormat:@"%ld",(long)count];
}

-(void) setupFeddsBadgeForNewsAndOffers {

    self.newsButton.enabled = NO;
    self.newsButton.hidden = YES;

    newsAllView = [[UIView alloc] init];
    [self.filterContainerView addSubview:newsAllView];
    newsAllView.translatesAutoresizingMaskIntoConstraints = false;

    
    newsAllImageView = [[UIImageView alloc] init];
    newsAllImageView.image = [UIImage imageNamed:@"news_all"];
    newsAllImageView.translatesAutoresizingMaskIntoConstraints = false;
    newsAllImageView.alpha = 0.6f;
    [newsAllView addSubview:newsAllImageView];
    
    
    feedsBagde = [[UIView alloc] init];
    [newsAllImageView addSubview:feedsBagde];

    feedsCountL = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [feedsBagde addSubview:feedsCountL];
    feedsCountL.backgroundColor = [UIColor clearColor];
    feedsCountL.textColor = [UIColor whiteColor];
    feedsCountL.font = [UIFont fontWithName:@"Thonburi" size:8];
    feedsCountL.textAlignment = NSTextAlignmentCenter;
    feedsCountL.translatesAutoresizingMaskIntoConstraints = false;
    

        feedsBagde.translatesAutoresizingMaskIntoConstraints = false;
    
    if (isPhone){
        
        [newsAllView.topAnchor constraintEqualToAnchor:self.filterContainerView.topAnchor].active = YES;
        [newsAllView.leftAnchor constraintEqualToAnchor:self.mapButton.rightAnchor constant:15].active = YES;
        [newsAllView.bottomAnchor constraintEqualToAnchor:self.filterContainerView.bottomAnchor].active = YES;
        [newsAllView.widthAnchor constraintEqualToConstant:30].active = YES;
        
        [newsAllImageView.leftAnchor constraintEqualToAnchor:newsAllView.leftAnchor constant:0].active = YES;
        [newsAllImageView.bottomAnchor constraintEqualToAnchor:newsAllView.bottomAnchor constant:-3].active = YES;
        [newsAllImageView.heightAnchor constraintEqualToConstant:18].active = YES;
        [newsAllImageView.widthAnchor constraintEqualToConstant:18].active = YES;
        
        feedsBagde.layer.cornerRadius = 10;
        feedsBagde.layer.masksToBounds = YES;
        
        feedsCountL.font = [UIFont fontWithName:@"Thonburi" size:8];
        [feedsBagde.centerXAnchor constraintEqualToAnchor:self.newsButton.rightAnchor constant:0].active = YES;
        [feedsBagde.centerYAnchor constraintEqualToAnchor:self.newsButton.topAnchor constant:0].active = YES;
        [feedsBagde.heightAnchor constraintEqualToConstant:20].active = YES;
        [feedsBagde.widthAnchor constraintEqualToConstant:20].active = YES;
        
        [feedsCountL.centerXAnchor constraintEqualToAnchor:feedsBagde.centerXAnchor constant:0].active = YES;
        [feedsCountL.centerYAnchor constraintEqualToAnchor:feedsBagde.centerYAnchor constant:0].active = YES;
        [feedsCountL.heightAnchor constraintEqualToConstant:20].active = YES;
        [feedsCountL.widthAnchor constraintEqualToConstant:20].active = YES;
        
        newsPhoneButton = [[UIButton alloc] init];
        newsPhoneButton.translatesAutoresizingMaskIntoConstraints = false;
        newsPhoneButton.userInteractionEnabled = YES;
        [newsAllView addSubview:newsPhoneButton];
        [newsAllView bringSubviewToFront:newsPhoneButton];
        [newsPhoneButton addTarget:self action:@selector(newsButtonOver:) forControlEvents:UIControlEventTouchUpInside];
        
        [newsPhoneButton.centerXAnchor constraintEqualToAnchor:newsAllView.centerXAnchor constant:0].active = YES;
        [newsPhoneButton.centerYAnchor constraintEqualToAnchor:newsAllView.centerYAnchor].active = YES;
        [newsPhoneButton.widthAnchor constraintEqualToConstant:40].active = YES;
        [newsPhoneButton.heightAnchor constraintEqualToConstant:40].active = YES;
        
    } else {
        
        [newsAllView.centerXAnchor constraintEqualToAnchor:self.newsButton.centerXAnchor].active = YES;
        [newsAllView.centerYAnchor constraintEqualToAnchor:self.mapButton.centerYAnchor constant:0].active = YES;
        [newsAllView.heightAnchor constraintEqualToConstant:40].active = YES;
        [newsAllView.widthAnchor constraintEqualToConstant:40].active = YES;
        
        [newsAllImageView.centerXAnchor constraintEqualToAnchor:newsAllView.centerXAnchor constant:0].active = YES;
        [newsAllImageView.centerYAnchor constraintEqualToAnchor:newsAllView.centerYAnchor constant:0].active = YES;
        [newsAllImageView.heightAnchor constraintEqualToConstant:30].active = YES;
        [newsAllImageView.widthAnchor constraintEqualToConstant:30].active = YES;
        
        feedsBagde.layer.cornerRadius = 15;
        feedsBagde.layer.masksToBounds = YES;

        
        feedsCountL.font = [UIFont fontWithName:@"Thonburi" size:12];
        [feedsBagde.centerXAnchor constraintEqualToAnchor:self.newsButton.rightAnchor constant:-10].active = YES;
        [feedsBagde.centerYAnchor constraintEqualToAnchor:self.newsButton.topAnchor constant:10].active = YES;
        [feedsBagde.heightAnchor constraintEqualToConstant:30].active = YES;
        [feedsBagde.widthAnchor constraintEqualToConstant:30].active = YES;
        
        [feedsCountL.centerXAnchor constraintEqualToAnchor:feedsBagde.centerXAnchor constant:0].active = YES;
        [feedsCountL.centerYAnchor constraintEqualToAnchor:feedsBagde.centerYAnchor constant:0].active = YES;
        [feedsCountL.heightAnchor constraintEqualToConstant:20].active = YES;
        [feedsCountL.widthAnchor constraintEqualToConstant:30].active = YES;
        
        newsPhoneButton = [[UIButton alloc] init];
        newsPhoneButton.translatesAutoresizingMaskIntoConstraints = false;
        newsPhoneButton.userInteractionEnabled = YES;
        [newsAllView addSubview:newsPhoneButton];
        [newsAllView bringSubviewToFront:newsPhoneButton];
        [newsPhoneButton addTarget:self action:@selector(newsButtonOver:) forControlEvents:UIControlEventTouchUpInside];
        
        [newsPhoneButton.centerXAnchor constraintEqualToAnchor:newsAllView.centerXAnchor constant:0].active = YES;
        [newsPhoneButton.centerYAnchor constraintEqualToAnchor:newsAllView.centerYAnchor].active = YES;
        [newsPhoneButton.widthAnchor constraintEqualToConstant:30].active = YES;
        [newsPhoneButton.heightAnchor constraintEqualToConstant:30].active = YES;
    }
}




- (IBAction)mapButton:(id)sender {

    if ([self isInternetConnect]){

        MapViewController * mapViewController = [storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        CLLocationCoordinate2D userCoor2D = {.latitude =  latitude, .longitude =  longitude};

        mapViewController.userLocation = userCoor2D;

        mapViewController.restonsItems = workRestons;
        mapViewController.isAllRestons = YES;
        mapViewController.isFiltered = !isClear;

        mapViewController.cityName = cityName;
        
        UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:mapViewController];
        
        [self presentViewController:navVC animated:YES completion:nil];
    }
}


- (void)getMinAndMaxAvgBill{
    NSArray* minMaxRestons = [workRestons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        RNRestaurant *r1 = obj1;
        RNRestaurant *r2 = obj2;
        
        return r2.avgBill.integerValue - r1.avgBill.integerValue;
    }];
    RNRestaurant *maxRest = [minMaxRestons firstObject];
    maxAvgBill = [maxRest.avgBill integerValue];
    
    RNRestaurant *minRest = [minMaxRestons lastObject];
    minAvgBill = [minRest.avgBill integerValue];
}

- (void)sortData{
    switch (filterType) {
            
        case RNFilterTypePopular: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * intCatalogPriorityDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"intCatalogPriority" ascending:YES];
            NSSortDescriptor * viewsPriorityDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"views" ascending:NO];
            self.descriptorsArray = @[intCatalogPriorityDescriptor, viewsPriorityDescriptor];
            [self prepareRestaurantsFetchRequest];
        }
            break;
            
        case RNFilterTypeAVGBillDescending: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * avgBillDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"avgBill" ascending:NO];
            self.descriptorsArray = @[avgBillDescriptor];
            [self prepareRestaurantsFetchRequest];
        }
            break;
            
        case RNFilterTypeAVGBillAscending: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * avgBillDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"avgBill" ascending:YES];
            self.descriptorsArray = @[avgBillDescriptor];
            [self prepareRestaurantsFetchRequest];
        }
            break;
            
        case RNFilterTypeDiscount: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * discountsDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"discounts" ascending:NO];
            self.descriptorsArray = @[discountsDescriptor];
            [self prepareRestaurantsFetchRequest];
        }
            break;
            
        case RNFilterTypeAlphabet: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * titleDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
            self.descriptorsArray = @[titleDescriptor];
            [self prepareRestaurantsFetchRequest];
        }
            break;
            
        case RNFilterTypeNews: {
            
            self.descriptorsArray = @[];
            NSSortDescriptor * additionDateDescriptor =
            [[NSSortDescriptor alloc] initWithKey:@"addition_date" ascending:NO];
            self.descriptorsArray = @[additionDateDescriptor];
            
            [self prepareRestaurantsFetchRequest];
        }
            break;
        default:
            NSAssert(NO, @"Unsupported filterType");
            break;
    }
}

- (void)prepareRestaurantsFetchRequest {
    
    [self.fetchRequest setSortDescriptors:self.descriptorsArray];
    [self.fetchRequest setPredicate:self.predicateCity];
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Handle you error here
    }
    [self reloadData];
    NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    NSInteger rowHeight = 299;
    
    if (screenWidth < 322) {
        rowHeight = 289;
    }
    
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSLog(@"restaurants.count: %lu", (unsigned long)[sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RestonCell *cell = (RestonCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifierRestTableCell];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:reuseIdentifierRestTableCell owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CDRestaurant *rest = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureTableCell:cell withRestaurant:rest];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isInternetConnect]){

        CDRestaurant *currentRestoraunt = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        __block UIImage* backImage;
        NSArray *images = (NSArray*)currentRestoraunt.images;
        if (images.count >0){
            [UIImage cachedImage:images[0]
                   fullURLString:nil
                    withCallBack:^(UIImage *image) {
                        backImage = image;
                    }];
            
        } else {
            backImage = [UIImage imageNamed:@"restLogo"];
        }
        
        RestonItemViewController * restonItemViewController = [storyboard instantiateViewControllerWithIdentifier:@"RestonItemViewController"];
        restonItemViewController.restCD = currentRestoraunt;
        restonItemViewController.cityName = cityName;

        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:restonItemViewController];
        [self.navigationController presentViewController:navVC animated:YES completion:nil];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSLog(@"numberOfItemsInSection: %ld",[sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CDRestaurant *rest = [self.fetchedResultsController objectAtIndexPath:indexPath];

    RestCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifierRestCollectionCell forIndexPath:indexPath];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:reuseIdentifierRestCollectionCell owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [self configureCollectionCell:cell withRestaurant:rest];

    NSString* type = [NSString stringWithFormat:@"%@    ",rest.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",rest.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";
    
    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:12];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:10];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];
    
    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:10];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];
    
    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];
    
    [cell.restType setAttributedText:attributedString1];
    
//    NSLog(@"rest.title %@---------------------------------", rest.title);
//    NSLog(@"rest.catalogpriority %@---------------------------------", rest.catalogPriority);
//    NSLog(@"rest.id %@---------------------------------", rest.restaurantId);
    
//    NSUInteger rating = (int)(([rest.rating doubleValue])*100);
    NSUInteger rating = (int)(rest.rating*100);
    cell.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];
    
    
    if ([RNUser sharedInstance].isGeoEnabled && latitude != 0 && longitude != 0){
        cell.distansIcon.hidden = NO;
//        NSLog(@"[RNUser sharedInstance].isGeoEnabled");
    CLLocationCoordinate2D coord= CLLocationCoordinate2DMake(rest.lat, rest.lon);
    CGFloat distancRest = [self getDistanceToRest:coord];
//    NSLog(@"distanceRest %ld--------------------------------", (long)distancRest);
//    NSLog(@"distanceRest %f--------------------------------", (floor(distancRest)));
    NSInteger numKM = (NSInteger)distancRest / 1000;
//    NSLog(@" NSInteger numKM  %ld---------------------------------", (long)numKM);
    NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
//    NSLog(@" NSInteger numMM  %ld---------------------------------", (long)numMM);
    NSString* strKM = @"";
    if (numKM != 0){
        strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
    }
    NSString* strMM = @"";
    if (numMM != 0){
        strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
    }
        NSString* distanceRest = @"";
        
        if (numKM < 100){
            distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];
            
        } else {
            distanceRest = [NSString stringWithFormat:@"%@", strKM];
        }
    
    //    NSString* dinstStr = DPLocalizedString(@"dinst_short", nil);
    //    NSString* distanceRest = [NSString stringWithFormat:@"%0.2ld %@",(long)[self getDistanceToRest:rest.coordinate], dinstStr];
    cell.distanceRest.text = distanceRest;
//        NSLog(@"restaurant.coordinate.latitude %f restaurant.coordinate.longitude %f", rest.coordinate.latitude, rest.coordinate.longitude);
    } else {
        cell.distansIcon.hidden = YES;
        cell.distanceRest.text = @"";
    }
    
    NSString* restName = [rest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    cell.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];
    
    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    cell.commentsCount.text = [NSString stringWithFormat:@"%lld %@",rest.reviewCount, commStr];
    //     cell.commentsCount.text = [NSString stringWithFormat:@"%@ %@",rest.reviewCount,[RNCountPeopleHelper returnCorrectNameFeedbackWithCount:rest.reviewCount] ];
    NSArray *images = (NSArray*)rest.images;
    //    NSLog(@"CDRestaurant.workTimes: %@",timeWorkArr);
    if (images.count > 0){
    [UIImage cachedImage:images[0]
           fullURLString:nil
            withCallBack:^(UIImage *image) {
                cell.restImage.image = image;
            }];
        
    } else {
        cell.restImage.image = [UIImage imageNamed:@"restLogo"];
    }
    //    NSString *str = [model.kitchen componentsJoinedByString:@", "];
    //    _cuisineLabel.text = str;
    
    [cell.submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.callButton addTarget:self action:@selector(callButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //    NSString *addresssStr = [rest.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
    //                                                                     withString:@""];
    //    cell.restAddress.text = addresssStr;
    if (rest.avgBill == 0) {
        cell.averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        cell.averageLabel.text = [NSString stringWithFormat:@"%d %@",rest.avgBill, avgStr];
    }
    //    _subwayLabel.text = [[RNUser sharedInstance] subwayNameFord:model.subway];
    
    if (rest.discounts == 0) {
        cell.discountBack.image = nil;
        cell.restDiscount.hidden = YES;
    } else {
        cell.restDiscount.hidden = NO;
        cell.restDiscount.text = [NSString stringWithFormat:@"-%hd%%",rest.discounts];
        cell.discountBack.image = [UIImage imageNamed:@"discont"];
    }
//    NSLog(@"currIndex %ld", (long)currIndex);
//    NSLog(@"indexPath.row %ld", (long)indexPath.row);
    if(currIndex == indexPath.row)
        
        //    {
        cell.callButton.hidden = NO;
    //        [self.tableView reloadData];
    //    } else {
    //        cell.callButton.hidden = YES;
    //    }
    
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat s;

    s = (self.view.bounds.size.width-10)/2;

    return CGSizeMake(s, s*0.61f + 100);
}



-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self isInternetConnect]){

        CDRestaurant *currentRestoraunt = [self.fetchedResultsController objectAtIndexPath:indexPath];

        __block UIImage* backImage;
        NSArray *images = (NSArray*)currentRestoraunt.images;
        if (images.count >0){
        [UIImage cachedImage:images[0]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    backImage = image;
                }];
            
        } else {
            backImage = [UIImage imageNamed:@"restLogo"];
        }
        
        RestonItemViewController * restonItemViewController = [storyboard instantiateViewControllerWithIdentifier:@"RestonItemViewController"];
        restonItemViewController.restCD = currentRestoraunt;
        restonItemViewController.cityName = cityName;
        
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:restonItemViewController];
        [self.navigationController presentViewController:navVC animated:YES completion:nil];
    }
    
}

- (void)submitButton:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    UIView *descripView = button.superview;
    UIView* containerView = descripView.superview;
    UIView* contentView = containerView.superview;
    UICollectionViewCell *cell = (UICollectionViewCell*)contentView.superview;
    NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
//    NSLog(@"row containing button: %ld", (long)indexPath.row);
    
    RNRestaurant* currentRestourant = _restons[indexPath.row];
    
    currentRestourant = [self convertToHumanRest:currentRestourant];
    
    
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = currentRestourant;
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
    
}

- (void)callButton:(id)sender{

    NSString* telephone = @"+380509003493";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",telephone]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        //       UIAlertView* callAlert = [[UIAlertView alloc]initWithTitle:@"Звонок" message:@"в данное время не возможен!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        //        [callAlert show];
    }
    
}

-(RNRestaurant*) convertToHumanRest:(RNRestaurant*)rawRest{

    NSString* restName = [rawRest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    rawRest.title = restName;
    
    NSString *addresssStr = [rawRest.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                                       withString:@""];
    rawRest.address = addresssStr;
    
    return rawRest;
}


-(NSString*) getRating:(NSUInteger)rating{

    NSString* nameRating;

    if (rating == 0){
        nameRating = @"rating5_0";

    } else if (rating < 19){

        nameRating = @"rating5_1";

    } else if (rating >= 20 && rating < 40){

        nameRating = @"rating5_2";

    } else if (rating >= 40 && rating < 60){

        nameRating = @"rating5_3";

    } else if (rating >= 60 && rating < 80){

        nameRating = @"rating5_4";

    } else if (rating > 80){

        nameRating = @"rating5_5";

    } else {

        nameRating = @"rating5_0";

    }
//    NSLog(@"nameRating %@", nameRating);
    return nameRating;
}

#pragma mark - Navigation


-(void) setupView {
    
    
    
    self.filterContainerView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.newsButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.filterButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.sortButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.mapButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.filterCancelButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.markerImage.translatesAutoresizingMaskIntoConstraints = false;
    
    self.cityButton.translatesAutoresizingMaskIntoConstraints = false;
    
    self.collectionContainer.translatesAutoresizingMaskIntoConstraints = false;
    
    self.langButton.translatesAutoresizingMaskIntoConstraints = false;
    self.cityLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    self.searchField.translatesAutoresizingMaskIntoConstraints = false;
    self.searchField.delegate = self;
    
    self.bookMarkLabel.translatesAutoresizingMaskIntoConstraints = false;
    
    _tableCellContainer.alpha = 1.0;
    _collectionContainer.alpha = 0.0;
    
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        
//        [self.topContainerView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//        [self.topContainerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant: 65].active = YES;
//        [self.topContainerView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
//        [self.topContainerView.heightAnchor constraintEqualToConstant:82].active = YES;
        
        
        [self.filterContainerView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.filterContainerView.topAnchor constraintEqualToAnchor:self.topContainerView.topAnchor].active = YES;
        [self.filterContainerView.widthAnchor constraintEqualToAnchor:self.topContainerView.widthAnchor].active = YES;
        [self.filterContainerView.heightAnchor constraintEqualToConstant:30].active = YES;
        
        
        //    [self.restonsButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.leftAnchor constant: screenWidth * 0.07].active = YES;
        //    [self.restonsButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        //    [self.restonsButton.widthAnchor constraintEqualToConstant:18].active = YES;
        //    [self.restonsButton.heightAnchor constraintEqualToConstant:18].active = YES;
        
        
        [self.filterButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.leftAnchor constant: screenWidth * 0.07].active = YES;
        [self.filterButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.filterButton.widthAnchor constraintEqualToConstant:28].active = YES;
        [self.filterButton.heightAnchor constraintEqualToConstant:28].active = YES;
//        self.filterButton.imageView.frame = CGRectMake(0, 0, 18, 18);
        [self.filterButton setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        
        //    [self.sortButton addTarget:self
        //                          action:@selector(showFilter)
        //                forControlEvents:UIControlEventTouchUpInside];
        [self.sortButton.centerXAnchor constraintEqualToAnchor:self.filterButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.sortButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.sortButton.widthAnchor constraintEqualToConstant:28].active = YES;
        [self.sortButton.heightAnchor constraintEqualToConstant:28].active = YES;
        [self.sortButton setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        
        [self.mapButton.centerXAnchor constraintEqualToAnchor:self.sortButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.mapButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.mapButton.widthAnchor constraintEqualToConstant:28].active = YES;
        [self.mapButton.heightAnchor constraintEqualToConstant:28].active = YES;
        [self.mapButton setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        
        self.newsButton.hidden = YES;
        self.newsButton.enabled = NO;
        [self.newsButton.centerXAnchor constraintEqualToAnchor:self.mapButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.newsButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor constant:6].active = YES;
        [self.newsButton.widthAnchor constraintEqualToConstant:18].active = YES;
        [self.newsButton.heightAnchor constraintEqualToConstant:18].active = YES;
        
        [self.filterCancelButton.centerXAnchor constraintEqualToAnchor:self.mapButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.filterCancelButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor constant:-2].active = YES;
        [self.filterCancelButton.widthAnchor constraintEqualToConstant:18].active = YES;
        [self.filterCancelButton.heightAnchor constraintEqualToConstant:18].active = YES;
        
        [self.markerImage.rightAnchor constraintEqualToAnchor:self.cityButton.leftAnchor constant:-5].active = YES;
        [self.markerImage.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.markerImage.widthAnchor constraintEqualToConstant:10].active = YES;
        [self.markerImage.heightAnchor constraintEqualToConstant:16].active = YES;
        
        self.searchField.placeholder = DPLocalizedString(@"search_place", nil);
        [self.searchField.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.searchField.topAnchor constraintEqualToAnchor:self.filterContainerView.bottomAnchor].active = YES;
        [self.searchField.widthAnchor constraintEqualToConstant:screenWidth * 0.968].active = YES;
        [self.searchField.heightAnchor constraintEqualToConstant:44].active = YES;
        
        
        NSString* langName = [NSString string];
        
        if ([dp_get_current_language() isEqualToString:@"ru"]){
            langName = @"РУС";
        } else if ([dp_get_current_language() isEqualToString:@"uk"]){
            langName = @"УКР";
        }
        langName = [langName uppercaseString];
        [self.langButton setTitle:langName forState:UIControlStateNormal];
        [self.langButton.rightAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.036)].active = YES;
        [self.langButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: -(4)].active = YES;
        [self.langButton.widthAnchor constraintEqualToConstant:34].active = YES;
        [self.langButton.heightAnchor constraintEqualToConstant:14].active = YES;
        
        self.langButton.layer.cornerRadius = 7.0;
        self.langButton.layer.borderWidth = 1.0;
        self.langButton.backgroundColor = [UIColor whiteColor];
        self.langButton.layer.borderColor = [[[UIColor darkGrayColor] colorWithAlphaComponent:0.7] CGColor];
        self.langButton.layer.masksToBounds = true;
        
        [self setupCityName];
        [self setupFeddsBadgeForNewsAndOffers];
        
        self.cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.cityButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.28)].active = YES;
        [self.cityButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: 4].active = YES;
        [self.cityButton.widthAnchor constraintEqualToConstant:100].active = YES;
        [self.cityButton.heightAnchor constraintEqualToConstant:24].active = YES;
        
//        [self.tableCellContainer.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//        [self.tableCellContainer.topAnchor constraintEqualToAnchor:self.topContainerView.bottomAnchor].active = YES;
//        [self.tableCellContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
//        [self.tableCellContainer.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
        //    [self.tableCellContainer.heightAnchor constraintEqualToConstant:92].active = YES;
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        self.topContainerView.translatesAutoresizingMaskIntoConstraints = false;
        self.tableCellContainer.translatesAutoresizingMaskIntoConstraints = false;
        
        [self.topContainerView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.topContainerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant: 65].active = YES;
        [self.topContainerView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
        [self.topContainerView.heightAnchor constraintEqualToConstant:134].active = YES;
        
        
        [self.filterContainerView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.filterContainerView.topAnchor constraintEqualToAnchor:self.topContainerView.topAnchor].active = YES;
        [self.filterContainerView.widthAnchor constraintEqualToAnchor:self.topContainerView.widthAnchor].active = YES;
        [self.filterContainerView.heightAnchor constraintEqualToConstant:80].active = YES;
        
        
        //    [self.restonsButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.leftAnchor constant: screenWidth * 0.07].active = YES;
        //    [self.restonsButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        //    [self.restonsButton.widthAnchor constraintEqualToConstant:18].active = YES;
        //    [self.restonsButton.heightAnchor constraintEqualToConstant:18].active = YES;
        
        
        [self.filterButton.leftAnchor constraintEqualToAnchor:self.filterContainerView.leftAnchor constant: 22].active = YES;
        [self.filterButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.filterButton.widthAnchor constraintEqualToConstant:40].active = YES;
        [self.filterButton.heightAnchor constraintEqualToConstant:46].active = YES;
        
        //    [self.sortButton addTarget:self
        //                          action:@selector(showFilter)
        //                forControlEvents:UIControlEventTouchUpInside];
        [self.sortButton.centerXAnchor constraintEqualToAnchor:self.filterButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.sortButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.sortButton.widthAnchor constraintEqualToConstant:40].active = YES;
        [self.sortButton.heightAnchor constraintEqualToConstant:46].active = YES;
        
        [self.mapButton.centerXAnchor constraintEqualToAnchor:self.sortButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.mapButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.mapButton.widthAnchor constraintEqualToConstant:40].active = YES;
        [self.mapButton.heightAnchor constraintEqualToConstant:46].active = YES;
        
        self.newsButton.hidden = YES;
        self.newsButton.enabled = NO;
        [self.newsButton.centerXAnchor constraintEqualToAnchor:self.mapButton.centerXAnchor constant: screenWidth * 0.086].active = YES;
        [self.newsButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.newsButton.widthAnchor constraintEqualToConstant:40].active = YES;
        [self.newsButton.heightAnchor constraintEqualToConstant:40].active = YES;
        
        [self.markerImage.rightAnchor constraintEqualToAnchor:self.cityButton.leftAnchor constant:-5].active = YES;
        [self.markerImage.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor].active = YES;
        [self.markerImage.widthAnchor constraintEqualToConstant:24].active = YES;
        [self.markerImage.heightAnchor constraintEqualToConstant:36].active = YES;
        
        self.searchField.placeholder = DPLocalizedString(@"search_place", nil);
        [self.searchField.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:16].active = YES;
        [self.searchField.bottomAnchor constraintEqualToAnchor:self.topContainerView.bottomAnchor].active = YES;
        [self.searchField.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-16].active = YES;
        [self.searchField.heightAnchor constraintEqualToConstant:84].active = YES;
        
        
        NSString* langName = [NSString string];
        
        if ([dp_get_current_language() isEqualToString:@"ru"]){
            langName = @"РУС";
        } else if ([dp_get_current_language() isEqualToString:@"uk"]){
            langName = @"УКР";
        }
        langName = [langName uppercaseString];
        [self.langButton setTitle:langName forState:UIControlStateNormal];
        [self.langButton.rightAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.036)].active = YES;
        [self.langButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: -(4)].active = YES;
        [self.langButton.widthAnchor constraintEqualToConstant:60].active = YES;
        [self.langButton.heightAnchor constraintEqualToConstant:20].active = YES;
        
        self.langButton.layer.cornerRadius = 10.0;
        self.langButton.layer.borderWidth = 1.0;
        self.langButton.backgroundColor = [UIColor whiteColor];
        self.langButton.layer.borderColor = [[[UIColor darkGrayColor] colorWithAlphaComponent:0.7] CGColor];
        self.langButton.layer.masksToBounds = true;
        
        [self setupCityName];
        
        [self setupFeddsBadgeForNewsAndOffers];
        
        
        self.cityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.cityButton.centerXAnchor constraintEqualToAnchor:self.filterContainerView.rightAnchor constant: -(screenWidth * 0.28)].active = YES;
        [self.cityButton.centerYAnchor constraintEqualToAnchor:self.filterContainerView.centerYAnchor  constant: 4].active = YES;
        [self.cityButton.widthAnchor constraintEqualToConstant:200].active = YES;
        [self.cityButton.heightAnchor constraintEqualToConstant:24].active = YES;
        
        [self.tableCellContainer.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
        [self.tableCellContainer.topAnchor constraintEqualToAnchor:self.topContainerView.bottomAnchor].active = YES;
        [self.tableCellContainer.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
        [self.tableCellContainer.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
        
    }
}

#pragma mark - Fetched results controller

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.tableView;

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureTableCell:[tableView cellForRowAtIndexPath:indexPath] withRestaurant:anObject];
            break;

        case NSFetchedResultsChangeMove:
            [self configureTableCell:[tableView cellForRowAtIndexPath:indexPath] withRestaurant:anObject];
            [tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


- (void)configureCollectionCell:(RestCollectionCell *)cell withRestaurant:(CDRestaurant *)rest {
    
}

- (void)configureTableCell:(RestonCell *)cell withRestaurant:(CDRestaurant *)rest {

    NSString* type = [NSString stringWithFormat:@"%@    ",rest.type];
    type = [type stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    type = [type stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    NSString *addressStr = [NSString stringWithFormat:@" %@",rest.address];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    NSString* mapIconString = @"\uf279";

    UIFont *typeFont = [UIFont fontWithName:@"Thonburi" size:12];
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:type attributes:@{
                                                                        NSFontAttributeName : typeFont}];
    UIFont *mapFont = [UIFont fontWithName:@"FontAwesome" size:10];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:mapIconString attributes:@{NSFontAttributeName : mapFont, NSForegroundColorAttributeName : mainColor}];

    UIFont *addressFont = [UIFont fontWithName:@"Thonburi" size:10];
    NSMutableAttributedString *attributedString3 =
    [[NSMutableAttributedString alloc] initWithString:addressStr attributes:@{NSFontAttributeName : addressFont }];

    [attributedString1 appendAttributedString:attributedString2];
    [attributedString1 appendAttributedString:attributedString3];

    [cell.restType setAttributedText:attributedString1];

    NSUInteger rating = (int)(rest.rating*100);
    cell.ratingImage.image = [UIImage imageNamed:[self getRating:rating]];


    if ([RNUser sharedInstance].isGeoEnabled && [RNUser sharedInstance].latitude != 0 && [RNUser sharedInstance].longitude != 0){
        cell.distansIcon.hidden = NO;
        CLLocationCoordinate2D coord= CLLocationCoordinate2DMake(rest.lat, rest.lon);
        CGFloat distancRest = [self getDistanceToRest:coord];
        NSInteger numKM = (NSInteger)distancRest / 1000;
        NSInteger numMM = (NSInteger)(((distancRest)/1000 - numKM)*1000);
        NSString* strKM = @"";
        if (numKM != 0){
            strKM = [NSString stringWithFormat:@"%ldкм", (long)numKM];
        }
        NSString* strMM = @"";
        if (numMM != 0){
            strMM = [NSString stringWithFormat:@"%ldм", (long)numMM];
        }
        NSString* distanceRest = @"";

        if (numKM < 100){
            distanceRest = [NSString stringWithFormat:@"%@ %@", strKM, strMM];

        } else {
            distanceRest = [NSString stringWithFormat:@"%@", strKM];
        }

        cell.distanceRest.text = distanceRest;

    } else {
        cell.distansIcon.hidden = YES;
        cell.distanceRest.text = @"";
    }

    NSString* restName = [rest.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];

    cell.restNamelable.text = [NSString stringWithFormat:@"\"%@\"",restName];

    NSString* commStr = DPLocalizedString(@"comments_short", nil);
    cell.commentsCount.text = [NSString stringWithFormat:@"%lld %@",rest.reviewCount, commStr];
    NSArray *images = (NSArray*)rest.images;
    if (images.count > 0){
        [UIImage cachedImage:images[0]
               fullURLString:nil
                withCallBack:^(UIImage *image) {
                    cell.restImage.image = image;
                }];

    } else {
        cell.restImage.image = [UIImage imageNamed:@"restLogo"];
    }

    [cell.submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.callButton addTarget:self action:@selector(callButton:) forControlEvents:UIControlEventTouchUpInside];

    if (rest.avgBill == 0) {
        cell.averageLabel.text = @"";
    } else {
        NSString* avgStr = DPLocalizedString(@"avg_short", nil);
        cell.averageLabel.text = [NSString stringWithFormat:@"%d %@",rest.avgBill, avgStr];
    }

    if (rest.discounts == 0) {
        cell.discountBack.image = nil;
        cell.restDiscount.hidden = YES;
    } else {
        cell.restDiscount.hidden = NO;
        cell.restDiscount.text = [NSString stringWithFormat:@"-%hd%%",rest.discounts];
        cell.discountBack.image = [UIImage imageNamed:@"discont"];
    }
}
@end
