//
//  NewMenuViewController.m
//  ammonitum
//
//  Created by Vladimir Nybozhinsky on 5/7/17.
//  Copyright © 2017 Dima. All rights reserved.
//

//@import FirebaseAuth;
#import "RNUser.h"
#import "NewMenuViewController.h"
#import "CatalogRestViewController.h"
#import "FavRestonsViewController.h"
#import "FavPadViewController.h"
#import "ProfileViewController.h"
#import "OrdersViewController.h"
#import "EmailViewController.h"
#import "SlideMenuController.h"
#import "MenuCell.h"
#import "ReservedViewController.h"
#import "PageViewController.h"
#import "SlideEnterViewController.h"
#import "NSObject+DPLocalization.h"
#import "define.h"
#import "DPLocalization.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NSURLSession+CancelTasks.h"

#import "Reachability.h"


static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kPhoneKey = @"PhoneKey";
static NSString *const kEmailKey = @"EmailKey";
static NSString *const kUserPhotoKey = @"UserPhotoKey";
static NSString *const kUserKey = @"UserKey";
static NSString *const kPassKey = @"PassKey";
static NSString *const kEmailFBKey = @"EmailFBKey";
static NSString *const kClientIDKey = @"ClientIDKey";
static NSString *const kStrategyKey = @"StrategyKey";
static NSString *const kUserBirthdayKey = @"UserBirthdayKey";
//NSString *const RestonsChangeNotification = @"RestonsChangeNotification";


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}

@end


@interface NewMenuViewController (){
    CatalogRestViewController *enterMainViewController;
    FavRestonsViewController *favoritViewController;
    FavPadViewController *favPadViewController;
    OrdersViewController *ordViewController;
    //    PageViewController *enterViewController;
    UIStoryboard *storyboard;
    Boolean isPhone;
    
    
}
@end

@implementation NewMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        isPhone = YES;
        
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        isPhone = NO;
    }
    
    if ([RNUser sharedInstance].token != nil){
        
        self.enterLabel.text = DPLocalizedString(@"exit", nil);
        self.enterExitImage.image = [UIImage imageNamed:@"Exit"];
        
    } else {
        
        self.enterLabel.text = DPLocalizedString(@"enter", nil);
        self.enterExitImage.image = [UIImage imageNamed:@"enter"];
        
    }
    
    //    _menuItemsDict = [NSMutableDictionary
    //                      dictionaryWithObjects:@[@"YES",@"NO",@"NO",@"NO", @"NO"]
    //                      forKeys:@[@"catalog_rest_menu", @"Профиль", @"Мои бронирования", @"Избранные", @"Написать нам"]];
    //
    //
    //    _menuItemsArray = @[@"Рестон",@"Каталог", @"Профиль", @"Мои бронирования", @"Избранные", @"Написать нам"];
    
    
    enterMainViewController = (CatalogRestViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"CatalogRestViewController"];
    self.catalogViewController = [[UINavigationController alloc] initWithRootViewController: enterMainViewController];
    
    ProfileViewController *profileViewController = (ProfileViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    self.profileViewController = [[UINavigationController alloc] initWithRootViewController: profileViewController];
    
    ordViewController = (OrdersViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"OrdersViewController"];
    self.ordersViewController = [[UINavigationController alloc] initWithRootViewController: ordViewController];
    
    if (isPhone){
        
        favoritViewController = (FavRestonsViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"FavRestonsViewController"];
        self.favoritesViewController = [[UINavigationController alloc] initWithRootViewController: favoritViewController];
        
    } else {
        //
        favPadViewController = (FavPadViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"FavPadViewController"];
        self.favoritesViewController = [[UINavigationController alloc] initWithRootViewController: favPadViewController];
    }
    //    enterViewController = (PageViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"PageViewController"];
    //    self.exitViewController = [[UINavigationController alloc] initWithRootViewController: enterViewController];
    
    EmailViewController *emailViewController = (EmailViewController *)[storyboard  instantiateViewControllerWithIdentifier:@"EmailViewController"];
    self.emailViewController = [[UINavigationController alloc] initWithRootViewController: emailViewController];
    
    
    
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
//        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void) exit{
    
    
    PageViewController * pageViewController = [storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//    pageViewController.currCity = _currCity;
    pageViewController.isAuth = NO;
    
    NSLog(@"LeftExit exit");
    self.restons = nil;
    
    NSURLSession* session;
    
    [session cancelTasks];
    
//    NSError *signOutError;
//    BOOL status = [[FIRAuth auth] signOut:&signOutError];
//    if (!status) {
//        NSLog(@"Error signing out: %@", signOutError);
//        return;
//    }
    
    [self closeLeft];
    [self removeFromParentViewController];
    [RNUser sharedInstance].token = nil;
    [RNUser sharedInstance].email = nil;
    [RNUser sharedInstance].phone = nil;
    [RNUser sharedInstance].birthDate = nil;
    [RNUser sharedInstance].name = nil;
    [RNUser sharedInstance].photo = nil;
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kFBNameKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kEmailFBKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kClientIDKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kStrategyKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kUserKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kPassKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kPhoneKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kEmailKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil
                                              forKey:kUserPhotoKey];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:kUserBirthdayKey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    FBSDKAccessToken.currentAccessToken = nil;
    
    
    //    [self.slideMenuController changeMainViewController:pageViewController close:YES withIndex:0];
    [[[UIApplication sharedApplication] delegate].window setRootViewController:pageViewController];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restonsChangeNotification:)
                                                 name:RestonsChangeNotification
                                               object:nil];
    
    if ([RNUser sharedInstance].token != nil){
        
        self.enterLabel.text = DPLocalizedString(@"exit", nil);
        self.enterExitImage.image = [UIImage imageNamed:@"Exit"];
        
    } else {
        
        self.enterLabel.text = DPLocalizedString(@"enter", nil);
        self.enterExitImage.image = [UIImage imageNamed:@"enter"];
        
    }
    
    [self.tableView reloadData];
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeViewController:(LeftMenu) menu {
    switch (menu) {
            
        case LeftCatalogue:
            NSLog(@"enterMainViewController.restons %ld", enterMainViewController.restons.count);
            enterMainViewController.restons = _restons;
//            enterMainViewController.currCity = _currCity;
            [self.slideMenuController changeMainViewController:self.catalogViewController close:YES withIndex:0];
            break;
            
        case LeftProfile:
            [self.slideMenuController changeMainViewController:self.profileViewController close:YES withIndex:0];
            break;
            
        case LeftOrders:
            ordViewController.delegate = self;
            ordViewController.isMenu = YES;
            [self.slideMenuController changeMainViewController:self.ordersViewController close:YES withIndex:0];
            break;
            
        case LeftFavorites:
            if (isPhone){
                
                favoritViewController.delegate = self;
//                favoritViewController.isThisCity = _isThisCity;
                
            } else {
                
                favPadViewController.delegate = self;
//                favPadViewController.isThisCity = _isThisCity;
            }
            
            [self.slideMenuController changeMainViewController:self.favoritesViewController close:YES withIndex:0];
            break;
            
        case LeftExit:
            NSLog(@"LeftExit");
            //            enterViewController.delegate = self;
            //            [self.slideMenuController changeMainViewController:self.exitViewController close:YES withIndex:0];
            [self exit];
            break;
            
        case LeftEmpty:
            
            break;
        case LeftEmail:
            [self.slideMenuController changeMainViewController:self.emailViewController close:YES withIndex:0];
            break;
    }
}

-(void)restonsChangeNotification:(NSNotification *)notif
{
    if ([notif.userInfo objectForKey:@"restons"]){
        _restons = [notif.userInfo objectForKey:@"restons"];
//        NSLog(@"_restons.count %ld", _restons.count);
        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    
    return cell;
 
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if ([self isInternetConnect]){
        [self changeViewController:indexPath.row];
    }
  
    
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end

