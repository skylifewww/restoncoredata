//
//  DistricTableViewController.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterInfo.h"

extern NSString *const DistricChangeNotification;

@interface DistricTableViewController : UITableViewController

@property (strong, nonatomic) FilterInfo* filterInfo;
@property (strong, nonatomic) NSString* currCity;
@property (strong, nonatomic) NSArray* filteredDistrics;
@property (strong, nonatomic) NSArray* allDistrics;
@end
