//
//  Annotation.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/25/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
//@synthesize coordinate, title, subtitle, restID, index;

-(void) setIsSelected:(Boolean)isSelected{
    _isSelected = isSelected;
}

- (id)initWithDictionary:(NSDictionary <NSString *, id> *)dictionary {
    
    NSDictionary * coordinateDictionary = [dictionary objectForKey:@"coordinates"];
    
    return [self initWithCoordinates:CLLocationCoordinate2DMake([[coordinateDictionary objectForKey:@"latitude"] doubleValue], [[coordinateDictionary objectForKey:@"longitude"] doubleValue])
//                               title:[dictionary objectForKey:@"name"]
                               title:nil
                            subtitle:nil
                              restID:[dictionary objectForKey:@"restID"]
                               index:[[dictionary objectForKey:@"index"] integerValue]
            isSelected:[[dictionary objectForKey:@"isSelected"] boolValue]
            ];
}

- (id)initWithCoordinates:(CLLocationCoordinate2D)location title:(NSString *)title subtitle:(NSString *)subtitle restID:(NSNumber*)restID index:(NSInteger)index isSelected:(Boolean)isSelected{
    self = [super init];
    if (self != nil) {
        _coordinate = location;
        _title = title;
        _subtitle = subtitle;
        _restID = restID;
        _index = index;
        _isSelected = isSelected;
    }
    return self;
}

@end
