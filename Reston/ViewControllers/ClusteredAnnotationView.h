//
//  ClusteredAnnotationView.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/26/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <TSClusterMapView/TSRefreshedAnnotationView.h>
#import <QuartzCore/QuartzCore.h>

@interface ClusteredAnnotationView : TSRefreshedAnnotationView

@property (strong, nonatomic) CATextLayer *textLayer;

@end
