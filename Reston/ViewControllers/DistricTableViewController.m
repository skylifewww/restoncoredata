//
//  DistricTableViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/29/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "DistricTableViewController.h"
#import "DistricCell.h"
#import "GetDistricRequest.h"
#import "GetDistricResponse.h"
#import "Distric.h"

#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


NSString *const DistricChangeNotification = @"DistricChangeNotification";

static NSString *const kDistricArr = @"DistricArr";

@interface DistricTableViewController (){

    NSString* currentDistricName;
    NSString* currentDistricID;
    NSMutableArray* selectArr;
    NSMutableArray* sendArr;
    UIColor* mainColor;
    Reachability *_reachability;
}

@end

@implementation DistricTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"area_title", nil);
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    selectArr = [NSMutableArray array];
    if (_filteredDistrics.count > 0 && (![_filteredDistrics.firstObject isEqualToString:@""])){
    selectArr = [NSMutableArray arrayWithArray:_filteredDistrics];
    }
    
    NSLog(@"selectArr %@", selectArr);
    sendArr = [NSMutableArray array];
    
    
    if (_allDistrics.count > 0){
        
        _allDistrics = [_allDistrics sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* setSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kDistricArr];
        if (setSelect.count > selectArr.count && (![setSelect.firstObject  isEqualToString: @""])){
            selectArr = [NSMutableArray arrayWithArray:setSelect];
        }
        
        [self.tableView reloadData];
    } else {

    }
    self.tableView.allowsMultipleSelection = true;

}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)loadDistrics{
//    GetDistricRequest *request = [[GetDistricRequest alloc] init];
//    
//    NSString* langCode = [[NSString alloc] init];
//    
//    if ([dp_get_current_language() isEqualToString:@"ru"]){
//        langCode = @"ru";
//    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
//        langCode = @"ua";
//    }
//    request.langCode = langCode;
//    request.city = _currCity;
//    
//    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
//        if (error){
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
//        }
//        GetDistricResponse *r = (GetDistricResponse *)response;
//     
//        districs = [r.districs sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//            Distric *r1 = obj1;
//            Distric *r2 = obj2;
//            
//            return [r1.districName compare:r2.districName];
//        }];
//        
//        if (districs.count == 0){
//            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
// 
//        } else {
//            
//            NSDictionary* dictSelect = [[NSUserDefaults standardUserDefaults] objectForKey:kDistricArr];
//          
//            NSArray* setSelect = [dictSelect allValues];
//            if (setSelect.count > selectArr.count){
//                for (Distric* d in districs){
//                    for(NSString* str in setSelect){
//                        if ([d.districName isEqualToString:str]){
//                            
//                            [selectArr addObject:d];
//                        }
//                    }
//                }
//            }
//            [self.tableView reloadData];
//        }
//    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)subvitButton:(id)sender {
 NSLog(@"selectArr %@", selectArr);
    for (NSString* str in selectArr){
        if (![str isEqualToString:@""] && (![sendArr containsObject:str])){
            
            [sendArr addObject:str];
        }
        
        NSLog(@"sendArr.count %ld", sendArr.count);
        NSLog(@"sendArr %@", sendArr);
    }
    
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DistricChangeNotification object:nil userInfo:@{@"distric" : sendArr}];
        
        [[NSUserDefaults standardUserDefaults] setObject:sendArr
                                                  forKey:kDistricArr];

        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _allDistrics.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    DistricCell* cell = (DistricCell *)[tableView dequeueReusableCellWithIdentifier:@"DistricCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DistricCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [cell setBackgroundView:selectedBackgroundView];
  
    if ([selectArr containsObject:_allDistrics[indexPath.row]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    
    NSNumber* dis =  _allDistrics[indexPath.row];
    NSInteger distric = dis.integerValue;
    
    cell.textLabel.text = [self helperRaions:distric];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  currentDistricName =  _allDistrics[indexPath.row];
    
    if ([selectArr containsObject:_allDistrics[indexPath.row]] == NO) {
        [selectArr addObject:_allDistrics[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    } else {
        [selectArr removeObject:_allDistrics[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([selectArr containsObject:_allDistrics[indexPath.row]]) {
        [selectArr removeObject:_allDistrics[indexPath.row]];
       
         [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [[tableView  cellForRowAtIndexPath:indexPath].backgroundView setBackgroundColor:[UIColor whiteColor]];
    } else {
        [selectArr addObject:_allDistrics[indexPath.row]];
        [tableView  cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
       [tableView  cellForRowAtIndexPath:indexPath].backgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

-(NSString*) helperRaions:(NSUInteger)raion{
    
    switch (raion) {
        case 1:
            return  DPLocalizedString(@"1", nil);
            
        case 2:
            return  DPLocalizedString(@"2", nil);
            
        case 3:
            return  DPLocalizedString(@"3", nil);
            
        case 4:
            return  DPLocalizedString(@"4", nil);
            
        case 5:
            return  DPLocalizedString(@"5", nil);
            
        case 6:
            return  DPLocalizedString(@"6", nil);
            
        case 7:
            return  DPLocalizedString(@"7", nil);
            
        case 8:
            return  DPLocalizedString(@"8", nil);
            
        case 9:
            return  DPLocalizedString(@"9", nil);
            
        case 10:
            return  DPLocalizedString(@"10", nil);
            
        case 41:
            return  DPLocalizedString(@"41", nil);
            
        case 40:
            return  DPLocalizedString(@"40", nil);
            
        case 39:
            return  DPLocalizedString(@"39", nil);
            
        case 38:
            return  DPLocalizedString(@"38", nil);
            
        case 36:
            return  DPLocalizedString(@"36", nil);
            
        case 35:
            return  DPLocalizedString(@"35", nil);
            
        case 34:
            return  DPLocalizedString(@"34", nil);
            
        case 33:
            return  DPLocalizedString(@"33", nil);
            
        case 37:
            return  DPLocalizedString(@"37", nil);
            
        default:
            return [NSString stringWithFormat:@""];
    }
}

@end
