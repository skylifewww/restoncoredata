//
//  News1ViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/29/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "News1ViewController.h"
#import "RNGetNewsRequest.h"
#import "RNGetNewsResponse.h"
#import "RNNEw.h"
#import "NewsTableViewCell.h"
#import "NewsHeaderCell.h"
#import "UITableView+Registration.h"
#import "UIImage+Cache.h"
#import "ExampleTableViewCell.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "ReservTableViewController.h"
#import "Reachability.h"


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end



@interface News1ViewController () <HVTableViewDataSource, HVTableViewDelegate> {
//ExampleTableViewCellDelegate

    UIView* containerView;
    UILabel* _textFavlabel;
    UIImageView* _backFavIcon;
    Reachability *_reachability;
    UIStoryboard* storyboard;
    UIColor* mainColor;
    Boolean isPhone;
    double addHeight;
    double textFontSize;
    double restNameFontSize;
    double buttonFontSize;
    double cornerRadius;
    double typeFontSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    double estimateHeight;
  
}
@property (nonatomic, copy) NSArray *news;
@property (weak, nonatomic) IBOutlet UIButton *resrvedButton;

@end


@implementation News1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        typeFontSize = 10.0;
        addHeight = 40;
        restNameFontSize = 14.0;
        textFontSize = 12.0;
        buttonFontSize = 17.0;
        cornerRadius = 3.0;
        isPhone = YES;
        estimateHeight = 144;
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        typeFontSize = 16.0;
        addHeight = 100;
        restNameFontSize = 22.0;
        textFontSize = 16.0;
        buttonFontSize = 26.0;
        cornerRadius = 5.0;
        isPhone = NO;
        estimateHeight = 240;
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    self.tableView.HVTableViewDelegate = self;
    self.tableView.HVTableViewDataSource = self;

    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self.navigationController.navigationBar setTranslucent:YES];

     if ([self isInternetConnect]){
         [self loadNews:_restaurantID];
     }
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = estimateHeight;
    
    NSString* titleReservButton = DPLocalizedString(@"reserv_action", nil);
    [self.resrvedButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.resrvedButton.translatesAutoresizingMaskIntoConstraints = false;
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleReservButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.resrvedButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    self.resrvedButton.layer.cornerRadius = cornerRadius;
    self.resrvedButton.layer.masksToBounds = true;
    
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        
        return YES;
    }
}

-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)reservButtonAction:(id)sender {
   
    ReservTableViewController * reservTableViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReservTableViewController"];
    reservTableViewController.restourant = _restaurant;
    
    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:reservTableViewController];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)loadNews:(NSNumber *)ident {
    RNGetNewsRequest *request = [[RNGetNewsRequest alloc] init];
    request.restaurantId = ident;
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
        if (error != nil || [response isKindOfClass:[NSNull class]]){
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        RNGetNewsResponse *r = (RNGetNewsResponse *)response;
        _news = r.news;
        
        if (_news.count == 0){
            [self setupView];
        } else {
            _news = [_news sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                RNNEw *n1 = obj1;
                RNNEw *n2 = obj2;
                
                return -1*[n1.date compare:n2.date];
            }];
            
        [_tableView reloadData];
        }
        
    }];
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)backButton:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
//
//
//-(void)ExampleTableViewCellDidTapPurchaseButton:(ExampleTableViewCell *)cell
//{
//    NSString *alertTitle = [NSString stringWithFormat:@"'%@' Purchased Successfully.", cell.titlesLabel.text];
//    UIAlertController* purchaseAlert = [UIAlertController alertControllerWithTitle:nil message: alertTitle preferredStyle:UIAlertControllerStyleAlert];
//    [self presentViewController:purchaseAlert animated:YES completion:nil];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [purchaseAlert dismissViewControllerAnimated:YES completion:nil];
//    });
//}


#pragma mark HVTableViewDatasource
-(void)tableView:(UITableView *)tableView expandCell:(ExampleTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
//    cell.purchaseButton.alpha = 0;
    
    [UIView animateWithDuration:.5 animations:^{
        
        RNNEw* new = [_news objectAtIndex:indexPath.row];
        
        NSString* text = [NSString stringWithFormat:@"%@",new.text];
        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        cell.detailLabel.text = text;
        [cell.detailLabel sizeToFit];
        
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFill;
       
//        cell.purchaseButton.alpha = 1;
        cell.arrow.transform = CGAffineTransformMakeRotation(0);
    }];
    
}

-(void)tableView:(UITableView *)tableView collapseCell:(ExampleTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{

    cell.arrow.transform = CGAffineTransformMakeRotation(0);

    
    [UIView animateWithDuration:.5 animations:^{
        cell.arrow.transform = CGAffineTransformMakeRotation(-M_PI+0.00);
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _news.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    ExampleTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:[ExampleTableViewCell cellIdentifier]];
//    cell.delegate = self;
    
    if (indexPath.row %2 ==1)
        cell.backgroundColor = [UIColor colorWithRed:.96 green:.96 blue:.96 alpha:1];
    else
        cell.backgroundColor = [UIColor whiteColor];
    
    RNNEw* new = [_news objectAtIndex:indexPath.row];
    
    NSString* date = [NSString stringWithFormat:@"%@",new.date];
    date = [date stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];

    cell.datelabel.text = date;
    
    NSString* title = [NSString stringWithFormat:@"%@",new.title];
    title = [title stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    title = [title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    cell.titlesLabel.text = title;

    [cell.titlesLabel sizeToFit];
    
    
    [UIImage cachedImage:new.image
           fullURLString:@"http://reston.com.ua/uploads/img/news/original/"
            withCallBack:^(UIImage *image) {
                
                if (image == nil) {
                    [cell.theImageView removeFromSuperview];
                    return;
                }
                
                cell.theImageView.image = image;
            }];
  cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if (!isExpanded) {
        NSString* shortText = [NSString stringWithFormat:@"%@",new.shortText];
        shortText = [shortText stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        shortText = [shortText stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        shortText = [shortText stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        shortText = [shortText stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        shortText = [shortText stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        cell.detailLabel.text = shortText;
        [cell.detailLabel sizeToFit];
        cell.arrow.transform = CGAffineTransformMakeRotation(M_PI);
//        cell.purchaseButton.alpha = 0;
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    else
    {
        NSString* text = [NSString stringWithFormat:@"%@",new.text];
        text = [text stringByReplacingOccurrencesOfString:@"&#" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
        text = [text stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
        text = [text stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
        cell.detailLabel.text = text;

        [cell.detailLabel sizeToFit];
        cell.arrow.transform = CGAffineTransformMakeRotation(0);
//        cell.purchaseButton.alpha = 1;
        cell.theImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    RNNEw* new = [_news objectAtIndex:indexPath.row];
    
    if (isexpanded){
        
        CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
        CGRect frame = CGRectMake(0, 0, width*0.6, 28);
        UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
        myTextView.text = new.text;
        myTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
        CGSize result = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
        
        return result.height + addHeight;
        

    } else {
        
    return estimateHeight;
    }
}

-(void) setupView {
    
    containerView = [[UIView alloc] initWithFrame:_tableView.frame];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    
    containerView.translatesAutoresizingMaskIntoConstraints = false;
  
    _backFavIcon = [[UIImageView alloc] init];
    _backFavIcon.image = [UIImage imageNamed:@"news"];
    _backFavIcon.contentMode = UIViewContentModeScaleAspectFit;
    [containerView addSubview:_backFavIcon];
    _backFavIcon.translatesAutoresizingMaskIntoConstraints = false;
    

    
    _textFavlabel = [[UILabel alloc] init];
    _textFavlabel.text = DPLocalizedString(@"no_news", nil);
    _textFavlabel.textColor = [UIColor lightGrayColor];
    _textFavlabel.numberOfLines = 2;
    _textFavlabel.textAlignment = NSTextAlignmentCenter;
    [containerView addSubview:_textFavlabel];
    
    
    [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:16.0]];
    _textFavlabel.translatesAutoresizingMaskIntoConstraints = false;
 
    [containerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [containerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [containerView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:62].active = YES;
    [containerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;

    [_backFavIcon.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_backFavIcon.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.43].active = YES;
    [_backFavIcon.widthAnchor constraintEqualToConstant:screenWidth * 0.42].active = YES;
    [_backFavIcon.heightAnchor constraintEqualToConstant:screenHeight * 0.23].active = YES;

    if (screenWidth < 322) {
        [_textFavlabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [_textFavlabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [_textFavlabel.topAnchor constraintEqualToAnchor:_backFavIcon.bottomAnchor constant:60].active = YES;
    [_textFavlabel.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    [_textFavlabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
}


@end
