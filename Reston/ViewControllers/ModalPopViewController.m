//
//  ModalPopViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/1/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "ModalPopViewController.h"
#import "RNUser.h"
#import "RNMakeReserveRequest.h"
#import "RNMakeReserveResponse.h"
#import "ReservedViewController.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

#import "Reachability.h"


static NSString *const kFBNameKey = @"FBNameKey";
static NSString *const kPhoneKey = @"PhoneKey";
static NSString *const kEmailKey = @"EmailKey";
static NSString *const kReservCountKey = @"ReservCountKey";
static NSString *const kKeyboardHeightKey = @"KeyboardHeightKey";
NSString *const ReservCountChangeNotification = @"ReservCountChangeNotification";


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end




@interface ModalPopViewController()<UITextFieldDelegate, UITextViewDelegate>
{
    
    UIColor* mainColor;
    UIView* successView;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UIButton *closeModButton;
    
    UILabel *dearLabel;
    UILabel *fullNameLabel;
    UILabel *reservedLabel1;
    UILabel *reservedLabel2;
    UIImageView *reservedImage;
    double textFieldFontSize;
    double buttonFontSize;
    double cornerRadius;
    double textFontSize;
    double dearFontSize;
    double reservFontSize;
    Boolean isPhone;
    UILabel* placeholderLabel;
    
}


@end

@implementation ModalPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        textFontSize = 12.0;
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
        cornerRadius = 3;
        isPhone = YES;
        dearFontSize = 25;
        reservFontSize = 23;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        textFontSize = 16.0;
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
        cornerRadius = 5;
        isPhone = NO;
        dearFontSize = 32;
        reservFontSize = 27;
    }
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    NSLog(@"%@", self.orderInfo.restaurant.title);
    NSLog(@"%@", self.orderInfo.date);
    NSLog(@"%@", self.orderInfo.time);
    NSLog(@"%@", self.orderInfo.peopleCount);
    
    
    _restourant.title = _orderInfo.restaurant.title;
    
    
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doneBtnFromKeyboardClicked:)]];
    
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(doneBtnFromKeyboardClicked:)];
    [btnDoneOnKeyboard setTintColor:[UIColor lightGrayColor]];
    keyboardToolbar.items = @[flexBarButton, btnDoneOnKeyboard];
    _fullNameTextField.inputAccessoryView = keyboardToolbar;
//    _emailTextField.inputAccessoryView = keyboardToolbar;
    _phoneTextField.inputAccessoryView = keyboardToolbar;
    _conditionTextView.inputAccessoryView = keyboardToolbar;
    _conditionTextView.delegate = self;
    _fullNameTextField.delegate = self;
//    _emailTextField.delegate = self;
    _phoneTextField.delegate = self;
    
    [self setupView];
    [self setupTextView];
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)doneBtnFromKeyboardClicked:(id)sender
{
    if (_conditionTextView.isFirstResponder){
        [_conditionTextView resignFirstResponder];
        [self setupTextView];
    } else {
    [_fullNameTextField resignFirstResponder];
//    [_emailTextField resignFirstResponder];
        [_phoneTextField resignFirstResponder];
        
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_fullNameTextField resignFirstResponder];
//    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_conditionTextView resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    float newVerticalPosition = -keyboardSize.height;
    
    CGFloat keyY = self.view.frame.size.height + newVerticalPosition;
    CGFloat reservY = self.reservedView.frame.origin.y;
    CGFloat textY = _phoneView.frame.origin.y + _phoneView.frame.size.height + reservY + 10;
    
    if (_fullNameTextField.isFirstResponder){
        
        NSLog(@"keyY %f", keyY);
        NSLog(@"textY %f",textY);
        if (textY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
        }
    }
//    if (_emailTextField.isFirstResponder){
//
//        NSLog(@"keyY - textY %f", keyY - textY);
//        NSLog(@"keyY %f", keyY);
//        NSLog(@"textY %f",textY);
//        if (textY > keyY){
//            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
//        }
//    }
    if (_phoneTextField.isFirstResponder){
        
        NSLog(@"keyY - textY %f", keyY - textY);
        NSLog(@"keyY %f", keyY);
        NSLog(@"textY %f",textY);
        if (textY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textY) forDuration:0.3f];
        }
    }
    if (_conditionTextView.isFirstResponder){
        [placeholderLabel removeFromSuperview];
        CGFloat textViewY = _conditionTextView.frame.origin.y + _conditionTextView.frame.size.height + reservY + 10;
        NSLog(@"keyY - textY %f", keyY - textViewY);
        NSLog(@"keyY %f", keyY);
        NSLog(@"textY %f",textViewY);
        if (textViewY > keyY){
            [self moveFrameToVerticalPosition:(keyY - textViewY) forDuration:0.3f];
        }
    }
}


- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveFrameToVerticalPosition:0.0f forDuration:0.3f];
}


- (void)moveFrameToVerticalPosition:(float)position forDuration:(float)duration {
    CGRect frame = self.view.frame;
    frame.origin.y = position;
    
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

-(void) setupTextView{
    
    if (_conditionTextView.text.length == 0){
    placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.text = DPLocalizedString(@"enter_text", nil);
    placeholderLabel.textAlignment = NSTextAlignmentLeft;
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.textColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
    placeholderLabel.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
//    _conditionTextView.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
//    _conditionTextView.delegate = self;
//    _conditionTextView.translatesAutoresizingMaskIntoConstraints = false;
    placeholderLabel.translatesAutoresizingMaskIntoConstraints = false;
    [_conditionTextView addSubview:placeholderLabel];
    
    
    [placeholderLabel.leftAnchor constraintEqualToAnchor:self.conditionTextView.leftAnchor constant:0].active = YES;
    [placeholderLabel.rightAnchor constraintEqualToAnchor:self.conditionTextView.rightAnchor constant:-22].active = YES;
    
    [placeholderLabel.topAnchor constraintEqualToAnchor:_conditionTextView.topAnchor].active = YES;
    if (screenWidth <322){
        [placeholderLabel.heightAnchor constraintEqualToConstant:20].active = YES;
    } else {
        [placeholderLabel.heightAnchor constraintEqualToConstant:24].active = YES;
    }
        
    }
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitButton:(id)sender {
    if ([self isInternetConnect]){
        [self sendReservedRequest];
    }
    [_fullNameTextField resignFirstResponder];
//    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_conditionTextView resignFirstResponder];
    
}

- (void)sendReservedRequest {
    
    self.submitButton.enabled = NO;
//    NSString *phoneText = [_phoneTextField.text stringByReplacingOccurrencesOfString:@"("
//                                                                          withString:@""];
//    phoneText = [phoneText stringByReplacingOccurrencesOfString:@")"
//                                                     withString:@""];
//    phoneText = [phoneText stringByReplacingOccurrencesOfString:@"-"
//                                                     withString:@""];
//    
//    phoneText = [phoneText stringByReplacingOccurrencesOfString:@" "
//                                                     withString:@""];
    
    RNMakeReserveRequest *request = [[RNMakeReserveRequest alloc]init];
    request.userName = _fullNameTextField.text;
    NSString *phoneText = _phoneTextField.text;
    //    request.phone = phoneText;
//    request.email = _emailTextField.text;
    request.restaurantId = _orderInfo.restaurant.restaurantId;
    request.numberOfPeople = _orderInfo.peopleCount;
    
    NSDateFormatter *formDate = [[NSDateFormatter alloc] init];
    [formDate setDateFormat:@"yyyy-MM-dd"];
    request.date = [formDate stringFromDate:_orderInfo.date];
    
    NSDateFormatter *formTime = [[NSDateFormatter alloc] init];
    [formTime setDateFormat:@"HH:mm"];
    
    NSDate* roundTime = [self roundDateTo5Minutes:_orderInfo.time];
//    request.time = [NSString stringWithFormat:@"%@:00",[formTime stringFromDate:roundTime]];
    request.time = [NSString stringWithFormat:@"%@:00",_orderInfo.timeString];
    NSLog(@"request.time: %@", request.time);
    NSLog(@"request.date: %@", request.date);

    
    if (_fullNameTextField.text.length!=0) {
        request.userName = _fullNameTextField.text;
    } else {
        self.submitButton.enabled = YES;
        [self showAlertWithText:DPLocalizedString(@"less_name", nil)];
        return;
    }

    if (_conditionTextView.text.length > 0) {
        request.comments = _conditionTextView.text;
    }
    
    if (phoneText.length > 0 && phoneText.length < 12) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"format_phone", nil)
                                                                       message:DPLocalizedString(@"need_phone", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    if (phoneText.length!=0) {
        request.phone = phoneText;
        [[NSUserDefaults standardUserDefaults] setObject:phoneText forKey:kPhoneKey];
        //        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        self.submitButton.enabled = YES;
        [self showAlertWithText:DPLocalizedString(@"less_phone", nil)];
        return;
    }
//
//    if (_emailTextField.text.length != 0) {
//        [[NSUserDefaults standardUserDefaults] setObject:_emailTextField.text forKey:kEmailKey];
//    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"RNMakeReserveRequest");
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {

        RNMakeReserveResponse *reservResponse = (RNMakeReserveResponse *)response;

        if ([reservResponse.success isEqualToString:@"success"]){

            NSLog(@"RNMakeReserveResponse success: %@", reservResponse.success);
            NSLog(@"[[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]  %ld", [[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]);
            NSInteger reservCount = ([[NSUserDefaults standardUserDefaults] integerForKey:kReservCountKey]) + 1;

            NSLog(@"reservCount %@",[NSString stringWithFormat:@"%ld",(long)reservCount]);

            [[NSUserDefaults standardUserDefaults] setInteger:reservCount forKey:kReservCountKey];
            [[NSUserDefaults standardUserDefaults] synchronize];


            [[NSNotificationCenter defaultCenter] postNotificationName:ReservCountChangeNotification object:nil userInfo:@{@"reservCount" : @(reservCount)}];


            [self setupSuccessView];

        } else {

            NSLog(@"RNMakeReserveResponse error: %@", reservResponse.error);
            [self showAlertWithText:DPLocalizedString(@"table_unsuccessful", nil)];
        }

    }];
}


- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)closeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(NSDate *)roundDateTo5Minutes:(NSDate *)mydate{
    // Get the nearest 5 minute block
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSCalendarUnitHour | NSCalendarUnitMinute
                              fromDate:mydate];
    NSInteger minutes = [time minute];
    int remain = minutes % 5;
    
    mydate = [mydate dateByAddingTimeInterval:60*(5-remain)];
    
    return mydate;
}


-(void) setupView {
    
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    self.closeButton.translatesAutoresizingMaskIntoConstraints = false;
    //    self.fullNameTextField.translatesAutoresizingMaskIntoConstraints = false;
    //    self.emailTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.enterCommentslabel.translatesAutoresizingMaskIntoConstraints = false;
    self.conditionTextView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.backView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.reservedView.translatesAutoresizingMaskIntoConstraints = false;
    self.topLabel1.translatesAutoresizingMaskIntoConstraints = false;
    self.topLabel2.translatesAutoresizingMaskIntoConstraints = false;
    self.bottomlabel1.translatesAutoresizingMaskIntoConstraints = false;
    self.bottomlabel2.translatesAutoresizingMaskIntoConstraints = false;
    self.fullNameView.translatesAutoresizingMaskIntoConstraints = false;
//    self.emailView.translatesAutoresizingMaskIntoConstraints = false;
    self.phoneView.translatesAutoresizingMaskIntoConstraints = false;
    
    self.submitButton.layer.cornerRadius = cornerRadius;
    self.submitButton.layer.masksToBounds = true;
    
    NSString* nameFB = [[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey];
    
    
    if ([RNUser sharedInstance].name.length > 0 && nameFB.length == 0){
        self.fullNameTextField.text = [RNUser sharedInstance].name;
    } else if([[NSUserDefaults standardUserDefaults] objectForKey:kFBNameKey]){
        self.fullNameTextField.text = nameFB;
    }
    
    
    self.fullNameTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.fullNameTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
    [self.fullNameTextField setTintColor:mainColor];
    
    self.fullNameTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.fullNameTextField.attributedPlaceholder = [self placeholderString:DPLocalizedString(@"full_name", nil) withFontSize:textFieldFontSize];
    
    
    self.phoneTextField.translatesAutoresizingMaskIntoConstraints = false;
    self.phoneTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
    [self.phoneTextField setTintColor:mainColor];
    self.phoneTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.phoneTextField.attributedPlaceholder = [self placeholderString:@"38 000 000 00 00" withFontSize:textFieldFontSize];
    
    
//    self.emailTextField.translatesAutoresizingMaskIntoConstraints = false;
//    self.emailTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
//    [self.emailTextField setTintColor:mainColor];
//    self.emailTextField.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
//    self.emailTextField.attributedPlaceholder = [self placeholderString:@"Email" withFontSize:textFieldFontSize];
    
    self.conditionTextView.text = @"";
    self.conditionTextView.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    self.conditionTextView.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
    self.conditionTextView.font = [UIFont fontWithName:@"Thonburi" size:textFieldFontSize];
    [self.conditionTextView setTintColor:mainColor];
    
    
    if ([RNUser sharedInstance].token.length > 0) {
//        NSLog(@"[RNUser sharedInstance].token.length > 0 %d", [RNUser sharedInstance].token.length > 0);
        if ([RNUser sharedInstance].phone.length > 0 && [RNUser sharedInstance].phone.integerValue != 0){
            self.phoneTextField.text = [RNUser sharedInstance].phone;
//            NSLog(@"[RNUser sharedInstance].phone %@", [RNUser sharedInstance].phone);
        } else if ([[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey]){
            self.phoneTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey];
//            NSLog(@"objectForKey:kPhoneKey %@", [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey]);
        }
    } else {
//        NSLog(@"[RNUser sharedInstance].token.length < 0");
    }
//    NSLog(@"objectForKey:kPhoneKey %@", [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey]);
    
    //    if ([RNUser sharedInstance].email.length > 0){
    //        self.emailTextField.text = [RNUser sharedInstance].email;
    //    }
    
    
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    NSString* titleReservButton = DPLocalizedString(@"reserv_action", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleReservButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    self.backView.alpha = 0.6f;
    [self.backView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.backView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.backView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor].active = YES;
    [self.backView.heightAnchor constraintEqualToAnchor:self.view.heightAnchor].active = YES;
    
    self.reservedView.alpha = 1.0f;
    [self.reservedView.centerXAnchor constraintEqualToAnchor:self.backView.centerXAnchor].active = YES;
    [self.reservedView.centerYAnchor constraintEqualToAnchor:self.backView.centerYAnchor].active = YES;
    [self.reservedView.widthAnchor constraintEqualToConstant:screenWidth * 0.87].active = YES;
    [self.reservedView.heightAnchor constraintEqualToConstant:screenHeight * 0.71].active = YES;
    
    [self.closeButton.rightAnchor constraintEqualToAnchor:self.reservedView.rightAnchor constant:-5].active = YES;
    [self.closeButton.topAnchor constraintEqualToAnchor:self.reservedView.topAnchor constant:5].active = YES;
    [self.closeButton.widthAnchor constraintEqualToConstant:20].active = YES;
    [self.closeButton.heightAnchor constraintEqualToConstant:20].active = YES;
    
    [self.topLabel1 setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    [self.topLabel2 setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    [self.topLabel1.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.topLabel1.topAnchor constraintEqualToAnchor:self.reservedView.topAnchor constant:screenHeight * 0.03].active = YES;
    
    [self.topLabel2.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.topLabel2.topAnchor constraintEqualToAnchor:self.topLabel1.bottomAnchor].active = YES;
    
    [self.fullNameView.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.fullNameView.centerYAnchor constraintEqualToAnchor:self.reservedView.topAnchor
                                                    constant:screenHeight * 0.16].active = YES;
    [self.fullNameView.widthAnchor constraintEqualToConstant:screenWidth * 0.83].active = YES;
    [self.fullNameView.heightAnchor constraintEqualToConstant:screenHeight * 0.066].active = YES;
    
    [self.fullNameTextField.centerXAnchor constraintEqualToAnchor:self.fullNameView.centerXAnchor].active = YES;
    [self.fullNameTextField.centerYAnchor constraintEqualToAnchor:self.fullNameView.centerYAnchor].active = YES;
    [self.fullNameTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
//    [self.emailView.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
//    [self.emailView.centerYAnchor constraintEqualToAnchor:self.fullNameView.centerYAnchor
//                                                 constant:screenHeight * 0.08].active = YES;
//    [self.emailView.widthAnchor constraintEqualToConstant:screenWidth * 0.83].active = YES;
//    [self.emailView.heightAnchor constraintEqualToConstant:screenHeight * 0.066].active = YES;
//
//    [self.emailTextField.centerXAnchor constraintEqualToAnchor:self.emailView.centerXAnchor].active = YES;
//    [self.emailTextField.centerYAnchor constraintEqualToAnchor:self.emailView.centerYAnchor].active = YES;
//    [self.emailTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    
    [self.phoneView.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.phoneView.centerYAnchor constraintEqualToAnchor:self.fullNameView.centerYAnchor
                                                 constant:screenHeight * 0.08].active = YES;
    [self.phoneView.widthAnchor constraintEqualToConstant:screenWidth * 0.83].active = YES;
    [self.phoneView.heightAnchor constraintEqualToConstant:screenHeight * 0.066].active = YES;
    
    [self.phoneTextField.centerXAnchor constraintEqualToAnchor:self.phoneView.centerXAnchor].active = YES;
    [self.phoneTextField.centerYAnchor constraintEqualToAnchor:self.phoneView.centerYAnchor].active = YES;
    [self.phoneTextField.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    //    [self.emailTextField.heightAnchor constraintEqualToAnchor:self.emailView.heightAnchor constant:screenHeight * 0.36].active = YES;
    
    [self.enterCommentslabel.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    
    [self.enterCommentslabel.topAnchor constraintEqualToAnchor:self.phoneView.bottomAnchor
                                                      constant:screenHeight * 0.02].active = YES;
    [self.enterCommentslabel.widthAnchor constraintEqualToConstant:screenWidth * 0.83].active = YES;
    
    [self.conditionTextView.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.conditionTextView.topAnchor constraintEqualToAnchor:self.enterCommentslabel.bottomAnchor
                                                     constant:screenHeight * 0.01].active = YES;
    [self.conditionTextView.widthAnchor constraintEqualToConstant:screenWidth * 0.83].active = YES;
    [self.conditionTextView.heightAnchor constraintEqualToConstant:screenHeight * 0.2].active = YES;
    
    [self.bottomlabel1 setFont:[UIFont fontWithName:@"Thonburi" size:textFontSize]];
    [self.bottomlabel2 setFont:[UIFont fontWithName:@"Thonburi" size:textFontSize]];
    [self.bottomlabel1.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
  
    [self.bottomlabel1.topAnchor constraintEqualToAnchor:self.conditionTextView.bottomAnchor constant:screenHeight * 0.02].active = YES;
    
    [self.bottomlabel2.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.bottomlabel2.topAnchor constraintEqualToAnchor:self.bottomlabel1.bottomAnchor constant:-2].active = YES;
    
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    
    [self.submitButton.centerXAnchor constraintEqualToAnchor:self.reservedView.centerXAnchor].active = YES;
    [self.submitButton.bottomAnchor constraintEqualToAnchor:self.reservedView.bottomAnchor
                                                   constant:-screenHeight * 0.01].active = YES;
    [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
    [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    
    
}

-(void) closeAction:(id) sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) setupSuccessView{
    
    
    CGFloat screenWidth = self.view.bounds.size.width;
    CGFloat screenHeight = self.view.bounds.size.height;
    
    successView = [[UIView alloc] initWithFrame:self.view.frame];
    successView.backgroundColor = [UIColor whiteColor];
    
    closeModButton = [[UIButton alloc] init];
    [closeModButton setImage:[UIImage imageNamed:@"closeButton"] forState:UIControlStateNormal];
    
    dearLabel = [[UILabel alloc] init];
    dearLabel.font = [UIFont fontWithName:@"Thonburi" size:dearFontSize];
    dearLabel.text = DPLocalizedString(@"respected", nil);
    dearLabel.textAlignment = NSTextAlignmentCenter;
    dearLabel.textColor = mainColor;
    
    fullNameLabel = [[UILabel alloc] init];
    fullNameLabel.font = [UIFont fontWithName:@"Thonburi-Bold" size:dearFontSize];
    fullNameLabel.textAlignment = NSTextAlignmentCenter;
    fullNameLabel.textColor = mainColor;
    
    reservedLabel1 = [[UILabel alloc] init];
    reservedLabel1.text = DPLocalizedString(@"table_successful", nil);
    reservedLabel1.font = [UIFont fontWithName:@"Thonburi" size:reservFontSize];
    reservedLabel1.textAlignment = NSTextAlignmentCenter;
    reservedLabel1.textColor = mainColor;
    
    reservedLabel2 = [[UILabel alloc] init];
    reservedLabel2.text = DPLocalizedString(@"booked", nil);
    reservedLabel2.font = [UIFont fontWithName:@"Thonburi" size:reservFontSize];
    reservedLabel2.textAlignment = NSTextAlignmentCenter;
    reservedLabel2.textColor = mainColor;
    
    
    reservedImage = [[UIImageView alloc] init];
    reservedImage.image = [UIImage imageNamed:@"reserved"];
    reservedImage.contentMode = UIViewContentModeScaleAspectFit;
    
    [successView addSubview:closeModButton];
    [successView addSubview:dearLabel];
    [successView addSubview:fullNameLabel];
    [successView addSubview:reservedLabel1];
    [successView addSubview:reservedLabel2];
    [successView addSubview:reservedImage];
    
    [self.view addSubview:successView];
    
    closeModButton.translatesAutoresizingMaskIntoConstraints = false;
    dearLabel.translatesAutoresizingMaskIntoConstraints = false;
    fullNameLabel.translatesAutoresizingMaskIntoConstraints = false;
    reservedLabel1.translatesAutoresizingMaskIntoConstraints = false;
    reservedLabel2.translatesAutoresizingMaskIntoConstraints = false;
    reservedImage.translatesAutoresizingMaskIntoConstraints = false;
    
    [closeModButton.centerXAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-screenWidth * 0.09].active = YES;
    [closeModButton.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.09].active = YES;
    [closeModButton.widthAnchor constraintEqualToConstant:20].active = YES;
    [closeModButton.heightAnchor constraintEqualToConstant:20].active = YES;
    [closeModButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (screenWidth < 322) {
        [dearLabel setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [dearLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [dearLabel.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.16].active = YES;
    [dearLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.dearLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [fullNameLabel setFont:[UIFont fontWithName:@"Thonburi-Bold" size:18.0]];
    }
    fullNameLabel.text = [NSString stringWithFormat:@"%@!",_fullNameTextField.text];
    [fullNameLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [fullNameLabel.topAnchor constraintEqualToAnchor:dearLabel.bottomAnchor constant:0].active = YES;
    [fullNameLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.fullNameLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [reservedLabel1 setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [reservedLabel1.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [reservedLabel1.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.29].active = YES;
    [reservedLabel1.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.reservedLabel1.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    if (screenWidth < 322) {
        [reservedLabel2 setFont:[UIFont fontWithName:@"Thonburi" size:18.0]];
    }
    [reservedLabel2.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [reservedLabel2.topAnchor constraintEqualToAnchor:reservedLabel1.bottomAnchor constant:0].active = YES;
    [reservedLabel2.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
    //    [self.reservedLabel2.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
    [reservedImage.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [reservedImage.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.46].active = YES;
    [reservedImage.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-screenHeight * 0.06].active = YES;
    
}
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_fullNameTextField resignFirstResponder];
//    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    return YES;
}

-(NSAttributedString*) placeholderString:(NSString*)str withFontSize:(double)size{
    
    NSAttributedString* placeholderString =
    [[NSAttributedString alloc] initWithString:str
                                    attributes:@{
                                                 NSForegroundColorAttributeName:  [[UIColor lightGrayColor] colorWithAlphaComponent:0.5],
                                                 NSFontAttributeName : [UIFont fontWithName:@"Thonburi" size:size]
                                                 }
     ];
    
    return placeholderString;
}

@end
