//
//  OrdersViewController.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 5/2/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "OrdersViewController.h"
#import "RNCancelRequest.h"
#import "RNCancelResponse.h"
#import "UIViewController+SlideMenuControllerOC.h"
#import "OrderCell.h"
#import "OrderCellPad.h"
#import "RNOrderInfo.h"
#import "GetReserveRequest.h"
#import "GetReserveResponse.h"
#import "RNUser.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
#import "ReservTableViewController.h"

#import "Reachability.h"


static NSString *const kReservCountKey = @"ReservCountKey";
NSString *const OrdersCountChangeNotification = @"OrdersCountChangeNotification";


@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}
@end


@interface OrdersViewController ()<UITableViewDelegate, UITableViewDataSource, ReservTableViewControllerDelegate>{
    NSMutableArray* reservs;
    UIView* emptyContainerView;
    UIColor* mainColor;
    NSInteger reservsCount;
    
    double textFieldFontSize;
    double buttonFontSize;
    double cornerRadius;
    
    CGFloat screenWidth;
    CGFloat screenHeight;
    Boolean isPhone;
    ReservTableViewController* reservTableViewController;
    
    NSInteger currCount;
    
    Reachability *_reachability;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *treshBarButton;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;
@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        textFieldFontSize = 17.0;
        buttonFontSize = 17.0;
        cornerRadius = 3;
        isPhone = YES;
        
        if (screenWidth < 322) {
            [self.myOrdersLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
        }
        
        [self.myOrdersLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        textFieldFontSize = 20.0;
        buttonFontSize = 26.0;
        cornerRadius = 5;
        isPhone = NO;
        
        [self.myOrdersLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
        
        self.topView.translatesAutoresizingMaskIntoConstraints = false;
        self.myOrdersLabel.translatesAutoresizingMaskIntoConstraints = false;
        
        [self.topView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
        [self.topView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
        [self.topView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:90].active = YES;
        
        [self.myOrdersLabel.centerXAnchor constraintEqualToAnchor:self.topView.centerXAnchor].active = YES;
        [self.myOrdersLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:93].active = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
    _reachability = [Reachability reachabilityForInternetConnection];
    [_reachability startNotifier];
    
    self.title = DPLocalizedString(@"my_orders_title", nil);
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    reservsCount = 0;
    reservs = [NSMutableArray array];
    
    [self configureFormatters];
    self.editing = NO;
    
    [emptyContainerView removeFromSuperview];
    emptyContainerView = nil;
    
    if ([self isInternetConnect]){
        [self loadReservs];
    }
    
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    if (_isMenu == YES){
        [self setNavigationBarItem];
        
    } else {
        [self addLeftBarButtonWithImage];
        reservTableViewController = [[ReservTableViewController alloc] init];
        reservTableViewController.delegate = self;
    }
   
    
}

-(void)changeReservesCountDelegate: (ReservTableViewController *) controller{
    controller.delegate = self;
    NSString* currCountStr = [NSString stringWithFormat:@"%ld", currCount];
    [controller.openReservBarButton setTitle:currCountStr];
}

- (void)addLeftBarButtonWithImage{
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowLeft"] style:UIBarButtonItemStylePlain target:self action:@selector(goBackAction)];
    self.navigationItem.leftBarButtonItem = leftButton;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   [UIFont fontWithName:@"Thonburi" size:18], NSFontAttributeName,
                                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                   nil];
 
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.barTintColor = mainColor;

    [self.navigationController.navigationBar setTranslucent:YES];
}

-(void) goBackAction{
    [[NSNotificationCenter defaultCenter] postNotificationName:OrdersCountChangeNotification object:nil userInfo:@{@"reservCount" : @(currCount)}];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}


-(void)inetState:(NSNotification *)notif
{
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable && [[NSUserDefaults standardUserDefaults] objectForKey:@"totalObj"])
    {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.title = DPLocalizedString(@"my_orders_title", nil);
    [emptyContainerView removeFromSuperview];
    emptyContainerView = nil;
    if ([self isInternetConnect]){
        [self loadReservs];
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [emptyContainerView removeFromSuperview];
    emptyContainerView = nil;

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)configureFormatters {
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd MMMM yyyy"];
    _dateFormatter.locale  = [[NSLocale alloc] initWithLocaleIdentifier:@"ru"];
    
    self.timeFormatter = [[NSDateFormatter alloc] init];
    [_timeFormatter setDateFormat:@"HH:mm"];
}


- (void)loadReservs{
    GetReserveRequest *request = [[GetReserveRequest alloc] init];
    
    NSString* langCode = [[NSString alloc] init];
    
    if ([dp_get_current_language() isEqualToString:@"ru"]){
        langCode = @"ru";
    } else if ([dp_get_current_language() isEqualToString:@"uk"]){
        langCode = @"ua";
    }
    request.langCode = langCode;
    
    [request sendWithcompletion:^(RNResponse *response, NSError *error) {
       if (error != nil || [response isKindOfClass:[NSNull class]]) {
            [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
        }
        GetReserveResponse *r = (GetReserveResponse *)response;
        
        reservsCount = 0;
        NSArray* tempArr = r.reserves;
//        NSLog(@"reservsCount++++++++++ %ld",r.reserves.count);
        
        tempArr = [tempArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            RNOrderInfo *r1 = obj1;
            RNOrderInfo *r2 = obj2;
            
            return [r1.dateStr compare:r2.dateStr];
        }];
        
         reservs = [tempArr mutableCopy];
        
        currCount = reservs.count;
        
        if (!_isMenu){
            [self changeReservesCountDelegate:reservTableViewController];
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:reservs.count forKey:kReservCountKey];
        [[NSUserDefaults standardUserDefaults] synchronize];

        if (reservs.count > 0){

            [self.view bringSubviewToFront:_tableView];
            [self.treshBarButton setTintColor:[UIColor whiteColor]];
            [_tableView reloadData];
            [self getReservsCountFromArr:[NSArray arrayWithArray:reservs]];
        } else if (reservs.count == 0){
            [self.treshBarButton setTintColor:[UIColor clearColor]];
            [self setupView];
        }
    }];
}

-(void) getReservsCountFromArr:(NSArray*)reservsArr{
    
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
     NSComparisonResult result;
    
    for (int i = 0; i < reservsArr.count; i++) {
        
        RNOrderInfo* reserv = reservsArr[i];
        
        NSDate* reservDate = [formatter dateFromString:reserv.dateStr];

        result = [[NSDate date] compare:reservDate];
        
        if(result == NSOrderedAscending){
            NSLog(@"today is less");
          
            reservsCount = reservsCount + 1;
            NSLog(@"reservsCount %ld",reservsCount);
             NSLog(@"i %d",i);
        } 
    }
}

- (void)showAlertWithText:(NSString *)text {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel]; // 4
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)orderAction:(id)sender {
    
    [emptyContainerView removeFromSuperview];
    emptyContainerView = nil;
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(changeViewController:)]) {
        
        [_delegate changeViewController:LeftCatalogue];
        _delegate = nil;
        [emptyContainerView removeFromSuperview];
        emptyContainerView = nil;
    }
}

- (IBAction)deleteOrdersAction:(id)sender {
    
    [self addORDoneRows];
}


- (void)addORDoneRows
{
    if(self.editing)
    {
        [super setEditing:NO animated:NO];
        [_tableView setEditing:NO animated:NO];
        [_tableView reloadData];
                [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
    }
    else
    {
        [super setEditing:YES animated:YES];
        [_tableView setEditing:YES animated:YES];
        [_tableView reloadData];
                [self.treshBarButton setBackgroundImage:[UIImage imageNamed:@"Remote"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
                [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat heigth = 224;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        heigth = 360;
    }
    return heigth;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return reservs.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* newCell;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
    OrderCell *cell = (OrderCell *)[tableView dequeueReusableCellWithIdentifier:@"OrderCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.reserv = reservs[indexPath.row];
        [cell updateCell];
        
        newCell = (OrderCell *)cell;
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
    OrderCellPad *cell = (OrderCellPad *)[tableView dequeueReusableCellWithIdentifier:@"OrderCellPad"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OrderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.reserv = reservs[indexPath.row];
        [cell updateCell];
        
        newCell = (OrderCellPad *)cell;

    }
    return newCell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self isInternetConnect]){
        RNOrderInfo* reserv = reservs[indexPath.row];
        
        RNCancelRequest *request = [[RNCancelRequest alloc] init];
        request.reserveId = reserv.reservID;
        
        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
            if (error != nil || [response isKindOfClass:[NSNull class]]) {
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            }
            [self loadReservs];
        }];
      }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


////////////////////////////////////////////////////////////////////////////////////////////////////


-(void) setupView {
    
    
    
    emptyContainerView = [[UIView alloc] init];
    emptyContainerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:emptyContainerView];
    
    emptyContainerView.translatesAutoresizingMaskIntoConstraints = false;
    

    [emptyContainerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [emptyContainerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [emptyContainerView.topAnchor constraintEqualToAnchor:self.topView.bottomAnchor].active = YES;
    [emptyContainerView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    _backOrders = [[UIImageView alloc] init];
    _backOrders.image = [UIImage imageNamed:@"iconOrder"];
    _backOrders.contentMode = UIViewContentModeScaleAspectFit;
    
    [emptyContainerView addSubview:_backOrders];
    
    
    self.backOrders.translatesAutoresizingMaskIntoConstraints = false;
    
    [self.backOrders.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.backOrders.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:screenHeight * 0.43].active = YES;
    [self.backOrders.widthAnchor constraintEqualToConstant:screenWidth * 0.32].active = YES;
    [self.backOrders.heightAnchor constraintEqualToConstant:screenHeight * 0.23].active = YES;
    
    _submitButton = [[UIButton alloc] init];
   
    [emptyContainerView addSubview:_submitButton];
    
     _submitButton.translatesAutoresizingMaskIntoConstraints = false;
    
    [_submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:buttonFontSize]];
    
    _submitButton.backgroundColor = mainColor;
    
    [_submitButton setTitle:DPLocalizedString(@"reserv_action", nil) forState:UIControlStateNormal];
    [_submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _submitButton.layer.cornerRadius = cornerRadius;
    _submitButton.layer.masksToBounds = true;
    _submitButton.userInteractionEnabled = YES;
    [_submitButton addTarget:self action:@selector(orderAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if (screenWidth < 322) {
        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.submitButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(screenHeight * 0.13)].active = YES;
    if (isPhone){
        [_submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.47].active = YES;
        [_submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
    } else {
        [self.submitButton.widthAnchor constraintEqualToConstant:240].active = YES;
        [self.submitButton.heightAnchor constraintEqualToConstant:71].active = YES;
    }
   
    
    _textLabel = [[UILabel alloc] init];
    [emptyContainerView addSubview:_textLabel];
    
    [self.textLabel setFont:[UIFont fontWithName:@"Thonburi" size:textFieldFontSize]];
    self.textLabel.translatesAutoresizingMaskIntoConstraints = false;
  
    
    _textLabel.text = DPLocalizedString(@"no_orders", nil);
    _textLabel.textColor = [UIColor lightGrayColor];
    _textLabel.numberOfLines = 2;
    _textLabel.textAlignment = NSTextAlignmentCenter;
    
    if (screenWidth < 322) {
        [self.textLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
    }
    [self.textLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.textLabel.centerYAnchor constraintEqualToAnchor:self.submitButton.topAnchor constant:-screenHeight * 0.06].active = YES;
    [self.textLabel.widthAnchor constraintEqualToConstant:screenWidth * 0.8].active = YES;
    [self.textLabel.heightAnchor constraintEqualToConstant:screenHeight * 0.08].active = YES;
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

@end
