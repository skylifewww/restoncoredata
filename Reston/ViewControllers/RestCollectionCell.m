//
//  RestCollectionCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 08.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "RestCollectionCell.h"
#import "RNRestaurant.h"
#import "UIImage+Cache.h"
#import "RNUser.h"
#import "RNCountPeopleHelper.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"

//NSString* const SubmitButtonNotification = @"SubmitButtonNotification";

@implementation RestCollectionCell

@synthesize restNamelable = _restNamelable;
@synthesize restType = _restType;
//@synthesize restAddress = _restAddress;
@synthesize ratingImage = _ratingImage;
@synthesize commentsCount = _commentsCount;
@synthesize averageLabel = _averageLabel;
@synthesize distanceRest = _distanceRest;
@synthesize restImage = _restImage;
@synthesize discountBack = _discountBack;
@synthesize restDiscount = _restDiscount;
@synthesize callButton = _callButton;
@synthesize containerView = _containerView;
@synthesize deleteButton = _deleteButton;


//- (void)applyData:(RNRestaurant *)model {
//    [self applyRestaurantData:model];
//    //    _favoriteBtn.hidden = NO;
//}
//
//- (void)applyRestaurantData:(RNRestaurant *)model {
//    //    _favoriteBtn.hidden = YES;
//    _restNamelable.text = model.title;
//    _commentsCount.text = [NSString stringWithFormat:@"%@ %@",model.reviewCount,[RNCountPeopleHelper returnCorrectNameFeedbackWithCount:model.reviewCount] ];
//    [UIImage cachedImage:model.images[0]
//           fullURLString:nil
//            withCallBack:^(UIImage *image) {
//                _restImage.image = image;
//            }];
//    //    NSString *str = [model.kitchen componentsJoinedByString:@", "];
//    //    _cuisineLabel.text = str;
//
//    NSString *addresssStr = [model.address stringByReplacingOccurrencesOfString:@"Украина, Киев, "
//                                                                     withString:@""];
//    _restAddress.text = addresssStr;
//    if (model.avgBill.integerValue == 0) {
//        _averageLabel.text = @"Неизвестно";
//    } else {
//        _averageLabel.text = [NSString stringWithFormat:@"%@ грн.",model.avgBill];
//    }
//    //    _subwayLabel.text = [[RNUser sharedInstance] subwayNameFord:model.subway];
//
//    if (model.discounts.integerValue == 0) {
//        _discountBack.image = nil;
//        _restDiscount.hidden = YES;
//    } else {
//        _restDiscount.hidden = NO;
//        _restDiscount.text = [NSString stringWithFormat:@"-%@%%",model.discounts];
//        _discountBack.image = [UIImage imageNamed:@"DiscountEmpty"];
//    }
//}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    self.submitButton.layer.cornerRadius = 3;
    self.submitButton.layer.masksToBounds = true;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:17.0]];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
    NSString* titleButton = DPLocalizedString(@"reserv_action", nil);
    NSAttributedString *strEnter = [[NSAttributedString alloc] initWithString:titleButton attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    [self.submitButton setAttributedTitle:strEnter forState:UIControlStateNormal];
    
//    self.deleteButton.hidden = YES;
//    self.containerView.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.descripView.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.addressView.translatesAutoresizingMaskIntoConstraints = false;
//    //    self.mapIconImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.ratingView.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentView.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentsIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.avgIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.distansIcon.translatesAutoresizingMaskIntoConstraints = false;
//    self.submitButton.translatesAutoresizingMaskIntoConstraints = false;
//    self.restNamelable.translatesAutoresizingMaskIntoConstraints = false;
//    self.restType.translatesAutoresizingMaskIntoConstraints = false;
////    self.restAddress.translatesAutoresizingMaskIntoConstraints = false;
//    self.ratingImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.commentsCount.translatesAutoresizingMaskIntoConstraints = false;
//    self.averageLabel.translatesAutoresizingMaskIntoConstraints = false;
//    self.distanceRest.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.restImage.translatesAutoresizingMaskIntoConstraints = false;
//    self.restImage.contentMode = UIViewContentModeScaleAspectFill;
//    
//    self.discountBack.translatesAutoresizingMaskIntoConstraints = false;
//    self.restDiscount.translatesAutoresizingMaskIntoConstraints = false;
//    self.callButton.translatesAutoresizingMaskIntoConstraints = false;
//    
//    
//    //    self.callButton.hidden = YES;
//    //    self.callButton.userInteractionEnabled = NO;
//    
//    [self setupView];
}

//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    self.callButton.hidden = NO;
//    self.callButton.userInteractionEnabled = YES;
//
//
//}


//-(void) setupView {
//    
//    CGFloat screenWidth = self.bounds.size.width;
//    CGFloat screenHeight =self.bounds.size.height;
//    
//    
//    [self.containerView.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor].active = YES;
//    [self.containerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
//    [self.containerView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
//    
//    //    if (screenWidth < 322) {
//    //        [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.964].active = YES;
//    //    } else {
//    
//    [self.containerView.widthAnchor constraintEqualToConstant:screenWidth * 0.93].active = YES;
//    //    }
//    
//    //    if (screenWidth < 322) {
//    //        [self.containerView.heightAnchor constraintEqualToConstant:289].active = YES;
//    //    } else {
//    
//    [self.containerView.heightAnchor constraintEqualToConstant:299].active = YES;
//    //    }
//    
//    
//    
//    
//    
//    
//    //    if (screenWidth < 322) {
//    //        [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:screenWidth * 0.05].active = YES;
//    //
//    //        [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-screenWidth * 0.05].active = YES;
//    //    } else {
//    
//    [self.restImage.leftAnchor constraintEqualToAnchor:self.containerView.leftAnchor constant:16].active = YES;
//    
//    [self.restImage.rightAnchor constraintEqualToAnchor:self.containerView.rightAnchor constant:-16].active = YES;
//    //    }
//    
//    [self.restImage.topAnchor constraintEqualToAnchor:self.containerView.topAnchor].active = YES;
//    
//    
//    //    if (screenWidth < 322) {
//    //        [self.restImage.heightAnchor  constraintEqualToConstant:156].active = YES;
//    //    } else {
//    
//    [self.restImage.heightAnchor  constraintEqualToConstant:(screenWidth-32)*0.8f].active = YES;
//    //    }
//    
//    
//    //    if (screenWidth < 322) {
//    //        [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:12].active = YES;
//    //    } else {
//    
//    [self.callButton.leftAnchor constraintEqualToAnchor:self.restImage.leftAnchor constant:18].active = YES;
//    //    }
//    
//    
//    //    self.callButton.hidden = YES;
//    //    self.callButton.userInteractionEnabled = NO;
//    [self.callButton.centerYAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:116].active = YES;
//    [self.callButton.widthAnchor  constraintEqualToConstant:56].active = YES;
//    [self.callButton.heightAnchor  constraintEqualToConstant:56].active = YES;
//    
//    
//    //    if (screenWidth < 322) {
//    //        [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-2].active = YES;
//    //    } else {
//    
//    [self.discountBack.rightAnchor constraintEqualToAnchor:self.restImage.rightAnchor constant:-8].active = YES;
//    //    }
//    
//    
//    [self.discountBack.topAnchor constraintEqualToAnchor:self.restImage.topAnchor constant:11].active = YES;
//    [self.discountBack.widthAnchor  constraintEqualToConstant:60].active = YES;
//    [self.discountBack.heightAnchor  constraintEqualToConstant:34].active = YES;
//    
//    [self.restDiscount.rightAnchor constraintEqualToAnchor:self.discountBack.rightAnchor constant:-4].active = YES;
//    [self.restDiscount.centerYAnchor constraintEqualToAnchor:self.discountBack.centerYAnchor].active = YES;
//    
//    
//    [self.descripView.centerXAnchor constraintEqualToAnchor:self.containerView.centerXAnchor].active = YES;
//    [self.descripView.topAnchor constraintEqualToAnchor:self.restImage.bottomAnchor].active = YES;
//    [self.descripView.bottomAnchor constraintEqualToAnchor:self.containerView.bottomAnchor].active = YES;
//    [self.descripView.widthAnchor constraintEqualToAnchor:self.containerView.widthAnchor].active = YES;
//    
//    
//    [self.restNamelable.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:9].active = YES;
//    [self.restNamelable.topAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:5].active = YES;
//    [self.restNamelable.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-10].active = YES;
//    
//    
//    [self.addressView.centerXAnchor constraintEqualToAnchor:self.descripView.centerXAnchor].active = YES;
//    [self.addressView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:38].active = YES;
//    [self.addressView.widthAnchor constraintEqualToAnchor:self.descripView.widthAnchor].active = YES;
//    
//    
//    [self.restType.leftAnchor constraintEqualToAnchor:self.addressView.leftAnchor constant: 10].active = YES;
//    [self.restType.centerYAnchor constraintEqualToAnchor:self.addressView.centerYAnchor].active = YES;
//    [self.restType.widthAnchor constraintEqualToAnchor:self.addressView.widthAnchor].active = YES;
//    
//    
//    [self.ratingView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor constant:10].active = YES;
//    [self.ratingView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:64].active = YES;
//    [self.ratingView.widthAnchor constraintEqualToConstant:115].active = YES;
//    [self.ratingView.heightAnchor constraintEqualToConstant:24].active = YES;
//    
//    
//    [self.ratingImage.centerXAnchor constraintEqualToAnchor:self.ratingView.centerXAnchor].active = YES;
//    [self.ratingImage.centerYAnchor constraintEqualToAnchor:self.ratingView.centerYAnchor].active = YES;
//    [self.ratingImage.widthAnchor constraintEqualToConstant:115].active = YES;
//    [self.ratingImage.heightAnchor constraintEqualToConstant:19].active = YES;
//    
//    [self.commentView.leftAnchor constraintEqualToAnchor:self.descripView.leftAnchor].active = YES;
//    [self.commentView.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:91].active = YES;
//    [self.commentView.widthAnchor constraintEqualToConstant: screenWidth * 0.5].active = YES;
//    [self.commentView.heightAnchor constraintEqualToConstant:28].active = YES;
//    
//    [self.commentsIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: 12].active = YES;
//    [self.commentsIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
//    [self.commentsIcon.widthAnchor constraintEqualToConstant:17].active = YES;
//    [self.commentsIcon.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.commentsCount.leftAnchor constraintEqualToAnchor:self.commentsIcon.rightAnchor constant: 5].active = YES;
//    [self.commentsCount.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
//    
//    [self.avgIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: screenWidth * 0.21].active = YES;
//    [self.avgIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
//    [self.avgIcon.widthAnchor constraintEqualToConstant:17].active = YES;
//    [self.avgIcon.heightAnchor constraintEqualToConstant:18].active = YES;
//    
//    [self.averageLabel.leftAnchor constraintEqualToAnchor:self.avgIcon.rightAnchor constant: 5].active = YES;
//    [self.averageLabel.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
//    
//    [self.distansIcon.leftAnchor constraintEqualToAnchor:self.commentView.leftAnchor constant: screenWidth * 0.38].active = YES;
//    [self.distansIcon.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor].active = YES;
//    [self.distansIcon.widthAnchor constraintEqualToConstant:18].active = YES;
//    [self.distansIcon.heightAnchor constraintEqualToConstant:16].active = YES;
//    
//    [self.distanceRest.leftAnchor constraintEqualToAnchor:self.distansIcon.rightAnchor].active = YES;
//    [self.distanceRest.centerYAnchor constraintEqualToAnchor:self.commentView.centerYAnchor constant: 2].active = YES;
//    
//    
//    //
//    //    if (screenWidth < 322) {
//    //        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
//    //    }
//    
//    //    if (screenWidth < 322) {
//    //        [self.submitButton.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:14.0]];
//    //        [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
//    //    } else {
//    [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:0].active = YES;
//    //    }
//    
//    [self.submitButton.rightAnchor constraintEqualToAnchor:self.descripView.rightAnchor constant:-(screenWidth * 0.026)].active = YES;
//    [self.submitButton.centerYAnchor constraintEqualToAnchor:self.descripView.topAnchor constant:80].active = YES;
//    [self.submitButton.widthAnchor constraintEqualToConstant:screenWidth * 0.37].active = YES;
//    [self.submitButton.heightAnchor constraintEqualToConstant:screenHeight * 0.07].active = YES;
//    
//}
//
//
////- (IBAction)submitButton:(id)sender {
////    
////    [[NSNotificationCenter defaultCenter] postNotificationName:SubmitButtonNotification object:nil];
////    
////}
//
////- (IBAction)callActionButton:(id)sender {
////}
//-(void) setHighlighted:(BOOL)highlighted{
//    
//    //        self.callButton.hidden = NO;
//    //        self.callButton.userInteractionEnabled = YES;
//}



@end


