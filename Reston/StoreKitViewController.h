//
//  StoreKitViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.02.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface StoreKitViewController : UIViewController
<SKStoreProductViewControllerDelegate>

//- (IBAction)showStoreView:(id)sender;
@end

