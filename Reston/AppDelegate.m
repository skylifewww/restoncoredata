//
//  AppDelegate.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "AppDelegate.h"
@import UIKit;

#import "RNLoginViewController.h"
#import "LaunchViewController.h"
#import "RNAuthRequest.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"
#import "PageViewController.h"
#import "SlideEnterViewController.h"
#import "CatalogRestViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "define.h"
#import "DPLocalization.h"
#import "Reachability.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import <UAAppReviewManager.h>
#import <Appirater/Appirater.h>
#import "Harpy.h"
#import "LocationManager.h"

//#import <RestKit/CoreData.h>
//#import <RestKit/RestKit.h>

#import "CoreDataManager.h"

//@import Firebase;
//#import <UserNotifications/UserNotifications.h>
#define CHECK_VERSION(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

NSString *const kServerURLMain = @"http://api.reston.com.ua";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}

@end

@interface AppDelegate ()<CLLocationManagerDelegate, UIApplicationDelegate
//, UNUserNotificationCenterDelegate
>

{
    UIStoryboard *_storyboard;
    Reachability *_reachability;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@end

void print_free_memory ()
{
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
    }
    
    /* Stats in bytes */
    natural_t mem_used = (vm_stat.active_count +
                          vm_stat.inactive_count +
                          vm_stat.wire_count) * (natural_t)pagesize;
    natural_t mem_free = vm_stat.free_count * (natural_t)pagesize;
    natural_t mem_total = mem_used + mem_free;
    NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
}

@implementation AppDelegate

+ (void)initialize {
//    [AppDelegate setupUAAppReviewManager];
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    static NSString *appITunesItemIdentifier = @"1014075207";
    
    [Appirater setAppId:appITunesItemIdentifier];
    [Appirater setDaysUntilPrompt:0];
    [Appirater setUsesUntilPrompt:20];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:30];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
//    [FIRApp configure];
//    [self registerPushNotification];
 
    [Fabric with:@[[Crashlytics class]]];
    
//    [UAAppReviewManager showPromptIfNecessary];

//    [[SDImageCache sharedImageCache] setMaxMemoryCountLimit:30];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inetState:) name:kReachabilityChangedNotification object:nil];
//    _reachability = [Reachability reachabilityForInternetConnection];
//    [_reachability startNotifier];
//    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        
        [self showAlert];
    }
    
    
    // TODO: Move this to where you establish a user session
    [self logUser];


    _locationManager = [[CLLocationManager alloc]init]; // initializing locationManager
    _locationManager.delegate = self; // we set the delegate of locationManager to self.
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
    
    [_locationManager startUpdatingLocation];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [LocationManager sharedInstance];

    
    NSString *language = [[NSUserDefaults standardUserDefaults] stringForKey:languageOnApp];
    if(!language)
    {
        NSString *languageDivice = [NSLocale preferredLanguages][0] ;
        languageDivice = [languageDivice componentsSeparatedByString:@"-"][0];
        if( [langArray containsObject:languageDivice])
            [[NSUserDefaults standardUserDefaults] setObject:languageDivice forKey:languageOnApp];
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"ru" forKey:languageOnApp];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [DPLocalizationManager currentManager].defaultStringTableName = @"language";
    
    dp_set_current_language([[NSUserDefaults standardUserDefaults] stringForKey:languageOnApp]);
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    } else {
        _storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }
    
    LaunchViewController *launchViewController = [_storyboard instantiateViewControllerWithIdentifier:@"LaunchViewController"];
    
    [self.window setRootViewController:launchViewController];
    
    [[Harpy sharedInstance] setPresentingViewController:self.window.rootViewController];
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeForce];
    [[Harpy sharedInstance] setAlertControllerTintColor:[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0]];
    [[Harpy sharedInstance] setDebugEnabled:false];
    [[Harpy sharedInstance] checkVersion];
    
    
    
     return YES;
}


- (void)harpyDidShowUpdateDialog{
    NSLog(@"harpyDidShowUpdateDialog");
}

+ (void)setupUAAppReviewManager {

//    static NSInteger const appITunesItemIdentifier = 1014075207;
    [UAAppReviewManager setAppID:@"1014075207"];
    [UAAppReviewManager setDebug:YES];
    [UAAppReviewManager setAppName:@"RestOn"];
    [UAAppReviewManager setReviewTitle:@"Оцените приложение!"];
    [UAAppReviewManager setReviewMessage:@"Нам важна ваша оценка!"];
    [UAAppReviewManager setCancelButtonTitle:@"Отмена"];
    [UAAppReviewManager setRateButtonTitle:@"Оценить!"];
    [UAAppReviewManager setRemindButtonTitle:@"Напомнить позже"];
    [UAAppReviewManager setDaysUntilPrompt:30];
    [UAAppReviewManager setDaysBeforeReminding:1];
    [UAAppReviewManager setShouldPromptIfRated:YES];
    [UAAppReviewManager setSignificantEventsUntilPrompt:20];
    [UAAppReviewManager setTracksNewVersions:YES];
    [UAAppReviewManager setUseMainAppBundleForLocalizations:YES];
//    [UAAppReviewManager setOpensInStoreKit:YES];
    [UAAppReviewManager setUsesAnimation:YES];
    [UAAppReviewManager setAffiliateCode:@"11l7j9"];
    [UAAppReviewManager setAffiliateCampaignCode:@"Reston"];

    [UAAppReviewManager setOnDeclineToRate:^() {
        NSLog(@"The user just declined to rate");
    }];
    [UAAppReviewManager setOnDidDisplayAlert:^() {
        NSLog(@"We just displayed the rating prompt");
    }];
    [UAAppReviewManager setOnDidOptToRate:^() {
        NSLog(@"The user just opted to rate");
    }];
    [UAAppReviewManager setOnDidOptToRemindLater:^() {
        NSLog(@"The user just opted to remind later");
    }];
    [UAAppReviewManager setOnWillPresentModalView:^(BOOL animated) {
        NSLog(@"About to present the modal view: %@animated", (animated?@"":@"not "));
    }];
    [UAAppReviewManager setOnDidDismissModalView:^(BOOL animated) {
        NSLog(@"Just dismissed the modal view: %@animated", (animated?@"":@"not "));
    }];
    [UAAppReviewManager setKey:@"UAAppReviewManagerKeySignificantEventCount" forUAAppReviewManagerKeyType:UAAppReviewManagerKeySignificantEventCount];

    // YES here means it is ok to show, but it doesn't matter because we have debug on.
    [UAAppReviewManager userDidSignificantEvent:YES];

    // You can also call it with a block to circumvent any of UAAppReviewManager's should rate logic.
    [UAAppReviewManager userDidSignificantEventWithShouldPromptBlock:^BOOL(NSDictionary *trackingInfo) {
        //the tracking info dictionary has all the keys/value UAAppReviewManager uses to determine whether or not to show a prompt
        return NO;
    }];

    // Or you can set a global one to get one last chance to stop the prompt, or do your own logic
    [UAAppReviewManager setShouldPromptBlock:^BOOL(NSDictionary *trackingInfo) {
        // This will be called once all other rating conditions have been met, but before the prompt.
        // if a local UAAppReviewManagerShouldPromptBlock is called using the local methods, this will not be called.
        // Return YES to allow the prompt, NO to stop the presentation.
        return YES;
    }];
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if(self.restrictRotation)
        return UIInterfaceOrientationMaskPortrait;
    else
        return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



- (void) logUser {
    // TODO: Use the current user's information
    // You can call any combination of these three methods
    [CrashlyticsKit setUserIdentifier:@"12345"];
    [CrashlyticsKit setUserEmail:@"skylife@ukr.net"];
    [CrashlyticsKit setUserName:@"Test User"];
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                          options:options];
}

// Still need this for iOS8
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(nullable NSString *)sourceApplication
         annotation:(nonnull id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet connection error"
                                                    message:DPLocalizedString(@"Try later!", nil)
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [Appirater appEnteredForeground:YES];
    
//    [UAAppReviewManager showPromptWithShouldPromptBlock:^(NSDictionary *trackingInfo) {
//        // This is the block syntax for showing prompts.
//        // It lets you decide if it should be shown now or not based on
//        // the UAAppReviewManager trackingInfo or any other factor.
//        NSLog(@"UAAppReviewManager trackingInfo: %@", trackingInfo);
//        // Don't show the prompt now, but do it from the buttons in the example app view controller
//        return NO;
//    }];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [[CoreDataManager sharedManager] saveContext];
}

#pragma mark - Register UNUserNotificationCenter

//- (void) registerPushNotification{
//
//    if(CHECK_VERSION(@"10.0")) { // iOS 10+
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
//            if( !error ){
//                dispatch_async(dispatch_get_main_queue(), ^{
//                                        [[UIApplication sharedApplication] registerForRemoteNotifications];
//                                    });
//            }
//        }];
//    }
//    else { // < iOS 10
//        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [[UIApplication sharedApplication] registerForRemoteNotifications];
//        });
//    }

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    
//    if(CHECK_VERSION(@"10.0")) { // iOS 10+
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
//            if( !error ){
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[UIApplication sharedApplication] registerForRemoteNotifications];
//                });
//                [self getNotificationSettings];
//            }
//        }];
//    }
//    else { // < iOS 10
//        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [[UIApplication sharedApplication] registerForRemoteNotifications];
//        });
//        [self getNotificationSettings];
//    }
//}

//-(void) getNotificationSettings{
//
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings *settings){
//
//        //1. Query the authorization status of the UNNotificationSettings object
//        switch (settings.authorizationStatus) {
//            case UNAuthorizationStatusAuthorized:
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[UIApplication sharedApplication] registerForRemoteNotifications];
//                });
//                NSLog(@"Status Authorized");
//                break;
//            case UNAuthorizationStatusDenied:
//                NSLog(@"Status Denied");
//                break;
//            case UNAuthorizationStatusNotDetermined:
//                NSLog(@"Undetermined");
//                break;
//            default:
//                break;
//        }
//
//
//        //2. To learn the status of specific settings, query them directly
//        NSLog(@"Checking Badge settings");
//        if (settings.badgeSetting == UNAuthorizationStatusAuthorized)
//            NSLog(@"Yeah. We can badge this puppy!");
//        else
//            NSLog(@"Not authorized");
//
//    }];
//}
//
//#pragma mark - Get token
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
//    NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
//    NSLog(@"Token = %@", strDevicetoken);
//    [RNUser sharedInstance].tokenPushNotif = strDevicetoken;
//}
//#pragma mark - UNUserNotificationCenterDelegate iOS 10+
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    NSLog(@"APNS MESSAGE = %@",notification.request.content.userInfo);
//    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
//}
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
//    NSLog(@"APNS MESSAGE = %@",response.notification.request.content.userInfo);
//    completionHandler();
//}
//#pragma mark - UNUserNotificationCenterDelegate iOS 9+
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
//    [application registerForRemoteNotifications];
//}
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//    NSLog(@"APNS MESSAGE: %@",userInfo);
//}
//#pragma mark - Error
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
//{
//    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
//    NSLog(@"Error = %@",error);
//}
@end
