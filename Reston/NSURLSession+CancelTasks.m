//
//  NSURLSession+CancelTasks.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 29.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "NSURLSession+CancelTasks.h"

@implementation NSURLSession (CancelTasks)

- (void)cancelTasks {
    
    [self getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if (!dataTasks || !dataTasks.count) {
            return;
        }
        for (NSURLSessionTask *task in dataTasks) {
            [task cancel];
        }
    }];
}

@end
