//
//  Telprompt.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/21/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Telprompt : NSObject

+ (void)callWithString:(NSString*)phoneString;
+ (void)callWithURL:(NSURL*)url;

@end
