//
//  UIViewController+Orientation.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/18/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Orientation)

- (void)setOrientation:(NSUInteger)orientation;

@end
