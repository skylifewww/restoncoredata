//
//  CDKitchen+CoreDataClass.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 14.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDRestaurant;

NS_ASSUME_NONNULL_BEGIN

@interface CDKitchen : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CDKitchen+CoreDataProperties.h"
