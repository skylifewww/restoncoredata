//
//  OrderCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/11/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "OrderCell.h"

@implementation OrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

-(void) updateCell{
    
    UIColor* mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    NSString *addressStr = [NSString stringWithFormat:@" %@",_reserv.restAddress];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"Украина, Киев, "
                                                       withString:@""];
    _adressLabel.text = addressStr;
    
    
    NSString* restName = [_reserv.restName stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    restName = [restName stringByReplacingOccurrencesOfString:@"&#" withString:@""];
    restName = [restName stringByReplacingOccurrencesOfString:@"039;" withString:@"'"];
    
    _restonName.text = [NSString stringWithFormat:@"\"%@\"",restName];
    
    
    NSString* reservDateStr = _reserv.dateStr;
    NSString* dateStr = nil;
    NSString* timeStr = nil;
    
    if ([reservDateStr length] >= 10){
        dateStr = [reservDateStr substringToIndex:NSMaxRange([reservDateStr rangeOfComposedCharacterSequenceAtIndex:10])];
        timeStr = [reservDateStr substringFromIndex: [reservDateStr length] - 8];
    } else {
        
        dateStr = @"00-0000-0000";

    }
    
    _dateLabel.text = dateStr;
    _timeLabel.text = timeStr;
    
    _numberPersons.text = [self helperPersonsWithCount:_reserv.peopleCount.integerValue];
    
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* reservDate = [[formatter dateFromString:_reserv.dateStr] dateByAddingTimeInterval:-0.5*60*60];
    
    
    NSComparisonResult result;
    
    result = [[NSDate date] compare:reservDate];
    
    if(result == NSOrderedAscending){
        NSLog(@"today is less");
        _statusLabel.text = DPLocalizedString(@"actual", nil);
        _statusLabel.textColor = mainColor;
        
    } else if(result == NSOrderedDescending){
        _statusLabel.text = DPLocalizedString(@"archive", nil);
        _statusLabel.textColor = [UIColor lightGrayColor];
        NSLog(@"newDate is less");
    } else if(result == NSOrderedSame){
        _statusLabel.text = DPLocalizedString(@"archive", nil);
        _statusLabel.textColor = [UIColor lightGrayColor];
        NSLog(@"Both dates are same");
    } else {
        NSLog(@"Date cannot be compared");
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


-(NSString*) helperPersonsWithCount:(NSUInteger)cP{
    
    switch (cP) {
        case 1:
            return [NSString stringWithFormat:@"1 %@", DPLocalizedString(@"1_person", nil)];
        case 2:
            return [NSString stringWithFormat:@"2 %@", DPLocalizedString(@"person", nil)];
        case 3:
            return [NSString stringWithFormat:@"3 %@", DPLocalizedString(@"person", nil)];
        case 4:
            return [NSString stringWithFormat:@"4 %@", DPLocalizedString(@"person", nil)];
            
            
        case 21:
            return [NSString stringWithFormat:@"21 %@", DPLocalizedString(@"1_person", nil)];
        case 22:
            return [NSString stringWithFormat:@"22 %@", DPLocalizedString(@"person", nil)];
        case 23:
            return [NSString stringWithFormat:@"23 %@", DPLocalizedString(@"person", nil)];
        case 24:
            return [NSString stringWithFormat:@"24 %@", DPLocalizedString(@"person", nil)];
            
        case 31:
            return [NSString stringWithFormat:@"31 %@", DPLocalizedString(@"1_person", nil)];
        case 32:
            return [NSString stringWithFormat:@"32 %@", DPLocalizedString(@"person", nil)];
        case 33:
            return [NSString stringWithFormat:@"33 %@", DPLocalizedString(@"person", nil)];
        case 34:
            return [NSString stringWithFormat:@"34 %@", DPLocalizedString(@"person", nil)];
            
        default:
            return [NSString stringWithFormat:@"%lu %@",(unsigned long)cP, DPLocalizedString(@"persons", nil)];
    }
}

@end
