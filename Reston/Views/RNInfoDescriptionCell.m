//
//  RNInfoDescriptionCell.m
//  Reston
//
//  Created by Yurii Oliiar on 6/14/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNInfoDescriptionCell.h"

NSString *const kInfoDescriptionCellId = @"RNInfoDescriptionCell";
//static const CGFloat kDefaultTextHeight = 60.0;
static const CGFloat kViewAddtion = 30.0;

@implementation RNInfoDescriptionCell



+ (CGFloat)sizeForText:(NSString *)text
          shouldResize:(BOOL)resize {
    
    CGFloat typeFontSize;
    CGFloat defaultTextHeight;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        
        typeFontSize = 11.0;
        defaultTextHeight = 60.0;
        
    } else {
        
        typeFontSize = 17.0;
        defaultTextHeight = 120.0;

    }
    if (!resize) {
        return defaultTextHeight + kViewAddtion;
    }
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, defaultTextHeight);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    

    myTextView.text = text;
    myTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > defaultTextHeight ? sz.height : defaultTextHeight;
    
    return result + kViewAddtion;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat typeFontSize;
    CGFloat buttonHeight;
    CGFloat buttonWidth;
    CGFloat buttonDown;
    CGFloat marginLabel;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        
        typeFontSize = 10.0;
        buttonHeight = 14;
        buttonWidth = 90;
        buttonDown = 0;
        marginLabel = 10;
        
    } else {
        
        typeFontSize = 16.0;
        buttonHeight = 30;
        buttonWidth = 160;
        buttonDown = 5;
        marginLabel = 30;
    }
   
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = false;
    self.disclosureBtn.translatesAutoresizingMaskIntoConstraints = false;
    
    self.titleLabel.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
    
    [self.titleLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:marginLabel].active = YES;
    [self.titleLabel.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-marginLabel].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.topAnchor constant:20].active = YES;
    [self.titleLabel.heightAnchor constraintEqualToConstant:42].active = YES;
    
    [self.disclosureBtn.titleLabel setFont:[UIFont fontWithName:@"Thonburi" size:typeFontSize]];
    
    [self.disclosureBtn.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.disclosureBtn.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:buttonDown].active = YES;
    [self.disclosureBtn.widthAnchor constraintEqualToConstant:buttonWidth].active = YES;
    [self.disclosureBtn.heightAnchor constraintEqualToConstant:buttonHeight].active = YES;
    
}





@end
