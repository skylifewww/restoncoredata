//
//  CockPadCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 13.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kCockPadCellId;

@interface CockPadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cockNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cockLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@end
