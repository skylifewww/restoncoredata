//
//  RNInfoDetailCell.m
//  Reston
//
//  Created by Yurii Oliiar on 6/14/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNInfoDetailCell.h"

NSString *const kInfoDetailCellId = @"RNInfoDetailCell";
const CGFloat kInfoDetailCellHeight = 260;

@implementation RNInfoDetailCell

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else {
        MKAnnotationView *result = [mapView dequeueReusableAnnotationViewWithIdentifier:@"Annotation"];
        if (result == nil) {
            result = [[MKAnnotationView alloc] initWithAnnotation:_restaurant
                                                  reuseIdentifier:@"Annotation"];
            
        }
        result.image = [UIImage imageNamed:@"Pin"];
        return result;
    }
}
@end
