//
//  RNNewsTableViewCell.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kNewsTableViewCellId;
extern const CGFloat kNewsTableViewCellHeight;

#import "RNNEw.h"

@interface RNNewsTableViewCell : UITableViewCell

- (void)applyNew:(RNNEw *)model;
+ (CGFloat)heightForText:(NSString *)text
            imagePresent:(BOOL)present;

@end
