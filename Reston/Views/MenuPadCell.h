//
//  MenuPadCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 13.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGActivityIndicatorView.h"

extern NSString *const kMenuPadCellId;
@interface MenuPadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) DGActivityIndicatorView *loadingIndicator;

@end
