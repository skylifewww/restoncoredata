//
//  RNInfoReviewTableViewCell.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNInfoReviewTableViewCell.h"
#import "UIImage+Cache.h"

NSString *const kInfoReviewTableViewCellId = @"RNInfoReviewTableViewCell";

@interface RNInfoReviewTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *subLabel;

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *nulikeBtn;

@end

@implementation RNInfoReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _photoImageView.layer.cornerRadius = _photoImageView.frame.size.width /2;
}

- (IBAction)likePressed:(id)sender {
    //TODO: send request
}

- (IBAction)unlikePressed:(id)sender {
    //TODO: send request
}

+ (CGFloat)heightForText:(NSString *)text {
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, 35);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = text;
    myTextView.font = [UIFont systemFontOfSize:13];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > 35 ? sz.height : 35;
    
    return result + 45;
}

- (void)applyReview:(RNFeedback *)feedback {
    _likeBtn.hidden = YES;
    _nulikeBtn.hidden = YES;
    NSString *name = feedback.name.length > 0 ? feedback.name : feedback.username;
    _mainLabel.text = [NSString stringWithFormat:@"%@ %@",feedback.date, name];
    _subLabel.text = feedback.text;
    _photoImageView.image = [UIImage imageNamed:@"EmptyAccount"];
    if (feedback.photo.length > 0) {
        [UIImage cachedImage:feedback.photo
               fullURLString:@" "
                withCallBack:^(UIImage *image) {
                    _photoImageView.image = image;
                }];
    }
}

@end
