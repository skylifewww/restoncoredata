//
//  RNInfoDescriptionCell.h
//  Reston
//
//  Created by Yurii Oliiar on 6/14/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kInfoDescriptionCellId;

@interface RNInfoDescriptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *disclosureBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


+ (CGFloat)sizeForText:(NSString *)text
          shouldResize:(BOOL)resize;

@end
