//
//  RNDatePickerView.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNDatePickerView.h"

@implementation RNDatePickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    [_cancelButton setTitle:@"Oтмена"
                   forState:UIControlStateNormal];
    [_doneButton setTitle:@"Готово"
                   forState:UIControlStateNormal];
}

+ (RNDatePickerView *)viewWithDelegate:(id<RNDatePickerViewDelegate>)delegate {
    RNDatePickerView *sv = [[[NSBundle mainBundle] loadNibNamed:@"RNDatePickerView"
                                                          owner:nil
                                                        options:nil] firstObject];
    sv.delegate = delegate;
    return sv;
}

- (IBAction)onCancel:(UIButton *)sender {
    [_delegate datePicker:self didCancelWithDate:_datePicker.date];
}

- (IBAction)onDone:(UIButton *)sender {
    [_delegate datePicker:self didChooseDate:_datePicker.date];
}

- (void)showInView:(UIView *)view {
    CGRect rc = self.frame;
    rc.size.height = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 260  : 300;
    rc.size.width = view.frame.size.width;
    rc.origin.y = view.frame.size.height;
    self.frame = rc;
    [view addSubview:self];
    rc.origin.y  -= self.frame.size.height;
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = rc;
    }];
}

- (void)dismissFromView:(UIView *)view completion:(void (^)(BOOL finished))completion {
    CGRect rc = self.frame;
    rc.origin.y = view.frame.size.height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.frame = rc;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         if (completion == NULL) {
                             return;
                         }
                         completion (finished);
                     }];

}

@end
