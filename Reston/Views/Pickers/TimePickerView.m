//
//  TimePickerView.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.12.2017.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "TimePickerView.h"
#import "define.h"
#import "DPLocalization.h"
#import "NSObject+DPLocalization.h"
NSString *const DateChangeNotification = @"DateChangeNotification";

@interface TimePickerView ()<UIPickerViewDataSource, UIPickerViewDelegate>{
    NSString* selectedTitle;
    NSMutableArray* arrMimutes;
    NSArray* arrMimutesFirst;
    NSArray* arrMimutesFull;
    Boolean isNow;
    NSArray* arrMimutesLast;
    Boolean isNext;
}
@end

@implementation TimePickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    [_cancelButton setTitle:DPLocalizedString(@"done_favorite", nil)
                   forState:UIControlStateNormal];
    [_doneButton setTitle:@"Готово"
                 forState:UIControlStateNormal];
    
    _datePicker.delegate = self;
    _datePicker.dataSource = self;
    _datePicker.showsSelectionIndicator = YES;
    
    arrMimutesFull = @[@"00", @"15", @"30", @"45"];
//    NSLog(@"awakeFromNib arrMimutes %@", self.arrFirstMinutes);
    arrMimutesLast = @[@"00", @"15", @"30"];
    arrMimutes = [NSMutableArray new];
    isNow = self.isThisDay;
//    NSLog(@"isAllTimes %hhu", self.isAllTimes);
}

+ (TimePickerView *)viewWithDelegate:(id<TimePickerViewDelegate>)delegate {
    TimePickerView *sv = [[[NSBundle mainBundle] loadNibNamed:@"TimePickerView"
                                                          owner:nil
                                                        options:nil] firstObject];
    sv.delegate = delegate;
    return sv;
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 3;
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString* title = @"";
    if (component == 0){
//        NSLog(@"row %ld", row);
    
    title = self.timeModel[row].hours;
        self.selectedHour = title;
        return title;
    } else if (component == 1){
        title = @":";
        
        return title;
    
    } else if (component == 2){

        title = arrMimutes[row];
        self.selectedMimutes = title;
        return title;
    }
    return title;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    NSInteger numberOfRows = 0;
    
    if (component == 0){
        return self.timeModel.count;
    } else if (component == 1){
        return 1;
    } else if (component == 2){
//        NSLog(@"arrMimutes.count %ld", arrMimutes.count);
        return arrMimutes.count;
    }
    return numberOfRows;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    
    if (component == 0){
        
        self.selectedHour = self.timeModel[row].hours;
        
//        if (self.timeModel[row].hours.length == 0 || [self.timeModel[row].hours isEqualToString: @""]){
//            
//            [arrMimutes removeAllObjects];
//            arrMimutes = [NSMutableArray arrayWithArray: @[@""]];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [pickerView reloadAllComponents];
//                
//            });
//            
//        } else
        if (self.timeModel[row].hours == self.timeModel.firstObject.hours && isNow == NO){
            
            [arrMimutes removeAllObjects];
//            if (self.isAllTimes == YES){
                //                NSLog(@"isAllTimes %hhu", self.isAllTimes);
                arrMimutes = [NSMutableArray arrayWithArray: self.arrFirstMinutes];
//            } else {
//                //                NSLog(@"isAllTimes %hhu", self.isAllTimes);
//                arrMimutes = [NSMutableArray arrayWithArray: arrMimutesLast];
//            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [pickerView reloadAllComponents];
                
            });
            
        } else if (self.timeModel[row].hours == self.timeModel.lastObject.hours || self.timeModel[row].isNowMorning == YES){
            
            [arrMimutes removeAllObjects];
            if (self.isAllTimes == YES){
//                NSLog(@"isAllTimes %hhu", self.isAllTimes);
                arrMimutes = [NSMutableArray arrayWithArray: arrMimutesFull];
            } else {
//                NSLog(@"isAllTimes %hhu", self.isAllTimes);
                arrMimutes = [NSMutableArray arrayWithArray: arrMimutesLast];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [pickerView reloadAllComponents];
                
            });
            
        } else if (isNow == YES && self.timeModel[row].hours == self.timeModel.firstObject.hours){
//            arrMimutesFirst = [self setupArrMimutesFirst:self.minMinutes];
            [arrMimutes removeAllObjects];
            arrMimutes = [NSMutableArray arrayWithArray: self.arrFirstMinutes];
//            arrMimutes = self.arrFirstMinutes;
             dispatch_async(dispatch_get_main_queue(), ^{
            [pickerView reloadAllComponents];
                 });
        } else {
//            arrMimutes = nil;
//            arrMimutes = [NSMutableArray arrayWithArray: arrMimutesFull];
            [arrMimutes removeAllObjects];
            arrMimutes = [NSMutableArray arrayWithArray: arrMimutesFull];
//            arrMimutes = arrMimutesFull;
            dispatch_async(dispatch_get_main_queue(), ^{
                [pickerView reloadAllComponents];

            });
        }
        [self dateChangeNotificationAction:self.timeModel[row]];
   
    } else if (component == 2){
        self.selectedMimutes = arrMimutes[row];
        
    }

}

-(void) dateChangeNotificationAction:(TimeModel*) timeModel{
    
//    if (self.selectedHour.integerValue < self.timeModel.firstObject.hours.integerValue){
    if (timeModel.isNextDay == YES){
        if (isNext == YES){
            return;
        }
        isNext = YES;
        
//        NSLog(@"self.selectedHour.integerValue < self.timeModel.firstObject.hours.integerValue");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DateChangeNotification object:nil userInfo:@{@"dataChange":@(isNext)}];
    } else {
        if (isNext == NO){
            return;
        }
        isNext = NO;
        
//        NSLog(@"self.selectedHour.integerValue >= self.timeModel.firstObject.hours.integerValue");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:DateChangeNotification object:nil userInfo:@{@"dataChange":@(isNext)}];
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    
    CGFloat widthForComponent = 0.0f;
    
    if (component == 0){
        widthForComponent = 40.0f;
    } else if (component == 1){
        widthForComponent = 15.0f;
    } else if (component == 2){
        widthForComponent = 40.0f;
    }
    return widthForComponent;
}


- (IBAction)onCancel:(UIButton *)sender {
    selectedTitle = [NSString stringWithFormat:@"%@:%@", self.selectedHour, self.selectedMimutes];
    [_delegate datePicker:self didCancelWithDate:selectedTitle];
}

- (IBAction)onDone:(UIButton *)sender {
    selectedTitle = [NSString stringWithFormat:@"%@:%@", self.selectedHour, self.selectedMimutes];
    [_delegate datePicker:self didChooseDate:selectedTitle];
}

- (void)showInView:(UIView *)view {
    CGRect rc = self.frame;
    rc.size.height = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 260  : 300;
    rc.size.width = view.frame.size.width;
    rc.origin.y = view.frame.size.height;
    self.frame = rc;
    [view addSubview:self];
    rc.origin.y  -= self.frame.size.height;
   
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = rc;
        
        isNow = self.isThisDay;
//        if (isNow == YES){
            arrMimutes = [NSMutableArray arrayWithArray: self.arrFirstMinutes];
//        } else {
//            arrMimutes = [NSMutableArray arrayWithArray: arrMimutesFull];
//        }
        self.selectedHour = self.timeModel.firstObject.hours;
        self.selectedMimutes = arrMimutes.firstObject;
        [self.datePicker reloadAllComponents];
//        NSLog(@"showInView arrMimutes %@", arrMimutes);
//        [self dateChangeNotificationAction];

    }];
}

- (void)dismissFromView:(UIView *)view completion:(void (^)(BOOL finished))completion {
    CGRect rc = self.frame;
    rc.origin.y = view.frame.size.height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.frame = rc;
                     } completion:^(BOOL finished) {
//                         [arrMimutes removeAllObjects];
                         [self removeFromSuperview];
                         if (completion == NULL) {
                             return;
                         }
                         completion (finished);
                     }];
    
}
@end
