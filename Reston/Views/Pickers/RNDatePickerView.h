//
//  RNDatePickerView.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSTimeIntervalPicker.h"

@class RNDatePickerView;

@protocol RNDatePickerViewDelegate <NSObject>

- (void)datePicker:(RNDatePickerView *)picker didChooseDate:(NSDate *)date;
- (void)datePicker:(RNDatePickerView *)picker didCancelWithDate:(NSDate *)date;

@end

@interface RNDatePickerView : UIView

@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (retain, nonatomic) IBOutlet GSTimeIntervalPicker *datePickerGS;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (assign, nonatomic) id<RNDatePickerViewDelegate> delegate;
@property (nonatomic, assign) NSInteger identifier;

+ (RNDatePickerView *)viewWithDelegate:(id<RNDatePickerViewDelegate>)delegate;
- (void)showInView:(UIView *)view;
- (void)dismissFromView:(UIView *)view completion:(void (^)(BOOL finished))completion;

@end
