//
//  TimePickerView.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 01.12.2017.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeModel.h"
extern NSString *const DateChangeNotification;

@class TimePickerView;

@protocol TimePickerViewDelegate <NSObject>

//- (void)datePicker:(TimePickerView *)picker didChooseDate:(NSDate *)date;
//- (void)datePicker:(TimePickerView *)picker didCancelWithDate:(NSDate *)date;

- (void)datePicker:(TimePickerView *)picker didChooseDate:(NSString *)date;
- (void)datePicker:(TimePickerView *)picker didCancelWithDate:(NSString *)date;

@end

@interface TimePickerView : UIView

@property (assign, nonatomic) Boolean isAllTimes;
@property (assign, nonatomic) Boolean isThisDay;
@property (strong, nonatomic) NSArray<NSString*>* arrFirstMinutes;
@property (strong, nonatomic) NSString* minMinutes;
@property (strong, nonatomic) NSString* selectedDay;
@property (strong, nonatomic) NSString* selectedDate;
@property (strong, nonatomic) NSString* selectedHour;
@property (strong, nonatomic) NSString* selectedMimutes;
@property (strong, nonatomic) NSArray<TimeModel*>* timeModel;
@property (retain, nonatomic) IBOutlet UIPickerView *datePicker;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (assign, nonatomic) id<TimePickerViewDelegate> delegate;
@property (nonatomic, assign) NSInteger identifier;

+ (TimePickerView *)viewWithDelegate:(id<TimePickerViewDelegate>)delegate;
- (void)showInView:(UIView *)view;
- (void)dismissFromView:(UIView *)view completion:(void (^)(BOOL finished))completion;

@end
