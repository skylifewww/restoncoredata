//
//  RNEntityPickerView.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

@import UIKit;
@class RNEntityPickerView;

@protocol RNEntityPickerViewDelegate <NSObject>

- (void)entityPickerView:(RNEntityPickerView *)picker
         didSelectObject:(id)object;
- (void)entityPickerViewDidCancel:(RNEntityPickerView *)picker;

@end

@interface RNEntityPickerView : UIView<UIPickerViewDataSource, UIPickerViewDelegate>

@property (retain, nonatomic) IBOutlet UIPickerView *picker;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) id<RNEntityPickerViewDelegate> delegate;
@property (copy, nonatomic) NSArray *dataSource;
@property (nonatomic, assign) BOOL infiniteSource;
@property (nonatomic, assign) NSInteger identifier;

+ (RNEntityPickerView *)viewWithDelegate:(id<RNEntityPickerViewDelegate>)delegate;
- (void)showInView:(UIView *)view;
- (void)dismissFromView:(UIView *)view
             completion:(void (^)(BOOL finished))completion;

@end
