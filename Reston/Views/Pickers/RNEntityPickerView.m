//
//  RNMenuTableViewCell.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNEntityPickerView.h"

@implementation RNEntityPickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    [_cancelButton setTitle:@"Oтмена"
                   forState:UIControlStateNormal];
    [_doneButton setTitle:@"Готово"
                 forState:UIControlStateNormal];
}

+ (RNEntityPickerView *)viewWithDelegate:(id<RNEntityPickerViewDelegate>)delegate {
    RNEntityPickerView *sv = [[[NSBundle mainBundle] loadNibNamed:@"RNEntityPickerView"
                                                          owner:nil
                                                        options:nil] firstObject];
    sv.delegate = delegate;
    return sv;
}

- (IBAction)onCancel:(UIButton *)sender {
    [_delegate entityPickerViewDidCancel:self];
}

- (IBAction)onDone:(UIButton *)sender {
    NSInteger row = [_picker selectedRowInComponent:0];
    if (_infiniteSource) {
        [_delegate entityPickerView:self
                    didSelectObject:@(row + 1)];
    } else {
        [_delegate entityPickerView:self
                    didSelectObject:_dataSource[row]];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _infiniteSource ? 30 : [_dataSource count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return _infiniteSource ? [NSString stringWithFormat:@"%ti",row + 1] : [_dataSource[row] description];
}

- (void)showInView:(UIView *)view {
    CGRect rc = self.frame;
    rc.size.height = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 260  : 300;
    rc.origin.y = view.frame.size.height;
    rc.size.width = view.frame.size.width;
    self.frame = rc;
    [view addSubview:self];
    rc.origin.y  -= self.frame.size.height;
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = rc;
    }];
}

- (void)dismissFromView:(UIView *)view completion:(void (^)(BOOL finished))completion {
    CGRect rc = self.frame;
    rc.origin.y = view.frame.size.height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.frame = rc;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         if (completion == NULL) {
                             return;
                         }
                         completion (finished);
                     }];
    
}


@end