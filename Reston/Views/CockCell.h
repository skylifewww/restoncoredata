//
//  CockCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/12/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kCockCellId;

@interface CockCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cockLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UILabel *cockNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionNamelabel;

//
//+ (CGFloat)sizeForKitchen:(NSString *)kitchen
//               andOptions:(NSString *)options;

@end
