//
//  RNInfoDetailCell.h
//  Reston
//
//  Created by Yurii Oliiar on 6/14/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RNRestaurant.h"

extern NSString *const kInfoDetailCellId;
extern const CGFloat kInfoDetailCellHeight;

@interface RNInfoDetailCell : UITableViewCell<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *subwayLabel;
@property (weak, nonatomic) IBOutlet UILabel *cuisineLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) RNRestaurant *restaurant;
@property (weak, nonatomic) IBOutlet UIButton *mapOverlayBtn;

@end
