//
//  MetroCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 12.05.17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "MetroCell.h"

@implementation MetroCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    selectedBackgroundView.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:233.0/255.0 blue:241.0/255.0 alpha:0.3];
    self.selectedBackgroundView = selectedBackgroundView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


    // Configure the view for the selected state
}

@end
