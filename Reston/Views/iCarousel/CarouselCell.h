//
//  CarouselCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/5/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarouselCell : UICollectionReusableView

@end
