//
//  CockMenuCell.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/12/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGActivityIndicatorView.h"

extern NSString *const kCockMenuCellId;

@interface CockMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) DGActivityIndicatorView *loadingIndicator;


+ (CGFloat)sizeForText:(NSString *)text
          shouldResize:(BOOL)resize;
@end
