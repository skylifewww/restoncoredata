//
//  CockPadCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 13.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "CockPadCell.h"
NSString *const kCockPadCellId = @"CockPadCell";
@implementation CockPadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
