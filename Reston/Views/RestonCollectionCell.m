//
//  RestonCollectionCell.m
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import "RestonCollectionCell.h"

@implementation RestonCollectionCell

@synthesize restName = _restName;
@synthesize restType = _restType;
@synthesize restAddress = _restAddress;
@synthesize ratingImage = _ratingImage;
@synthesize commentsCount = _commentsCount;
@synthesize averageCheck = _averageCheck;
@synthesize distanceLabel = _distanceLabel;
@synthesize restImage = _restImage;
@synthesize discountBack = _discountBack;
@synthesize discountLabel = _discountLabel;
@synthesize callButton = _callButton;
@synthesize containerView = _containerView;

- (void)awakeFromNib {
    [super awakeFromNib];

}

//- (IBAction)callActionButton:(id)sender {
//}

@end
