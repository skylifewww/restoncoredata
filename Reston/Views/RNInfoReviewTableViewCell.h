//
//  RNInfoReviewTableViewCell.h
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNFeedback.h"

extern NSString *const kInfoReviewTableViewCellId;

@interface RNInfoReviewTableViewCell : UITableViewCell

- (void)applyReview:(RNFeedback *)feedback;
+ (CGFloat)heightForText:(NSString *)text;

@end
