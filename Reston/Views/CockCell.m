//
//  CockCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/12/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "CockCell.h"

NSString *const kCockCellId = @"CockCell";
//static const CGFloat kViewCockAddtion = 30.0;
@implementation CockCell

//+ (CGFloat)sizeForKitchen:(NSString *)kitchen
//               andOptions:(NSString *)options {
//    
//    CGFloat typeFontSize;
//    CGFloat defaultTextHeight;
//    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
//        
//        
//        typeFontSize = 11.0;
//        defaultTextHeight = 10.0;
//        
//    } else {
//        
//        typeFontSize = 17.0;
//        defaultTextHeight = 30.0;
//        
//    }
// 
//    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
//    CGRect frame = CGRectMake(0, 0, width, defaultTextHeight);
//    
//    UITextView *kitchenTextView = [[UITextView alloc] initWithFrame:frame];
//    kitchenTextView.text = kitchen;
//    kitchenTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
//    CGSize sizeKitchen = [kitchenTextView sizeThatFits:CGSizeMake(kitchenTextView.frame.size.width, FLT_MAX)];
//    CGFloat resultKitchen = sizeKitchen.height + defaultTextHeight;
//    
//    UITextView *optionsTextView = [[UITextView alloc] initWithFrame:frame];
//    optionsTextView.text = kitchen;
//    optionsTextView.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
//    CGSize sizeOptions = [optionsTextView sizeThatFits:CGSizeMake(optionsTextView.frame.size.width, FLT_MAX)];
//    CGFloat resultOptions = sizeOptions.height + defaultTextHeight;
//    
//    CGFloat result = resultKitchen + resultOptions;
//    return result + kViewCockAddtion;
//    
//}
//
//
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    
//    CGFloat typeFontSize;
//    CGFloat nameFontSize;
//    CGFloat nameHeight;
//    CGFloat buttonWidth;
//    CGFloat buttonDown;
//    CGFloat marginLabel;
//    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
//        
//        
//        typeFontSize = 10.0;
//        nameFontSize = 15;
//        nameHeight = 20;
//        buttonWidth = 90;
//        buttonDown = 30;
//        marginLabel = 10;
//        
//    } else {
//        
//        typeFontSize = 16.0;
//        nameFontSize = 22;
//        nameHeight = 70;
//        buttonWidth = 160;
//        buttonDown = 50;
//        marginLabel = 30;
//    }
//    
////    self.cockNameLabel.translatesAutoresizingMaskIntoConstraints = false;
////    self.optionNamelabel.translatesAutoresizingMaskIntoConstraints = false;
////    self.optionLabel.translatesAutoresizingMaskIntoConstraints = false;
////    self.cockLabel.translatesAutoresizingMaskIntoConstraints = false;
//    
//    self.cockLabel.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
//    self.optionLabel.font = [UIFont fontWithName:@"Thonburi" size:typeFontSize];
//    self.cockNameLabel.font = [UIFont fontWithName:@"Thonburi" size:nameFontSize];
//    self.optionNamelabel.font = [UIFont fontWithName:@"Thonburi" size:nameFontSize];
//    
//    [self.cockNameLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:16].active = YES;
//    
//    [self.cockNameLabel.topAnchor constraintEqualToAnchor:self.topAnchor constant:20].active = YES;
//    [self.cockNameLabel.widthAnchor constraintEqualToConstant:160].active = YES;
//    [self.cockNameLabel.heightAnchor constraintEqualToConstant:nameHeight].active = YES;
//    
//    [self.cockLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:marginLabel].active = YES;
//    [self.cockLabel.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-marginLabel].active = YES;
//    [self.cockLabel.centerYAnchor constraintEqualToAnchor:self.cockNameLabel.centerYAnchor constant:buttonDown].active = YES;
////    [self.cockLabel.heightAnchor constraintEqualToConstant:42].active = YES;
//    
//    [self.optionNamelabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:16].active = YES;
//    
//    [self.optionNamelabel.topAnchor constraintEqualToAnchor:self.cockLabel.bottomAnchor constant:nameFontSize].active = YES;
//    [self.optionNamelabel.widthAnchor constraintEqualToConstant:200].active = YES;
//    [self.optionNamelabel.heightAnchor constraintEqualToConstant:nameHeight].active = YES;
//    
//    [self.optionLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:marginLabel].active = YES;
//    [self.optionLabel.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-marginLabel].active = YES;
//    [self.optionLabel.topAnchor constraintEqualToAnchor:self.optionNamelabel.bottomAnchor constant:buttonDown].active = YES;
////    [self.cockLabel.heightAnchor constraintEqualToConstant:42].active = YES;
//    
// 
//    
}
//
//
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end
