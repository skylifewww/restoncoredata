//
//  RNNewsTableViewCell.m
//  Reston
//
//  Created by Yurii Oliiar on 5/28/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNNewsTableViewCell.h"
#import "UIImage+Cache.h"

NSString *const kNewsTableViewCellId = @"RNNewsTableViewCell";
const CGFloat kNewsTableViewCellHeight = 140;

@interface RNNewsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTitle;
@property (nonatomic, strong) UIImageView *photoImageView;

@end

@implementation RNNewsTableViewCell

- (void)setPhoto:(UIImage *)image {
    if (image ==nil) {
        [_photoImageView removeFromSuperview];
        return;
    }
    CGRect rc = CGRectMake(0, 10, 120,80);
    self.photoImageView = [[UIImageView alloc] initWithFrame:rc];
    _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    _photoImageView.clipsToBounds = YES;
    _photoImageView.image = image;
    rc.origin.y -= 10;
    UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:rc];
    [_descriptionTitle addSubview:_photoImageView];
    _descriptionTitle.textContainer.exclusionPaths = @[imgRect];
}

- (void)applyNew:(RNNEw *)model {
    _titleLabel.text = model.title;
    _descriptionTitle.text = model.shortText;
    [UIImage cachedImage:model.image
          fullURLString:@"http://reston.com.ua/uploads/img/news/original/"
            withCallBack:^(UIImage *image) {
                [self setPhoto:image];
            }];
}

+ (CGFloat)heightForText:(NSString *)text
            imagePresent:(BOOL)present {
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, 85);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = text;
    myTextView.font = [UIFont systemFontOfSize:11];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > 85 ? sz.height : 85;
    
    return result + 55;
}

@end