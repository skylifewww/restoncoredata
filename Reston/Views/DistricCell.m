//
//  DistricCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 12.05.17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "DistricCell.h"

@implementation DistricCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
