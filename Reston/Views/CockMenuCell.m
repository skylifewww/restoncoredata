//
//  CockMenuCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/12/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "CockMenuCell.h"

NSString *const kCockMenuCellId = @"CockMenuCell";
static const CGFloat kDefaultTextHeight = 0.0;

@implementation CockMenuCell

+ (CGFloat)sizeForText:(NSString *)text
          shouldResize:(BOOL)resize {
    if (!resize) {
        return kDefaultTextHeight;
    }
    CGFloat width = [UIApplication sharedApplication].keyWindow.frame.size.width - 40;
    CGRect frame = CGRectMake(0, 0, width, kDefaultTextHeight);
    UITextView *myTextView = [[UITextView alloc] initWithFrame:frame];
    myTextView.text = text;
    myTextView.font = [UIFont fontWithName:@"Thonburi" size:10];
    CGSize sz = [myTextView sizeThatFits:CGSizeMake(myTextView.frame.size.width, FLT_MAX)];
    CGFloat result = sz.height > kDefaultTextHeight ? sz.height : kDefaultTextHeight;
    
    return result;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
 
    
    self.menuButton.layer.cornerRadius = 17;
    self.menuButton.layer.borderWidth = 2.0;
    self.menuButton.backgroundColor = [UIColor whiteColor];
    self.menuButton.layer.borderColor = [[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0] CGColor];
    
    self.menuButton.layer.masksToBounds = true;
    
    self.loadingIndicator = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0]];

    CGFloat width = 60.0f;
    CGFloat height = 30.0f;
    
    self.loadingIndicator.frame = CGRectMake(self.bounds.size.width/2 - width/2, self.bounds.size.height/2 - height/2, width, height);
    [self addSubview:self.loadingIndicator];
    
    self.loadingIndicator.translatesAutoresizingMaskIntoConstraints = false;
    [self.loadingIndicator.centerXAnchor constraintEqualToAnchor:self.menuButton.centerXAnchor].active = YES;
    [self.loadingIndicator.centerYAnchor constraintEqualToAnchor:self.menuButton.centerYAnchor].active = YES;
    [self.loadingIndicator.heightAnchor constraintEqualToConstant:20].active = YES;
    [self.loadingIndicator.widthAnchor constraintEqualToConstant:40].active = YES;
    [self bringSubviewToFront:self.loadingIndicator];
    [self.loadingIndicator startAnimating];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
