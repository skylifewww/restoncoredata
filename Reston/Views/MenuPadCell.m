//
//  MenuPadCell.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 13.09.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "MenuPadCell.h"


NSString *const kMenuPadCellId = @"MenuPadCell";
@implementation MenuPadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.menuButton.layer.cornerRadius = 27;
    self.menuButton.layer.borderWidth = 2.0;
    self.menuButton.backgroundColor = [UIColor whiteColor];
    self.menuButton.layer.borderColor = [[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0] CGColor];
    
    self.menuButton.layer.masksToBounds = true;
    
    self.loadingIndicator = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0]];
    
    CGFloat width = 60.0f;
    CGFloat height = 30.0f;
    
    self.loadingIndicator.frame = CGRectMake(self.bounds.size.width/2 - width/2, self.bounds.size.height/2 - height/2, width, height);
    [self addSubview:self.loadingIndicator];
    
    self.loadingIndicator.translatesAutoresizingMaskIntoConstraints = false;
    [self.loadingIndicator.centerXAnchor constraintEqualToAnchor:self.menuButton.centerXAnchor].active = YES;
    [self.loadingIndicator.centerYAnchor constraintEqualToAnchor:self.menuButton.centerYAnchor].active = YES;
    [self.loadingIndicator.heightAnchor constraintEqualToConstant:20].active = YES;
    [self.loadingIndicator.widthAnchor constraintEqualToConstant:40].active = YES;
    [self bringSubviewToFront:self.loadingIndicator];
    [self.loadingIndicator startAnimating];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
