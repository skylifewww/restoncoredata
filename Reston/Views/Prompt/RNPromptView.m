//
//  PAPromptView.m
//  Reston
//
//  Created by Yurii Oliiar on 6/9/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNPromptView.h"

@interface RNPromptView ()<UITextViewDelegate>

@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, assign) id<RNPromptViewDelegate> delegate;
// UI
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *unlikeBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (nonatomic, copy) NSNumber *option;

@end

@implementation RNPromptView


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.layer.cornerRadius = 2.0;
    self.option = @1;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:self
                                                                          action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:@[flex,doneButton]];
    _textView.inputAccessoryView = keyboardDoneButtonView;
}

- (void)doneClicked:(id)sender {
    [_textView resignFirstResponder];
}

#pragma mark - Core

+ (RNPromptView *)viewWithTitle:(NSString *)title
                       delegate:(id<RNPromptViewDelegate>) delegate {
    RNPromptView *pv = [[[NSBundle mainBundle] loadNibNamed:@"RNPromptView"
                                                    owner:nil
                                                  options:nil] firstObject];
    pv.delegate = delegate;
    return pv;
}

- (void)showInView:(UIView *)parentView {
    UIInterpolatingMotionEffect *horizontalEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                                                    type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalEffect.minimumRelativeValue = @(-10.0);
    horizontalEffect.maximumRelativeValue = @(10.0);
    
    UIInterpolatingMotionEffect *verticalEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalEffect.minimumRelativeValue = @(-10.0);
    verticalEffect.maximumRelativeValue = @(10.0);
    
    UIMotionEffectGroup *motionEffectGroup = [[UIMotionEffectGroup alloc] init];
    motionEffectGroup.motionEffects = @[horizontalEffect, verticalEffect];
    
    [self addMotionEffect:motionEffectGroup];
    
    self.center = parentView.center;
    self.layer.opacity = 0.5f;
    self.layer.transform = CATransform3DMakeScale(.1f, .1f, 1.0);
    UIView *targetView = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(viewTapped:)];
    self.coverView = [[UIView alloc] initWithFrame:targetView.frame];
    _coverView.backgroundColor = [UIColor blackColor];
    _coverView.alpha = 0.7;
    [_coverView addGestureRecognizer:recognizer];
    [targetView addSubview:_coverView];
    [targetView addSubview:self];
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.layer.opacity = 1.0f;
                         self.layer.transform = CATransform3DMakeScale(1, 1, 1);
                     }];
}

- (void)hideWithCompletionBlock:(void (^)(BOOL finished))completion {
    [_coverView removeFromSuperview];
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.layer.opacity = 0.5f;
                         self.layer.transform = CATransform3DMakeScale(.1f, .1f, 1.0);
                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         if (completion != NULL) {
                             completion(finished);
                         }
                     }];
}

#pragma mark - IBActions

- (void)viewTapped:(id)sender {
    [_delegate cancelledPromptView:self];
}
- (IBAction)submitTapped:(id)sender {
    if (_textView.text.length == 0 || [_textView.text isEqualToString:@"Текст отзыва"]) {
        return;
    }
    [_delegate promptView:self
         finihsedWithText:_textView.text
                   option:_option];
}
- (IBAction)likeTapped:(id)sender {
    _likeBtn.alpha = 1;
    _unlikeBtn.alpha = 0.4;
    self.option = @1;
}
- (IBAction)unlikeTapped:(id)sender {
    _likeBtn.alpha = 0.4;
    _unlikeBtn.alpha =1;
    self.option = @0;
}

#pragma mark - UITextView

- (void)textViewDidChange:(UITextView *)textView {
    CGRect frame = self.frame;
    
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    if (newSize.height < 45) {
        newSize.height = 45.0;
    }
    if (newSize.height > 150.0) {
        newSize.height = 150.0;
    }
    CGRect newFrame = textView.frame;
    CGFloat diff = newSize.height - newFrame.size.height;
    frame.size.height += diff;
    
    self.frame = frame;
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Текст отзыва"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Текст отзыва";
        textView.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - Keyboard

- (void)keyboardWillHide:(NSNotification *)n {
    if (self.center.y == [UIApplication sharedApplication].keyWindow.center.y) {
        return;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.center = [UIApplication sharedApplication].keyWindow.center;
    [UIView commitAnimations];
    
}

- (void)keyboardWillShow:(NSNotification *)n {
    CGRect keyboardFrame = [[[n userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedFrame = [self convertRect:keyboardFrame fromView:[UIApplication sharedApplication].keyWindow];
    CGFloat bottom = self.frame.origin.y + self.frame.size.height/2 -10.0;
    CGFloat kbDiff = bottom - convertedFrame.origin.y ;
    if (kbDiff < 0) {
        return;
    }
    
    // resize the noteView
    CGRect viewFrame = self.frame;
    
    viewFrame.origin.y -= kbDiff;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self setFrame:viewFrame];
    [UIView commitAnimations];
}


@end
