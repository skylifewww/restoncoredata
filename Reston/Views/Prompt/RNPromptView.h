//
//  PAPromptView.h
//  Reston
//
//  Created by Yurii Oliiar on 6/9/15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//


@import UIKit;
@class RNPromptView;

@protocol RNPromptViewDelegate <NSObject>

- (void)cancelledPromptView:(RNPromptView *)promptView;

- (void)promptView:(RNPromptView *)timeView
  finihsedWithText:(NSString *)text
            option:(NSNumber *)option;

@end

@interface RNPromptView : UIView

+ (RNPromptView *)viewWithTitle:(NSString *)title
                       delegate:(id<RNPromptViewDelegate>) delegate;

- (void)showInView:(UIView *)parentView;
- (void)hideWithCompletionBlock:(void (^)(BOOL finished))completion;

@end
