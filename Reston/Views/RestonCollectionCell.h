//
//  RestonCollectionCell.h
//  RestonNewObjC
//
//  Created by Vladimir Nybozhinsky on 4/30/17.
//  Copyright © 2017 Vladimir Nybozhinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestonCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *restImage;
@property (weak, nonatomic) IBOutlet UIImageView *discountBack;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UIView *callButton;

@property (weak, nonatomic) IBOutlet UILabel *restName;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImage;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UILabel *commentsCount;
@property (weak, nonatomic) IBOutlet UIImageView *avgIcon;
@property (weak, nonatomic) IBOutlet UILabel *averageCheck;
@property (weak, nonatomic) IBOutlet UIImageView *dinstIcon;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *restType;
@property (weak, nonatomic) IBOutlet UILabel *restAddress;
@property (weak, nonatomic) IBOutlet UIImageView *mapIcon;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
