//
//  Telprompt.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/21/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import "Telprompt.h"

@implementation Telprompt

+ (void)callWithString:(NSString *)phoneString
{
    [self callWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneString]]];
}

+ (void)callWithURL:(NSURL *)url
{
    static UIWebView *webView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        webView = [[UIWebView alloc] init];
    });
    
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
}

@end
