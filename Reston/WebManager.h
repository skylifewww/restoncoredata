//
//  WebManager.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 31.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebManager : NSObject
typedef void (^Block)(NSDictionary*,BOOL);
+ (instancetype)sharedInstance;
@property (nonatomic) BOOL isInternetAvialable;
-(void)getDataComplete:(void(^)(NSDictionary*, BOOL))complete;
-(void)hasupdateComplete:(void(^)(NSDictionary*, BOOL))complete;
-(void)registerComplete:(void(^)(NSDictionary*, BOOL))complete;
-(void)chackAPIVersionComplete:(void(^)(NSDictionary*, BOOL))complete;
-(void)checkPass:(NSString*)psw complete:(void(^)(NSDictionary*, BOOL))complete;
-(BOOL) inetConnect;
@end

