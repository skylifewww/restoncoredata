//
//  NSURLSession+CancelTasks.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 29.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@import Foundation;

@interface NSURLSession (CancelTasks)

- (void)cancelTasks;

@end
