//
//  LaunchViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//
@import CoreLocation;
#import "LaunchViewController.h"
#import "RNLoginViewController.h"
#import "RNAuthRequest.h"
#import "RNAuthorizationResponse.h"
#import "RNUser.h"

#import "PageViewController.h"
#import "SlideEnterViewController.h"
//#import "StoreKitViewController.h"
#import <StoreKit/StoreKit.h>

#import "CatalogRestViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "FacebookRegisterRequest.h"
#import "FacebookRegisterResponse.h"
#import "RNGetRestaurantsRequest.h"
#import "RNGetRestaurantsResponse.h"

#import "CatalogRestViewController.h"
#import "SlideMenuController.h"
#import "NewMenuViewController.h"
#import "UIWindow+LoadingIndicator.h"
#import "define.h"
#import "DPLocalization.h"
#import "Reachability.h"
#import "AppDelegate.h"

#import "EmailViewController.h"
#import "Harpy.h"

#import "LocationManager.h"
#import <INTULocationManager/INTULocationManager.h>

//#import <RestKit/CoreData.h>
//#import <RestKit/RestKit.h>

//#import "CoreDataManager.h"
//#import "CDRestaurant+CoreDataProperties.h"



static NSString *const kUserKey = @"UserKey";
static NSString *const kPassKey = @"PassKey";
static NSString *const kEmailFBKey = @"EmailFBKey";
static NSString *const kClientIDKey = @"ClientIDKey";
static NSString *const kStrategyKey = @"StrategyKey";
static NSString *const kUserBirthdayKey = @"UserBirthdayKey";
static NSString *const kFirstLaunchKey = @"FirstLaunchKey";



static NSString *const kTypeKey = @"TypeKey";
//static NSString *const kDistricKey = @"DistricKey";
static NSString *const kDistricArr = @"DistricArr";
static NSString *const kSubwayArr = @"SubwayArr";
static NSString *const kOptionsKey = @"OptionsKey";
//static NSString *const kSubwayKey = @"SubwayKey";
//static NSString *const kSubwayIDKey = @"SubwayIDKey";
static NSString *const kKitchenKey = @"KitchenKey";

@interface UINavigationController (SupportOrientation)

@end

@implementation UINavigationController (SupportOrientation)

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if([self.topViewController respondsToSelector:@selector(supportedInterfaceOrientations)]) {
        return(UIInterfaceOrientationMask)[self.topViewController performSelector:@selector(supportedInterfaceOrientations) withObject:nil];
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    if([self.topViewController respondsToSelector:@selector(shouldAutorotate)])
    {
        BOOL autoRotate = (BOOL)[self.topViewController
                                 performSelector:@selector(shouldAutorotate)
                                 withObject:nil];
        return autoRotate;
        
    }
    return NO;
}

@end


@interface LaunchViewController ()<
//CLLocationManagerDelegate, SKStoreProductViewControllerDelegate,
HarpyDelegate>{
    double latitude;
    double longitude;
    BOOL loading;
    NSString* currCity;
    NSArray<RNRestaurant*>* restons;
    Boolean isAuth;
    Boolean isThisCity;
    UIStoryboard *storyboard;
    Boolean isGeolocationEnable;
    UIColor* mainColor;
}
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UILabel *reservLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountlabel;
@property (weak, nonatomic) IBOutlet UILabel *bestRestLabel;
@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;
@property (assign, nonatomic) NSTimeInterval timeout;
@property (assign, nonatomic) INTULocationRequestID locationRequestID;

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.desiredAccuracy = INTULocationAccuracyCity;
    self.timeout = 10.0;
    self.locationRequestID = NSNotFound;

    [[Harpy sharedInstance] setDelegate:self];
    
//    [[CoreDataManager sharedManager] deleteAllObjectsWithEntityName:@"CDRestaurant"];
    
    mainColor = [UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }

    loading = NO;
    
    NSArray* arrEmpty = @[@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kSubwayArr];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kKitchenKey];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kDistricArr];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kTypeKey];
    [[NSUserDefaults standardUserDefaults] setObject:arrEmpty
                                              forKey:kOptionsKey];
   
}

- (void)harpyDidStart {
    NSLog(@"self start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    [self start];
}


-(void) start{
    
    if (![[[NSUserDefaults standardUserDefaults] stringForKey:kFirstLaunchKey] isEqual: @"1"]){
        NSLog(@"startLogin");
        [self startLogin];
    } else {
        NSLog(@"checkLocationServicesAndStartUpdates");
         [self registerDefaults];
        [self startSingleLocationRequest];
    }
}

- (void)startSingleLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:self.desiredAccuracy
                                                                timeout:self.timeout
                                                   delayUntilAuthorized:YES
                                                                  block:
                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                  __typeof(weakSelf) strongSelf = weakSelf;
                                  
                                  if (status == INTULocationStatusSuccess) {
                                      NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request successful!. Current address:\n%@", address);
                                          [self startDefaultFlowWithCity:address];
                                      }];
                                  }
                                  else if (status == INTULocationStatusTimedOut) {

                                      NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                      [RNUser sharedInstance].isGeoEnabled = YES;
                                      [[LocationManager sharedInstance] getAddressFromLocation:currentLocation completion:^(NSString *address) {
                                          NSLog(@"Location request timed out. Current address:\n%@", address);
                                          [self startDefaultFlowWithCity:address];
                                      }];
                                  }
                                  else {

                                      [RNUser sharedInstance].isGeoEnabled = NO;
                                      [self startDefaultFlowWithCity:@"1"];
                                  }
                                  
                                  strongSelf.locationRequestID = NSNotFound;
                              }];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = NO; //bool created in first step
    NSNumber *value1 = [NSNumber numberWithInt:UIInterfaceOrientationMaskPortrait]; //change the orientation as per your requirement
    [[UIDevice currentDevice] setValue:value1 forKey:@"orientation"];
}


-(Boolean) isInternetConnect{
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
//        NSLog(@"NotReachable");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"error_title", nil)
                                                                       message:DPLocalizedString(@"error_text", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:actionCancel];
        alert.view.tintColor = mainColor;
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(BOOL) prefersStatusBarHidden{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


- (void)showAlertWithText:(NSString *)text {
//    NSLog(@"NotReachable+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:DPLocalizedString(@"server_error", nil)
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:actionCancel];
    alert.view.tintColor = mainColor;
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleDefault;
}

- (void)sendSearchRequestWithCity:(NSString*)cityName {
    if (loading) {
        return;
    }
    loading = YES;
    
    if ([self isInternetConnect]){
        
        [[UIApplication sharedApplication].keyWindow showLoadingIndicator];
        
        RNGetRestaurantsRequest *request = [[RNGetRestaurantsRequest alloc] init];
        request.name = @"";
        request.isAuth = YES;
        request.token = [RNUser sharedInstance].token;
        request.cityName = cityName;

        [request sendWithcompletion:^(RNResponse *response, NSError *error) {
 
            loading = NO;
            
//            NSLog(@"request sendWithcompletion");
            
            if (error != nil || [response isKindOfClass:[NSNull class]] || response == nil) {
//                NSLog(@"error !!!!!!!!!!!!!!!!%@",error);
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            }
                RNGetRestaurantsResponse *r = (RNGetRestaurantsResponse *)response;
                [[UIApplication sharedApplication].keyWindow hideLoadingIndicator];

            if ([r.success isEqualToString:@"YES"]){
                NSLog(@"r.success %@", r.success);
                [self startFlow];
            } else {
                [self showAlertWithText:DPLocalizedString(@"try_later", nil)];
            }
        }];
    }
}


#pragma mark - Helpers

- (void)registerDefaults {
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kPassKey:@"",
                                                              kUserKey:@"",
                                                              kEmailFBKey:@"",
                                                              kClientIDKey:@"",
                                                              kStrategyKey:@"",
                                                              kFirstLaunchKey:@""
                                                              }];
}


- (void)startDefaultFlowWithCity:(NSString*)address {
//    NSLog(@"STRATEGY");
    NSString *strategy = [[NSUserDefaults standardUserDefaults] stringForKey:kStrategyKey];
    if ([strategy isEqualToString:@"FBSTRATEGY"]){
        NSLog(@"FBSTRATEGY");
        NSString *emailFB = [[NSUserDefaults standardUserDefaults] stringForKey:kEmailFBKey];
        NSString *clientId = [[NSUserDefaults standardUserDefaults] stringForKey:kClientIDKey];
//        NSLog(@"email %@", emailFB);
//        NSLog(@"pass %@", clientId);
        //            emailFB = @"";
        //            clientId = @"";
        
        if (emailFB.length > 0 && clientId.length >0 ) {
            
            NSLog(@"email %@", emailFB);
            NSLog(@"pass %@", clientId);
            
            
            
            FacebookRegisterRequest *request = [[FacebookRegisterRequest alloc]init];
            request.clientId = clientId;
            request.emailFB = emailFB;
            
            [request sendWithcompletion:^(RNResponse *response, NSError *error) {
                if (error != nil) {
                    return;
                }
                FacebookRegisterResponse *responseWithToken = (FacebookRegisterResponse *)response;
                if (responseWithToken.token > 0) {
                    
//                    [RNUser sharedInstance].photoFB;
//                    NSLog(@"authResponse [RNUser sharedInstance].photoFB: %@",[RNUser sharedInstance].photoFB);
                    [RNUser sharedInstance].token = responseWithToken.token;
                   
                    if ([RNUser sharedInstance].birthDate == nil) {
                        if([[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey]){
                            [RNUser sharedInstance].birthDate = [[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey];
                        }
                    }
                    
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:emailFB
                                                              forKey:kEmailFBKey];
                    [[NSUserDefaults standardUserDefaults] setObject:clientId
                                                              forKey:kClientIDKey];
                    [[NSUserDefaults standardUserDefaults] setObject:@"FBSTRATEGY"
                                                              forKey:kStrategyKey];
                    
                    NSLog(@"address Launch %@", address);
                    [self sendSearchRequestWithCity:address];
//                    [self startFlow];
                }
            }];
        } else {
//            NSLog(@"STRATEGY");
            [self startLogin];
        }
        
    } else if ([strategy isEqualToString:@"LOGINSTRATEGY"]) {
        
//        NSLog(@"LOGINSTRATEGY");
        NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:kUserKey];
        NSString *pass = [[NSUserDefaults standardUserDefaults] stringForKey:kPassKey];
//        NSLog(@"email %@", email);
//        NSLog(@"pass %@", pass);
        //            email = @"";
        //            pass = @"";
        
        if (email.length > 0 && pass.length >0 ) {
            
//            NSLog(@"email %@", email);
//            NSLog(@"pass %@", pass);
            
            RNAuthRequest*request = [[RNAuthRequest alloc]init];
            request.username = email;
            request.password = pass;
            
            [request sendWithcompletion:^(RNResponse *response, NSError *error) {
                if (error != nil) {
                    return;
                }
                RNAuthorizationResponse *responseWithToken = (RNAuthorizationResponse *)response;
                [RNUser sharedInstance].token = responseWithToken.token;
//                NSLog(@"[RNUser sharedInstance].photo %@", [RNUser sharedInstance].photo);
                if ([RNUser sharedInstance].birthDate == nil) {
                    if([[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey]){
                        [RNUser sharedInstance].birthDate = [[NSUserDefaults standardUserDefaults] objectForKey:kUserBirthdayKey];
                    }
                }
                
                
                [[NSUserDefaults standardUserDefaults] setObject:email
                                                          forKey:kUserKey];
                [[NSUserDefaults standardUserDefaults] setObject:pass
                                                          forKey:kPassKey];
                [[NSUserDefaults standardUserDefaults] setObject:@"LOGINSTRATEGY"
                                                          forKey:kStrategyKey];
                //                [self startIPhone];
                [self sendSearchRequestWithCity:address];
//                [self startFlow];
            }];
        } else {
            
            [self startLogin];
        }
        
    } else {
        
        [self startLogin];
    }
}

- (void)startLogin{

    PageViewController * pageViewController = [storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    pageViewController.isAuth = NO;
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = NO; //bool created in first step
    [[[UIApplication sharedApplication] delegate].window setRootViewController:pageViewController];
}

- (void)startFlow {

    [self startIPhone];
}


#pragma mark - Start

- (void)startIPhone {

    CatalogRestViewController* enterVC = [storyboard instantiateViewControllerWithIdentifier:@"CatalogRestViewController"];
//    enterVC.restons = restons;
    enterVC.isAuth = isAuth;
    UINavigationController *navVC = [[storyboard instantiateViewControllerWithIdentifier:@"CatalogNavRestViewController"] initWithRootViewController:enterVC];
    
    NewMenuViewController * menuVC = [storyboard instantiateViewControllerWithIdentifier:@"NewMenuViewController"];
    
//    menuVC.restons = restons;
    menuVC.isAuth = isAuth;

    SlideMenuController *slideMenuController = [[SlideMenuController alloc] initWithMainViewController:navVC leftMenuViewController:menuVC];
    
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = NO; //bool created in first step
    
    [[[UIApplication sharedApplication] delegate].window setRootViewController:slideMenuController];
}

@end
