//
//  LocationManager.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 25.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import CoreLocation;

@interface LocationManager : NSObject <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}

@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) CLLocation *currentLocation;

+ (instancetype)sharedInstance;

-(void)getAddressFromLocation:(CLLocation *)location completion:(void (^)(NSString *address))completionBlock;

@end
