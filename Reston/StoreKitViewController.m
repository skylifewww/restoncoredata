//
//  StoreKitViewController.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.02.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import "StoreKitViewController.h"

@interface StoreKitViewController ()

@end

@implementation StoreKitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"StoreKitViewController");
    [self showStoreView];
    // Do any additional setup after loading the view.
}
- (void)showStoreView{
    
    SKStoreProductViewController *storeViewController =
    [[SKStoreProductViewController alloc] init];
    
    storeViewController.delegate = self;
    
    NSLog(@"storeViewController.delegate");
    
    static NSInteger const appITunesItemIdentifier = 1014075207;
    
    NSDictionary *parameters =
    @{SKStoreProductParameterITunesItemIdentifier:
          [NSNumber numberWithInteger:333700869]};
    
NSLog(@"parameters %@", parameters);
    
    [storeViewController loadProductWithParameters:parameters
                                   completionBlock:^(BOOL result, NSError *error) {
                                    
                                       if (result)
                                          
                                           [self presentViewController:storeViewController
                                                              animated:YES
                                                            completion:nil];
                                   }];
    
}

#pragma mark -
#pragma mark SKStoreProductViewControllerDelegate

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
