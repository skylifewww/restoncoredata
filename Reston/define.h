//
//  define.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 7/20/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#ifndef Reston_define_h
#define Reston_define_h

#define INTERFACE_ORIENTATION() ([[UIApplication sharedApplication]statusBarOrientation])
#define scalePhone [UIScreen mainScreen].scale
#define serverImageLink @"http://reston.com.ua/uploads/img/zavedeniya/"
#define serverLink @"http://reston.com"
#define timestampUpdate @"timestamp_Update"
#define languageOnApp @"language_On_App"
#define langArray @[@"ru",@"uk"]
#define SYSTEM_VERSION_LESS_THAN(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define versionAPI 1.10

//#define categoryFilterProduct @"category_Filter_Product"
//#define styleFilterProduct @"style_Filter_Product"
//#define materialFilterProduct @"material_Filter_Product"
//#define colorFilterProduct @"color_Filter_Product"
//#define collectionFilterProduct @"collection_Filter_Product"
//
//#define filterProductNotify @"Filter_Product_Notify"
//#define enbUpdateNotify @"enb_Update_Notify"
//#define imagePhotoFolder @"imagesPhoto"
//#define imageNewsFolder @"imagesNews"
//#define imageProductFolder @"imagesProd"
//#define deviceTokenKey @"Device_Token_Key"
//#define kAutodowload @"Autodowload_Key"
//#define kVerificationPSW @"Verification_PSW"
#endif
