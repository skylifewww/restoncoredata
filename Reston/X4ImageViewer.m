//
//  X4ImageViewer.m
//  Pinnacle
//
//  Created by shengyuhong on 15/4/21.
//  Copyright (c) 2015年 The Third Rock Ltd. All rights reserved.
//

#import "X4ImageViewer.h"
#import "UIImageView+WebCache.h"
#import "UIImage+SolidColor.h"

//static const CGFloat HeightCarousel = 24;
//static const CGFloat YPaddingCarousel = 18;
//static const CGFloat XPaddingCarousel = 16;

@interface X4ImageViewer ()<UIGestureRecognizerDelegate>
{
    BOOL _panning;
    UIImage *_placeholderImage;
}

@property (nonatomic, strong) NSArray *imageSources;
@property (nonatomic, strong) NSMutableArray *imageViews;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSMutableArray *innerScrollViews;
@property (nonatomic, strong) NSMutableArray *imageMaxScale;
@property (nonatomic, strong) UIScrollView *scrollView;

@end


@implementation X4ImageViewer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    _currentPageIndex = 0;
    _carouselType = CarouselTypePageControl;
    _carouselPosition = CarouselPositionBottomCenter;
    _bZoomEnable = YES;
    _bZoomRestoreAfterDimissed = YES;
    _contentMode = ContentModeAspectNormal;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.scrollEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.showsHorizontalScrollIndicator = _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.tag = -1;
    self.backgroundColor = [UIColor clearColor];
    _scrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:_scrollView];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        [self setTranslatesAutoresizingMaskIntoConstraints: NO];
        NSDictionary *views = @{@"scrollView": _scrollView};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[scrollView]-0-|"options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[scrollView]-0-|" options:0 metrics:nil views:views]];
    }
    else
    {
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleBottomMargin;
    }
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScrollView:)];
    tapGesture.numberOfTapsRequired = 1;
    [_scrollView addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTappedScrollView:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [_scrollView addGestureRecognizer:doubleTapGesture];
    
    UIPanGestureRecognizer *pan;
    pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(Swipe4ScrollViews:)];
    [pan setMinimumNumberOfTouches:1];
    [pan setMaximumNumberOfTouches:1];
    pan.delegate = self;
    [_scrollView addGestureRecognizer:pan];
    
    [tapGesture requireGestureRecognizerToFail: doubleTapGesture];
    
    _images = [NSMutableArray array];
    _imageViews = [NSMutableArray array];
    _innerScrollViews = [NSMutableArray array];
    _imageMaxScale = [NSMutableArray array];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _currentPageIndex = self.currentPageIndex < [self.images count] ? self.currentPageIndex : 0;
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * [self.images count], self.bounds.size.height);
    self.scrollView.contentOffset = CGPointMake(self.currentPageIndex * self.scrollView.bounds.size.width, 0);
    
    [self removeAllImages];
    [self loadImages];
}

- (NSArray *)currentLoadedImages
{
    return self.images;
}

- (NSArray *)imageDataSources
{
    return self.imageSources;
}

- (UIImageView *)currentImageView
{
    if([self.imageViews count] > self.currentPageIndex)
    {
        return (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
    }
    else
    {
        return nil;
    }
}

- (UIScrollView *)currentScrollView
{
    if([self.innerScrollViews count] > self.currentPageIndex)
    {
        return (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
    }
    else
    {
        return nil;
    }
}

- (void)setImages:(NSArray *)images withPlaceholder:(UIImage *)placeholderImage
{
    if(images == nil)
    {
        [self setImages:images withPlaceholder:nil];
    }
    else
    {
        NSMutableArray *placeholderImages = [NSMutableArray array];
        _placeholderImage = placeholderImage ? placeholderImage : [UIImage imageWithSolidColor:[UIColor blackColor]];

        for(NSUInteger i = 0; i < [images count]; i++)
        {
            [placeholderImages addObject:_placeholderImage];
        }
        [self setImages:images withPlaceholders:placeholderImages];
    }
   
}

- (void)setImages:(NSArray *)images withPlaceholders:(NSArray *)placeholderImages
{
    if([images count] == 0)
    {
        NSLog(@"[X4ImageViewer] Warning: Images array is empty");
        return;
    }
    
    if([placeholderImages count] == 0)
    {
        NSLog(@"[X4ImageViewer] Warning: Placeholder images array is empty. Try to use setImages:withPlaceholder instead if you really don't want a placeholder image.");
        return;
    }

    if([images count] != [placeholderImages count])
    {
        NSLog(@"[X4ImageViewer] Warning: The number of the placeholder images is not match the number of images.");
        return;
    }
    
    [self removeAllImages];
    
    self.imageSources = images;
    
    [self.images removeAllObjects];
    [self.imageViews removeAllObjects];
    [self.innerScrollViews removeAllObjects];
    
    for(NSUInteger i = 0; i < [self.imageSources count]; i++)
    {
        UIScrollView *scrollView = [[UIScrollView alloc] init];
        scrollView.delegate = self;
        scrollView.pagingEnabled = NO;
        scrollView.showsHorizontalScrollIndicator = scrollView.showsVerticalScrollIndicator = NO;
        scrollView.tag = i;
        scrollView.bounces = NO;
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.userInteractionEnabled = NO;
    
        NSObject *object = [images objectAtIndex:i];
        
        if([object isKindOfClass:[UIImage class]])
        {
            UIImage *image = (UIImage *)object;
            [self.images insertObject:image atIndex:i];
        }

        else if([object isKindOfClass:[NSURL class]])
        {
            UIImage *placeholderImage = (UIImage *)[placeholderImages objectAtIndex:i];
            [self.images insertObject:placeholderImage atIndex:i];
        }
        else
        {
            NSLog(@"[X4ImageViewer] Warning: Unsupport type of images! Only `NSURL` or `UIImage` will be accepted.");
            UIImage *placeholderImage = (UIImage *)[placeholderImages objectAtIndex:i];
            [self.images insertObject:placeholderImage atIndex:i];
        }
        [self.imageViews addObject:imageView];
        [self.innerScrollViews addObject:scrollView];
        [self.imageMaxScale addObject:@(0)];
    }
    
    [self setNeedsLayout];
}


- (void)setContentMode:(ContentMode)contentMode
{
    _contentMode = contentMode;
    
    [self setNeedsLayout];
}


- (void)setCarouselPosition:(CarouselPosition)carouselPosition
{
    _carouselPosition = carouselPosition;
   [self setNeedsLayout];
}

- (void)setCurrentPageIndex:(NSInteger)currentPageIndex
{
      _currentPageIndex = currentPageIndex;
    [self setNeedsLayout];
}

- (void)setBZoomEnable:(BOOL)bZoomEnable
{
    _bZoomEnable = bZoomEnable;
    [self setNeedsLayout];
}

- (void)loadImageAtIndex:(NSInteger)index
{
    if(index < 0 || index >= [self.imageSources count])
    {
        return;
    }
    
    UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:index];
    UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:index];
    
    if(!imageView.image)
    {
        NSObject *object = [self.imageSources objectAtIndex:index];
        
        if([object isKindOfClass:[UIImage class]])
        {
            UIImage *image = (UIImage *)object;
            imageView.image = image;
        }
        else if([object isKindOfClass:[NSURL class]])
        {
            NSURL *url = (NSURL *)object;
            [imageView sd_setImageWithURL:url placeholderImage:_placeholderImage options:SDWebImageHighPriority  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                imageView.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
                imageView.transform = CGAffineTransformIdentity;
                
                [scrollView addSubview:imageView];
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
                {
                    scrollView.frame = CGRectMake(index * self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
                }
                else
                {
                    scrollView.frame = CGRectMake(index * self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
                }
                
                scrollView.contentSize = imageView.bounds.size;
                scrollView.contentOffset = CGPointMake(0,0);
                scrollView.bounces = NO;
                CGFloat scaleWidth = (CGFloat)scrollView.bounds.size.width / imageView.bounds.size.width;
                CGFloat scaleHeight = (CGFloat)scrollView.bounds.size.height / imageView.bounds.size.height;
                
                CGFloat minScale = MIN(scaleWidth, scaleHeight);
                CGFloat maxScale = MAX(scaleWidth, scaleHeight);
                [self.imageMaxScale setObject:@(maxScale) atIndexedSubscript:index];
                
                switch (self.contentMode) {
                    case ContentModeAspectNormal:
                        scrollView.minimumZoomScale = MIN(minScale, 1);
                        break;
                    case ContentModeAspectFit:
                        scrollView.minimumZoomScale = minScale;
                        break;
                    case ContentModeAspectFill:
                        scrollView.minimumZoomScale =   minScale ; //  maxScale;
                        break;
                    default:
                        scrollView.minimumZoomScale = MIN(minScale, 1);
                        break;
                }
                
                if(self.bZoomEnable)
                {
                    scrollView.scrollEnabled = YES;
                    scrollView.maximumZoomScale = MAX(maxScale, 1);
                }
                else
                {
                    scrollView.scrollEnabled = NO;
                    scrollView.maximumZoomScale = scrollView.minimumZoomScale;
                }
                
                if(self.contentMode == ContentModeAspectFill)
                {
                    scrollView.zoomScale = maxScale;
                    scrollView.scrollEnabled = NO;
                }
                else
                    scrollView.zoomScale = scrollView.minimumZoomScale;
                
                [self move:imageView toCenterOf:scrollView];
                
                for(UIView *subview in imageView.subviews)
                {
                    [subview removeFromSuperview];
                }
                
                NSArray *supplementaryViews;
                if(self.dataSource && [self.dataSource respondsToSelector:@selector(imageViewer:supplementaryViewsFor:atIndex:)])
                {
                    supplementaryViews = [self.dataSource imageViewer:self supplementaryViewsFor:imageView atIndex:index];
                }
                
                if([supplementaryViews count] > 0)
                {
                    for(UIView *view in supplementaryViews)
                    {
                        [imageView addSubview:view];
                    }
                }

                
                }];
        }
        [self.scrollView addSubview:scrollView];
    }
}

- (void)restoreImageAtIndex:(NSInteger)index
{
    if(index < 0 || index >= [self.images count])
    {
        return;
    }
    
    UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:index];
    UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:index];
    
    if(imageView.image)
    {
        scrollView.zoomScale = scrollView.minimumZoomScale;
        [self move:imageView toCenterOf:scrollView];
    }
}


- (void)removeImageAtIndex:(NSInteger)index
{
    if(index < 0 || index >= [self.images count])
    {
        return;
    }
    UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:index];
    UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:index];
    
    if(imageView.image)
    {
        imageView.image = nil;
        [scrollView removeFromSuperview];
    }
}

- (void)removeAllImages
{
    for(NSInteger i = 0; i < [self.images count]; i++)
    {
        [self removeImageAtIndex:i];
    }
}

- (void)loadImages
{
    NSInteger previousImageIndex = self.currentPageIndex - 1;
    NSInteger nextImageIndex = self.currentPageIndex + 1;
    
    for(NSInteger i = 0; i < previousImageIndex; i++)
    {
        [self removeImageAtIndex:i];
    }
    
    for(NSInteger i = previousImageIndex; i <= nextImageIndex; i++)
    {
        [self loadImageAtIndex:i];
    }
    
    for(NSInteger i = nextImageIndex + 1; i < [self.images count]; i++)
    {
        [self removeImageAtIndex:i];
    }
}
- (void)scrollEnable:(BOOL)enable
{
    self.scrollView.scrollEnabled = enable;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.tag == -1 && scrollView.isDragging )
    {
        [scrollView setContentOffset: CGPointMake(scrollView.contentOffset.x,0)];
        
        NSInteger newImageIndex = (NSInteger)floor((scrollView.contentOffset.x * 2 + scrollView.frame.size.width) / (scrollView.frame.size.width * 2));
        
        if(newImageIndex != self.currentPageIndex && self.imageViews.count > self.currentPageIndex)
        {
            UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
            UIImageView *newImageView = (UIImageView *)[self.imageViews objectAtIndex:newImageIndex];
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didSlideFrom:fromIndex:to:toIndex:)])
            {
                [self.delegate imageViewer:self didSlideFrom:imageView fromIndex:self.currentPageIndex to:newImageView toIndex:newImageIndex];
            }
        }
        if(self.imageViews.count > newImageIndex)
           _currentPageIndex = newImageIndex;
        [self loadImages];
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    if(scrollView.tag >= 0 && self.imageViews.count > scrollView.tag)
    {
        UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:scrollView.tag];
        
        [self move:imageView toCenterOf:scrollView];
        
        if(!self.bZoomEnable || scrollView.minimumZoomScale == scrollView.zoomScale)
        {
            scrollView.scrollEnabled = NO;
        }
        else
        {
            scrollView.scrollEnabled = YES;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.bZoomRestoreAfterDimissed)
    {
        NSInteger previousImageIndex = self.currentPageIndex - 1;
        [self restoreImageAtIndex:previousImageIndex];
        
        NSInteger nextImageIndex = self.currentPageIndex + 1;
        [self restoreImageAtIndex:nextImageIndex];
   
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didEndZoomingWith:atIndex:inScrollView:)] && self.innerScrollViews.count > self.currentPageIndex)
    {
        UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
        UIImageView *imageView = (UIImageView *)view;
        [self.delegate imageViewer:self didEndZoomingWith:imageView atIndex:self.currentPageIndex inScrollView:scrollView];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    
    if(scrollView.tag >= 0 && self.imageViews.count > scrollView.tag)
    {
        return [self.imageViews objectAtIndex:scrollView.tag];
    }
    else
    {
        return nil;
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *view = [super hitTest:point withEvent:event];
    
    if(view == self)
    {
        return self.scrollView;
    }
    
    return view;
}

- (void)onTappedScrollView:(UITapGestureRecognizer *)gesture
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didSingleTap:atIndex:inScrollView:)])
    {
        UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
        UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
        
        [self.delegate imageViewer:self didSingleTap:imageView atIndex:self.currentPageIndex inScrollView:scrollView];
    }
}

- (void)onDoubleTappedScrollView:(UITapGestureRecognizer *)gesture
{
    
    UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
    UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didDoubleTap:atIndex:inScrollView:)])
    {
        [self.delegate imageViewer:self didDoubleTap:imageView atIndex:self.currentPageIndex inScrollView:scrollView];
    }
    else
    {
        if(scrollView.zoomScale == scrollView.minimumZoomScale)
        {
            scrollView.zoomScale = scrollView.maximumZoomScale;
        }
        else
        {
            scrollView.zoomScale = scrollView.minimumZoomScale;
        }
        
        [self move:imageView toCenterOf:scrollView];
    }
}

-(void)Swipe4ScrollViews:(UIPanGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateBegan)
        _panning = NO;
    CGPoint v = [sender velocityInView:_scrollView];
    
    if(ABS(v.y) >= 20 && ABS(v.x) < 40 && !_panning)
    {
        _panning = YES;
        [sender cancelsTouchesInView];
        if(v.y < 0 && self.imageViews.count > self.currentPageIndex )
        {
            UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
            UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
            NSNumber *maxSize  = self.imageMaxScale[self.currentPageIndex];
            NSLog(@"=== %f",scrollView.zoomScale);
            if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didSwipeUp:atIndex:inScrollView:)])
            {
                if(scrollView.zoomScale == scrollView.minimumZoomScale || [@(scrollView.zoomScale) isEqualToNumber:maxSize])
                   [self.delegate imageViewer:self didSwipeUp:imageView atIndex:self.currentPageIndex inScrollView:scrollView];
            }
        }
        if(v.y > 0  && self.imageViews.count > self.currentPageIndex)
        {
            UIImageView *imageView = (UIImageView *)[self.imageViews objectAtIndex:self.currentPageIndex];
            UIScrollView *scrollView = (UIScrollView *)[self.innerScrollViews objectAtIndex:self.currentPageIndex];
            NSNumber *maxSize  = self.imageMaxScale[self.currentPageIndex];
            NSLog(@"=== %f",scrollView.zoomScale);
            if(self.delegate && [self.delegate respondsToSelector:@selector(imageViewer:didSwipeDown:atIndex:inScrollView:)])
            {
                if(scrollView.zoomScale == scrollView.minimumZoomScale || [@(scrollView.zoomScale) isEqualToNumber:maxSize])
                    [self.delegate imageViewer:self didSwipeDown:imageView atIndex:self.currentPageIndex inScrollView:scrollView];
            }
        }
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)move:(UIImageView *)imageView toCenterOf:(UIScrollView *)scrollView
{
    CGSize scrollViewSize = scrollView.bounds.size;
    CGRect imageFrame = imageView.frame;
    
    CGFloat differentWidth = imageFrame.size.width - scrollViewSize.width;
    CGFloat differentHeight = imageFrame.size.height - scrollViewSize.height;
    
    if(differentWidth >= 0 && differentHeight >= 0)
    {
        
        imageFrame.origin.x = 0;
        imageFrame.origin.y = 0;
        scrollView.contentOffset = CGPointMake(ABS(differentWidth) / 2, ABS(differentHeight) / 2);
        
    }
    else if(differentWidth >= 0 && differentHeight < 0)
    {
        
        imageFrame.origin.x = 0;
        imageFrame.origin.y = ABS(differentHeight) / 2;
        scrollView.contentOffset = CGPointMake(ABS(differentWidth) / 2, 0);

        
    }
    else if(differentWidth < 0 && differentHeight >= 0)
    {
        
        imageFrame.origin.x = ABS(differentWidth) / 2;
        imageFrame.origin.y = 0;
        scrollView.contentOffset = CGPointMake(0, ABS(differentHeight) / 2);

    }
    else
    {
        
        imageFrame.origin.x = ABS(differentWidth) / 2;
        imageFrame.origin.y = ABS(differentHeight) / 2;
        scrollView.contentOffset = CGPointMake(0, 0);
    }
    
    imageView.frame = imageFrame;
    
}

@end
