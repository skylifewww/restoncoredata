//
//  LaunchViewController.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 6/22/17.
//  Copyright © 2017 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface LaunchViewController : UIViewController
<SKStoreProductViewControllerDelegate>

@end
