//
//  UIImage+Cache.h
//  RestOn
//
//  Created by Yurii Oliiar on 3/23/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Cache)

+ (void)cachedImage:(NSString *)imageId
      fullURLString:(NSString *)fullUrlString
       withCallBack:(void (^)(UIImage *image)) completionBlock ;

@end
