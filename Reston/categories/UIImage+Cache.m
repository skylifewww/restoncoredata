//
//  UIImage+Cache.m
//  RestOn
//
//  Created by Yurii Oliiar on 3/23/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import "UIImage+Cache.h"
#import "RNRequest.h"

static NSString *const kThumbnailsCacheFolder = @"YoutabThumb";

@implementation UIImage(Cache)

+ (void)cachedImage:(NSString *)imageId
            fullURLString:(NSString *)fullUrlString
       withCallBack:(void (^)(UIImage *image)) completionBlock {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir = paths.firstObject;
   
    NSString *path = [NSString stringWithFormat:@"%@/%@",cacheDir,[imageId lastPathComponent]];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    if (image != nil) {
        completionBlock(image);
        return;
    }
    
    NSString *baseURL;
    if (fullUrlString.length > 0) {
        baseURL = fullUrlString;
    } else {
        baseURL = @"http://reston.com.ua/uploads/img/zavedeniya/";
    }
    
    NSString *urlString = [[NSString stringWithFormat:@"%@%@",baseURL,imageId] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSURL *thumbnailURL = [NSURL URLWithString:urlString];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData* data = [NSData dataWithContentsOfURL:thumbnailURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData: data];
            completionBlock(image);
        });
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm createFileAtPath:path
                    contents:data
                  attributes:nil];
    });
}

@end
