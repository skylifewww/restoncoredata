//
//  UITableView+Registration.m
//  RestOn
//
//  Created by Yurii Oliiar on 3/23/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import "UITableView+Registration.h"

@implementation UITableView (Registration)

- (void)registerCellWithReuseId:(NSString *)reuseId
                         useXIB:(BOOL)useXIB {
    if (useXIB) {
        UINib *nib = [UINib nibWithNibName:reuseId
                                    bundle:nil];
        [self registerNib:nib forCellReuseIdentifier:reuseId];
        return;
    }
    [self registerClass:NSClassFromString(reuseId) forCellReuseIdentifier:reuseId];
}

- (void)registerHeaderFooterWithReuseId:(NSString *)reuseId
                                 useXIB:(BOOL)useXIB {
    if (useXIB) {
        UINib *nib = [UINib nibWithNibName:reuseId
                                    bundle:nil];
        [self registerNib:nib forHeaderFooterViewReuseIdentifier:reuseId];
        return;
    }
    [self registerClass:NSClassFromString(reuseId) forHeaderFooterViewReuseIdentifier:reuseId];
}

@end
