//
//  UITableView+Registration.h
//  RestOn
//
//  Created by Yurii Oliiar on 3/23/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Registration)

- (void)registerCellWithReuseId:(NSString *)reuseId
                         useXIB:(BOOL)useXIB;
- (void)registerHeaderFooterWithReuseId:(NSString *)reuseId
                                 useXIB:(BOOL)useXIB;
@end
