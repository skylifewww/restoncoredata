//
//  RNCountPeopleHelper.m
//  Reston
//
//  Created by Yurii Oliiar on 30.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "RNCountPeopleHelper.h"

@implementation RNCountPeopleHelper

+ (NSString *)returnCorrectNameWithCount:(NSNumber *)count{
    switch (count.integerValue) {
        case 1:
            return @"Для 1 человека";
        case 21:
            return @"Для 21 человека";
        default:
            return [NSString stringWithFormat:@"Для %@ человек",count];
    }
}

+ (NSString*)returnCorrectNameFeedbackWithCount:(NSNumber *)count{
    NSInteger countInt = count.integerValue%100;
    if (countInt < 20) {
        switch (count.integerValue) {
            case 1:
                return @"отзыв";
            case 2:
            case 3:
            case 4:
                return @"отзывa";
            default:
                return @"отзывов";
        }
    } else {
        switch (countInt % 10) {
            case 0:
                return @"отзывов";
            case 1:
                return @"отзыв";
            case 2:
            case 3:
            case 4:
                return @"отзыва";
            default:
              return @"отзывов";
        }
    }
}
@end
