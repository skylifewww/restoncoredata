//
//  NonNillObject.m
//  RestOn
//
//  Created by Yurii Oliiar on 3/26/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import "NonNillObject.h"

@implementation NonNillObject

+ (NSString *)nonNilString:(NSString *)object {
    if ([object isKindOfClass:[NSNull class]]) {
        return @"";
    }
    return object == nil ? @"" : object;
}
+ (NSNumber *)nonNilNumber:(NSNumber *)object {
    if ([object isKindOfClass:[NSNull class]]) {
        return @0;
    }
    return object == nil ? @0 : object;
}

+ (NSNumber *)nonNilBOOL:(NSNumber *)object {
    if ([object isKindOfClass:[NSNull class]]) {
        return @NO;
    }
    return object == nil ? @NO : object;
}

+ (NSArray *)nonNilArray:(NSArray *)object {
    if ([object isKindOfClass:[NSNull class]]) {
        return @[];
    }
    return object == nil ? @[] : object;
}

+ (NSDate *)nonNilDate:(NSDate *)object {
    if ([object isKindOfClass:[NSNull class]]) {
        return [NSDate date];
    }
    return object == nil ? [NSDate date] : object;
}


@end
