//
//  UISplitViewController+RNSplitViewController.m
//  Reston
//
//  Created by Yurii Oliiar on 03.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import "UISplitViewController+RNSplitViewController.h"

@implementation UISplitViewController (RNSplitViewController)

- (void)toggleView {
    UIBarButtonItem *barButtonItem = [self displayModeButtonItem];
    [[UIApplication sharedApplication] sendAction:barButtonItem.action
                                               to:barButtonItem.target
                                             from:nil
                                         forEvent:nil];
}
@end
