//
//  NonNillObject.h
//  RestOn
//
//  Created by Yurii Oliiar on 3/26/15.
//  Copyright (c) 2015 Freeport Metrics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NonNillObject : NSObject

+ (NSNumber *)nonNilBOOL:(NSNumber *)object;
+ (NSString *)nonNilString:(NSString *)object;
+ (NSNumber *)nonNilNumber:(NSNumber *)object;
+ (NSArray *)nonNilArray:(NSArray *)object;
+ (NSDate *)nonNilDate:(NSDate *)object;

@end
