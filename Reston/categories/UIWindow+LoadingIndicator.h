//
//  UIWindow+LoadingIndicator.h
//  RestOn
//
//  Created by Iurii Oliiar on 12/1/14.
//  Copyright (c) 2014 Iurii Oliiar. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface UIWindow(LoadingIndicator)

- (void)showLoadingIndicator;
- (void)hideLoadingIndicator;

@end
