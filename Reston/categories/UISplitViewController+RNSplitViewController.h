//
//  UISplitViewController+RNSplitViewController.h
//  Reston
//
//  Created by Yurii Oliiar on 03.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISplitViewController (RNSplitViewController)

- (void)toggleView ;

@end
