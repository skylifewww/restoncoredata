//
//  RNCountPeopleHelper.h
//  Reston
//
//  Created by Yurii Oliiar on 30.06.15.
//  Copyright (c) 2015 Iurii Oliiar Inc. All rights reserved.
//

@import Foundation;

@interface RNCountPeopleHelper : NSObject

+ (NSString *)returnCorrectNameWithCount:(NSNumber *)count;
+ (NSString *)returnCorrectNameFeedbackWithCount:(NSNumber *)count;

@end
