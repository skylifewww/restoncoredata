//
//  UIWindow+LoadingIndicator.m
//  RestOn
//
//  Created by Iurii Oliiar on 12/1/14.
//  Copyright (c) 2014 Iurii Oliiar. All rights reserved.
//

#import "UIWindow+LoadingIndicator.h"
#import "DGActivityIndicatorView.h"

static const NSInteger kLoadingIndicatorTag = 0xDEADBEEF;
static const NSInteger kLoadingContainerTag = 0xDEEDBEEF;
static const CGFloat kContainerSide = 60.0;

@implementation UIWindow (LoadingIndicator)

- (void)showLoadingIndicator {
    UIView *container = [self viewWithTag:kLoadingContainerTag];
    container.hidden = NO;
    if (container != nil) {
        DGActivityIndicatorView *v = (DGActivityIndicatorView *)[self viewWithTag:kLoadingIndicatorTag];
        [v startAnimating];
        [self bringSubviewToFront:container];
        return;
    }
    DGActivityIndicatorView *loadIndicator = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithRed:0.392 green:0.79 blue:0.0 alpha:1.0]];
//    loadIndicator.hidesWhenStopped = YES;
    loadIndicator.tag = kLoadingIndicatorTag;
//    loadIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kContainerSide, kContainerSide)];
    container.tag = kLoadingContainerTag;
    [container addSubview:loadIndicator];
//    container.layer.cornerRadius = 10.0;
    container.center = self.center;
    loadIndicator.translatesAutoresizingMaskIntoConstraints = false;
    [loadIndicator.centerXAnchor constraintEqualToAnchor:container.centerXAnchor].active = YES;
    [loadIndicator.centerYAnchor constraintEqualToAnchor:container.centerYAnchor].active = YES;
    [loadIndicator.heightAnchor constraintEqualToConstant:kContainerSide].active = YES;
    [loadIndicator.widthAnchor constraintEqualToConstant:kContainerSide].active = YES;
//    loadIndicator.center = container.center;
//    loadIndicator.frame = container.frame;
    container.backgroundColor = [UIColor clearColor];
    
    [self addSubview:container];
    [loadIndicator startAnimating];
    self.userInteractionEnabled = NO;
    
}

- (void)hideLoadingIndicator {
    UIView *v = [self viewWithTag:kLoadingContainerTag];
    v.hidden = YES;
    [((DGActivityIndicatorView *)[self viewWithTag:kLoadingIndicatorTag]) stopAnimating];
    self.userInteractionEnabled = YES;
}

@end
