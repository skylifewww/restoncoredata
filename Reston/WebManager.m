//
//  WebManager.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 31.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import "WebManager.h"
#import "Reachability.h"
#import "define.h"

@implementation WebManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(BOOL) inetConnect
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return networkStatus == NotReachable;
}

-(void)checkPass:(NSString*)psw complete:(void(^)(NSDictionary*, BOOL))complete
{
//    NSString *url =  [NSString stringWithFormat:@"%@/update/checkPass",serverLink];
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    if(![self inetConnect])
//    {
//        [manager POST:url parameters:@{@"pass":psw.MD5} success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             complete(responseObject,NO);
//         }
//              failure:^(AFHTTPRequestOperation *operation, NSError *error)
//         {
//             
//             complete(@{@"error":error},YES);
//             NSLog(@"%@",error);
//         }];
//    }
//    else
//    {
//        complete(@{@"error":@"no inet"},YES);
//    }
}

-(void)getDataComplete:(void(^)(NSDictionary*, BOOL))complete
{
    NSString *stamptime =  [[NSUserDefaults standardUserDefaults] stringForKey:timestampUpdate];
    stamptime = stamptime.length > 0 ? stamptime : @"";
//    NSString *psw = [[NSUserDefaults standardUserDefaults] stringForKey:kVerificationPSW];
//    NSDictionary *param = nil;
//    if(psw.length > 0)
//        param = @{@"pass" : psw};
//    NSString *url =  [NSString stringWithFormat:@"%@/update/get/%@",serverLink, stamptime];
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    if(![self inetConnect])
//    {
//        [manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             complete(responseObject,NO);
//         }
//              failure:^(AFHTTPRequestOperation *operation, NSError *error)
//         {
//             complete(@{@"error":error},YES);
//             NSLog(@"%@",error);
//         }];
//    }
//    else
//    {
//        complete(@{@"error":@"no inet"},YES);
//    }
}

-(void)hasupdateComplete:(void(^)(NSDictionary*, BOOL))complete
{
//    NSString *stamptime =  [[NSUserDefaults standardUserDefaults] stringForKey:timestampUpdate];
//    stamptime = stamptime.length> 0 ? stamptime : @"";
//    NSString *url =  [NSString stringWithFormat:@"%@/update/hasupdate/%@",serverLink, stamptime];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    if(![self inetConnect])
//    {
//        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             complete(responseObject,NO);
//             NSLog(@"%@",responseObject);
//         }
//             failure:^(AFHTTPRequestOperation *operation, NSError *error)
//         {
//             complete(@{@"error":error},YES);
//             NSLog(@"%@",error);
//         }];
//    }
//    else
//    {
//        complete(@{@"error":@"no inet"},YES);
//    }
}

-(void)chackAPIVersionComplete:(void(^)(NSDictionary*, BOOL))complete
{
//    NSString *url =  [NSString stringWithFormat:@"%@/minVersion/ios",serverLink];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    if(![self inetConnect])
//    {
//        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             complete(responseObject,NO);
//             NSLog(@"%@",responseObject);
//         }
//             failure:^(AFHTTPRequestOperation *operation, NSError *error)
//         {
//             complete(@{@"error":error},YES);
//             NSLog(@"%@",error);
//         }];
//    }
//    else
//    {
//        complete(@{@"error":@"no inet"},YES);
//    }
}

-(void)registerComplete:(void(^)(NSDictionary*, BOOL))complete
{
//    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:deviceTokenKey].length > 0 ? [[NSUserDefaults standardUserDefaults] stringForKey:deviceTokenKey] : @"";
//    NSString * strLang = [[NSUserDefaults standardUserDefaults] stringForKey:languageOnApp].length > 0 ? [[NSUserDefaults standardUserDefaults] stringForKey:languageOnApp] : @"";
//    NSString *url =  [NSString stringWithFormat:@"%@/GCM/register/%@/1/%@",serverLink, str,strLang];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    if(![self inetConnect])
//    {
//        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
//         {
//             complete(responseObject,NO);
//             NSLog(@"%@",responseObject);
//         }
//             failure:^(AFHTTPRequestOperation *operation, NSError *error)
//         {
//             complete(@{@"error":error},YES);
//             NSLog(@"%@",error);
//         }];
//    }
//    else
//    {
//        complete(@{@"error":@"no inet"},YES);
//    }
    
}
@end

