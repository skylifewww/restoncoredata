//
//  UIColor+Hexadecimal.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 30.08.17.
//  Copyright © 2017 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Hexadecimal)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
