//
//  CDKitchen+CoreDataProperties.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//
//

#import "CDKitchen+CoreDataProperties.h"

@implementation CDKitchen (CoreDataProperties)

+ (NSFetchRequest<CDKitchen *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDKitchen"];
}

@dynamic kitchenId;
@dynamic kitchenName;

@end
