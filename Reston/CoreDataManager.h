//
//  CoreDataManager.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 14.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)deleteAllObjectsWithEntityName:(NSString*)entityName;

+ (CoreDataManager*) sharedManager;


//- (void) generateAndAddUniversity;

@end
