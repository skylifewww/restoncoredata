//
//  CDKitchen+CoreDataProperties.h
//  Reston
//
//  Created by Vladimir Nybozhinsky on 17.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//
//

#import "CDKitchen+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDKitchen (CoreDataProperties)

+ (NSFetchRequest<CDKitchen *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *kitchenId;
@property (nullable, nonatomic, copy) NSString *kitchenName;

@end

NS_ASSUME_NONNULL_END
