//
//  LocationManager.m
//  Reston
//
//  Created by Vladimir Nybozhinsky on 25.06.2018.
//  Copyright © 2018 skylife.com. All rights reserved.
//

#import "LocationManager.h"
#import "RNUser.h"

@implementation LocationManager

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        [self locationManager];
    }
    return self;
}


+ (instancetype)sharedInstance
{
    static LocationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocationManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void) locationManager
{
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    }
    else{
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Continue",nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert setDelegate:self];
    }
}

- (void)requestWhenInUseAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertViews = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"Settings", nil];
        [alertViews show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestWhenInUseAuthorization];
    }
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    CLLocation *location;
    location =  [manager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    _currentLocation = [[CLLocation alloc] init];
    _currentLocation = newLocation;
    _longitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
    _latitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
    //    globalObjects.longitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
    //    globalObjects.latitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
    CLLocation *locUser = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    //    isGeolocationEnable = YES;
    [RNUser sharedInstance].isGeoEnabled = YES;
//    [self getAddressFromLocation:locUser];
}

//- (void) loadUserLocation
//{
//
////    [self registerDefaults];
//
//    //    [_locationManager startUpdatingLocation]; //Will update location immediately
//
//        latitude = _locationManager.location.coordinate.latitude;
//        longitude = _locationManager.location.coordinate.longitude;
//
//    CLLocation *locUser = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
//
//    //    isGeolocationEnable = YES;
//    [RNUser sharedInstance].isGeoEnabled = YES;
//    [self getAddressFromLocation:locUser];
//
//
//    //    NSLog(@"LaunchContr latitude %f longitude %f",latitude, longitude);
//
//    [locationManager stopUpdatingLocation];
//
//
//    //    [_locationManager stopUpdatingLocation];
//}

-(void)getAddressFromLocation:(CLLocation *)location completion:(void (^)(NSString *))completionBlock
{

    __block NSString *city;
    __block NSString *administrativeArea;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark= [placemarks objectAtIndex:0];
             
             city = [NSString stringWithFormat:@"%@",[placemark locality]];

             administrativeArea = [NSString stringWithFormat:@"%@",[placemark administrativeArea]];

             if (![city isKindOfClass:[NSNull class]] || ![administrativeArea isKindOfClass:[NSNull class]]){

                 if ([city isEqualToString:@"Kiev"] || [administrativeArea isEqualToString:@"Kiev Oblast"] || [city isEqualToString:@"Киев"] || [city isEqualToString:@"Київ"]){
                     
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].realCity = @"1";
                     [RNUser sharedInstance].isThisCity = YES;
                     //50.27 30.31
     
                 } else if ([city isEqualToString:@"Lviv"] || [administrativeArea isEqualToString:@"Lviv Oblast"] || [city isEqualToString:@"Львів"] || [city isEqualToString:@"Львов"]){
                     
                     [RNUser sharedInstance].currCity = @"6";
                     [RNUser sharedInstance].realCity = @"6";
                     [RNUser sharedInstance].isThisCity = YES;

                 } else if ([city isEqualToString:@"Ivano-Frankivs'k"] || [administrativeArea isEqualToString:@"Ivano-Frankivsk Oblast"] || [city isEqualToString:@"Івано-Франківськ"] || [city isEqualToString:@"Ивано-Франковск"]){
                     [RNUser sharedInstance].realCity = @"8";
                     [RNUser sharedInstance].currCity = @"8";
                     
                     [RNUser sharedInstance].isThisCity = YES;

                 } else if ([city isEqualToString:@"Kharkiv"] || [administrativeArea isEqualToString:@"Kharkiv Oblast"] || [city isEqualToString:@"Харків"] || [city isEqualToString:@"Харьков"]){
                     
                     //49.58 36.15
                     [RNUser sharedInstance].realCity = @"9";
                     [RNUser sharedInstance].currCity = @"9";
                     
                     [RNUser sharedInstance].isThisCity = YES;
                 } else {
                     [RNUser sharedInstance].realCity = @"1234567";
                     [RNUser sharedInstance].currCity = @"1";
                     [RNUser sharedInstance].isThisCity = NO;
                 }
             } else {

                 [RNUser sharedInstance].currCity = @"1";
                 [RNUser sharedInstance].isThisCity = NO;
                 
                 [RNUser sharedInstance].realCity = @"1234567";
             }
             completionBlock([RNUser sharedInstance].currCity);
         }
     }];
}


@end
