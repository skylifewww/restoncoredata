#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ADClusterAnnotation.h"
#import "ADMapCluster.h"
#import "ADMapPointAnnotation.h"
#import "CLLocation+Utilities.h"
#import "NSDictionary+MKMapRect.h"
#import "TSClusterAnimationOptions.h"
#import "TSClusterAnnotationView.h"
#import "TSClusterMapView.h"
#import "TSClusterOperation.h"
#import "TSPlatformCompatibility.h"
#import "TSRefreshedAnnotationView.h"

FOUNDATION_EXPORT double TSClusterMapViewVersionNumber;
FOUNDATION_EXPORT const unsigned char TSClusterMapViewVersionString[];

